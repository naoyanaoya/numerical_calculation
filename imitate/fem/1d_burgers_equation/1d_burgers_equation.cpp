#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<string>
#include<sstream>
using namespace std;
int main(){
    int i,j,k;
    int Ele=100;
    int Node=Ele+1;
    double LL=1.0;
    double dx=LL/Ele;
    double dt=0.001;
    double NT=1000;
    double Diff=0.01;
    double VE=0.0;
    double theta;
    cout << "θを入力、θ=0の場合、Eulerの前進差分法（陽解法）、θ=0.5の場合、Crank-Nicolson法（半員解法）、θ=1の場合、後退差分法（完全陰解法）" << endl;
	cin >> theta;
    double D0=0.0;
    double D1=0.0;
    double max;
    double eps2=pow(10.0,-10);
    double *b=new double[Node];
	double *x=new double[Node];
	double *xx=new double[Node];
	double *f=new double[Node];
	double *AD=new double[Node];
	double *AL=new double[Node];
	double *AR=new double[Node];
	double *BD=new double[Node];
	double *BL=new double[Node];
	double *BR=new double[Node];
	double *xr=new double[Node];
    ofstream fk;
	fk.open("Answer_0.txt");
	
	//initial condition//
	for(i=0;i<Node;i++)
	{
		x[i]=sin(4.0*atan(1.0)/LL*float(i)*dx);
		fk<<dx*float(i)<<" "<<x[i]<<endl;
	}

    for(i=1;i<=NT;i++){
        //preserve x^n//
		for(j=0;j<Node;j++){
            xr[j]=x[j];
        }

        //picard iteration
        for(j=0;j<100000;j++){
            //make mtarix
            //initialization
            for(k=0;k<Node;k++){
                AD[k]=0.0;
                AL[k]=0.0;
                AR[k]=0.0;
                BD[k]=0.0;
                BL[k]=0.0;
                BR[k]=0.0;
            }
            for(k=0;k<Ele;k++){
                VE=(x[k]+x[k+1])/2.0;
                //A//
                //teporal//
                AD[k]+=dx*2.0/6.0/dt;
                AR[k]+=dx*1.0/6.0/dt;
                AL[k+1]+=dx*1.0/6.0/dt;
                AD[k+1]+=dx*2.0/6.0/dt;
                
                //diffusion//
                AD[k]+=theta*Diff/dx;
                AR[k]+=theta*-Diff/dx;
                AL[k+1]+=theta*-Diff/dx;
                AD[k+1]+=theta*Diff/dx;
                
                //advection//
                AD[k]+=theta*-VE/2.0;
                AR[k]+=theta*VE/2.0;
                AL[k+1]+=theta*-VE/2.0;
                AD[k+1]+=theta*VE/2.0;
                
                //B//
                //temporal//
                BD[k]+=dx*2.0/6.0/dt;
                BR[k]+=dx*1.0/6.0/dt;
                BL[k+1]+=dx*1.0/6.0/dt;
                BD[k+1]+=dx*2.0/6.0/dt;
                
                //diffusion//
                BD[k]+=-(1.0-theta)*Diff/dx;
                BR[k]+=-(1.0-theta)*-Diff/dx;
                BL[k+1]+=-(1.0-theta)*-Diff/dx;
                BD[k+1]+=-(1.0-theta)*Diff/dx;
                
                //advection//
                BD[k]+=-(1.0-theta)*-VE/2.0;
                BR[k]+=-(1.0-theta)*VE/2.0;
                BL[k+1]+=-(1.0-theta)*-VE/2.0;
                BD[k+1]+=-(1.0-theta)*VE/2.0;
	        }
            //introduce boundary
            AD[0]=1.0;
            AR[0]=0.0;
            BD[0]=1.0;
            BR[0]=0.0;
		    AL[Node-1]=0.0;
            AD[Node-1]=1.0;
            BL[Node-1]=0.0;
            BD[Node-1]=1.0;

            //for b
            for(k=0;k<Node;k++)
			{
				b[k]=BL[k]*xr[k-1]+BD[k]*xr[k]+BR[k]*xr[k+1];
			}
            b[0]=D0;
			b[Node-1]=D1;

            //TDMA
            double *P=new double[Node];
            double *Q=new double[Node];
            //first step
            P[0]=-AR[0]/AD[0];
            Q[0]=b[0]/AD[0];
            //second ste
            for(k=1;k<Node;k++){
                P[k]=-AR[k]/(AD[k]+AL[k]*P[k-1]);
                Q[k]=(b[k]-AL[k]*Q[k-1])/(AD[k]+AL[k]*P[k-1]);
            }
            //third step
            xx[Node-1]=Q[Node-1];
            //backward
            for(k=Node-2;k>-1;k=k-1)
            {
                xx[k]=P[k]*xx[k+1]+Q[k];
            }
            
            delete [] P,Q;

            max=0.0;
            for(k=0;k<Node;k++){
                if(max<abs(x[k]-xx[k])){
                    max=abs(x[k]-xx[k]);
                }
            }
            for(k=0;k<Node;k++){
                x[k]=xx[k];
            }
            if(eps2>max) break;
        }

        if(i%100==0){
			cout << i << endl;
			stringstream ss;
			string name;
			ofstream fo;
			ss << i;
			name=ss.str();
			name="answer_" + name + ".txt";
			fo.open(name.c_str ());
			for(j=0;j<Node;j++){
				fo << dx*float(j) << " " << x[j] << endl;
			}
		}
    }
    delete[] b,x,xx,f,AD,AL,AR,BD,BL,BR,xr;
    return 0;
}

/*
plot "answer_0.txt","answer_1000.txt","answer_2000.txt","answer_3000.txt","answer_4000.txt","answer_5000.txt","answer_7000.txt","answer_8000.txt","answer_9000.txt","answer_10000.txt"
*/