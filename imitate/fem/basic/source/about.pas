﻿unit about;

{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF}

(***************************************)
(* Copyright (C) 2017, SHIRAISHI Kazuo *)
(***************************************)


interface

uses  Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ComCtrls, ExtCtrls, LResources,LCLIntf;

type

  { TAboutBox }

  TAboutBox = class(TForm)
    Web: TLabel;
    Panel1: TPanel;
    OKButton: TBitBtn;
    ProductName: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    Memo1: TMemo;
    version: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WebClick(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  AboutBox: TAboutBox;


implementation
uses sconsts;
{$R *.lfm}

procedure TAboutBox.FormCreate(Sender: TObject);
begin
   version.Caption:='Version 0.8.1.3'{$IFDEF CPU64}+' (x86_64)'{$ENDIF};
   CopyRight.Caption:='Copyright(C) 2021 SHIRAISHI Kazuo';
   Web.Font.Color:=clBlue;
   {$IFDEF LCLGTK2}
   memo1.enabled:=false;
   {$ENDIF}
end;

procedure TAboutBox.WebClick(Sender: TObject);
begin
   OpenURL(s_URL);
end;



initialization



end.

