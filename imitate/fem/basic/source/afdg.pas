unit afdg;

{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF}

(***************************************)
(* Copyright (C) 2003, SHIRAISHI Kazuo *)
(***************************************)


interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,  CheckLst, LResources, Buttons;


type

  { TAutoFormatDlg }

  TAutoFormatDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    Bevel1: TBevel;
    CheckListBox1: TCheckListBox;
    programCharset1: TRadioGroup;
    procedure HelpBtnClick(Sender: TObject);
    procedure programCharset1Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  AutoFormatDlg: TAutoFormatDlg;
  AutoFormatKw:boolean=true;
  ProgramFileCharsetUTF8:boolean=true;


procedure  setAutoFormat;

implementation
uses base,htmlhelp;

{$R *.lfm}

procedure TAutoFormatDlg.HelpBtnClick(Sender: TObject);
begin
  OpenHelp(HelpContext);
  //Application.HelpContext(HelpContext);
end;

procedure TAutoFormatDlg.programCharset1Click(Sender: TObject);
begin

end;

procedure  setAutoFormat;
begin
 with AutoFormatDlg do
  begin
    //OkBtn.Focused;
    {$IFNDEF Windows}
    ProgramCharset1.enabled:=false;
    {$ENDIF}
    CheckListBox1.checked[0]:=AutoFormatKw;
    ProgramCharset1.ItemIndex:=byte(ProgramFileCharsetUTF8);
    if ShowModal=mrOk then
      begin
        AutoFormatKw:=CheckListBox1.checked[0];
        byte(ProgramFileCharsetUTF8) := ProgramCharset1.ItemIndex;

      end;
  end;
end;

initialization
    with TMyIniFile.create('AutoFormat') do
       begin
         AutoFormatKw:=ReadBool('kw',AutoFormatKw);
         {$IFDEF Windows}
         ProgramFileCharsetUTF8:=ReadBool('cs',ProgramFileCharsetUTF8);
         {$ENDIF}
         free
        end;

finalization
      with TMyIniFile.create('AutoFormat') do
         begin
             WriteBool('kw',AutoFormatKw);
             {$IFDEF Windows}
             WriteBool('cs',ProgramFileCharsetUTF8);
             {$ENDIF}
             free
         end;

end.

