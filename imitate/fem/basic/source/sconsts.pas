unit sconsts;

{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF}
(***************************************)
(* Copyright (C) 2003, SHIRAISHI Kazuo *)
(***************************************)


interface

{$IFDEF Windows}
const EOL=#13#10;
const EOL1=#13;
      EOL2=#10;
{$ELSE}
const EOL=#10;
const EOL1=#10;
      EOL2=#10;
{$ENDIF}

const
  TextExt= '.txt';
var
  BASExt:string[7]='.bas';
  LibExt:string[7]='.lib';
var  BreakKey:char='B';

const
  AppTitle='BASIC';

ResourceString
  c_language='J';
  s_BASICProgram='BASIC ﾌﾟﾛｸﾞﾗﾑ ﾌｧｲﾙ';
  s_Edit='編集';
  s_RUN='実行';

  s_OpenFile='ファイルを開く';
  s_SaveFile='名前を付けて保存';
  s_InitEnv='次回起動時にオプション設定を初期化します';
  s_File='ファイル';

 s_CannotQuit='プログラム実行中です。'+EOL+'終了できません。';
 s_Tilde='～';
 s_decimal='十進';
 s_1000digits='1000桁';
 s_binary='2進';
 s_complex='複素数';
 s_rational='有理数';
 s_Standard=''; {'標準';}
 s_Minimal='基本';
 s_MS=     'ＭＳ';
 s_Overwrite='';
 s_Insert='';
 s_Protected='書込禁止';
 statusBarMems3='Ready';   //'F1 ヘルプ  F2 機能語一覧  F9 実行';
 Contact = 'BASICのバージョン番号および'+EOL+'現在のプログラムとともにご連絡ください。';
 s_internalErrorCompiling='翻訳時内部エラー';
 s_InternalErrorRunning='実行時内部エラー';
 s_SyntaxError='文法の誤り';
 s_Extype1000= '桁あふれ';
 s_Extype1001='数値定数の桁あふれ' ;
 s_Extype1002='数値演算の桁あふれ';
 s_Extype1003='数値組込み関数の桁あふれ' ;
 s_Extype1006= '桁あふれ(read文)';
 s_Extype1007= '桁あふれ(input文)';
 s_Extype1008= '桁あふれ(ファイルからの入力)';
 s_Extype1050= '文字列の桁あふれ';

 s_Extype2001= '添字が範囲外';
 s_Extype3000= '無効演算(定義域外の引数など)';
 s_Extype3001='ゼロ除算';
 s_Extype3002='負数の非整数乗';
 s_Extype3003= 'ゼロの負数乗';
 s_Extype3004='範囲外の実引数';
 s_Extype3009='INV関数の引数が特異行列';
 s_Extype5001='配列の再定義に必要な要素数の不足';
 s_Extype6001='配列寸法が不正';
 s_Extype7001='経路番号が許されない値';
 s_Extype7003='すでにopenされている';
 s_Extype7004='ファイルはまだopenされていない';
 s_Extype7101='ファイルI/Oエラー ファイルをopenできない';
 s_Extype7102='ファイルI/Oエラー ファイルをopenできない'+EOL+'access inputでは既存のファイルのみを開ける。';
 s_Extype7103='ファイルI/Oエラー ファイルを作れない。'+EOL+'おそらくファイル数が多すぎる。';
 s_Extype7301='ACCESS INPUT，ACCESS OUTPUTが指定されているとERASE文は実行できない.';
 s_Extype7302='入力専用ファイルに対する出力';
 s_Extype7303='出力専用ファイルに対する入力';
 s_Extype7305='記録が存在しない';
 s_Extype7308='既存の記録に対する書き出し';
 s_Extype7317='内部形式ファイルにPRINT文は使えない';
 s_Extype7318='内部形式ファイルにINPUT文は使えない';
 s_Extype7000='ﾌｧｲﾙ利用の誤り';
 s_Extype8001='データの終りを超えた';
 s_Extype8011='ﾌｧｲﾙの終り';
 s_Extype8012='データが不足';
 s_Extype8013='データが余分';
 s_Extype8101='読み込もうとしたデータが数値定数でなかった';
 s_Extype8002='illegal input reply';
 s_Extype8105='読み込もうとしたデータの構文誤り';
 s_Extype8120='内部形式記録の入力における型の不一致';
 s_Extype8201='書式文字列が正しくない';
 s_Extype8202='書式がない';
 s_Extype8401='時間切れ';
 s_Extype8402='時間式の値が負';
 s_Extype8203='出力文字列が書式より長い';
 s_Extype8204='指数部が書式の指数部より長い';
 s_Extype9000='ファイルI/Oエラー';
 s_Extype9002='ディスクまたはプリンタの異常';
 s_Extype9003='ファイルが存在しない';
 s_Extype9004='ファイルはすでに存在する';
 s_Extype9102='この機能はプリンタには使用できません';
 s_Extype10002='return文のスタックが空';
 s_Extype10004='一致するcase区がない';
 s_Extype11004='座標変換実行中のset window';
 s_Extype11051='SET文で幅または高さに0(または負)が指定された．';
 s_Extype12004='数値時間式が過大';
 s_OutoOfMemory='out of Memory';
 s_VStackOverflow='仮想メモリーが確保できなかった';
 s_OutputOverflow='text出力領域が限界に達した。';
 s_StackOverflow='スタックオーバーフロー';
 s_ArraySizeOverflow='配列が大きすぎる';
 s_OnCompiling='翻訳中';
 s_OnRuunnig=' 実行中';

 s_program='ﾌﾟﾛｸﾞﾗﾑ';
 s_Library='ﾗｲﾌﾞﾗﾘ';
 s_TextFile='ﾃｷｽﾄ ﾌｧｲﾙ';
 s_BitMap='ﾋﾞｯﾄﾏｯﾌﾟ';
 s_ImageFile='画像ﾌｧｲﾙ';
 s_AllFile='すべてのﾌｧｲﾙ';
 CloseMsg = '''%s'' は変更されています。保存しますか？';
 s_NotFound='は見付かりません.';
 s_Margin='マージン';
 s_InitialMargin='マージンの初期値';

 TemplateErrMes='BASIC.KWSまたはBASIC.KWF 形式不正'+EOL;

 s_IncludesAnError='に誤りがあります';
 s_IsExpected=' が必要です.';
 s_Restricted='に限ります';
 s_CantBelongHere=' はここに書けません．';
 s_DimmensionError='配列の次元が正しくない．';
 s_QuoteIsExpected='"が必要です';
 s_MultiStatementIsNotAvailable='マルチステートメントは使えません。修正して実行しますか。';
 s_ColonIsAnExtra=': は不要です。削除していいですか？';
 s_ConvertTailComment=chr(39)+' を ! に書き換えていいですか？';
 s_IllegalLineNumber='行番号の形式が正しくない';
 s_ConfirmInsert=EOL+' 挿入していいですか？';
 s_Identifier='識別名';
 s_Constant='定数';
 s_Integer='整数';
 s_NumericalConstant='数値定数';
 s_StringConstant='文字列定数';
 s_DEleteLineNumberFailed='行番号削除は失敗しました。';
 s_SupplementLineNumberFailed='行番号付加に失敗しました。';
 s_CorrespondingParenNotFound='対応する)が見つかりません';

 s_DuplicatedLineNumber='行番号重複';
 s_ConfirmTraceWithLargeArray='大きな配列があるとトレースは時間がかかります'+EOL
                                       +'トレースを実行しますか？';
 s_CanNotBrachLine='%s行には分岐できません。';
 s_LineNumber='行番号';
 s_LineNotFound='%0:s行が見つかりません';
 s_DuplicaltedRoutineName='ルーチン識別名重複';
 s_DuplicatedVariableName='はすでに変数名として用いられています。';
 s_IsDeclaredAsExternalFunction='は外部関数として宣言されています。';
 s_DuplicatedParameter='仮引数名重複:';
 s_InternalRoutineCanntotbeInProcedure='モジュール本体に内部手続きは書けません.';

 s_ConfirmEndToStop='ここにEND文を書くことはできません。'+EOL+
                         'ENDをSTOPに書き換えて続行しますか？';
 s_ConfirmEndToStop2='END行以降に書くのは外部手続きです。'+EOL+
                      'END文をSTOP文に書き換えて続行しますか？';
 s_ConfirmMoveDataLIne='END行以降にDATA文を書くことはできません。'+EOL+
                   'DATA行をEND行の手前に移動して続行しますか？';
 s_ProtectionBlockInsideExceptionHandler=
                              '例外処理区の内部に保護区を書くことはできません。';
 s_IFTHENCorrectConfirm='THEN あるいはELSEに続けてif文を書くことはできません。'+EOL+
                        '修正して実行しますか。';
 s_ENDCorrectConfirm= 'ENDをSTOPに書き換えていいですか';
 s_NestedSameVarFOR='同じ変数名のforが入れ子になっています';
 s_InquireInsert=' 挿入しますか？';
 s_ConfirmWHILE1toDO='WHILE 1 をDOに書き換えていいですか？';
 s_ConfirmWHILEtoDOWHILE='WHILE を DO WHILE に書き換えていいですか？';
 s_ConfirmWENDtoLOOP='WEND を LOOP に書き換えていいですか？';

 s_IMAGEstatement='IMAGE文には行番号が必要です';
 s_IsNotAgreeArithmetic='呼び出し元と %s はarithmetic選択子が一致しない';
 s_BodyIsNotFound='%s の定義部が見つかりません．';
 s_ExpressionIncorrect='数値式の形が正しくない．';
 s_or=EOL+'または'+EOL;

 s_CanBeParenthesized='を()で囲んでいいですか？';
 s_IsReserved=' は予約語.';
 s_IsNotExternalDeclared='はEXTERNAL宣言されていない.';
 s_DisAgreeArithmetic='arithmetic選択子不一致' ;
 s_IsNotDeclared='は宣言されていない';
 s_IsFunctionName= ' は関数名.';
 s_VarName='変数名';
 s_NumVar='数値変数';
 s_SimpleVar='単純変数';
 s_IllegalStringVar='文字列式の形が正しくない';
 s_ConfirmPlusSignToAnpersand='+ を & に書き換えていいですか？';
 s_IsNotArrayName=  'は配列名ではありません．';
 s_NumArrayName='数値配列名';
 s_StringArrayName='文字列配列名';
 s_ComparisonExp='比較式';
 s_FunctionName='関数名';
 s_OnlyStringVar='文字列変数に限る';
 s_StringIdentifier='文字列識別名';
 s_StringVariable='文字列変数名' ;
 s_ArrayShouldBeDeclared= '配列はdim文による宣言が必要です。';
 s_ConfirmInsertLET='LETが必要です。挿入しますか？';

 s_IsCorectAskConvert= 'が正しい形です。修正しますか？';
  s_DuplicatedIdetifier= '識別名重複:';
  s_SubscriptRange= '添字は -2147483647 から 2147483647 までの範囲にとどめてください。';
  s_DimParameter='DIM文の引数は定数に限る';
  s_ImperativeDIM='内部手続き内に実行DIM文は書けません';
  s_ModifiedIdentifierExpected='修飾識別名が必要です';
  s_module='モジュール';
  s_NotPublicDeclaredIn='モジュール %0:s で %1:s はPUBLIC宣言されていません.';
  s_ModuleDimemsion='モジュール %0:s で %1:s は %2:s次元で定義されています．';
  s_IsNotFound='が見つからない.';
  s_IsNotHandler='はhandlerではない';
  s_IsNotPublicDeclared='はPUBLIC宣言されていない.';
  s_OPTION_ARITHMETIC= 'OPTION ARITHMETIC文は数値式より手前の行に書く必要があります';
  s_JISmode='JISモードで実行してください。';
  s_MSmodeOnly=' はMicrosoft互換モードでのみ使えます。';
  s_OnlyOneOPTION_ANGLE=  'OPTION ANGLEはプログラム単位ごとに１回しか書けません。';
  s_OnlyOneOPTION_ARITHMETIC='OPTION ARITHMETICはプログラム単位ごとに１回しか書けません。';
  s_OPTION_BASE='OPTION BASE文はDIM文より手前に書いてください';
  s_ConfirmAngle='このまま実行すると，'+EOL+
         '外部手続き %s で角の大きさの単位が radians になります。'+EOL+
         '「はい」を選ぶと,このまま実行し，以後，このメッセージを表示しません。'+EOL+
         '「いいえ」を選ぶと OPTION ANGLE DEGREES を挿入します。';
  s_ConfirmArithmetic=   'このまま実行すると，'+EOL+
         '外部手続き %0:s で数値が %1:s になります。'+EOL+
         '「はい」を選ぶと,このまま実行し，以後，このメッセージを表示しません。'+EOL+
         '「いいえ」を選ぶと %2:s を挿入します。';

  s_GuideOptionBase='(ヒント)'+EOL+'DIM文より手前の行に'+EOL+
                     'OPTION BASE 0'+EOL+
                     'を追加してやり直してみてください。';
  s_TDrawOpName='変形行列計算中の桁あふれ（DRAW文）';
  s_TooLargeConstant='数値定数が大きすぎる';
  s_UsedByAnotherApplication='は別のアプリケーションで使用されています';
  //s_BASICProgram='BASIC ﾌﾟﾛｸﾞﾗﾑ ﾌｧｲﾙ';
  //s_Edit='編集';
  //s_RUN='実行';

  s_PowerIndex='べき指数は-9223372036854775807～9223372036854775807の整数に限る';
  s_RPowerIndex='べき指数は-2147483647～2147483647の整数に限る';                  //2010.3.28
  s_InvalidArgInSQR='SQR関数の引数が負';
  s_InvalidArgInLOG='対数関数の引数が負';

  WriteSyntaxErrorMes='write文に ; または TAB を書くことはできません';
  s_ConfirmCorrectPRINT_USING='print using文の書式指定直後の ; を'+EOL+
                            ' : に書き換えていいですか？';
  s_QuestionMark='? を PRINT に書き換えていいですか？';
  s_InvalidFunctionOnMode='%0:s関数は %1:sモードでは使えません';
  s_ChannelExp='経路式';
  s_FailedOpen= 'をオープンできませんでした';
  s_TestCtrlBreak='中止しますか？'+EOL+'Y 中止'+EOL+'N 文の実行が終わるのを待ってデバッグ';
  s_MarginError='marginをzone widthより小さくすることはできません';
  s_ZoneWidthError= 'zone widthをmarginより大きくすることはできません';
  s_ConfirmAbort='プログラムの実行を中止しますか？';
  s_ConfirmToBreak='実行中の文の処理の終了を待ってDebug状態に移行します。'+EOL+
                   '「中止」を選ぶとプログラムの実行を中止します。';
  s_DataFoundForWrite='すでに存在する記録を上書きしようとしました。'+EOL+
        '古い記録を消去してよければあらかじめerase文を実行してください。'+EOL+
        '記録を追加したいのであればACCESS OUTPUTでファイルをopenしてください。';

  s_ConfirmPrintBMPPartially='全体を印刷できませんが実行しますか？';
  s_InvalidPrinter='このプリンタでは印刷できません';

  s_ImaginaryInComparable='虚数は比較できない';
  s_ImaginaryNotAvailable='ここで虚数は扱えません' ;
  s_FormatInvalidForImaginary='虚数には書式指定できない';
  s_ImaginaryHasNoSign='虚数には符号はない';

  s_WINDOW='座標系の設定は'+EOL+
           'SET WINDOW 左端x座標, 右端x座標, 下端y座標, 上端y座標'+EOL+
           EOL+
           'マイクロソフト系BASICのWINDOW文を実行したいときは，'+EOL+
           'オプション－文法でMicrosoft BASIC互換を選択してください.' ;
  s_LPRINT='LPRINT文はオプション－文法を'+EOL+'Microsoft BASIC互換に設定した場合にのみ使えます';
  s_To_Break='中断します.お待ちください.' ;

  s_RToNOverflow='有理数を内部十進数形式に変換する際の桁あふれ';

  s_HT_Mes='制御文字の水平TABを検出しました。'+EOL+
           'すべての水平TABを空白に置き換えて翻訳し直しますか？';
  s_ZenKuu_Mes='全角空白を検出しました。'+EOL+
           'すべての全角空白を半角空白に置き換えて翻訳し直しますか？';
  s_CTRLChar1='制御文字';
  s_Zenkaku='全角文字 ';
  s_appears=' がある';

  s_AllowGlobalInternalProc=
    '外部手続きの中から主プログラムの内部手続を呼び出そうとしています。'+EOL
    +'正しくないプログラムですが，実行しますか？';

  s_RND='RND関数は引数を取りません';
  s_URL='http://hp.vector.co.jp/authors/VA008683/index.htm';
  s_Select_Directory='フォルダを指定';
  s_Pause_Mes=#13#10#13#10'Ok　続行'#13#10'ESC　Debug状態';

implementation

end.
