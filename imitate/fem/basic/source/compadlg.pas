unit compadlg;

{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF}

(***************************************)
(* Copyright (C) 2003, SHIRAISHI Kazuo *)
(***************************************)


interface

uses  SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, LResources;

type

  { TcompatibilityDialog }

  TcompatibilityDialog = class(TForm)
    CheckGroup1: TCheckGroup;
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    PageControl1: TPageControl;
    RadioGroup12: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup13: TRadioGroup;
    RadioGroup14: TRadioGroup;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    RadioGroup1: TRadioGroup;
    RadioGroup4: TRadioGroup;
    RadioGroup5: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup6: TRadioGroup;
    RadioGroup9: TRadioGroup;
    RadioGroup11: TRadioGroup;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  compatibilityDialog: TcompatibilityDialog;

procedure setCompatibility;

implementation

uses base,graphsys, express, texthand, htmlhelp;
{$R *.lfm}


procedure TcompatibilityDialog.HelpBtnClick(Sender: TObject);
begin
  OpenHelp(HelpContext);
end;

procedure setCompatibility;

begin
   with compatibilityDialog do
   begin
       RadioGroup1.ItemIndex:=byte(JISFormat);
       RadioGroup2.ItemIndex:=byte(InitialCharacterByte0);
       RadioGroup3.ItemIndex:=byte(JISSetWindow);
       RadioGroup4.ItemIndex:=byte(JISDim);
       RadioGroup5.ItemIndex:=byte(ForceFunctionDeclare);
       RadioGroup6.ItemIndex:=byte(JISDef);
       RadioGroup9.ItemIndex:=byte(ForNextBroadOwn);
       RadioGroup11.ItemIndex:=1+byte(TextProblemCoordinate)-byte(TextPhysicalCoordinate);
       RadioGroup12.ItemIndex:=byte(ResultVarStatic);
       RadioGroup13.ItemIndex:=byte(DisableAbbreviatedPLOT);
       RadioGroup14.ItemIndex:=byte(ForceSubPictDeclare);
       CheckGroup1.Checked[0]:=GreekIdf;
       CheckGroup1.Checked[1]:=KanjiIdf;
      if ShowModal=mrOK then
        begin
          byte(JISFormat):=RadioGroup1.ItemIndex;
          byte(InitialCharacterByte0):=RadioGroup2.ItemIndex;
          byte(JISSetWindow):=RadioGroup3.ItemIndex;
          byte(JISDim):=RadioGroup4.ItemIndex;
          byte(ForceFunctionDeclare):=RadioGroup5.ItemIndex;
          byte(JISDef):=RadioGroup6.ItemIndex;
          byte(ForNextBroadown):=RadioGroup9.ItemIndex;
          TextPhysicalCoordinate:=RadioGroup11.ItemIndex = 0;
          TextProblemCoordinate:=RadioGroup11.ItemIndex = 2;
          byte(ResultVarStatic):=RadioGroup12.ItemIndex;
          byte(DisableAbbreviatedPLOT):=RadioGroup13.ItemIndex;
          byte(ForceSubPictDeclare):=RadioGroup14.ItemIndex;
          GreekIdf := CheckGroup1.Checked[0];
          KanjiIdf := CheckGroup1.Checked[1];
          initIdentifierChar;
        end;
   end;
end;

initialization

  with TMyIniFile.create('Frame') do
   begin
     JISFormat:=            ReadBool('JISFormat',JISFormat);
     InitialCharacterByte0:=ReadBool('CharacterByte', InitialCharacterByte0);
     JISSetWindow:=         ReadBool('JISSetWindow',JISSetWindow);
     JISDim:=               ReadBool('JISDim',JISDim);
     ForceFunctionDeclare:= ReadBool('ForceFunctionDeclare',ForceFunctionDeclare);
     ForceSubPictDeclare:=  ReadBool('ForceSubPictDeclare', ForceSubPictDeclare);
     JISDef:=               ReadBool('JISDef',JISDef);
     ForNextBroadOwn:=      ReadBool('ForNextBroadOwn',ForNextBroadOwn);
     ResultVarStatic:=      ReadBool('ResultVarStatic',ResultVarStatic);
     GreekIdf:=             ReadBool('GreekIdf',GreekIdf);
     KanjiIdf:=             ReadBool('KanjiIdf',GreekIdf);
     free
   end;
 with TMyIniFile.create('Graphics') do
  begin
     //GeometricPenOnly:=     ReadBool('GeometricPenOnly',GeometricPenOnly);
     //ForwardPlot:=          ReadBool('ForwardPlot',ForwardPlot);
     TextProblemCoordinate:= ReadBool('TextProblemCoordinate',TextProblemCoordinate);
     TextPhysicalCoordinate:=ReadBool('TextPhysicalCoordinate',TextPhysicalCoordinate);
     DisableAbbreviatedPLOT:=  ReadBool('DisableAbbreviatedPLOT',DisableAbbreviatedPLOT);
     free
  end;

finalization
  with TMyIniFile.create('Frame') do
   begin
    WriteBool('JISFormat',JISFormat);
    WriteBool('CharacterByte', InitialCharacterByte0);
    WriteBool('JISSetWindow',JISSetWindow);
    WriteBool('JISDim',JISDim);
    WriteBool('ForceFunctionDeclare',ForceFunctionDeclare);
    WriteBool('ForceSubPictDeclare', ForceSubPictDeclare);
    WriteBool('JISDef',JISDef);
    WriteBool('ForNextBroadOwn',ForNextBroadOwn);
    WriteBool('ResultVarStatic',ResultvarStatic);
    WriteBool('GreekIdf',GreekIdf);
    WriteBool('KanjiIdf',KanjiIdf);
    free
   end;
 with TMyIniFile.create('Graphics') do
  begin
      //WriteBool('GeometricPenOnly',GeometricPenOnly);
      //WriteBool('ForwardPlot',ForwardPlot);
      WriteBool('TextProblemCoordinate',TextProblemCoordinate);
      WriteBool('TextPhysicalCoordinate', TextPhysicalCoordinate);
      WriteBool('DisableAbbreviatedPLOT',DisableAbbreviatedPLOT);
   free
  end;



end.

