unit kwlist;
{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF}
(***************************************)
(* Copyright (C) 2003, SHIRAISHI Kazuo *)
(***************************************)


interface
uses classes, Forms,SysUtils,Dialogs;

type
   TKeyWordList=Class(TStringList)
       constructor create(ch:char);
   end;

var
   KeywordList1,KeywordList2:TKeywordList;


implementation


Constructor TKeywordList.create(ch:char);
var
  F:TextFile;
  s,n:string;
begin
   inherited create;
   sorted:=true;
   n:=ChangeFileExt(Application.ExeName,'.kw')+ch;
   AssignFile(F,n);
   try
       reset(F);
       while not EOF(F) do
        begin
          readln(F,s);
          add(s);
        end;
        close(F);
   except
       showMessage(n+' Not Found');
   end;
end;


initialization
  keywordlist1:=TKeywordList.create('1');
  keywordList2:=TKeywordList.create('2');

finalization
  keywordlist1.free;
  keywordlist2.free;


end.
