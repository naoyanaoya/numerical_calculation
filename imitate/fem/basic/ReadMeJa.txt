十進BASIC　0.8.1.3　2021/1/30

Rosseta2を介して実行する際の不具合回避を目的として作成したバージョンです。

USBメモリ等にインストールした場合は、ドライブ名から空白文字を取り除いてください。

以下の独自拡張機能は正しく動作しません。
  SET DRAW MODE (NOTXOR, MASK, MERGE, XOR) （無視される)
  MOUSE POLL (余分なクリックが必要)


alt-Kで日本語入力枠を表示します。

GRAPHICS DEVICE PRINTER をプログラムの始めに書くとプリンタにグラフィックスを描きます。


十進BASICのホームページ
http://hp.vector.co.jp/authors/VA008683/
十進BASIC第2掲示板
http://6317.teacup.com/basic/bbs
