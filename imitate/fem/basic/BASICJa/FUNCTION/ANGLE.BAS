! ANGLE(x,y)は、原点と点(x,y)を結ぶ半直線がｘ軸の正の向きとなす角を表す。
! OPTION ANGLE DEGREES を書いたとき、-180＜θ≦180 である。
OPTION ANGLE DEGREES
LET x=1
LET y=1
PRINT ANGLE(x,y)
END


