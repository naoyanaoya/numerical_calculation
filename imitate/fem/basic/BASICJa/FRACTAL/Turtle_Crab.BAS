100 OPTION ANGLE DEGREES
110 DECLARE NUMERIC CurPosX, CurPosY, NewPosX, NewPosY,Pen, theta
120 REM 初期値を設定
130 LET CurPosX=0
140 LET CurPosY=0
150 LET Pen=1     ! Pen Down
160 LET theta=90  ! 上向き
170 SUB RightTurn(a)
180    LET theta=theta-a
190 END SUB
200 SUB LeftTurn(a)
210    LET theta=theta+a
220 END SUB
230 SUB PenUp
240    LET Pen=0
250 END SUB
260 SUB PenDown
270    LET Pen=1
280 END SUB
290 SUB Forward(r)
300    LET NewPosX=CurPosX+r*COS(theta)
310    LET NewPosY=CurPosY+r*SIN(theta)
320    IF Pen=1 THEN PLOT LINES:CurPosX,CurPosY; NewPosX,NewPosY
330    LET CurPosX=NewPosX
340    LET CurPosY=NewPosY
350 END SUB
360 REM ―――ここまでタートルライブラリ―――
400 SET WINDOW -0.5,1.5,-0.5,1.5
410 DRAW grid
420 SUB Crab(LENGTH,sign,depth)
430    IF depth=0 THEN
440       CALL Forward(LENGTH)
450    ELSE
460       CALL LeftTurn(45*sign)
470       CALL Crab(LENGTH/SQR(2),sign,depth-1)
480       CALL RightTurn(90*sign)
490       CALL Crab(LENGTH/SQR(2),-sign,depth-1)
500       CALL LeftTurn(45*sign)
510    END IF
520 END  SUB
530 CALL RightTurn(90)
540 CALL Crab(1,1,12)
550 END
  
