! 微分方程式y'=ay,y(0)=bの近似解
INPUT a,b         ! たとえば，-1,5
LET x=0
LET y=b
LET dx=0.01
SET WINDOW 0,10,-5,5
DRAW grid
SET POINT STYLE 1
DO WHILE X<=10
   PLOT POINTS:x,y
   LET y=y+a*y*dx
   LET x=x+dx
LOOP
END

