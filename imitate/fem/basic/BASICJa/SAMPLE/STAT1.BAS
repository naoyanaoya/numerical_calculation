! 平均，分散，標準偏差を求める関数定義の例
DECLARE EXTERNAL FUNCTION mean, variance, StandardDeviation
DATA 1,2,3,4,5
DIM x(5)
MAT READ x
PRINT "平均";mean(x),"分散";variance(x),"標準偏差";StandardDeviation(x)
END
! 以下の関数は統計ライブラリとして利用できる
! 平均
EXTERNAL FUNCTION mean(x())
LET a=LBOUND(x)
LET b=UBOUND(x)
LET s=0
FOR i=a TO b
   LET s=s+x(i)
NEXT i
LET mean=s/SIZE(x)
END FUNCTION 
! 分散
EXTERNAL FUNCTION variance(x())
DECLARE EXTERNAL FUNCTION mean
LET a=LBOUND(x)
LET b=UBOUND(x)
LET m=mean(x)
LET s=0
FOR i=a TO b
   LET s=s+(x(i)-m)^2
NEXT i
LET variance=s/SIZE(x)
END FUNCTION 
! 標準偏差
EXTERNAL FUNCTION StandardDeviation(x())
DECLARE EXTERNAL FUNCTION variance
LET StandardDeviation=SQR(variance(x))
END FUNCTION

