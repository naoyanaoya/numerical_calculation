PICTURE House   
   SET AREA COLOR 15                           
   PLOT AREA:  0,1; 0,0; 2,0; 2,1                          ! 壁     
   SET AREA COLOR 2                   
   PLOT AREA: -0.6,1; 2.6,1; 2,2; 0,2                     ! 屋根             
   SET AREA COLOR 10
   PLOT AREA: 0.1,0; 0.1,0.8; 0.5,0.8; 0.5,0              ! ドア              
   SET AREA COLOR 5
   PLOT AREA: 1.4,0.4; 1.9,0.4; 1.9,0.8; 1.4,0.8         ! 窓 
   SET AREA COLOR 12
   PLOT AREA: 1.7,2; 1.7,2.3; 1.5,2.3; 1.5,2              ! 煙突
END PICTURE
! 一次変換(退化する例)
DIM T(4,4)
MAT T=IDN       ! 単位行列
LET T(1,1)=2
LET T(1,2)=-1
LET T(2,1)=-1
LET T(2,2)=1/2                                            
SET WINDOW -5,5,-5,5                                       
DRAW axes                                                  
DRAW House WITH T                    
END                                                        
