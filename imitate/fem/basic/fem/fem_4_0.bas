10 REM -----------------------------
20 REM 1次元熱伝導方程式4-0
30 REM -----------------------------
40 DIM M1(4, 4)
50 DIM M1I(4, 4)
60 DIM M2(4, 4)
70 DIM M3(4, 4)
80 DIM M(4, 4)
90 DIM Z0(4)
100 DIM Z1(4)
110 LET DT = 0.01
120 LET T = 0.3
130 LET Z0(1) = 0
140 LET Z0(2) = 10
150 LET Z0(3) = 10
160 LET Z0(4) = 0
170 FOR I = 1 TO 4
180    FOR J = 1 TO 4
190       LET M1(I, J) = 0
200       LET M2(I, J) = 0
210       LET M3(I, J) = 0
220       LET M(I, J) = 0
230    NEXT J
240 NEXT I
250 LET M1(1, 1) = 1 / 3
260 LET M1(1, 2) = 1 / 6
270 LET M1(2, 1) = 1 / 6
280 LET M1(2, 2) = 2 / 3
290 LET M1(2, 3) = 1 / 6
300 LET M1(3, 2) = 1 / 6
310 LET M1(3, 3) = 2 / 3
320 LET M1(3, 4) = 1 / 6
330 LET M1(4, 3) = 1 / 6
340 LET M1(4, 4) = 1 / 3
350 LET M2(1, 1) = 1
360 LET M2(1, 2) = -1
370 LET M2(2, 1) = -1
380 LET M2(2, 2) = 2
390 LET M2(2, 3) = -1
400 LET M2(3, 2) = -1
410 LET M2(3, 3) = 2
420 LET M2(3, 4) = -1
430 LET M2(4, 3) = -1
440 LET M2(4, 4) = 1
450 FOR I = 1 TO 4
460    FOR J = 1 TO 4
470       LET M3(I, J) = M1(I, J) - DT * M2(I, J)
480    NEXT J
490 NEXT I
500 LET M1I(1, 1) = 52
510 LET M1I(1, 2) = -14
520 LET M1I(1, 3) = 4
530 LET M1I(1, 4) = -2
540 LET M1I(2, 1) = -14
550 LET M1I(2, 2) = 28
560 LET M1I(2, 3) = -8
570 LET M1I(2, 4) = 4
580 LET M1I(3, 1) = 4
590 LET M1I(3, 2) = -8
600 LET M1I(3, 3) = 28
610 LET M1I(3, 4) = -14
620 LET M1I(4, 1) = -2
630 LET M1I(4, 2) = 4
640 LET M1I(4 ,3) = -14
650 LET M1I(4, 4) = 52
660 FOR I = 1 TO 4
670    FOR J = 1 TO 4
680       LET M1I(I, J) = M1I(I, J) / 15
690    NEXT J
700 NEXT I
710 FOR I = 1 TO 4
720    FOR J = 1 TO 4
730       FOR K = 1 TO 4
740          LET M(I, J) = M(I, J) + M1I(I, K) * M3(K, J)
750       NEXT K
760    NEXT J
770 NEXT I
780 FOR J = 1 TO 4
790    LET M(1, J) = 0
800    LET M(4, J) = 0
810 NEXT J
820 LET M(1, 1) = 1
830 LET M(4, 4) = 1
840 LET N = 100 * T
850 FOR L = 1 TO N
860    FOR I = 1 TO 4
870       LET Z1(I) = 0
880    NEXT I
890    FOR I = 1 TO 4
900       FOR K = 1 TO 4
910          LET Z1(I) = Z1(I) + M(I, K) * Z0(K)
920       NEXT K
921    NEXT I
922    FOR I = 1 TO 4
923       LET Z0(I) = Z1(I)
930    NEXT I
940 NEXT L
    !' 950 CLS 3
960 PRINT T;"秒後の温度分布"
970 FOR I = 1 TO 4
980    PRINT "z";I;"=";Z0(I)
990 NEXT I
1000 END
