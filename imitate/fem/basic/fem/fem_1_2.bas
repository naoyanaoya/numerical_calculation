10 REM ----------------------
20 REM 有限要素法1次元モデル2-2
30 REM ----------------------
40 LET ALF = -1 / 12
50 LET N = 10
60 LET N1 = N + 1
70 LET N2 = N + 2
80 DIM M(N1, N2)
90 DIM X(N1)
100 DIM L(N)
110 DIM A(2, 2, N)
120 DIM B(2, N)
130 FOR I = 1 TO N1
140     READ X(I)
150 NEXT I
160 LET LL = X(N1)
170 FOR I = 1 TO N
180    LET L(I) = X(I + 1) - X(I)
190 NEXT I
200 FOR I = 1 TO N1
210    FOR J = 1 TO N2
220       LET M(I, J) = 0
230    NEXT J
240 NEXT I
250 FOR I = 1 TO N
260    LET A(1, 1, I) = 1 / L(I)
270    LET A(2, 2, I) = 1 / L(I)
280    LET A(1, 2 ,I) = - 1 / L(I)
290    LET A(2, 1, I) = - 1 / L(I)
300    LET B(1, I) = ALF * L(I) / 2
310    LET B(2, I) = ALF * L(I) / 2
320 NEXT I
330 FOR I = 1 TO N
340    LET M(I, I) = M(I, I) + A(1, 1, I)
350    LET M(I, I + 1) = M(I, I + 1) + A(1, 2, I)
360    LET M(I + 1, I) = M(I + 1, I) + A(2, 1, I)
370    LET M(I + 1, I + 1) = M(I + 1, I + 1) + A(2, 2, I)
380    LET M(I, N2) = M(I, N2) - B(1, I)
390    LET M(I + 1, N2) = M(I + 1, N2) - B(2, I)
400 NEXT I
420 FOR J = 2 TO N2
430    LET M(1, J) = 0
431    LET M(N1, J) = 0
440 NEXT J
441 LET M(1, 1) = 1
442 LET M(N1, N1) = 1
443 FOR I = 1 TO N1
444    PRINT "u";I;"=";M(I, N2)
445 NEXT I
450 REM -----------------N元1次連立方程式の解法---------
460 LET EPS = 10 ^ (-6)
470 FOR K = 1 TO N1
480    LET K1 = K + 1
490    LET MAX = ABS(M(K, K))
500    LET IR = K
510    IF K = N1 THEN 570
520    FOR I = K1 TO N1
530       IF ABS(M(I, K)) < MAX THEN 560
540       LET MAX = ABS(M(I, K))
550       LET IR = I
560    NEXT I
570    IF MAX < EPS THEN 870
580    IF IR = K THEN 620
590    FOR J = K TO N2
600       SWAP M(K, J), M(IR, J)
610    NEXT j
620    LET W = M(K, K)
630    FOR J = K TO N2
640       LET M(K, J) = M(K, J) / W
650    NEXT J
660    IF K = N1 THEN 740
670    FOR I = K1 TO N1
680       LET MI = M(I, K)
690       FOR J = K1 TO N2
700          LET M(I, J) = M(I, J) - MI * M(K, J)
710       NEXT J
720    NEXT I
730 NEXT K
740 IF N1 = 1 THEN 820
750 FOR K = N1 - 1 TO 1 STEP -1
760    LET S = M(K, N2)
770    FOR J = K + 1 TO N1
780       LET S = S - M(K, J) * M(J, N2)
790    NEXT J
800    LET M(K, N2) = S
810 NEXT K
820 PRINT "solution"
830 FOR I = 1 TO N1
840    PRINT "u";I;"=";M(I, N2)
850 NEXT I
860 GOTO 880
870 PRINT "no solution"
880 STOP
890 DATA 0, 1, 2, 3, 4, 6, 8, 10, 12, 16, 20
900 END