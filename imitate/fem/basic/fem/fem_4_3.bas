10 REM ------------------------------
20 REM 1次元熱伝導方程式4-3
30 REM ------------------------------
40 LET N = 13
50 LET N1 = N + 1
60 LET N2 = N - 1
70 LET DT = 0.001
80 LET T = 6.4
90 LET NC = 1000 * T
100 DIM M1(N, N)
110 DIM M1I(N, N)
120 DIM M2(N, N)
130 DIM M3(N, N)
140 DIM M(N, N)
150 DIM X(N)
160 DIM L(N2)
170 DIM Z1(N)
180 DIM Z0(N)
190 DIM MA(N, N1)
200 DIM MA0(N, N1)
210 FOR I = 1 TO N
220    READ X(I)
230 NEXT I
240 FOR I = 1 TO N
250    READ Z0(I)
260 NEXT I
    !' 270 REM ---------XY座標の作成と初期条件のグラフ-------------
280 REM ----------------行列M1,M2,M3の計算-----------------
290 FOR I = 1 TO N2
300    LET L(I) = X(I + 1) - X(I)
310 NEXT I
320 FOR I = 1 TO N
330    FOR J = 1 TO N
340       LET M1(I, J) = 0
350       LET M1I(I, J) = 0
360       LET M2(I, J) = 0
370       LET M3(I, J) = 0
380       LET M(I, J) = 0
390    NEXT J
400 NEXT I
410 FOR I = 1 TO N2
420    LET M1(I, I) = M1(I, I) + L(I) / 3
430    LET M1(I, I + 1) = M1(I, I + 1) + L(I) / 6
440    LET M1(I + 1, I) = M1(I + 1, I) + L(I) / 6
450    LET M1(I + 1, I + 1) = M1(I + 1, I + 1) + L(I) / 3
460 NEXT I
470 FOR I = 1 TO N2
480    LET M2(I, I) = M2(I, I) + 1 / L(I)
490    LET M2(I, I + 1) = M2(I, I + 1) - 1 / L(I)
500    LET M2(I + 1, I) = M2(I + 1, I) - 1 / L(I)
510    LET M2(I + 1, I + 1) = M2(I + 1, I + 1) + 1 / L(I)
511 NEXT I
520 FOR I = 1 TO N
530    FOR J = 1 TO N
540       LET M3(I, J) = M1(I, J) - DT * M2(I, J)
550    NEXT J
560 NEXT I
570 REM ---------------------行列M1の逆行列M1Iの計算---------
580 FOR I = 1 TO N
590    FOR J = 1 TO N
600       LET MA0(I, J) = M1(I, J)
610    NEXT J
620 NEXT I
630 LET EPS = 10 ^ (-6)
640 FOR LL = 1 TO N
650    FOR I = 1 TO N
660       FOR J = 1 TO N
670          LET MA(I, J) = MA0(I, J)
680       NEXT J
690    NEXT I
700    FOR I = 1 TO N
710       LET MA(I, N1) = 0
720    NEXT I
730    LET MA(LL, N1) = 1
740    FOR K = 1 TO N
750       LET K1 = K + 1
760       LET MAX = ABS(MA(K, K))
770       LET IR = K
780       IF K = N THEN 840
790       FOR I = K1 TO N
800          IF ABS(MA(I, K)) < MAX THEN 830
810          LET MAX = ABS(MA(I, K))
820          LET IR = I
830       NEXT I
840       IF MAX < EPS THEN 1140
850       IF IR = K THEN 890
860       FOR J = K TO N1
870          SWAP MA(K, J), MA(IR, J)
880       NEXT J
890       LET W = MA(K, K)
900       FOR J = K TO N1
910          LET MA(K, J) = MA(K, J) / W
920       NEXT J
930       IF K = N THEN 1010
940       FOR I = K1 TO N
950          LET MI = MA(I, K)
960          FOR J = K1 TO N1
970             LET MA(I, J) = MA(I, J) - MI * MA(K, J)
980          NEXT J
990       NEXT I
1000    NEXT K
1010    IF N = 1 THEN 1090
1020    FOR K = N - 1 TO 1 STEP -1
1030       LET S = MA(K, N1)
1040       FOR J = K + 1 TO N
1050          LET S = S - MA(K, J) * MA(J, N1)
1060       NEXT J
1070       LET MA(K, N1) = S
1080    NEXT K
1090    FOR I = 1 TO N
1100       LET M1I(I, LL) = MA(I, N1)
1110    NEXT I
1120 NEXT LL
1130 GOTO 1160
1140 PRINT "no solution"
1150 STOP
1160 REM ---------行列Mの決定--------------
1170 FOR I = 1 TO N
1180    FOR J = 1 TO N
1190       FOR K = 1 TO N
1200          LET M(I, J) = M(I, J) + M1I(I, K) * M3(K, J)
1210       NEXT K
1220    NEXT J
1230 NEXT I
1240 REM FOR J = 1 TO N
1250 REM LET M(1, J) = 0
1260 REM LET M(N, J) = 0
1270 REM NEXT J
1280 REM LET M(1, 1) = 1
1290 REM LET M(N, N) = 1
1300 REM ------------温度zの決定---------------
1310 FOR LL = 1 TO NC
1320    FOR I = 1 TO N
1330       LET Z1(I) = 0
1340    NEXT I
1350    FOR I = 1 TO N
1360       FOR  K = 1 TO N
1370          LET Z1(I) = Z1(I) + M(I, K) * Z0(K)
1380       NEXT K
1390    NEXT I
1400    FOR I = 1 TO N
1410       LET Z0(I) = Z1(I)
1440    NEXT I
1450    FOR J = 0 TO 6
1560       IF LL = 100 * 2 ^ J THEN GOTO 1574
1570    NEXT J
1574    PRINT "time is ";LL 
1571    FOR I = 1 TO N
1572       PRINT "z";"(";I;")";" ";z0(I)
1573    NEXT I
1580 NEXT LL
1590 STOP
1591 DATA 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6
1592 DATA 0, 0, 1, 2, 3, 4, 5, 6, 6, 4, 2, 0, 0
1600 END
