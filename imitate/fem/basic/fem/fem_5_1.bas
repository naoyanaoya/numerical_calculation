10 REM -----------------------------
20 REM ２次元熱伝導方程式5-1(放熱)
30 REM -----------------------------
40 LET T = 100
50 LET NN = 25
60 LET NE = 32
70 LET NN1 = NN + 1
80 LET ND = 16
90 LET DT = 0.01
100 LET NC = T / DT
110 DIM M(NN, NN)
111 DIM IP(3, NE)
120 DIM M1I(NN, NN)
130 DIM X(NN)
140 DIM Y(NN)
150 DIM Z0(NN)
160 DIM Z1(NN)
170 DIM ID(ND)
180 DIM ZD(ND)
190 DIM M1(NN, NN)
200 DIM M2(NN, NN)
210 DIM M3(NN, NN)
220 DIM MA(NN, NN1)
230 FOR I = 1 TO 3
240    FOR J = 1 TO NE
250       READ IP(I, j)
260    NEXT J
270 NEXT I
280 FOR I = 1 TO NN
290    READ X(I)
300 NEXT I
310 FOR I = 1 TO NN
320    READ Y(I)
330 NEXT I
340 FOR I = 1 TO ND
350    READ ID(I)
360 NEXT I
370 FOR I = 1 TO ND
380    READ ZD(I)
390 NEXT I
400 FOR I = 1 TO NN
410    READ Z0(I)
420 NEXT I
' 430 IF NC = 0 THEN 1860
440 REM ---------------行列M1,M2の作成---------------------
450 FOR I = 1 TO NN
460    FOR J = 1 TO NN
470       LET M1(I, J) = 0
480       LET M2(I, J) = 0
481    NEXT J
482 NEXT I
490 FOR K = 1 TO NE
500    LET I1 = IP(1, K)
510    LET I2 = IP(2, K)
520    LET I3 = IP(3, K)
530    LET X1 = X(I1)
540    LET X2 = X(I2)
550    LET X3 = X(I3)
560    LET Y1 = Y(I1)
570    LET Y2 = Y(I2)
580    LET Y3 = Y(I3)
590    LET DET = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2)
600    LET M1(I1, I1) = M1(I1, I1) + DET / 12
610    LET M1(I1, I2) = M1(I1, I2) + DET / 24
620    LET M1(I1, I3) = M1(I1, I3) + DET / 24
630    LET M1(I2, I1) = M1(I2, I1) + DET / 24
640    LET M1(I2, I2) = M1(I2, I2) + DET / 12
650    LET M1(I2, I3) = M1(I2, I3) + DET / 24
660    LET M1(I3, I1) = M1(I3, I1) + DET / 24
670    LET M1(I3, I2) = M1(I3, I2) + DET / 24
680    LET M1(I3, I3) = M1(I3, I3) + DET / 12
690    LET B1 = (Y2 - Y3) / DET
700    LET B2 = (Y3 - Y1) / DET
710    LET B3 = (Y1 - Y2) / DET
720    LET C1 = (X3 - X2) / DET
730    LET C2 = (X1 - X3) / DET
740    LET C3 = (X2 - X1) / DET
750    LET M2(I1, I1) = M2(I1, I1) + DET * (B1 * B1 + C1 * C1) / 2
760    LET M2(I1, I2) = M2(I1, I2) + DET * (B1 * B2 + C1 * C2) / 2
770    LET M2(I1, I3) = M2(I1, I3) + DET * (B1 * B3 + C1 * C3) / 2
780    LET M2(I2, I1) = M2(I2, I1) + DET * (B2 * B1 + C2 * C1) / 2
790    LET M2(I2, I2) = M2(I2, I2) + DET * (B2 * B2 + C2 * C2) / 2
800    LET M2(I2, I3) = M2(I2, I3) + DET * (B2 * B3 + C2 * C3) / 2
810    LET M2(I3, I1) = M2(I3, I1) + DET * (B3 * B1 + C3 * C1) / 2
820    LET M2(I3, I2) = M2(I3, I2) + DET * (B3 * B2 + C3 * C2) / 2
830    LET M2(I3, I3) = M2(I3, I3) + DET * (B3 * B3 + C3 * C3) / 2
840 NEXT K
850 REM ----------- 行列M3の作成-----------------
860 FOR I = 1 TO NN
870    FOR J = 1 TO NN
880       LET M3(I, J) = M1(I, J) - DT * M2(I, J)
890    NEXT J
900 NEXT I
910 DATA 1, 1, 2, 2, 3, 4, 4, 5, 6, 6, 7, 7, 8, 9, 9, 10, 11, 12, 12, 13, 13, 13, 14, 14, 16, 17, 17, 18, 18, 18, 19, 19
920 DATA 6, 7, 7, 8, 8, 8, 9, 9, 11, 12, 12, 13, 13, 13, 14, 14, 16, 16, 17, 17, 18, 19, 19, 20, 21, 21, 22, 22, 23, 24, 24, 25
930 DATA 7, 2, 8, 3, 4, 9, 5, 10, 12, 7, 13, 8, 9, 14, 10, 15, 12, 17, 13, 18, 19, 14, 20, 15, 17, 22, 18, 23, 24, 19, 25, 20
940 DATA 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4
950 DATA 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4
960 DATA 1, 2, 3, 4, 5, 6, 10, 11, 15 ,16, 20, 21, 22, 23, 24, 25
961 DATA 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
970 DATA 0, 0, 0, 0, 0, 0, 9, 12, 9, 0, 0, 12, 16, 12, 0, 0, 9, 12, 9, 0, 0, 0, 0, 0, 0
980 REM ---------------行列M1の逆行列M1Iの作成-------------------------
990 LET EPS = 10 ^ (-6)
1000 FOR L = 1 TO NN
1010    FOR I = 1 TO NN
1020       FOR J = 1 TO NN
1030          LET MA(I, J) = M1(I, J)
1040       NEXT J
1050    NEXT I
1060    FOR I = 1 TO NN
1070       LET MA(I, NN1) = 0
1080    NEXT I
1090    LET MA(L, NN1) = 1
1100    REM -------------------前進消去------------------
1110    FOR K = 1 TO NN
1120       LET K1 = K + 1
1130       LET MAX = ABS(MA(K, K))
1140       LET IR = K
1150       IF K = NN THEN 1210
1160       FOR I = K1 TO NN
1170          IF ABS(MA(I, K)) < MAX THEN 1200
1180          LET MAX = ABS(MA(I, K))
1190          LET IR = I
1200       NEXT I
1210       IF MAX < EPS THEN 1490
1220       IF IR = K THEN 1250
1221       FOR J = K TO NN1
1230          SWAP MA(K, J), MA(IR, J)
1240       NEXT J
1250       LET W = MA(K, K)
1260       FOR J = K TO NN1
1270          LET MA(K, J) = MA(K, J) / W
1280       NEXT J
1290       IF K = N THEN 1370
1300       FOR I = K1 TO NN
1310          LET MI = MA(I, K)
1320          FOR J = K1 TO NN1
1330             LET MA(I, J) = MA(I, J) - MI * MA(K, J)
1340          NEXT J
1350       NEXT I
1360    NEXT K
1370    REM ------------------------後退代入-------------------
1380    FOR K = NN - 1 TO 1 STEP -1
1390       LET S = MA(K, NN1)
1400       FOR J = K + 1 TO NN
1410          LET S = S - MA(K, J) * MA(J, NN1)
1420       NEXT J
1430       LET MA(K, NN1) = S
1440    NEXT K
1450    FOR I = 1 TO NN
1460       LET M1I(I, L) = MA(I, NN1)
1470    NEXT I
1480 NEXT L
1481 GOTO 1510
1490 PRINT "no solution"
1500 STOP
1510 REM ------------------行列Mの計算--------------------
1520 FOR I = 1 TO NN
1530    FOR J = 1 TO NN
1540       LET M(I, J) = 0
1550    NEXT J
1560 NEXT I
1570 FOR I = 1 TO NN
1580    FOR J = 1 TO NN
1590       FOR K = 1 TO NN
1600          LET M(I, J) = M(I, J) + M1I(I, K) * M3(K, J)
1610       NEXT K
1620    NEXT J
1630 NEXT I
1640 FOR I = 1 TO ND
1650    LET IDR = ID(I)
1660    FOR J = 1 tO NN
1670       LET M(IDR, J) = 0
1680    NEXT J
1690    LET M(IDR, IDR) = 1
1700    LET Z0(IDR) = ZD(I)
1710 NEXT I
1711 PRINT "test"
1720 REM -------------------z0の計算--------------------
1730 FOR L = 1 TO NC
1740    FOR I = 1 TO NN
1750       LET Z1(I) = 0
1760    NEXT I
1770    FOR I = 1 TO NN
1780       FOR J = 1 TO NN
1790          LET Z1(I) = Z1(I) + M(I, J) * Z0(J)
1800       NEXT J
1810    NEXT I
1820    FOR I = 1 TO NN
1830       LET Z0(I) = Z1(I)
1840    NEXT I
1841    FOR J = 0 TO 6
1842        IF L = 100 * 2 ^ J THEN GOTO 1844
1843    NEXT J
1844    PRINT "time is ";L
1845    FOR I = 1 TO NN
1846       PRINT "z";"(";I;")";" ";z0(I)
1847    NEXT I
1850 NEXT L
1860 REM -------------------結果--------------------
941 FOR I = 1 TO NN
942 FOR J = 1 TO NN
943 PRINT M1(I, J)
944 NEXT J
945 NEXT I
' 941 FOR I = 1 TO NN
' 942 FOR J = 1 TO NN
' 943 PRINT M2(I, J)
' 944 NEXT J
' 945 NEXT I
1870 END
