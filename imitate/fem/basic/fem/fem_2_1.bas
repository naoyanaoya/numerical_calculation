10 REM --------------------------------
20 REM 有限要素法2次元モデル3-0
30 REM -------------------------------
40 LET NN = 25
50 LET NE = 32
60 LET NN1 = NN + 1
70 LET NB = 16
71 OPTION BASE 0
80 DIM M(NN, NN1)
90 DIM IP(3, NE)
100 DIM X(NN)
110 DIM Y(NN)
' 120 DIM Z(NN)
130 DIM ID(NB)
140 DIM FAD(NB)
150 FOR I = 1 TO 3
160    FOR J = 1 TO NE
170       READ IP(I, J)
180    NEXT J
190 NEXT I
200 FOR I = 1 TO NN
210    READ X(I)
220 NEXT I
230 FOR I = 1 TO NN
240    READ Y(I)
250 NEXT I
260 FOR I = 1 TO NB
270    READ ID(I)
280 NEXT I
290 FOR I = 1 TO NB
300    READ FAD(I)
310 NEXT I
320 FOR I = 1 TO NN
330    FOR J = 1 TO NN1
340       LET M(i, J) = 0
350    NEXT J
360 NEXT I
' 364 PRINT "temp"
' 361 FOR I = 1 TO NN
' 362    PRINT "z";I;" ";M(I, NN1)
' 363 NEXT I
370 FOR K = 1 TO NE
380    LET I1 = IP(1, K)
390    LET I2 = IP(2, K)
400    LET I3 = IP(3, K)
410    LET X1 = X(I1)
420    LET X2 = X(I2)
430    LET X3 = X(I3)
440    LET Y1 = Y(I1)
450    LET Y2 = Y(I2)
460    LET Y3 = Y(I3)
' 462    PRINT "X1";I;" ";X1
470    LET DET = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2)
480    LET B1 = (Y2 - Y3) / DET
490    LET B2 = (Y3 - Y1) / DET
500    LET B3 = (Y1 - Y2) / DET
510    LET C1 = (X3 - X2) / DET
520    LET C2 = (X1 - X3) / DET
530    LET C3 = (X2 - X1) / DET
540    LET M(I1, I1) = M(I1, I1) + (B1 * B1 + C1 * C1) * DET / 2
550    LET M(I1, I2) = M(I1, I2) + (B1 * B2 + C1 * C2) * DET / 2
560    LET M(I1, I3) = M(I1, I3) + (B1 * B3 + C1 * C3) * DET / 2
570    LET M(I2, I1) = M(I2, I1) + (B2 * B1 + C2 * C1) * DET / 2
580    LET M(I2, I2) = M(I2, I2) + (B2 * B2 + C2 * C2) * DET / 2
590    LET M(I2, I3) = M(I2, I3) + (B2 * B3 + C2 * C3) * DET / 2
600    LET M(I3, I1) = M(I3, I1) + (B3 * B1 + C3 * C1) * DET / 2
610    LET M(I3, I2) = M(I3, I2) + (B3 * B2 + C3 * C2) * DET / 2
620    LET M(I3, I3) = M(I3, I3) + (B3 * B3 + C3 * C3) * DET / 2
' 621    PRINT "M(I1, I1)";" ";M(I1, I1)
630 NEXT K
' 666 PRINT "matrix"
' 631 FOR I = 1 TO NN
' 632 FOR J = 1 TO NN1
' 633    PRINT "M(";I;",";J;")";" ";M(I, J)
' 634 NEXT J
' 635 NEXT I
640 FOR I = 1 TO NB
650    LET IDR = ID(I)
660    FOR J = 1 TO NN
670       LET M(IDR, J) = 0
680    NEXT J
690    LET M(IDR, IDR) = 1
700    LET M(IDR, NN1) = FAD(I)
710 NEXT I
' 711 FOR I = 1 TO NN
' 713    PRINT "z";I;" ";M(I, NN1)
' 714 NEXT I
720 DATA 1, 1, 2, 2, 3, 3, 4, 4, 6, 6, 7, 7, 8, 8, 9, 9, 11, 11, 12, 12, 13, 13, 14, 14, 16, 16, 17, 17, 18, 18, 19, 19
730 DATA 6, 7, 7, 8, 8, 9, 9, 10, 11, 12, 12, 13, 13, 14, 14, 15, 16, 17, 17, 18, 18, 19, 19, 20, 21, 22, 22, 23, 23, 24, 24, 25
740 DATA 7, 2, 8, 3, 9, 4, 10, 5, 12, 7, 13, 8, 14, 9, 15, 10, 17, 12, 18, 13, 19, 14, 20, 15, 22, 17, 23, 18, 24, 19, 25, 20
750 DATA 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4
760 DATA 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4
770 DATA 1, 2, 3, 4, 5, 6, 10, 11, 15, 16, 20, 21, 22, 23, 24, 25
780 DATA 0, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
790 REM ---------NN元1次方程式の解法-------------------
800 LET EPS = 10 ^ (-6)
810 FOR K = 1 TO NN
820    LET K1 = K + 1
830    LET MAX = ABS(M(K, K))
840    LET IR = K
850    IF K = NN THEN 910
860    FOR I = K1 TO NN
870       IF ABS(M(I, K)) < MAX THEN 900
880       LET MAX = ABS(M(I, K))
890       LET IR = I
900    NEXT I
910    IF MAX < EPS THEN 1210
920    IF IR = K THEN 960
930    FOR J = K TO NN1
940       SWAP M(K, J), M(IR, J)
950    NEXT J
960    LET W = M(K, K)
970    FOR J = K TO NN1
980       LET M(K, J) = M(K, J) / W
990    NEXT J
1000    IF K = N THEN 1070
1010    FOR I = K1 TO NN
1020       LET MI = M(I, K)
1030       FOR J = K1 TO NN1
1040          LET M(I, J) = M(I, J) - MI * M(K, J)
1050       NEXT J
1060    NEXT I
1061 NEXT K
1070 IF N = 1 THEN 1150
1080 FOR K = NN - 1 TO 1 STEP -1
1090    LET S = M(K, NN1)
1100    FOR J = K + 1 TO NN
1110       LET S = S - M(K, J) * M(J, NN1)
1120    NEXT J
1130    LET M(K, NN1) = S
1140 NEXT K
1150 PRINT "solution"
1160 FOR I = 1 TO NN
' 1170    IF ABS(M(I, NN1)) < 10 ^ (-6) THEN LET M(I, NN1) = 0
1180    PRINT "z";I;" ";M(I, NN1)
1190 NEXT I
1200 GOTO 1220
1210 PRINT "no solution"
1211 STOP
1220 END
