#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

//TDMA//a:diagonal,c:left,b:right,
inline void tdma(double a[], double c[], double b[], double d[], double x[], int size)
{
	int i;
	double *P=new double[size];
	double *Q=new double[size];
	
	//first step//
	P[0]=-b[0]/a[0];
	Q[0]=d[0]/a[0];
	
	//second step//
	for(i=1;i<size;i++)
	{
		P[i]=-b[i]/(a[i]+c[i]*P[i-1]);
		Q[i]=(d[i]-c[i]*Q[i-1])/(a[i]+c[i]*P[i-1]);
	}
	
	//third step, backward//
	x[size-1]=Q[size-1];

	for(i=size-2;i>-1;i=i-1)
	{
		x[i]=P[i]*x[i+1]+Q[i];
	}
	
	delete [] P,Q;
}

inline void mat(double AD[],double AL[],double AR[],double BD[],double BL[],double BR[],double b[],double f[], int &Ele, double &dx, double &Diff, double &dt, double &V, int &Node)
{
	//initialization//
	for(int i=0;i<Node;i++)
	{
		AD[i]=0.0;AL[i]=0.0;AR[i]=0.0;BD[i]=0.0;BL[i]=0.0;BR[i]=0.0;
	}
	
	for(int i=0;i<Ele;i++)
	{
		double time=(2.0/dt);
		double advec=(2.0*abs(V)/dx);
		double tau=1.0/sqrt(time*time+advec*advec);
		
		double the=0.0;

		//A//
		//temporal//
		AD[i]+=dx*2.0/6.0/dt;
		AR[i]+=dx*1.0/6.0/dt;
		AL[i+1]+=dx*1.0/6.0/dt;
		AD[i+1]+=dx*2.0/6.0/dt;

		//SUPG for temporal//
		AD[i]+=-tau*V/2.0/dt;
		AR[i]+=tau*V/2.0/dt;
		AL[i+1]+=-tau*V/2.0/dt;
		AD[i+1]+=tau*V/2.0/dt;

		//advection//
        AD[i]+=the*-V/2.0;
		AR[i]+=the*V/2.0;
		AL[i+1]+=the*-V/2.0;
		AD[i+1]+=the*V/2.0;

		//SUPG for advection//
		AD[i]+=the*tau*V*V/dx;
		AR[i]+=-the*tau*V*V/dx;
		AL[i+1]+=the*-tau*V*V/dx;
		AD[i+1]+=the*tau*V*V/dx;
		
		//B//
		//temporal//
		BD[i]+=dx*2.0/6.0/dt;
		BR[i]+=dx*1.0/6.0/dt;
		BL[i+1]+=dx*1.0/6.0/dt;
		BD[i+1]+=dx*2.0/6.0/dt;

		//SUPG for temporal//
		BD[i]+=-tau*V/2.0/dt;
		BR[i]+=tau*V/2.0/dt;
		BL[i+1]+=-tau*V/2.0/dt;
		BD[i+1]+=tau*V/2.0/dt;
		
		//advection//
		BD[i]-=(1.0-the)*-V/2.0;
		BR[i]-=(1.0-the)*V/2.0;
		BL[i+1]-=(1.0-the)*-V/2.0;
		BD[i+1]-=(1.0-the)*V/2.0;

		//SUPG for advection//
		BD[i]-=(1.0-the)*tau*V*V/dx;
		BR[i]-=(1.0-the)*-tau*V*V/dx;
		BL[i+1]-=(1.0-the)*-tau*V*V/dx;
		BD[i+1]-=(1.0-the)*tau*V*V/dx;
	}
}

inline void boundary(int &bc,double AD[],double AL[],double AR[],double BD[],double BL[],double BR[],double b[],int &Node)
{
	if(bc==1)
	{
		AD[0]=1.0;AR[0]=0.0;BD[0]=1.0;BR[0]=0.0;
		AL[Node-1]=0.0;AD[Node-1]=1.0;BL[Node-1]=0.0;BD[Node-1]=1.0;
	}
	if(bc==2)
	{
		AL[Node-1]=0.0;AD[Node-1]=1.0;BL[Node-1]=0.0;BD[Node-1]=1.0;
	}
	if(bc==3)
	{
		AD[0]=1.0;AR[0]=0.0;BD[0]=1.0;BR[0]=0.0;
	}
}

inline void output(double x[], int Node, double dx)
{
	stringstream ss;
	string name;
	ofstream fo;
	static int count=0;
	
	ss<<count;
	name=ss.str();
	name="answer_" + name + ".txt";
	fo.open(name.c_str ());
	
	for(int i=0;i<Node;i++)
	{
		fo<<dx*double(i)<<" "<<x[i]<<endl;
	}
	fo<<endl;
	
	count+=1;
}

int main()
{
	int i,j;
	int Ele=600;
	int Node=Ele+1;
	double LL=1.0;
	double dx=LL/Ele;
	double dt=0.001;
	double NT=10000;
	double eps=pow(2.0,-50);
	double Diff=0.01;
	double V=0.1;
	int bc=3;//bc=1 both Diriclet bc=2 left Neumann bc=3 right Neumann
	double D0=0.0;
	double D1=0.0;
	double N0=0.0;
	double N1=0.0;
    int div=5;
	
	double *b=new double[Node];
	double *x=new double[Node];
	double *xx=new double[Node];
	double *f=new double[Node];
	double *AD=new double[Node];
	double *AL=new double[Node];
	double *AR=new double[Node];
	double *BD=new double[Node];
	double *BL=new double[Node];
	double *BR=new double[Node];
	
	//initial condition//
	for(i=0;i<Node;i++)
	{
		x[i]=0.0;
	}

	for(i=20;i<100;i++)
	{
		x[i]=1.0;
	}

    output(x,Node,dx);
	mat(AD,AL,AR,BD,BL,BR,b,f,Ele,dx,Diff,dt,V,Node);
	boundary(bc,AD,AL,AR,BD,BL,BR,b,Node);
	
	for(i=1;i<=NT;i++)
	{
		//for b//
		if(bc==1)
		{
			b[0]=D0;
			b[Node-1]=D1;
		}
		if(bc==2)
		{
			b[Node-1]=D1;
			b[0]=BD[0]*x[0]+BR[0]*x[1]-N0*Diff;
		}
		if(bc==3)
		{
			b[0]=D0;
			b[Node-1]=BL[Node-1]*x[Node-2]+BD[Node-1]*x[Node-1]+N1*Diff;
		}
		
		for(j=1;j<Node-1;j++)
		{
			b[j]=BL[j]*x[j-1]+BD[j]*x[j]+BR[j]*x[j+1];
		}

		tdma(AD,AL,AR,b,xx,Node);
		for(j=0;j<Node;j++) x[j]=xx[j];
		
		if(i%div==0)
		{
			cout<<i<<endl;
			output(x,Node,dx);
		}
	}
	
	delete[] b,x,xx,f,AD,AL,AR,BD,BL,BR;
	
	return 0;
}

/*
do for [i=0:10000]{
    plot "answer_".i.".txt"
}
do for [i=0:5000]{
    plot "answer_".i.".txt"
}
plot "answer_0.txt","answer_10.txt","answer_20.txt","answer_30.txt","answer_40.txt","answer_50.txt","answer_60.txt","answer_70.txt","answer_80.txt","answer_90.txt","answer_100.txt"
*/