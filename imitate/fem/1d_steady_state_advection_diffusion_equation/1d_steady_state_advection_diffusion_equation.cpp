#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
using namespace std;
//V*du/dx-D*d^2u/dx^2=0,0<x<L
//Gauss-Seidel//
inline void GS(double D[],double L[],double R[],double x[],double b[], int &Node, double &eps)
{
	int i,k;
	double in,sum,max;
	for(k=0;k<100000;k++)
	{
		max=0.0;
		in=x[0];
		
		for(i=0;i<Node;i++)
		{
			in=x[i];
			x[i]=(b[i]-(L[i]*x[i-1]+R[i]*x[i+1]))/D[i];
			if(max<abs(in-x[i])) max=abs(in-x[i]);
		}
		
		if(eps>max) break;
	}
}

//TDMA//a:diagonal,c:left,b:right,
inline void tdma(double a[], double c[], double b[], double d[], double x[], int size)
{
	int i;
	double *P=new double[size];
	double *Q=new double[size];
	
	//first step//
	P[0]=-b[0]/a[0];
	Q[0]=d[0]/a[0];
	
	//second step//
	for(i=1;i<size;i++)
	{
		P[i]=-b[i]/(a[i]+c[i]*P[i-1]);
		Q[i]=(d[i]-c[i]*Q[i-1])/(a[i]+c[i]*P[i-1]);
	}
	
	//third step, backward//
	x[size-1]=Q[size-1];

	for(i=size-2;i>-1;i=i-1)
	{
		x[i]=P[i]*x[i+1]+Q[i];
	}
	
	delete [] P,Q;
}
inline void mat(double D[],double L[],double R[],double b[],double f[],int &Ele,double &dx,double &V,double &Diff){
    int i;
    for(i=0;i<Ele;i++){
        //Diffusion
        D[i] += Diff/dx;
        R[i] += -Diff/dx;
        L[i+1] += -Diff/dx;
        D[i+1] += Diff/dx;
        //Advection
        D[i] += -V/2.0;
        R[i] += V/2.0;
        L[i+1] += -V/2.0;
        D[i+1] += V/2.0;
        //Source
        b[i]+=(2.0*f[i]+1.0*f[i+1])*dx/6.0;
		b[i+1]+=(1.0*f[i]+2.0*f[i+1])*dx/6.0;
    }    
}
inline void boundary(int &bc,double &D0,double &D1,double &N0,double &N1,double D[],double L[],double R[],double b[],int &Node,double &Diff){
    if(bc==1)
	{
		D[0]=1.0;R[0]=0.0;b[0]=D0;
		L[Node-1]=0.0;D[Node-1]=1.0;b[Node-1]=D1;
	}
	if(bc==2)
	{
        b[0]+=-N0*Diff;
		L[Node-1]=0.0;D[Node-1]=1.0;b[Node-1]=D1;
	}
	if(bc==3)
	{
		D[0]=1.0;R[0]=0.0;b[0]=D0;
		b[Node-1]+=N1*Diff;
	}
}
int main(){
    int i;
    int Ele;
    cin >> Ele;
    int Node=Ele+1;
    double LL=1.0;
    double dx=LL/Ele;
    double eps=pow(2.0,-50);
    int bc;
    cin >> bc;
    double D0=0.0;
    double D1=1.0;
    double N0=0.0;
    double N1=2.0;
    double Diff=0.01;
    double V=0.1;
    double *b = new double[Node];
	double *x = new double[Node];
	double *f = new double[Node];
	double *D = new double[Node];
	double *L = new double[Node];
	double *R = new double[Node];
    for(i=0;i<Node;i++){
        b[i]=0.0;x[i]=0.0;f[i]=0.0;D[i]=0.0;L[i]=0.0;R[i]=0.0;
    }
    ofstream fk;
    fk.open("answer.txt");
    mat(D,L,R,b,f,Ele,dx,V,Diff);
    boundary(bc,D0,D1,N0,N1,D,L,R,b,Node,Diff);
    //Gauss-Seidel or TDMA//
	GS(D,L,R,x,b,Node,eps);
	//tdma(D,L,R,b,x,Node);
    for(i=0;i<Node;i++){
        cout << x[i] << endl;
    }
    for(i=0;i<Node;i++){
        fk << dx*i << " " << x[i] << endl;
    }
    delete [] b,x,f,D,L,R;
    return 0;
}

/*
plot "answer.txt"
*/