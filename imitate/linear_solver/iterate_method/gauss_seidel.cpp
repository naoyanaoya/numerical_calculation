// #include<iostream>
// #include<iomanip>
// #include<math.h>
// using namespace std;

// void gauss_seidel(double a[4][4],double b[4],double c[4]);

// int main(){
//     double a[4][4] = {{9,2,1,1},{2,8,-2,1},{-1,-2,7,-2},{1,-1,-2,6}}; 
//     double b[4]    = {20,16,8,17};
//     double c[4]    = {0,0,0,0};

//     gauss_seidel(a,b,c);
//     for(int i=0;i<=3;i++){
//         cout << setw(12) << fixed << setprecision(10) << c[i] << "\t"; //fixed << setprecision(10);Display up to 10 digits after the decimal point
//         cout << endl;
//     }
//     return 0;
// }
// //fixed << setprecision(10);Display up to 10 digits after the decimal point
// //setw(12);Secure character width 12

// void gauss_seidel(double a[4][4],double b[4],double c[4]){
//     for(int n=0;n<=100;n++){
//         double s;
//         double e=0;
//         for(int i=0;i<=3;i++){
//             s=b[i];
//             for(int j=0;j<=(i-1);j++){
//             s -= a[i][j]*c[j];
//             }
//             for(int j=i+1;j<=3;j++){
//             s -= a[i][j]*c[j];
//             }
//             s /= a[i][i];
//             e += fabs(s-c[i]);
//             c[i]=s;
//             cout << setw(12) << fixed << setprecision(10) << c[i] << "\t";
//         }
//         cout << endl;
        
//         if(e <= 0.0000000001){
//             return;
//         }
//     }
//     cout << "収束しない" << endl;
// }

#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<vector>
using namespace std;

//inner product
inline double ip(vector<double> &a,vector<double> &b,int &size){
    double value=0;
    int i;
    for(i=0;i<size;i++){
        value += a[i]*b[i];
    }
    return value;
}
inline void GS(vector<vector<double>> &A,vector<double> &x,vector<double> &b,int &size, double &eps){
    int i,k;
    double in,sum,max;
    for(k=0;k<1000000;k++){
        max=0.0;
        for(i=0;i<size;i++){
            in=x[i];
            x[i]=(b[i]-(ip(A[i],x,size)-A[i][i]*x[i]))/A[i][i];
            if(max<abs(in-x[i])){
                max=abs(in-x[i]);
            }
        }
        if(eps>max){
                break;
            }
    }
}
int main(){
    int size=3;
    int i;
    double eps=pow(2.0,-50);
    double in,max;
    vector<double> b(size,0);
    vector<double> x(size,0);
    vector<vector<double>> A(size,(vector<double>(size,0)));
    A[0][0]=3;A[0][1]=2;A[0][2]=1;
	A[1][0]=1;A[1][1]=4;A[1][2]=1;
	A[2][0]=2;A[2][1]=2;A[2][2]=5;
	b[0]=10;b[1]=12;b[2]=21;
	
	GS(A,x,b,size,eps);

	for(i=0;i<size;i++){
         cout<<x[i]<<endl;
    }

        return 0;
}