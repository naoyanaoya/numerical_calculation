#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

int main()
{	
	const int xn=6;
	const int yn=6;
	int i,j,k,l;
	double divv;
	double err;
	double udiv,vdiv;
	double ua,ub,va,vb;
	double pres;
	double uad,vad;
	double udif,vdif;
	double umid,vmid;
	double u[xn+2][yn+1],v[xn+1][yn+2],p[xn+1][yn+1];//velocities and pressure
	double d[xn+1][yn+1];//for continuity
	double r[xn+1][yn+1];//for r.h.s. of Poisson equation
	double uwall=1.;
	double dx=1./(double)(xn-1);
	double dy=1./(double)(yn-1);
	double dt=0.001;
	double re=100;
	double C1=0.5*dy*dy/(dx*dx+dy*dy);
	double C2=0.5*dx*dx/(dx*dx+dy*dy);
	double C3=0.5*dy*dy/(1.+dy*dy/(dx*dx));
	int lm=20000;
	int km=100;

	ofstream fk,ff;
	fk.open("vel.txt");
	ff.open("pre.txt");

	//initialization//
	for(i=0;i<xn+1;i++)
	{
		for(j=0;j<yn+1;j++)
		{
			p[i][j]=0.;
		}
	}
	
	for(i=0;i<xn+2;i++)
	{
		for(j=0;j<yn+1;j++)
		{
			u[i][j]=0.;
		}
	}
	
	for(i=0;i<xn+1;i++)
	{
		for(j=0;j<yn+2;j++)
		{
			v[i][j]=0.;
		}
	}
	
	//time step//
	for(l=1;l<=lm;l++)
	{
		//BC for left and right//
		for(j=0;j<yn+1;j++)
		{
			u[1][j]=0.;
			u[0][j]=u[2][j];
			v[0][j]=-v[1][j];
			
			u[xn][j]=0.;
			u[xn+1][j]=u[xn-1][j];
			v[xn][j]=-v[xn-1][j];
		}
		v[0][yn+1]=-v[1][yn+1];
		v[xn][yn+1]=-v[xn-1][yn+1];
		
		//BC for bottom and top//
		for(i=0;i<xn+1;i++)
		{
			v[i][1]=0.;
			v[i][0]=v[i][2];
			u[i][0]=-u[i][1];
			
			v[i][yn]=0.;
			v[i][yn+1]=v[i][yn-1];
			u[i][yn]=2.0*uwall-u[i][yn-1];//for wall
		}
		u[xn+1][0]=-u[xn][0];
		u[xn+1][yn]=-u[xn][yn];
		
		divv=0.;
		//r.h.s. of Poisson equation
		for(i=1;i<xn;i++)
		{
			for(j=1;j<yn;j++)
			{
				udiv=(u[i+1][j]-u[i][j])/dx;
				vdiv=(v[i][j+1]-v[i][j])/dy;
				d[i][j]=udiv+vdiv;
				divv+=fabs(udiv+vdiv);
				ua=(u[i][j]+u[i+1][j]+u[i+1][j+1]+u[i][j+1])/4.0;
				ub=(u[i][j]+u[i+1][j]+u[i+1][j-1]+u[i][j-1])/4.0;
				va=(v[i][j]+v[i][j+1]+v[i+1][j+1]+v[i+1][j])/4.0;
				vb=(v[i][j]+v[i][j+1]+v[i-1][j+1]+v[i-1][j])/4.0;
				r[i][j]=-udiv*udiv-2.0*(ua-ub)*(va-vb)/dx/dy-vdiv*vdiv+1.0/dt*(udiv+vdiv);
			}
		}
		
		//Poisson equation//
		for(k=1;k<=km;k++)
		{
			err=0.;
			//Neumann BC//
			for(j=0;j<yn+1;j++)
			{
				p[0][j]=p[1][j]-1.0/re*2.0*u[2][j];
				p[xn][j]=p[xn-1][j]+1.0/re*2.0*u[xn-1][j];
			}
			
			for(i=0;i<xn+1;i++)
			{
				p[i][0]=p[i][1]-1.0/re*2.0*v[i][2];
				p[i][yn]=p[i][yn-1]+1.0/re*2.0*v[i][yn-1];
			}
			
			//iteration SOR//
			for(i=1;i<xn;i++)
			{
				for(j=1;j<yn;j++)
				{
					pres=C1*(p[i+1][j]+p[i-1][j])+C2*(p[i][j+1]+p[i][j-1])-C3*r[i][j]-p[i][j];
					err+=pres*pres;
					p[i][j]=pres+p[i][j];
				}
			}
			
			if(err<=0.000005) break;
		}
	
		if(l%1000==0) cout<<l<<" "<<err<<" "<<divv<<endl;
		
		//u//
		for(i=2;i<xn;i++)
		{
			for(j=1;j<yn;j++)
			{
				vmid=(v[i][j]+v[i][j+1]+v[i-1][j+1]+v[i-1][j])/4.0;
				uad=u[i][j]*(u[i+1][j]-u[i-1][j])/2.0/dx+vmid*(u[i][j+1]-u[i][j-1])/2.0/dy;
				udif=(u[i+1][j]-2.0*u[i][j]+u[i-1][j])/dx/dx+(u[i][j+1]-2.0*u[i][j]+u[i][j-1])/dy/dy;
				u[i][j]=u[i][j]+dt*(-uad-(p[i][j]-p[i-1][j])/dx+1.0/re*udif);
			}
		}
		
		//v//
		for(i=1;i<xn;i++)
		{
			for(j=2;j<yn;j++)
			{
				umid=(u[i][j]+u[i+1][j]+u[i+1][j-1]+u[i][j-1])/4.0;
				vad=umid*(v[i+1][j]-v[i-1][j])/2.0/dx+v[i][j]*(v[i][j+1]-v[i][j-1])/2.0/dy;
				vdif=(v[i+1][j]-2.0*v[i][j]+v[i-1][j])/dx/dx+(v[i][j+1]-2.0*v[i][j]+v[i][j-1])/dy/dy;
				v[i][j]=v[i][j]+dt*(-vad-(p[i][j]-p[i][j-1])/dy+1.0/re*vdif);
			}
		}
	}

	for(i=1;i<xn;i++)
	{
		for(j=1;j<yn;j++)
		{
			fk<<double((i-0.5)*dx)<<" "<<double((j-0.5)*dy)<<" "<<(u[i][j]+u[i+1][j])/2.0<<" "<<(v[i][j]+v[i][j+1])/2.0<<endl;
			ff<<double((i-0.5)*dx)<<" "<<double((j-0.5)*dy)<<" "<<p[i][j]<<endl;
		}
	}
	
	return 0;
}
