#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

inline void rhspoi(int xn, int yn, double u[], double v[], double &uwall, double &divv, double r[], double &dx, double &dy, double &dt)
{
	int i,j;
	double udiv,vdiv;
	double ua,ub,va,vb;
	
	//BC for left and right//
	for(j=0;j<yn+1;j++)
	{
		u[1*(yn+1)+j]=0.;
		u[0*(yn+1)+j]=u[2*(yn+1)+j];
		v[0*(yn+2)+j]=-v[1*(yn+2)+j];
		
		u[xn*(yn+1)+j]=0.;
		u[(xn+1)*(yn+1)+j]=u[(xn-1)*(yn+1)+j];
		v[xn*(yn+2)+j]=-v[(xn-1)*(yn+2)+j];
	}
	v[0*(yn+2)+yn+1]=-v[1*(yn+2)+yn+1];
	v[xn*(yn+2)+yn+1]=-v[(xn-1)*(yn+2)+yn+1];
	
	//BC for bottom and top//
	for(i=0;i<xn+1;i++)
	{
		v[i*(yn+2)+1]=0.;
		v[i*(yn+2)+0]=v[i*(yn+2)+2];
		u[i*(yn+1)+0]=-u[i*(yn+1)+1];
		
		v[i*(yn+2)+yn]=0.;
		v[i*(yn+2)+yn+1]=v[i*(yn+2)+yn-1];
		u[i*(yn+1)+yn]=2.0*uwall-u[i*(yn+1)+yn-1];//for wall
	}
	u[(xn+1)*(yn+1)+0]=-u[xn*(yn+1)+0];
	u[(xn+1)*(yn+1)+yn]=-u[xn*(yn+1)+yn];
	
	divv=0.;
	//r.h.s. of Poisson equation
	for(i=1;i<xn;i++)
	{
		for(j=1;j<yn;j++)
		{
			udiv=(u[(i+1)*(yn+1)+j]-u[i*(yn+1)+j])/dx;
			vdiv=(v[i*(yn+2)+j+1]-v[i*(yn+2)+j])/dy;
			divv+=fabs(udiv+vdiv);
			ua=(u[i*(yn+1)+j]+u[(i+1)*(yn+1)+j]+u[(i+1)*(yn+1)+j+1]+u[i*(yn+1)+j+1])/4.0;
			ub=(u[i*(yn+1)+j]+u[(i+1)*(yn+1)+j]+u[(i+1)*(yn+1)+j-1]+u[i*(yn+1)+j-1])/4.0;
			va=(v[i*(yn+2)+j]+v[i*(yn+2)+j+1]+v[(i+1)*(yn+2)+j+1]+v[(i+1)*(yn+2)+j])/4.0;
			vb=(v[i*(yn+2)+j]+v[i*(yn+2)+j+1]+v[(i-1)*(yn+2)+j+1]+v[(i-1)*(yn+2)+j])/4.0;
			r[i*(yn+1)+j]=-udiv*udiv-2.0*(ua-ub)*(va-vb)/dx/dy-vdiv*vdiv+1.0/dt*(udiv+vdiv);
		}
	}
}

inline void poi(int &km, int xn, int yn, double p[], double &dx, double &dy, double r[], double &err, double &re, double u[], double v[])
{
	int i,j,k;
	double C1=0.5*dy*dy/(dx*dx+dy*dy);
	double C2=0.5*dx*dx/(dx*dx+dy*dy);
	double C3=0.5*dy*dy/(1.+dy*dy/(dx*dx));
	double pres;
	
	//Poisson equation//
	for(k=1;k<=km;k++)
	{
		err=0.;
		//Neumann BC//
		for(j=0;j<yn+1;j++)
		{
			p[0*(yn+1)+j]=p[1*(yn+1)+j]-1.0/re*2.0*u[2*(yn+1)+j];
			p[xn*(yn+1)+j]=p[(xn-1)*(yn+1)+j]+1.0/re*2.0*u[(xn-1)*(yn+1)+j];
		}
		
		for(i=0;i<xn+1;i++)
		{
			p[i*(yn+1)+0]=p[i*(yn+1)+1]-1.0/re*2.0*v[i*(yn+2)+2];
			p[i*(yn+1)+yn]=p[i*(yn+1)+yn-1]+1.0/re*2.0*v[i*(yn+2)+yn-1];
		}
		
		//iteration//
		for(i=1;i<xn;i++)
		{
			for(j=1;j<yn;j++)
			{
				pres=C1*(p[(i+1)*(yn+1)+j]+p[(i-1)*(yn+1)+j])+C2*(p[i*(yn+1)+j+1]+p[i*(yn+1)+j-1])-C3*r[i*(yn+1)+j]-p[i*(yn+1)+j];
				err+=pres*pres;
				p[i*(yn+1)+j]=pres+p[i*(yn+1)+j];
			}
		}
		
		if(err<=0.000005) break;
	}
}

inline void vel(int xn, int yn, double u[], double v[], double &dx, double &dy, double &dt, double p[], double &re)
{
	int i,j;
	double uad,vad;
	double udif,vdif;
	double umid,vmid;
	
	//u//
	for(i=2;i<xn;i++)
	{
		for(j=1;j<yn;j++)
		{
			vmid=(v[i*(yn+2)+j]+v[i*(yn+2)+j+1]+v[(i-1)*(yn+2)+j+1]+v[(i-1)*(yn+2)+j])/4.0;
			uad=u[i*(yn+1)+j]*(u[(i+1)*(yn+1)+j]-u[(i-1)*(yn+1)+j])/2.0/dx+vmid*(u[i*(yn+1)+j+1]-u[i*(yn+1)+j-1])/2.0/dy;
			udif=(u[(i+1)*(yn+1)+j]-2.0*u[i*(yn+1)+j]+u[(i-1)*(yn+1)+j])/dx/dx+(u[i*(yn+1)+j+1]-2.0*u[i*(yn+1)+j]+u[i*(yn+1)+j-1])/dy/dy;
			u[i*(yn+1)+j]=u[i*(yn+1)+j]+dt*(-uad-(p[i*(yn+1)+j]-p[(i-1)*(yn+1)+j])/dx+1.0/re*udif);
		}
	}
	
	//v//
	for(i=1;i<xn;i++)
	{
		for(j=2;j<yn;j++)
		{
			umid=(u[i*(yn+1)+j]+u[(i+1)*(yn+1)+j]+u[(i+1)*(yn+1)+j-1]+u[i*(yn+1)+j-1])/4.0;
			vad=umid*(v[(i+1)*(yn+2)+j]-v[(i-1)*(yn+2)+j])/2.0/dx+v[i*(yn+2)+j]*(v[i*(yn+2)+j+1]-v[i*(yn+2)+j-1])/2.0/dy;
			vdif=(v[(i+1)*(yn+2)+j]-2.0*v[i*(yn+2)+j]+v[(i-1)*(yn+2)+j])/dx/dx+(v[i*(yn+2)+j+1]-2.0*v[i*(yn+2)+j]+v[i*(yn+2)+j-1])/dy/dy;
			v[i*(yn+2)+j]=v[i*(yn+2)+j]+dt*(-vad-(p[i*(yn+1)+j]-p[i*(yn+1)+j-1])/dy+1.0/re*vdif);
		}
	}
}

int main()
{	
	const int xn=6;
	const int yn=6;
	int i,j,l;
	double divv;
	double err;
	double *u=new double[(xn+2)*(yn+1)];
	double *v=new double[(xn+1)*(yn+2)];
	double *p=new double[(xn+1)*(yn+1)];
	double *r=new double[(xn+1)*(yn+1)];//for r.h.s. of Poisson equation
	double uwall=1.;
	double dx=1./(double)(xn-1);
	double dy=1./(double)(yn-1);
	double dt=0.001;
	double re=100;
	int lm=20000;
	int km=100;

	ofstream fk,ff;
	fk.open("vel.txt");
	ff.open("pre.txt");

	//initialization//
	for(i=0;i<xn+1;i++)
	{
		for(j=0;j<yn+1;j++)
		{
			p[i*(yn+1)+j]=0.;
		}
	}
	
	for(i=0;i<xn+2;i++)
	{
		for(j=0;j<yn+1;j++)
		{
			u[i*(yn+1)+j]=0.;
		}
	}
	
	for(i=0;i<xn+1;i++)
	{
		for(j=0;j<yn+2;j++)
		{
			v[i*(yn+2)+j]=0.;
		}
	}
	
	//time step//
	for(l=1;l<=lm;l++)
	{
		rhspoi(xn,yn,u,v,uwall,divv,r,dx,dy,dt);
		poi(km,xn,yn,p,dx,dy,r,err,re,u,v);
	
		if(l%1000==0) cout<<l<<" "<<err<<" "<<divv<<endl;
		
		vel(xn,yn,u,v,dx,dy,dt,p,re);
	}
	
	//output
	ofstream fout("pressure.vtk");
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << (xn-1)*(yn-1) << " " << "float" << endl;
    for(int i=1;i<xn;i++){
        for(int j=1;j<yn;j++){
            fout << double((i-0.5)*dx) << " "<< double((j-0.5)*dy) << " " << double(0) << endl;
        }
    }
    fout << "CELLS" << " " << (xn-1)*(yn-1) << " " << (xn-1)*(yn-1)*2 << endl;
    for(int i=0;i<(xn-1)*(yn-1);i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << (xn-1)*(yn-1) << endl;
    for(int i=0;i<(xn-1)*(yn-1);i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << (xn-1)*(yn-1) << endl;
    fout << "SCALARS p float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(i=1;i<xn;i++)
	{
		for(j=1;j<yn;j++)
		{
            fout << p[i*(yn+1)+j] << endl;
		}
	}
    fout.close();

    // ofstream fout("pressure.vtk");
    // if(fout.fail()){
    //     cout << "出力ファイルをオープンできません" << endl;
    //     return 1;
    // }
    // fout << "# vtk DataFile Version 2.0" << endl;
    // fout << "vtk output" << endl;
    // fout << "ASCII" << endl;
    // fout << "DATASET UNSTRUCTURED_GRID" << endl;
    // fout << "POINTS" << " " << (xn+2)*(yn+2) << " " << "float" << endl;
    // for(int i=0;i<xn+2;i++){
    //     for(int j=0;j<yn+2;j++){
    //         fout << double((i-0.5)*dx) << " "<< double((j-0.5)*dy) << " " << double(0) << endl;
    //     }
    // }
    // fout << "CELLS" << " " << (xn+2)*(yn+2) << " " << (xn+2)*(yn+2)*2 << endl;
    // for(int i=0;i<(xn+2)*(yn+2);i++){
    //     fout << 1 << " " << i << endl;
    // }
    // fout << "CELL_TYPES" << " " << (xn+2)*(yn+2) << endl;
    // for(int i=0;i<(xn+2)*(yn+2);i++){
    //     fout << 1 << endl;
    // }
    // fout << "POINT_DATA" << " " << (xn+2)*(yn+2) << endl;
    // fout << "SCALARS p float" << endl;
    // fout << "LOOKUP_TABLE default" << endl;
    // for(i=0;i<xn+2;i++)
	// {
	// 	for(j=0;j<yn+2;j++)
	// 	{
    //         fout << p[i*(yn+1)+j] << endl;
	// 	}
	// }
    // fout.close();

    ofstream fout2("vector.vtk");
    if(fout2.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    fout2 << "# vtk DataFile Version 2.0" << endl;
    fout2 << "vtk output" << endl;
    fout2 << "ASCII" << endl;
    fout2 << "DATASET UNSTRUCTURED_GRID" << endl;
    fout2 << "POINTS" << " " << (xn-1)*(yn-1) << " " << "float" << endl;
    for(int i=1;i<xn;i++){
        for(int j=1;j<yn;j++){
            fout2 << double((i-0.5)*dx) << " "<< double((j-0.5)*dy) << " " << double(0) << endl;
        }
    }
    fout2 << "POINT_DATA" << " " << (xn-1)*(yn-1) << endl;
    fout2 << "VECTORS velocity float" << endl;
    for(i=1;i<xn;i++)
	{
		for(j=1;j<yn;j++)
		{
            fout2 << (u[i*(yn+1)+j]+u[(i+1)*(yn+1)+j])/2.0 << " " << (v[i*(yn+2)+j]+v[i*(yn+2)+j+1])/2.0 << " " << double(0) << endl;
		}
	}
    fout.close(); 
	
	delete[] u,v,p,r;
	
	return 0;
}