#include<stdio.h>
#include<stdlib.h>
#include<math.h>

const int N_X=31;
const int N_Y=31;
const double L_X=1.0;
const double L_Y=1.0;
const double V[2]={10.0,5.0};
const double ALPHA=1.0;

const double DT=0.01;
const int N_STEP=100;

typedef struct
{
    double** x;
    double* u;

    double v[2];
    double alpha;

    int n_x;
    int n_y;
    double l_x;
    double l_y;
    double dx;
    double dy;

} CALC_POINTS;

typedef struct
{
    double** mat;
    double* rhs;
} LIN_SYS;


void memory_allocartion(
    CALC_POINTS* cp,
    LIN_SYS*     ls,
    int          dof)
{
    cp->x=(double**)calloc(dof,sizeof(double*));
    for(int i=0;i<dof;i++){
        cp->x[i]=(double*)calloc(2,sizeof(double));
    }

    cp->u=(double*)calloc(dof,sizeof(double));

    ls->mat=(double**)calloc(dof,sizeof(double*));
    for(int i=0;i<dof;i++){
        ls->mat[i]=(double*)calloc(dof,sizeof(double));
    }

    ls->rhs=(double*)calloc(dof,sizeof(double));
}

void memory_free(
    CALC_POINTS* cp,
    LIN_SYS*     ls,
    int          dof)
{

    for(int i=0;i<dof;i++){
        free(cp->x[i]);
    }
    free(cp->x);

    free(cp->u);

    for(int i=0;i<dof;i++){
        free(ls->mat[i]);
    }
    free(ls->mat);

    free(ls->rhs);
}

int set_index(
    int i,
    int j,
    int n_x)
{
    return (n_x*j+i);
}

void set_calc_points(
    CALC_POINTS*   cp)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;

    cp->dx=l_x/(n_x-1.0);
    cp->dy=l_y/(n_y-1.0);

    for(int j=0;j<n_y;j++){
        for(int i=0;i<n_x;i++){
            int k=set_index(i,j,n_x);
            cp->x[k][0]=cp->dx*i;
            cp->x[k][1]=cp->dy*j;
        }
    }
}

void set_matrix(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    double         dt)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double dx=cp->dx;
    double dy=cp->dy;
    double v_x=cp->v[0];
    double v_y=cp->v[1];
    double alpha=cp->alpha;

    for(int j=1;j<n_y-1;j++){
        for(int i=1;i<n_x-1;i++){
            int k=set_index(i,j,n_x);

            ls->mat[k][k-n_x]=-(v_y/dy+alpha/(dy*dy))*dt;
            ls->mat[k][k-1  ]=-(v_x/dx+alpha/(dx*dx))*dt;
            ls->mat[k][k    ]=1.0+(v_x/dx+v_y/dy+2.0*alpha/(dx*dx)+2.0*alpha/(dy*dy))*dt;
            ls->mat[k][k+1  ]=-alpha/(dx*dx)*dt;
            ls->mat[k][k+n_x]=-alpha/(dy*dy)*dt;

            ls->rhs[k]=cp->u[k]+1.0*dt;
        }
    }
}

void set_bc(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;

    for(int j=0;j<n_y;j++){
        for(int i=0;i<n_x;i++){
            int k=set_index(i,j,n_x);
            
            if(i==0 || i==n_x-1 || j==0 || j==n_y-1){
                for(int l=0;l<n_x*n_y;l++){
                    ls->mat[k][l]=0.0;
                    ls->mat[l][k]=0.0;
                }
                ls->mat[k][k]=1.0;
                ls->rhs[k]   =0.0;
            }
        }
    }
}

static const int MAX_ITERATIONS=10000;
static const double EPSIRON=0.0000001;
void solve_mat_GS(
    double* sol,
    double** mat,
    double* rhs,
    int length)
{
    for(int l=0;l<MAX_ITERATIONS;l++){
        for(int i=0;i<length;i++){
            sol[i]=rhs[i];
            for(int j=0;j<length;j++){
                if(i != j){
                    sol[i] -= mat[i][j]*sol[j];
                }
            }      
            sol[i] /= mat[i][i];      
        }

        double residual=0.0;
        for(int i=0;i<length;i++){
            double r_i = -rhs[i];
            for(int j=0;j<length;j++){
                r_i += mat[i][j]*sol[j];
            }
            residual += r_i*r_i;
        }
        residual=sqrt(residual);
        printf("GS_loop %d: %e\n", l, residual);
        if(residual < EPSIRON){
            return;
        }
    }
}

void write_vtk(
    CALC_POINTS* cp,
    int file_num)
{
    FILE* fp;
    char filename[10000];
    snprintf(filename,10000,"./data/out_%06d.vtk",file_num);
    fp=fopen(filename,"w");

    fprintf(fp,"# vtk DataFile Version 2.0\n");
    fprintf(fp,"vtk output\n");
    fprintf(fp,"ASCII\n");
    fprintf(fp,"DATASET UNSTRUCTURED_GRID\n");

    int num=cp->n_x*cp->n_y;
    fprintf(fp,"POINTS %d float\n",num);
    for(int i=0;i<num;i++){
        fprintf(fp,"%e %e %e\n",cp->x[i][0],cp->x[i][1],0.0);
    }

    fprintf(fp,"CELLS %d %d\n",num,2*num);
    for(int i=0;i<num;i++){
        fprintf(fp,"%d %d\n",1,i);
    }

    fprintf(fp,"CELL_TYPES %d\n",num);
    for(int i=0;i<num;i++){
        fprintf(fp,"%d\n",1);
    }

    fprintf(fp,"POINT_DATA %d\n",num);
    fprintf(fp,"SCALARS u float\n");
    fprintf(fp,"LOOKUP_TABLE default\n");
    for(int i=0;i<num;i++){
        fprintf(fp,"%e\n",cp->u[i]);
    }

    fclose(fp);
}


int main(
    int argc,//コマンドライン引数
    char* argv[])
{
    CALC_POINTS cp;
    LIN_SYS     ls;

    //ファイル入力関数の実装を予定
    cp.n_x= N_X;
    cp.n_y= N_Y;
    cp.l_x= L_X;
    cp.l_y= L_Y;
    cp.v[0]=V[0];
    cp.v[0]=V[1];
    cp.alpha=ALPHA;

    memory_allocartion(&cp,&ls,N_X*N_Y);

    set_calc_points(&cp);

    for(int n=0;n<N_STEP;n++){
        printf("----%d STEP -----\n",n);
        set_matrix(&cp,&ls,DT);
        set_bc(&cp,&ls);
        solve_mat_GS(cp.u,ls.mat,ls.rhs,N_X*N_Y);
        write_vtk(&cp,n);
        printf("\n");
    }

    memory_free(&cp,&ls,N_X*N_Y);


    return 0;
}