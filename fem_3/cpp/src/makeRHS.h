#ifndef __INCLUDE_MAKERHS_H__
#define __INCLUDE_MAKERHS_H__
#include"struct.h"

void makeRHS(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
