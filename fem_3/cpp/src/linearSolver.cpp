#include<iostream>
#include<iomanip>
#include"linearSolver.h"
#include"bound0.h"
#include"matvec.h"

void GPBiCG(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start GPBiCG" << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        cp->u[i] = cp->u[i] / ls->diag_x[i];
        cp->v[i] = cp->v[i] / ls->diag_y[i];
        cp->p[i] = cp->p[i] / ls->diag_c[i];
        ls->w01_u[i] = ls->bv_x[i] * ls->diag_x[i];
        ls->w01_v[i] = ls->bv_y[i] * ls->diag_y[i];
        ls->w01_p[i] = ls->bv_c[i] * ls->diag_c[i];
    }

    // // std::cout << std::fixed << std::setprecision(9);
    // // for (int i = 0; i < cp->number_of_nodes; i++) {
    // //     std::cout << i << " " << cp->u[i] << "\n";
    // //     if (i == 20) {
    // //         break;
    // //     }
    // // }

    // zero clear on dirichlet boundary
    bound0(cp, ls, cc);

    // // std::cout << std::fixed << std::setprecision(9);
    // // for (int i = 0; i < cp->number_of_nodes; i++) {
    // //     std::cout << i << " " << cp->u[i] << "\n";
    // //     if (i == 20) {
    // //         break;
    // //     }
    // // }

    matvec(cp, ls, cc, cp->number_of_nodes, cp->u, cp->v, cp->p, ls->w09_x, ls->w09_y, ls->w09_c, cp->iubc_max, cp->iubc, cp->nubc);


    std::cout << "finish GPBiCG" << "\n";
    // exit(1);
}
