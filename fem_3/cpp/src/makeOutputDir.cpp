#include<iostream>
#include<string>
#include<sys/stat.h>
#include"makeOutputDir.h"

void makeOutputDir() {
    std::string folder_name = ("../output");
    const char* path = "../output";
    struct stat statBuf;
    if (stat(path, &statBuf) == 0) {
        printf("directory already exist.\n");
    } else {
        printf("directory not exist, so create output dir.\n");
        if (mkdir(folder_name.c_str(), 0777) == 0) {
            printf("success create output dir in the upper hierarchy.\n");
        } else {
            printf("not success create output dir in the upper hierarchy.\n");
        }
    }
}
