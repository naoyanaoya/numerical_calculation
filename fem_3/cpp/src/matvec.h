#ifndef __INCLUDE_MATVEC_H__
#define __INCLUDE_MATVEC_H__
#include"struct.h"

void matvec(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc,
    int NN,
    double *a,
    double *b,
    double *c,
    double *r_x,
    double *r_y,
    double *r_c,
    int iubc_max,
    int* iubc,
    int** nubc
);

#endif
