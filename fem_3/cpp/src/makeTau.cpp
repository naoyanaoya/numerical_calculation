#include<iostream>
#include<iomanip>
#include<math.h>
#include"makeTau.h"

void makeTau(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start makeTau" << "\n";

    int n1 = 0;
    int n2 = 0;
    int n3 = 0;
    double area = 0;
    double b1 = 0;
    double b2 = 0;
    double b3 = 0;
    double c1 = 0;
    double c2 = 0;
    double c3 = 0;
    double u = 0;
    double v = 0;
    double uv = 0;
    double d = 0;
    double t1 = 4.0 * cc->dt_inverse * cc->dt_inverse;
    double t2 = 0;
    double t3 = 0;
    for (int i = 0; i < cp->number_of_elements; i++) {
        n1 = ls->npe[i][0];
        n2 = ls->npe[i][1];
        n3 = ls->npe[i][2];
        area = ls->area_array[i];
        b1 = ls->b_array[3 * i + 0];
        b2 = ls->b_array[3 * i + 1];
        b3 = ls->b_array[3 * i + 2];
        c1 = ls->c_array[3 * i + 0];
        c2 = ls->c_array[3 * i + 1];
        c3 = ls->c_array[3 * i + 2];
        u = (cp->ua[n1] + cp->ua[n2] + cp->ua[n3]) / 3.0;
        v = (cp->va[n1] + cp->va[n2] + cp->va[n3]) / 3.0;
        uv = u * u + v * v;
        d = fabs(u * b1 + v * c1) + fabs(u * b2 + v * c2) + fabs(u * b3 + v * c3);
        t2 = d * d;
        if (uv < 1e-10) {
            t3 = (2.0 * cc->Re_inverse / area) * (2.0 * cc->Re_inverse / area);
        } else {
            t3 = (cc->Re_inverse * d * d / uv) * (cc->Re_inverse * d * d / uv);
        }
        ls->tau[i] = 1.0 / sqrt(t1 + t2 + t3);
    }

    std::cout << "finish makeTau" << "\n";
}
