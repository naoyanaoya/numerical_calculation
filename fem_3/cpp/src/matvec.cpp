#include<iostream>
#include<iomanip>
#include<cmath>
#include"matvec.h"
#include"bound0.h"

void matvec(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc,
    int NN,
    double *a,
    double *b,
    double *c,
    double *r_x,
    double *r_y,
    double *r_c,
    int iubc_max,
    int* iubc,
    int** nubc
)
{
    std::cout << "- start matvec" << "\n";

    for (int i = 0; i < NN * 3; i++) {
        r_x[i] = 0.0;
        r_y[i] = 0.0;
        r_c[i] = 0.0;
    }

    // for (int i = 0; i < NN; i++) {
    //     for (int j = 0; j < ls->nrow_max; j++) {
    //         std::cout << i << " " << j << " " << a[ls->jcoef[i][j]] << "\n";
    //     }
    //     if (i == 10) {
    //         exit(1);
    //     }
    // }

    for (int i = 0; i < NN; i++) {
        for (int j = 0; j < ls->nrow_max; j++) {
            ls->wkg_row[j + ls->nrow_max * 0] = a[ls->jcoef[i][j]];
            ls->wkg_row[j + ls->nrow_max * 1] = b[ls->jcoef[i][j]];
            ls->wkg_row[j + ls->nrow_max * 2] = c[ls->jcoef[i][j]];
        }
        for (int j = 0; j < ls->nrow_max * 3; j++) {
            r_x[i] = r_x[i] + ls->coef_x[i][j] * ls->wkg_row[j];
            r_y[i] = r_y[i] + ls->coef_y[i][j] * ls->wkg_row[j];
            r_c[i] = r_c[i] + ls->coef_c[i][j] * ls->wkg_row[j];
        }
    }

    double a_1 = 0;
    double a_2 = 0;
    double a_3 = 0;
    for (int i = 0; i < NN; i++) {
        a_1 += r_x[i];
        a_2 += r_y[i];
        a_3 += r_c[i];
    }
    std::cout << a_1 << " " << a_2 << " " << a_3 << "\n";


    bound0(cp, ls, cc);


    std::cout << "- finish matvec" << "\n";
}
