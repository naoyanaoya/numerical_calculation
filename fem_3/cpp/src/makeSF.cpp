#include<iostream>
#include<iomanip>
#include"makeSF.h"

void makeSF(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start makeSF" << "\n";

    int n1 = 0;
    int n2 = 0;
    int n3 = 0;
    double x1 = 0;
    double x2 = 0;
    double x3 = 0;
    double y1 = 0;
    double y2 = 0;
    double y3 = 0;
    double det = 0;
    double det_inverse = 0;
    double area = 0;

    for (int i = 0; i < cp->number_of_elements; i++) {
        n1 = ls->npe[i][0];
        n2 = ls->npe[i][1];
        n3 = ls->npe[i][2];
        x1 = cp->x[n1];
        x2 = cp->x[n2];
        x3 = cp->x[n3];
        y1 = cp->y[n1];
        y2 = cp->y[n2];
        y3 = cp->y[n3];
        std::cout << std::fixed << std::setprecision(3);
        // std::cout << n1 << " " << n2 << " " << n3 << "\n";
        // std::cout << x1 << " " << x2 << " " << x3 << "\n";
        // std::cout << y1 << " " << y2 << " " << y3 << "\n";
        det = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2);
        area = 0.5 * det;
        if (det <= 0.0) {
            printf("area failure\n");
            exit(1);
        }
        ls->area_array[i] = area;
        det_inverse = 1.0 / det;
        // b_array、c_arrayは形状関数
        ls->b_array[3 * i + 0] = (y2 - y3) * det_inverse;
        ls->b_array[3 * i + 1] = (y3 - y1) * det_inverse;
        ls->b_array[3 * i + 2] = (y1 - y2) * det_inverse;
        ls->c_array[3 * i + 0] = (x3 - x2) * det_inverse;
        ls->c_array[3 * i + 1] = (x1 - x3) * det_inverse;
        ls->c_array[3 * i + 2] = (x2 - x1) * det_inverse;
        // std::cout << ls->b_array[3 * i + 0] << " " << ls->b_array[3 * i + 1] << " " << ls->b_array[3 * i + 2] << "\n";
        // std::cout << ls->c_array[3 * i + 0] << " " << ls->c_array[3 * i + 1] << " " << ls->c_array[3 * i + 2] << "\n";
    }

    std::cout << "finish makeSF" << "\n";
}
