#include<iostream>
#include<iomanip>
#include"bound0.h"

void bound0(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "- start bound0" << "\n";

    // TODO: これuが1.0になるべきところも0.0にしていいの？
    // 実際に0にしてるのはu,v,pではなく、w01という別のGPBi-CG法でのワーキングベクトルのひとつ
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < cp->iubc[i]; j++) {
            if (i == 0) { // uについて
                // std::cout << i << " " << j << " " << cp->nubc[j][i] << "\n";
                ls->w01_u[cp->nubc[j][i]] = 0.0;
            } else if (i == 1) { // vについて
                // std::cout << i << " " << j << " " << cp->nubc[j][i] << "\n";
                ls->w01_v[cp->nubc[j][i]] = 0.0;
            }
        }
    }

    // for (int i = 0; i < cp->number_of_nodes; i++) {
    //     std::cout << i << " " << ls->w01_u[i] << "\n";
    //     if (i == 123) {
    //         exit(1);
    //     }
    // }

    std::cout << "- finish bound0" << "\n";
}
