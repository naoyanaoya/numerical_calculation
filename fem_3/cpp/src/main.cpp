#include<iostream>
#include<string>
#include<sstream>
#include<fstream>
#include<vector>
#include<iomanip>
#include"struct.h"
#include"readFile.h"
#include"makeNet.h"
#include"memoryFree.h"
#include"makeOutputDir.h"
#include"writeVTK.h"
#include"initialCondition.h"
#include"makeSF.h"
#include"makeTau.h"
#include"makeLHS.h"
#include"makeRHS.h"
#include"linearSolver.h"

/* memo */
// fortran
// intent(in) 入力引数 関数内で変更が起きない値に対し
// intent(out) 出力引数 関数内で変更が起きてもいい
// intent(inout) 入出力関数


int main() {
    CALC_POINTS cp;
    LIN_SYS ls;
    CAL_COND cc;

    cp.number_of_nodes_per_element = 3;

    makeOutputDir();

    readFile(&cp, &ls, &cc);

    makeNet(&cp, &ls, &cc);

    initialCondition(&cp, &ls, &cc);

    makeSF(&cp, &ls, &cc);

    int istep = 0;

    write_vtk(&cp, &ls, istep);

    // for (int istep = 1; istep <= cc.iend; istep++) {
        std::cout << "-------------- " << istep << " ---------------" << "\n";
        makeTau(&cp, &ls, &cc);
        makeLHS(&cp, &ls, &cc);
        makeRHS(&cp, &ls, &cc);
        GPBiCG(&cp, &ls, &cc);
    // }

    memory_free(&cp, &ls);

    std::cout << "main finished" << "\n";

    system("leaks main");

    return 0;
}
