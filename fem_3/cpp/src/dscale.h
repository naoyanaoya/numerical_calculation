#ifndef __INCLUDE_DSCALE_H__
#define __INCLUDE_DSCALE_H__
#include"struct.h"

void dscale(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
