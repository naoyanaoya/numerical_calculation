#ifndef __INCLUDE_MAKETAU_H__
#define __INCLUDE_MAKETAU_H__
#include"struct.h"

void makeTau(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif