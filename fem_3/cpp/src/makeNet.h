#ifndef __INCLUDE_MAKENET_H__
#define __INCLUDE_MAKENET_H__
#include"struct.h"

void makeNet(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif