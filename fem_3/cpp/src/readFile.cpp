#include<iostream>
#include<fstream>
#include<vector>
#include"readFile.h"
#include"memoryAllocation.h"

void readFile(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start readFile" << "\n";

    std::string inpfile;
    std::string mesfile;
    std::string bdcfile;
    std::string stafile;
    std::string endfile;
    std::ifstream file_in1("./file.dat");
    if (file_in1.fail()) {
        std::cout << "can not open the ./file.data" << "\n";
        exit(1);
    }
    std::vector<std::string> fileList(6);
    for (int i = 0; i < 6; i++) {
        file_in1 >> fileList[i];
    }
    inpfile = fileList[0];
    mesfile = fileList[1];
    bdcfile = fileList[2];
    stafile = fileList[3];
    endfile = fileList[5];
    // std::cout << inpfile << std::endl;
    file_in1.close();

    std::ifstream file_in2(inpfile);
    if (file_in2.fail()) {
        std::cout << "can not open the " << inpfile << "\n";
        exit(1);
    }
    file_in2 >> cc->ista >> cc->iend >> cc->iout;
    file_in2 >> cc->dt >> cc->Re;
    cc->dt_inverse = 1.0 / cc->dt;
    cc->Re_inverse = 1.0 / cc->Re;
    // std::cout << cc->ista << " " << cc->iend << " " << cc->iout << "\n";
    // std::cout << cc->dt << " " << cc->Re << "\n";
    file_in2.close();

    std::ifstream file_in3(mesfile);
    if (file_in3.fail()) {
        std::cout << "can not open the " << mesfile << "\n";
        exit(1);
    }
    file_in3 >> cp->number_of_nodes >> cp->number_of_elements;
    // std::cout << cp->number_of_nodes << " " << cp->number_of_elements << "\n";
    file_in3.close();

    memory_allocation(cp, ls);

    std::ifstream file_in4(mesfile);
    if (file_in4.fail()) {
        std::cout << "can not open the " << mesfile << "\n";
        exit(1);
    } else {
        // std::cout << "can open the " << mesfile << "\n";
    }
    int tmp = 0;
    file_in4 >> cp->number_of_nodes >> cp->number_of_elements;
    for (int i = 0; i < cp->number_of_nodes; i++) {
        file_in4 >> tmp >> cp->x[i] >> cp->y[i];
    }
    for (int i = 0; i < cp->number_of_elements; i++) {
        file_in4 >> tmp >> ls->npe[i][0] >> ls->npe[i][1] >> ls->npe[i][2];
        ls->npe[i][0] -= 1;
        ls->npe[i][1] -= 1;
        ls->npe[i][2] -= 1;
    }
    file_in4.close();

    std::ifstream file_in5(bdcfile);
    if (file_in5.fail()) {
        std::cout << "can not open the " << bdcfile << "\n";
    }
    file_in5 >> cp->iubc[0] >> cp->iubc[1];
    // std::cout << cp->iubc[0] << " " << cp->iubc[1] << "\n";

    cp->iubc_max = std::max(cp->iubc[0], cp->iubc[1]);

    // TODO:: ファイルの境界条件の記述の順番的にnubcは[境界条件の最大個数][2]の順番になる
    cp->nubc = (int**)calloc(cp->iubc_max, sizeof(int*));
    for (int i = 0; i < cp->iubc_max; i++) {
        cp->nubc[i] = (int*)calloc(2, sizeof(int));
    }
    cp->fubc = (double**)calloc(cp->iubc_max, sizeof(double*));
    for (int i = 0; i < cp->iubc_max; i++) {
        cp->fubc[i] = (double*)calloc(2, sizeof(double));
    }
    for (int i = 0; i < 2; i++) {
        int tmp1 = 0;
        for (int j = 0; j < cp->iubc[i]; j++) {
            file_in5 >> tmp1 >> cp->nubc[j][i] >> cp->fubc[j][i];
            cp->nubc[j][i] -= 1;
        }
    }
    file_in5.close();

    std::cout << "finish readFile" << "\n";
}
