#ifndef __INCLUDE_MAKESF_H__
#define __INCLUDE_MAKESF_H__
#include"struct.h"

void makeSF(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif