#ifndef __INCLUDE_MAKELHS_H__
#define __INCLUDE_MAKELHS_H__
#include"struct.h"

void makeLHS(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif