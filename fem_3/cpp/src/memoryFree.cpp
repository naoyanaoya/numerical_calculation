#include<iostream>
#include"memoryFree.h"

void memory_free(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    std::cout << "start memoryFree" << "\n";

    free(cp->x);
    free(cp->y);
    free(cp->u);
    free(cp->v);
    free(cp->p);
    free(cp->uu);
    free(cp->vu);
    free(cp->ua);
    free(cp->va);
    free(cp->iubc);

    for (int i = 0; i < cp->iubc_max; i++) {
        free(cp->nubc[i]);
    }
    free(cp->nubc);

    for (int i = 0; i < cp->iubc_max; i++) {
        free(cp->fubc[i]);
    }
    free(cp->fubc);

    for (int i = 0; i < cp->number_of_elements; i++){
        free(ls->npe[i]);
    }
    free(ls->npe);
    free(ls->infn);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->infe[i]);
    }
    free(ls->infe);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->infc[i]);
    }
    free(ls->infc);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->infnc_a[i]);
    }
    free(ls->infnc_a);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->infnc_b[i]);
    }
    free(ls->infnc_b);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->jcoef[i]);
    }
    free(ls->jcoef);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->jcoef_before[i]);
    }
    free(ls->jcoef_before);

    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->coef_x[i]);
    }
    free(ls->coef_x);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->coef_y[i]);
    }
    free(ls->coef_y);
    for (int i = 0; i < cp->number_of_nodes; i++) {
        free(ls->coef_c[i]);
    }
    free(ls->coef_c);

    free(ls->wkg_row);

    free(ls->w01_u);
    free(ls->w01_v);
    free(ls->w01_p);
    free(ls->w02_u);
    free(ls->w02_v);
    free(ls->w02_p);
    free(ls->w09_x);
    free(ls->w09_y);
    free(ls->w09_c);

    for (int i = 0; i < 3; i++) {
        std::cout << i << "\n";
        free(ls->nltable[i]);
    }
    free(ls->nltable);

    free(ls->diag_x);
    free(ls->diag_y);
    free(ls->diag_c);

    free(ls->bv_x);
    free(ls->bv_y);
    free(ls->bv_c);

    free(ls->area_array);
    free(ls->b_array);
    free(ls->c_array);
    free(ls->tau);

    std::cout << "finish memoryFree" << "\n";
}
