#include <algorithm>
#include <iomanip>
#include <iostream>
#include "makeNet.h"


// infn 節点nの周辺要素総数
// infnn ?
// infn_max infnの最大値 今回は6個だと思う
// infe 節点nの周辺要素の番号m
// infc 節点nの周辺要素mにおけるnの要素内節点番号
// infnc 左辺圧縮行列の非ゼロ成分位置
// jcoef 左辺非ゼロ成分の全体系節点番号

void makeNet(CALC_POINTS* cp, LIN_SYS* ls, CAL_COND* cc) {
    std::cout << "start makeNet" << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infn[i] = 0;
    }
    for (int i = 0; i < cp->number_of_elements; i++) {
        for (int j = 0; j < 3; j++) {
            ls->infn[ls->npe[i][j]] = ls->infn[ls->npe[i][j]] + 1;
        }
    }
    // この上の計算は一旦infn_maxを求めるために必要
    int tmp = 0;
    for (int i = 0; i < cp->number_of_nodes; i++) {
        tmp = std::max(ls->infn[i], tmp);
    }
    ls->infn_max = tmp;
    ls->infe = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infe[i] = (int*)calloc(ls->infn_max, sizeof(int));
    }
    if (ls->infe == NULL) {
        printf("Cat not allocate infe memory.\n");
    }
    ls->infc = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infc[i] = (int*)calloc(ls->infn_max, sizeof(int));
    }
    if (ls->infc == NULL) {
        printf("Cat not allocate infc memory.\n");
    }
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infn[i] = 0;
    }
    for (int i = 0; i < cp->number_of_nodes; i++) {
        for (int j = 0; j < 3; j++) {
            ls->infe[i][j] = 0;
            ls->infc[i][j] = 0;
        }
    }

    for (int i = 0; i < cp->number_of_elements; i++) {
        // std::cout << "========================" << std::endl;
        for (int j = 0; j < 3; j++) {
            // ここは要素の処理をしてから要素数を足すべき？
            // fortranは1-indexだから先に処理しても問題なかった？
            // ls->infn[ls->npe[i][j]] = ls->infn[ls->npe[i][j]] + 1;
            ls->infe[ls->npe[i][j]][ls->infn[ls->npe[i][j]]] = i;
            ls->infc[ls->npe[i][j]][ls->infn[ls->npe[i][j]]] = j;
            ls->infn[ls->npe[i][j]] = ls->infn[ls->npe[i][j]] + 1;
            // std::cout << ls->infn[ls->npe[i][j]] << " " << ls->infe[ls->npe[i][j]][ls->infn[ls->npe[i][j]]] << " " << ls->infc[ls->npe[i][j]][ls->infn[ls->npe[i][j]]] << std::endl;
        }
    }

    // ここまでで完了していなければならない
    // std::cout << "nltable = " << ls->nltable[0][0] << "\n";

    ls->nltable[0][0] = 1;
    ls->nltable[0][1] = 2;
    ls->nltable[1][0] = 2;
    ls->nltable[1][1] = 0;
    ls->nltable[2][0] = 0;
    ls->nltable[2][1] = 1;

    ls->nrow_max = 1;
    int infnn_max = ls->infn_max + 2;

    ls->infnc_a = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infnc_a[i] = (int*)calloc(ls->infn_max, sizeof(int));
    }
    if (ls->infnc_a == NULL) {
        printf("Cat not allocate infnc_a memory.\n");
    }

    ls->infnc_b = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->infnc_b[i] = (int*)calloc(ls->infn_max, sizeof(int));
    }
    if (ls->infnc_b == NULL) {
        printf("Cat not allocate infnc_b memory.\n");
    }

    ls->jcoef = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->jcoef[i] = (int*)calloc(infnn_max, sizeof(int));
    }
    if (ls->jcoef == NULL) {
        printf("Cat not allocate jcoef memory.\n");
    }

    ls->jcoef_before = (int**)calloc(cp->number_of_nodes, sizeof(int*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->jcoef_before[i] = (int*)calloc(infnn_max, sizeof(int));
    }
    if (ls->jcoef_before == NULL) {
        printf("Cat not allocate jcoef_before memory.\n");
    }

    int i1_counter = 0;
    int i2_counter = 0;
    for (int i = 0; i < cp->number_of_nodes; i++) {
        // printf("----------------- %d -----------------\n", i);
        int infnn = 1;
        ls->jcoef[i][0] = i;
        // printf("infn[i] = %d\n", ls->infn[i]);
        for (int j = 0; j < ls->infn[i]; j++) {
            // printf("-------- %d --------\n", j);
            int ne = ls->infe[i][j];
            int nt = ls->infc[i][j];
            // printf("ne = %d, nt = %d\n", ne, nt);
            // int n1 = ls->npe[ls->nltable[nt][0]][ne];
            // int n2 = ls->npe[ls->nltable[nt][1]][ne];
            int n1 = ls->npe[ne][ls->nltable[nt][0]];
            int n2 = ls->npe[ne][ls->nltable[nt][1]];
            int i1 = 0;
            int i2 = 0;
            for (int k = 0; k < j; k++) {
                // printf("---- %d ----\n", k);
                // printf("infe[i][k] = %d, infc[i][k] = %d\n", ls->infe[i][k], ls->infc[i][k]);
                int m1 = ls->npe[ls->infe[i][k]][ls->nltable[ls->infc[i][k]][0]];
                int m2 = ls->npe[ls->infe[i][k]][ls->nltable[ls->infc[i][k]][1]];
                // printf("n1 = %d, n2 = %d\n", n1, n2);
                // printf("m1 = %d, m2 = %d\n", m1, m2);
                if (n1 == m2) {
                    i1 = 1;
                    ls->infnc_a[i][j] = ls->infnc_b[i][k];
                }
                if (n2 == m1) {
                    i2 = 1;
                    ls->infnc_b[i][j] = ls->infnc_a[i][k];
                }
            }
            // exit(1);

            if (i1 == 0) {
                // ここの処理はfortran版ではgotoを使っているのでc++ではどうしようか考え中
                // if (infnn >= infnn_max) {
                //     infnn_max = infnn_max + 2;
                //     for (int i_tmp = 0; i_tmp < infnn_max; i_tmp++) {
                //         free(ls->jcoef[i_tmp]);
                //     }
                //     free(ls->jcoef);
                // }
                ls->jcoef[i][infnn] = n1;
                ls->infnc_a[i][j] = infnn;
                infnn = infnn + 1;
                // printf("i1\n");
                i1_counter++;
            }
            if (i2 == 0) {
                // ここの処理はfortran版ではgotoを使っているのでc++ではどうしようか考え中
                // if (infnn >= infnn_max) {
                //     infnn_max = infnn_max + 2;
                //     for (int i_tmp = 0; i_tmp < infnn_max; i_tmp++) {
                //         free(ls->jcoef[i_tmp]);
                //     }
                //     free(ls->jcoef);
                // }
                ls->jcoef[i][infnn] = n2;
                ls->infnc_b[i][j] = infnn;
                infnn = infnn + 1;
                // printf("i2\n");
                i2_counter++;
            }
        }
        ls->nrow_max = std::max(ls->nrow_max, infnn);

        for (int i_tmp = 0; i_tmp < cp->number_of_nodes; i_tmp++) {
            for (int j_tmp = 0; j_tmp < infnn_max; j_tmp++) {
                ls->jcoef_before[i_tmp][j_tmp] = ls->jcoef[i_tmp][j_tmp];
            }
        }
        for (int j_tmp = infnn; j_tmp < infnn_max; j_tmp++) {
            ls->jcoef[i][j_tmp] = i;
        }
        for (int i_tmp = 0; i_tmp < cp->number_of_nodes; i_tmp++) {
            for (int j_tmp = 0; j_tmp < infnn_max; j_tmp++) {
                if (ls->jcoef_before[i_tmp][j_tmp] != ls->jcoef[i_tmp][j_tmp]) {
                    // printf("i = %d, j = %d, jcoef = %d\n", i_tmp, j_tmp, ls->jcoef[i_tmp][j_tmp]);
                }
            }
        }
        if (i == 2) {
            // exit(1);
        }
    }
    // printf("ok4\n");
    // printf("i1_counter = %d, i2_counter = %d\n", i1_counter, i2_counter);
    // for (int j = 0; j < infnn_max; j++) {
    //     if (j == 2) {
    //         for (int i = 0; i < cp->number_of_nodes; i++) {
    //             printf("jcoef(%d, %d) = %d\n", i, j, ls->jcoef[i][j]);
    //         }
    //         // exit(1);
    //     }
    // }

    // for (int j = 0; j < infnn_max; j++) {
    //     for (int i = 0; i < cp->number_of_nodes; i++) {
    //         printf("jcoef(%7d,%7d) = %7d\n", j + 1 , i + 1, ls->jcoef[i][j] + 1);
    //     }
    // }

    ls->coef_x = (double**)calloc(cp->number_of_nodes, sizeof(double*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->coef_x[i] = (double*)calloc(ls->nrow_max * 3, sizeof(double));
    }
    if (ls->coef_x == NULL) {
        printf("Cat not allocate coef_x memory.\n");
    }
    ls->coef_y = (double**)calloc(cp->number_of_nodes, sizeof(double*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->coef_y[i] = (double*)calloc(ls->nrow_max * 3, sizeof(double));
    }
    if (ls->coef_y == NULL) {
        printf("Cat not allocate coef_y memory.\n");
    }
    ls->coef_c = (double**)calloc(cp->number_of_nodes, sizeof(double*));
    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->coef_c[i] = (double*)calloc(ls->nrow_max * 3, sizeof(double));
    }
    if (ls->coef_c == NULL) {
        printf("Cat not allocate coef_c memory.\n");
    }
    ls->wkg_row = (double*)calloc(ls->nrow_max * 3, sizeof(double));
    if (ls->wkg_row == NULL) {
        printf("Cat not allocate wkg_row memory.\n");
    }

    printf("finish makeNet\n");
}
