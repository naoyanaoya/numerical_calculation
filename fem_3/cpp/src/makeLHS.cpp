#include<iostream>
#include<iomanip>
#include"makeLHS.h"
#include"dscale.h"

void makeLHS(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start makeLHS" << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->diag_x[i] = 0.0;
        ls->diag_y[i] = 0.0;
        ls->diag_c[i] = 0.0;
    }

    for (int i = 0; i < cp->number_of_nodes; i++) {
        for (int j = 0; j < ls->nrow_max * 3; j++) {
            ls->coef_x[i][j] = 0;
            ls->coef_y[i][j] = 0;
            ls->coef_c[i][j] = 0;
        }
    }

    for (int i = 0; i < cp->number_of_nodes; i++) {
        for (int j = 0; j < ls->infn[i]; j++) {
            int ne = ls->infe[i][j];
            int nt = ls->infc[i][j];
            int n1 = ls->npe[ne][nt];
            int n2 = ls->npe[ne][ls->nltable[nt][0]];
            int n3 = ls->npe[ne][ls->nltable[nt][1]];
            double a01 = ls->area_array[ne];
            double a03 = ls->area_array[ne] / 3.0;
            double ts = ls->tau[ne];
            double tp = ls->tau[ne];
            double b1 = ls->b_array[3 * ne + nt];
            double b2 = ls->b_array[3 * ne + ls->nltable[nt][0]];
            double b3 = ls->b_array[3 * ne + ls->nltable[nt][1]];
            double c1 = ls->c_array[3 * ne + nt];
            double c2 = ls->c_array[3 * ne + ls->nltable[nt][0]];
            double c3 = ls->c_array[3 * ne + ls->nltable[nt][1]];
            double ua1 = cp->ua[n1];
            double ua2 = cp->ua[n2];
            double ua3 = cp->ua[n3];
            double va1 = cp->va[n1];
            double va2 = cp->va[n2];
            double va3 = cp->va[n3];
            // std::cout << a01 << " " << a03 << " " << ts << " " << tp << "\n";
            // std::cout << b1 << " " << b2 << " " << b3 << "\n";
            // std::cout << c1 << " " << c2 << " " << c3 << "\n";
            // std::cout << ua1 << " " << ua2 << " " << ua3 << "\n";
            // std::cout << va1 << " " << va2 << " " << va3 << "\n";

            // M matrix
            double emd = a03 * 0.5;
            double emu = a03 * 0.25;
            // std::cout << emd << " " << emu << "\n";
            // if (i == 49) {
            //     exit(1);
            // }

            // Ms matrix
            double emu1 = emd * ua1 + emu * ua2 + emu * ua3;
            double emu2 = emu * ua1 + emd * ua2 + emu * ua3;
            double emu3 = emu * ua1 + emu * ua2 + emd * ua3;
            double emv1 = emd * va1 + emu * va2 + emu * va3;
            double emv2 = emu * va1 + emd * va2 + emu * va3;
            double emv3 = emu * va1 + emu * va2 + emd * va3;
            double eMs_11 = ts * (b1 * emu1 + c1 * emv1);
            double eMs_12 = ts * (b1 * emu2 + c1 * emv2);
            double eMs_13 = ts * (b1 * emu3 + c1 * emv3);

            // std::cout << emu1 << " " << emu2 << " " << emu3 << "\n";
            // // std::cout << emu1 << " " << emu2 << " " << emu3 << "\n";
            // if (i == 49) {
            //     exit(1);
            // }

            // Mp matrix
            double eMp1_1 = tp * b1 * a03;
            double eMp2_1 = tp * c1 * a03;

            // A matrix
            double eA_11 = emu1 * b1 + emv1 * c1;
            double eA_12 = emu1 * b2 + emv1 * c2;
            double eA_13 = emu1 * b3 + emv1 * c3;

            // As matrix
            double easu1 = eMs_11 * ua1 + eMs_12 * ua2 + eMs_13 * ua3;
            double easv1 = eMs_11 * va1 + eMs_12 * va2 + eMs_13 * va3;
            double eAs_11 = easu1 * b1 + easv1 * c1;
            double eAs_12 = easu1 * b2 + easv1 * c2;
            double eAs_13 = easu1 * b3 + easv1 * c3;

            // Ap matrix
            double tua = tp * (ua1 + ua2 + ua3) * a03;
            double tva = tp * (va1 + va2 + va3) * a03;

            double eAp1_11 = tua * b1 * b1 + tva * b1 * c1;
            double eAp1_12 = tua * b1 * b2 + tva * b1 * c2;
            double eAp1_13 = tua * b1 * b3 + tva * b1 * c3;
            double eAp2_11 = tua * c1 * b1 + tva * c1 * c1;
            double eAp2_12 = tua * c1 * b2 + tva * c1 * c2;
            double eAp2_13 = tua * c1 * b3 + tva * c1 * c3;

            // D matrix
            double eD11_11 = a01 * cc->Re_inverse * (b1 * b1 + b1 * b1 + c1 * c1);
            double eD11_12 = a01 * cc->Re_inverse * (b1 * b2 + b1 * b2 + c1 * c2);
            double eD11_13 = a01 * cc->Re_inverse * (b1 * b3 + b1 * b3 + c1 * c3);
            double eD22_11 = a01 * cc->Re_inverse * (c1 * c1 + c1 * c1 + b1 * b1);
            double eD22_12 = a01 * cc->Re_inverse * (c1 * c2 + c1 * c2 + b1 * b2);
            double eD22_13 = a01 * cc->Re_inverse * (c1 * c3 + c1 * c3 + b1 * b3);

            double eD12_11 = a01 * cc->Re_inverse * c1 * b1;
            double eD12_12 = a01 * cc->Re_inverse * c1 * b2;
            double eD12_13 = a01 * cc->Re_inverse * c1 * b3;
            double eD12_21 = a01 * cc->Re_inverse * c2 * b1;
            double eD12_31 = a01 * cc->Re_inverse * c3 * b1;

            // G & C matrix
            double eG1_1 = b1 * a03;
            double eG1_2 = b2 * a03;
            double eG1_3 = b3 * a03;
            double eG2_1 = c1 * a03;
            double eG2_2 = c2 * a03;
            double eG2_3 = c3 * a03;

            // Gs matrix
            tua = ts * (ua1 + ua2 + ua3) * a03;
            tva = ts * (va1 + va2 + va3) * a03;
            double at1 = tua * b1 + tva * c1;
            double eGs1_11 = at1 * b1;
            double eGs1_12 = at1 * b2;
            double eGs1_13 = at1 * b3;
            double eGs2_11 = at1 * c1;
            double eGs2_12 = at1 * c2;
            double eGs2_13 = at1 * c3;

            // Gp matrix
            double eGp_11 = tp * (b1 * b1 + c1 * c1) * a01;
            double eGp_12 = tp * (b1 * b2 + c1 * c2) * a01;
            double eGp_13 = tp * (b1 * b3 + c1 * c3) * a01;

            // Matrix form
            n1 = ls->nrow_max * 0;
            n2 = ls->nrow_max * 1;
            n3 = ls->nrow_max * 2;
            int m1 = 0;
            int m2 = ls->infnc_a[i][j];
            int m3 = ls->infnc_b[i][j];

            // std::cout << n1 << " " << n2 << " " << n3 << "\n";
            // std::cout << m1 << " " << m2 << " " << m3 << "\n";

            // Momentum X
            ls->coef_x[i][n1 + m1] = ls->coef_x[i][n1 + m1] + cc->dt_inverse * (emd + eMs_11) + 0.5 * (eA_11 + eAs_11 + eD11_11);
            ls->coef_x[i][n1 + m2] = ls->coef_x[i][n1 + m2] + cc->dt_inverse * (emu + eMs_12) + 0.5 * (eA_12 + eAs_12 + eD11_12);
            ls->coef_x[i][n1 + m3] = ls->coef_x[i][n1 + m3] + cc->dt_inverse * (emu + eMs_13) + 0.5 * (eA_13 + eAs_13 + eD11_13);
            ls->coef_x[i][n2 + m1] = ls->coef_x[i][n2 + m1] + 0.5 * eD12_11;
            ls->coef_x[i][n2 + m2] = ls->coef_x[i][n2 + m2] + 0.5 * eD12_12;
            ls->coef_x[i][n2 + m3] = ls->coef_x[i][n2 + m3] + 0.5 * eD12_13;
            ls->coef_x[i][n3 + m1] = ls->coef_x[i][n3 + m1] - eG1_1 + eGs1_11;
            ls->coef_x[i][n3 + m2] = ls->coef_x[i][n3 + m2] - eG1_1 + eGs1_12;
            ls->coef_x[i][n3 + m3] = ls->coef_x[i][n3 + m3] - eG1_1 + eGs1_13;

            // Momentum Y
            ls->coef_y[i][n1 + m1] = ls->coef_y[i][n1 + m1] + 0.5 * eD12_11;
            ls->coef_y[i][n1 + m2] = ls->coef_y[i][n1 + m2] + 0.5 * eD12_21;
            ls->coef_y[i][n1 + m3] = ls->coef_y[i][n1 + m3] + 0.5 * eD12_31;
            ls->coef_y[i][n2 + m1] = ls->coef_y[i][n2 + m1] + cc->dt_inverse * (emd + eMs_11) + 0.5 * (eA_11 + eAs_11 + eD22_11);
            ls->coef_y[i][n2 + m2] = ls->coef_y[i][n2 + m2] + cc->dt_inverse * (emu + eMs_12) + 0.5 * (eA_12 + eAs_12 + eD22_12);
            ls->coef_y[i][n2 + m3] = ls->coef_y[i][n2 + m3] + cc->dt_inverse * (emu + eMs_13) + 0.5 * (eA_13 + eAs_13 + eD22_13);
            ls->coef_y[i][n3 + m1] = ls->coef_y[i][n3 + m1] - eG2_1 + eGs2_11;
            ls->coef_y[i][n3 + m2] = ls->coef_y[i][n3 + m2] - eG2_1 + eGs2_12;
            ls->coef_y[i][n3 + m3] = ls->coef_y[i][n3 + m3] - eG2_1 + eGs2_13;

            // Continuity
            ls->coef_c[i][n1 + m1] = ls->coef_c[i][n1 + m1] + eG1_1 + cc->dt_inverse * eMp1_1 + 0.5 * eAp1_11;
            ls->coef_c[i][n1 + m2] = ls->coef_c[i][n1 + m2] + eG1_2 + cc->dt_inverse * eMp1_1 + 0.5 * eAp1_12;
            ls->coef_c[i][n1 + m3] = ls->coef_c[i][n1 + m3] + eG1_3 + cc->dt_inverse * eMp1_1 + 0.5 * eAp1_13;
            ls->coef_c[i][n2 + m1] = ls->coef_c[i][n2 + m1] + eG2_1 + cc->dt_inverse * eMp2_1 + 0.5 * eAp2_11;
            ls->coef_c[i][n2 + m2] = ls->coef_c[i][n2 + m2] + eG2_2 + cc->dt_inverse * eMp2_1 + 0.5 * eAp2_12;
            ls->coef_c[i][n2 + m3] = ls->coef_c[i][n2 + m3] + eG2_3 + cc->dt_inverse * eMp2_1 + 0.5 * eAp2_13;
            ls->coef_c[i][n3 + m1] = ls->coef_c[i][n3 + m1] + eGp_11;
            ls->coef_c[i][n3 + m2] = ls->coef_c[i][n3 + m2] + eGp_12;
            ls->coef_c[i][n3 + m3] = ls->coef_c[i][n3 + m3] + eGp_13;
        }

        // for Scaling
        // std::cout << std::setprecision(8);
        // std::cout << "coef_x[" << i << "][0] = " << ls->coef_x[i][0 + ls->nrow_max * 0] << "\n";
        // std::cout << "coef_y[" << i << "][0] = " << ls->coef_y[i][0 + ls->nrow_max * 1] << "\n";
        // std::cout << "coef_c[" << i << "][0] = " << ls->coef_c[i][0 + ls->nrow_max * 2] << "\n";
        // exit(1);
        ls->diag_x[i] = ls->coef_x[i][0 + ls->nrow_max * 0];
        ls->diag_y[i] = ls->coef_y[i][0 + ls->nrow_max * 1];
        ls->diag_c[i] = ls->coef_c[i][0 + ls->nrow_max * 2];
        // std::cout << ls->coef_c[i][0 + ls->nrow_max * 2] << "\n";
        // exit(1);
    }

    // std::cout << ls->diag_c[0] << "\n";
    // exit(1);

    // std::cout << std::fixed << std::setprecision(9);
    // for (int i = 0; i < cp->number_of_nodes; i++) {
    //     std::cout << i << " " << cp->u[i] << " " << ls->diag_x[i] << "\n";
    //     if (i == 200) {
    //         break;
    //     }
    // }
    // exit(1);

    dscale(cp, ls, cc);

    // std::cout << ls->npe[0][0] << "\n";

    std::cout << "finish makeLHS" << "\n";
}
