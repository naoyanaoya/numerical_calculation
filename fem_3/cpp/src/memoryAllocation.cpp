#include<iostream>
#include"memoryAllocation.h"

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    std::cout << "- start memoryAllocation" << "\n";

    int NN = cp->number_of_nodes;
    int NE = cp->number_of_elements;
    // std::cout << "number_of_nodes = " << cp->number_of_nodes << "\n";
    // std::cout << "number_of_elements = " << cp->number_of_elements << "\n";

    cp->x = (double*)calloc(NN, sizeof(double));
    if (cp->x == NULL) {
        printf("Can not allocate x memory.\n");
    }

    cp->y = (double*)calloc(NN, sizeof(double));
    if (cp->y == NULL) {
        printf("Can not allocate y memory.\n");
    }

    cp->u = (double*)calloc(NN, sizeof(double));
    if (cp->u == NULL) {
        printf("Can not allocate u memory.\n");
    }

    cp->v = (double*)calloc(NN, sizeof(double));
    if (cp->v == NULL) {
        printf("Can not allocate v memory.\n");
    }

    cp->p = (double*)calloc(NN, sizeof(double));
    if (cp->p == NULL) {
        printf("Cat not allocate p memory.\n");
    }

    cp->uu = (double*)calloc(NN, sizeof(double));
    if (cp->uu == NULL) {
        printf("Cat not allocate uu memory.\n");
    }

    cp->vu = (double*)calloc(NN, sizeof(double));
    if (cp->vu == NULL) {
        printf("Cat not allocate vu memory.\n");
    }

    cp->ua = (double*)calloc(NN, sizeof(double));
    if (cp->ua == NULL) {
        printf("Cat not allocate ua memory.\n");
    }

    cp->va = (double*)calloc(NN, sizeof(double));
    if (cp->va == NULL) {
        printf("Cat not allocate va memory.\n");
    }

    cp->iubc = (int*)calloc(2, sizeof(int));
    if (cp->iubc == NULL) {
        printf("Cat not allocate iubc memory.\n");
    }

    ls->infn = (int*)calloc(NN, sizeof(int));
    if (ls->infn == NULL) {
        printf("Cat not allocate infn memory.\n");
    }

    ls->nltable = (int**)calloc(3, sizeof(int*));
    for (int i = 0; i < 3; i++) {
        ls->nltable[i] = (int*)calloc(2, sizeof(int));
    }
    if (ls->nltable == NULL) {
        printf("Cat not allocate nltable memory.\n");
    }

    ls->diag_x = (double*)calloc(NN, sizeof(double));
    if (ls->diag_x == NULL) {
        printf("Cat not allocate diag_x memory.\n");
    }
    ls->diag_y = (double*)calloc(NN, sizeof(double));
    if (ls->diag_y == NULL) {
        printf("Cat not allocate diag_y memory.\n");
    }
    ls->diag_c = (double*)calloc(NN, sizeof(double));
    if (ls->diag_c == NULL) {
        printf("Cat not allocate diag_c memory.\n");
    }

    ls->bv_x = (double*)calloc(NN, sizeof(double));
    if (ls->bv_x == NULL) {
        printf("Cat not allocate bv_x memory.\n");
    }
    ls->bv_y = (double*)calloc(NN, sizeof(double));
    if (ls->bv_y == NULL) {
        printf("Cat not allocate bv_y memory.\n");
    }
    ls->bv_c = (double*)calloc(NN, sizeof(double));
    if (ls->bv_c == NULL) {
        printf("Cat not allocate bv_c memory.\n");
    }

    ls->w01_u = (double*)calloc(NN, sizeof(double));
    if (ls->w01_u == NULL) {
        printf("Cat not allocate w01_u memory.\n");
    }
    ls->w01_v = (double*)calloc(NN, sizeof(double));
    if (ls->w01_v == NULL) {
        printf("Cat not allocate w01_v memory.\n");
    }
    ls->w01_p = (double*)calloc(NN, sizeof(double));
    if (ls->w01_p == NULL) {
        printf("Cat not allocate w01_p memory.\n");
    }

    ls->w02_u = (double*)calloc(NN, sizeof(double));
    if (ls->w02_u == NULL) {
        printf("Cat not allocate w02_u memory.\n");
    }
    ls->w02_v = (double*)calloc(NN, sizeof(double));
    if (ls->w02_v == NULL) {
        printf("Cat not allocate w02_v memory.\n");
    }
    ls->w02_p = (double*)calloc(NN, sizeof(double));
    if (ls->w01_p == NULL) {
        printf("Cat not allocate w02_p memory.\n");
    }

    ls->w09_x = (double*)calloc(NN * 3, sizeof(double));
    if (ls->w09_x == NULL) {
        printf("Cat not allocate w09_x memory.\n");
    }
    ls->w09_y = (double*)calloc(NN * 3, sizeof(double));
    if (ls->w09_y == NULL) {
        printf("Cat not allocate w09_y memory.\n");
    }
    ls->w09_c = (double*)calloc(NN * 3, sizeof(double));
    if (ls->w09_c == NULL) {
        printf("Cat not allocate w09_c memory.\n");
    }

    ls->area_array = (double*)calloc(NE, sizeof(double));
    if (ls->area_array == NULL) {
        printf("Cat not allocate area_array memory.\n");
    }
    ls->b_array = (double*)calloc((NE * 3), sizeof(double));
    if (ls->b_array == NULL) {
        printf("Cat not allocate b_array memory.\n");
    }
    ls->c_array = (double*)calloc((NE * 3), sizeof(double));
    if (ls->c_array == NULL) {
        printf("Cat not allocate c_array memory.\n");
    }
    ls->tau = (double*)calloc(NE, sizeof(double));
    if (ls->tau == NULL) {
        printf("Cat not allocate tau memory.\n");
    }

    ls->npe = (int**)calloc(NE, sizeof(int*));
    for (int i = 0; i < NE; i++){
        ls->npe[i] = (int*)calloc(3, sizeof(int));
    }
    if (ls->npe == NULL) {
        printf("Cat not allocate npe memory.\n");
    }

    std::cout << "- finish memoryAllocation" << "\n";
}
