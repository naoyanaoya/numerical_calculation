#ifndef __INCLUDE_STRUCT_H__
#define __INCLUDE_STRUCT_H__

typedef struct {
    double* x; // coordinate x
    double* y; // coordinate y
    double* z; // coordinate z
    double* u; // velocity x value
    double* v; // velocity y value
    double* p; // pressure scalar
    double* uu; // 既知流速 x
    double* vu; // 既知速度 y
    double* ua; // 移流速度(Adams-Bashforth近似)? x
    double* va; // 移流速度(Adams-Bashforth近似)? y
    int number_of_nodes; // ノードの個数
    int number_of_elements; // エレメントの個数
    int number_of_initial_conditions; // 初期条件の個数(ノードの個数と多分一緒)
    int number_of_nodes_per_element;
    int* iubc; // 境界条件を課す節点数 1列目はuに関する境界条件、2列目はvに関する境界条件
    int iubc_max; // iubcの1列目と2列目の節点数の大きいほう
    int** nubc; // 境界条件を表す節点番号
    double** fubc; // 境界条件を課す値
}CALC_POINTS;

typedef struct {
    int** npe; // 要素結合情報
    int* infn; // 節点n周辺の要素数
    int infn_max = 0; // この２つの違いは？
    int nrow_max = 0; // 1自由度当たりの左辺非ゼロ成分数
    int** infe; // 節点n周辺の要素番号
    int** infc; // 要素mにおける節点nの要素内部点番号
    int** infnc_a; // 左辺圧縮行列の非ゼロ成分位置
    int** infnc_b; // 左辺圧縮行列の非ゼロ成分位置
    int** jcoef; // 左辺非ゼロ成分の全体系節点番号
    int** jcoef_before;
    double** coef_x; // 左辺行列(ELLPACK形式) (Mom(x))
    double** coef_y; // 左辺行列(ELLPACK形式) (Mom(y))
    double** coef_c; // 左辺行列(ELLPACK形式) (Con)
    double* wkg_row; // ワーキングベクトル(1:nrow_max*3)
    double* w01_u;
    double* w01_v;
    double* w01_p;
    double* w02_u;
    double* w02_v;
    double* w02_p;
    double* w09_x;
    double* w09_y;
    double* w09_c;
    int** nltable; //
    double* diag_x; // 左辺行列対角成分x
    double* diag_y; // 左辺行列対角成分y
    double* diag_c; // 左辺行列対角成分c
    double* bv_x; // 右辺ベクトル
    double* bv_y; // 右辺ベクトル
    double* bv_c; // 右辺ベクトル
    double* area_array; // 面積座標
    double* b_array; // 形状関数の係数
    double* c_array; // 形状関数の係数
    double* tau;
}LIN_SYS;

typedef struct {
    const int iteration_max = 1000;
    const double EPS = 1.0e-6;
    const int TIMESTEP = 250;
    const double diffusion_coef = 0.001;
    const double theta = 1.0;
    int ista;
    int iend;
    int iout;
    double dt;
    double dt_inverse;
    double Re;
    double Re_inverse;
}CAL_COND;

#endif
