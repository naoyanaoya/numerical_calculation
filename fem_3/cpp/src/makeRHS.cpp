#include<iostream>
#include<iomanip>
#include"makeRHS.h"

void makeRHS(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start makeRHS" << "\n";

    // std::cout << ls->npe[0][0] << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->bv_x[i] = 0;
        ls->bv_y[i] = 0;
        ls->bv_c[i] = 0;
    }

    for (int i = 0; i < cp->number_of_nodes; i++) {
        for (int j = 0; j < ls->infn[i]; j++) {
            int ne = ls->infe[i][j];
            int nt = ls->infc[i][j];
            int n1 = ls->npe[ne][nt];
            int n2 = ls->npe[ne][ls->nltable[nt][0]];
            int n3 = ls->npe[ne][ls->nltable[nt][1]];
            double a01 = ls->area_array[ne];
            double a03 = ls->area_array[ne] / 3.0;
            double b1 = ls->b_array[3 * ne + nt];
            double b2 = ls->b_array[3 * ne + ls->nltable[nt][0]];
            double b3 = ls->b_array[3 * ne + ls->nltable[nt][1]];
            double c1 = ls->c_array[3 * ne + nt];
            double c2 = ls->c_array[3 * ne + ls->nltable[nt][0]];
            double c3 = ls->c_array[3 * ne + ls->nltable[nt][1]];
            double u1 = cp->uu[n1];
            double u2 = cp->uu[n2];
            double u3 = cp->uu[n3];
            double v1 = cp->vu[n1];
            double v2 = cp->vu[n2];
            double v3 = cp->vu[n3];
            double ua1 = cp->ua[n1];
            double ua2 = cp->ua[n2];
            double ua3 = cp->ua[n3];
            double va1 = cp->va[n1];
            double va2 = cp->va[n2];
            double va3 = cp->va[n3];

            double emd = a03 * 0.5;
            double emu = a03 * 0.25;

            double dxu = b1 * u1 + b2 * u2 + b3 * u3;
            double dyu = c1 * u1 + c2 * u2 + c3 * u3;
            double dxv = b1 * v1 + b2 * v2 + b3 * v3;
            double dyv = c1 * v1 + c2 * v2 + c3 * v3;

            // Galerkin term -------------------------------

            // M Term
            double eMu1 = emd * u1 + emu * u2 + emu * u3;
            double eMv1 = emd * v1 + emu * v2 + emu * v3;

            // A Term
            double emua1 = emd * ua1 + emu * ua2 + emu * ua3;
            double emua2 = emu * ua1 + emd * ua2 + emu * ua3;
            double emua3 = emu * ua1 + emu * ua2 + emd * ua3;
            double emva1 = emd * va1 + emu * va2 + emu * va3;
            double emva2 = emu * va1 + emd * va2 + emu * va3;
            double emva3 = emu * va1 + emu * va2 + emd * va3;
            double eAu1 = emua1 * dxu + emva1 * dyu;
            double eAv1 = emua1 * dxv + emva1 * dyv;

            // D Term
            double sxx = cc->Re_inverse * (dxu + dxu) * a01;
            double sxy = cc->Re_inverse * (dyu + dxv) * a01;
            double syy = cc->Re_inverse * (dyv + dyv) * a01;
            double eDu1 = b1 * sxx + c1 * sxy;
            double eDv1 = b1 * sxy + c1 * syy;

            ls->bv_x[i] = ls->bv_x[i] + eMu1 * cc->dt_inverse - 0.5 * (eAu1 + eDu1);
            ls->bv_y[i] = ls->bv_y[i] + eMv1 * cc->dt_inverse - 0.5 * (eAv1 + eDv1);


            // Stabilized term -------------------------------

            double ts = ls->tau[ne];
            double tp = ls->tau[ne];

            // Ms Term
            double tumu = ts * (emua1 * u1 + emua2 * u2 + emua3 * u3);
            double tvmu = ts * (emva1 * u1 + emva2 * u2 + emva3 * u3);
            double tumv = ts * (emua1 * v1 + emua2 * v2 + emua3 * v3);
            double tvmv = ts * (emva1 * v1 + emva2 * v2 + emva3 * v3);
            double eMsu1 = b1 * tumu + c1 * tvmu;
            double eMsv1 = b1 * tumv + c1 * tvmv;

            // Mp Term
            double tu = tp * (u1 + u2 + u3) * a03;
            double tv = tp * (v1 + v2 + v3) * a03;
            double eMp1 = b1 * tu + c1 * tv;

            // As Term
            tumu = ts * (ua1 * emua1 + ua2 * emua2 + ua3 * emua3);
            tumv = ts * (ua1 * emva1 + ua2 * emva2 + ua3 * emva3);
            tvmv = ts * (va1 * emva1 + va2 * emva2 + va3 * emva3);
            double stxu = tumu * dxu + tumv * dyu;
            double styu = tumv * dxu + tvmv * dyu;
            double stxv = tumu * dxv + tumv * dyv;
            double styv = tumv * dxv + tvmv * dyv;
            double eAsu1 = b1 * stxu + c1 * styu;
            double eAsv1 = b1 * stxv + c1 * styv;

            // Ap Term
            tu = tp * (ua1 + ua2 + ua3) * a03;
            tv = tp * (va1 + va2 + va3) * a03;
            double atu = tu * dxu + tv * dyu;
            double atv = tu * dxv + tv * dyv;
            double eAp1 = b1 * atu + c1 * atv;

            ls->bv_x[i] = ls->bv_x[i] + eMsu1 * cc->dt_inverse - 0.5 * eAsu1;
            ls->bv_y[i] = ls->bv_y[i] + eMsv1 * cc->dt_inverse - 0.5 * eAsv1;
            ls->bv_c[i] = ls->bv_c[i] + eMp1 * cc->dt_inverse - 0.5 * eAp1;
        }
    }

    // double a = 0;
    // double b = 0;
    // double c = 0;
    // for (int i = 0; i < cp->number_of_nodes; i++) {
    //     for (int j = 0; j < ls->infn[i]; j++) {
    //         a += ls->bv_x[i];
    //         b += ls->bv_y[i];
    //         c += ls->bv_c[i];
    //     }
    // }
    // std::cout << std::setprecision(12);
    // std::cout << a << "\n";
    // std::cout << b << "\n";
    // std::cout << c << "\n";


    std::cout << "finish makeRHS" << "\n";
}
