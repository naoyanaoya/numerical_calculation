#ifndef __INCLUDE_INITIALCONDITION_H__
#define __INCLUDE_INITIALCONDITION_H__
#include"struct.h"

void initialCondition(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
