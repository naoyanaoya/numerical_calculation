#ifndef __INCLUDE_READFILE_H__
#define __INCLUDE_READFILE_H__
#include"struct.h"

void readFile(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
