#include<iostream>
#include<iomanip>
#include"initialCondition.h"

void initialCondition(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "start initialCondition" << "\n";

    // initial condition data
    if (cc->ista == -1) {
        cc->ista = 0;
        for (int i = 0; i < cp->number_of_nodes; i++) {
            cp->u[i] = 0;
            cp->v[i] = 0;
            cp->p[i] = 0;
        }
    } else {
        std::cout << "please give me initial condition external file!" << "\n";
        exit(1);
    }

    // boundary condition
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < cp->iubc[i]; j++) {
            if (i == 0) {
                cp->u[cp->nubc[j][i]] = cp->fubc[j][i];
            } else if (i == 1) {
                cp->v[cp->nubc[j][i]] = cp->fubc[j][i];
            }
        }
    }

    // std::cout << std::fixed << std::setprecision(9);
    // for (int i = 0; i < cp->number_of_nodes; i++) {
    //     std::cout << i << " " << cp->u[i] << "\n";
    //     if (i == 200) {
    //         break;
    //     }
    // }
    // exit(1);

    // 既知流速と移流速度(Adams-Bashforth近似)
    for (int i = 0; i < cp->number_of_nodes; i++) {
        cp->uu[i] = cp->u[i];
        cp->vu[i] = cp->v[i];
        cp->ua[i] = cp->u[i];
        cp->va[i] = cp->v[i];
    }

    std::cout << "finish initialCondition" << "\n";
}
