#include<iostream>
#include<iomanip>
#include<cmath>
#include"dscale.h"

void dscale(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    std::cout << "- start dscale" << "\n";

    // std::cout << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        ls->diag_x[i] = 1.0 / std::sqrt(std::abs(ls->diag_x[i]));
        ls->diag_y[i] = 1.0 / std::sqrt(std::abs(ls->diag_y[i]));
        ls->diag_c[i] = 1.0 / std::sqrt(std::abs(ls->diag_c[i]));
        // std::cout << ls->diag_x[i] << " " << ls->diag_y[i] << " " << ls->diag_c[i] << "\n";
        // if (i == 10) {
        //     exit(1);
        // }
    }

    // std::cout << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";

    for (int i = 0; i < cp->number_of_nodes; i++) {
        // std::cout << i << " " << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";
        for (int j = 0; j < ls->nrow_max; j++) {
            // std::cout << j + ls->nrow_max * 0 << " " << j + ls->nrow_max * 1 << " " << j + ls->nrow_max * 2 << "\n";
            ls->wkg_row[j + ls->nrow_max * 0] = ls->diag_x[ls->jcoef[i][j]];
            ls->wkg_row[j + ls->nrow_max * 1] = ls->diag_y[ls->jcoef[i][j]];
            ls->wkg_row[j + ls->nrow_max * 2] = ls->diag_c[ls->jcoef[i][j]];
            // std::cout << i << " " << j << " " << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";
        }
        // std::cout << i << " " << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";
        double dx = ls->diag_x[i];
        double dy = ls->diag_y[i];
        double dc = ls->diag_c[i];
        for (int j = 0; j < ls->nrow_max * 3; j++) {
            ls->coef_x[i][j] = ls->coef_x[i][j] * dx * ls->wkg_row[j];
            ls->coef_y[i][j] = ls->coef_y[i][j] * dy * ls->wkg_row[j];
            ls->coef_c[i][j] = ls->coef_c[i][j] * dc * ls->wkg_row[j];
        }
    }

    // std::cout << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";


    double a = 0;
    double b = 0;
    double c = 0;
    for (int i = 0; i < cp->number_of_nodes; i++) {
        for (int j = 0; j < ls->nrow_max * 3; j++) {
            a += ls->coef_x[i][j];
            b += ls->coef_y[i][j];
            c += ls->coef_c[i][j];
        }
    }

    // TODO: ここまででエラーが起きている可能性がある
    // std::cout << a << "\n";
    // std::cout << b << "\n";
    // std::cout << c << "\n";

    // std::cout << &ls->npe[0][0] << " " << ls->npe[0][0] << "\n";

    std::cout << "- finish dscale" << "\n";
}


