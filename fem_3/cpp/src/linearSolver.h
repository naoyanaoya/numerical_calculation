#ifndef __INCLUDE_LINEARSOLVER_H__
#define __INCLUDE_LINEARSOLVER_H__
#include"struct.h"

void GPBiCG(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
