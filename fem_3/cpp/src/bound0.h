#ifndef __INCLUDE_BOUND0_H__
#define __INCLUDE_BOUND0_H__
#include"struct.h"

void bound0(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif
