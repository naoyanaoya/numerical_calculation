program make_sample_mesh
  implicit none
  double precision, allocatable :: xy(:, :, :)
  integer :: i, j, k = 1, l
  integer :: m = 2 ! x方向とy方向のindex
  integer :: a = 0, b = 0, c = 0, d= 0
  integer :: n(2)=(/250, 50/) ! x方向とy方向の分割数
  open(11, file='data/mesh.mes')
  open(12, file='data/boundary.bc')
  allocate(xy(m, n(1) + 1, n(2) + 1))

  do j=1, n(2) + 1
    do i=1, n(1) + 1
      xy(1,i,j)= (0.1d0 - 0.0d0) * (i - 1)
       ! 整数倍して最後に割って、浮動小数点数の誤差が溜まるのを避ける
      xy(2,i,j)= (0.1d0 - 0.0d0) * (j - 1)
      ! xy(1,i,j)= (1.0d0 - 0.0d0) * (i - 1) / real(n(1), kind = 8)
      !  ! 整数倍して最後に割って、浮動小数点数の誤差が溜まるのを避ける
      ! xy(2,i,j)= (1.0d0 - 0.0d0) * (j - 1) / real(n(2), kind = 8)
    enddo
  enddo

  write(11,"(2i10)") (n(1) + 1) * (n(2) + 1), n(1) * n(2) * 2
  do i = 1, n(1) + 1
    do j = n(2) + 1, 1, -1
      write(11,"(i10,100e12.4)") k, xy(1, i, j), xy(2, i, j)
      k = k + 1
    end do
  end do

  k = 1
  a = 1
  do i = 1, n(1)
    do j = 1, n(2) / 2
      write(11,"(4i10)") k, a, a + (n(2) + 1) + 1, a + (n(2) + 1)
      k = k + 1
      write(11,"(4i10)") k, a + (n(2) + 1) + 1, a, a + 1
      k = k + 1
      a = a + 1
    end do
      a = a + 1
    do j =1 , n(2) / 2
      write(11,"(4i10)") k, a, a + n(2), a - 1
      k = k + 1
      write(11,"(4i10)") k, a + n(2), a, a + n(2) + 1
      k = k + 1
      a = a + 1
    end do
    ! write(11,*) "end one loop"
    ! write(11,"(4i4)") k, a + 2, a + 2 + n(2), a + 1
    ! k = k + 1
    ! write(11,"(4i4)") k, a + 2 + n(2), a + 2, a + 2 + n(2) + 1
    ! k = k + 1
    ! a = a + n(2) + 1
  end do

  ! number of vec_x's and vec_y's boundary condition
  write(12,"(2i10)") n(2) + 1 + (n(1)) * 2, (n(1) + 1) * 2
  ! vec_x's boundary condition
  l = 1
  do j = 1, n(2) + 1
    if (j == 1) then
      write(12,"(2i10,100e12.4)") l, j, 0.0d0
      l = l + 1
    else if(j == n(2) + 1) then
      write(12,"(2i10,100e12.4)") l, j, 0.0d0
      l = l + 1
    else
      write(12,"(2i10,100e12.4)") l , j, 1.0d0
      l = l + 1
    end if
  end do
  do i = 1, n(1)
    do j =1 , n(2) + 1
      if (j == 1) then
          write(12,"(2i10,100e12.4)") l, i * (n(2) + 1) + j, 0.0d0
          l = l + 1
      else if(j == n(2) + 1) then
          write(12,"(2i10,100e12.4)") l, i * (n(2) + 1) + j, 0.0d0
          l = l + 1
      end if
    end do
  end do
  ! vec_y's boundary condition
  l = 1
  do i = 0, n(1)
    do j =1 , n(2) + 1
      if (j == 1) then
          write(12,"(2i10,100e12.4)") l, i * (n(2) + 1) + j, 0.0d0
          l = l + 1
      else if(j == n(2) + 1) then
          write(12,"(2i10,100e12.4)") l, i * (n(2) + 1) + j, 0.0d0
          l = l + 1
      end if
    end do
  end do


  close(11)
  close(12)
end program make_sample_mesh
