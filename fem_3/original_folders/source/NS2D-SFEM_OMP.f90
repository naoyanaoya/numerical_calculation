!=======================================================================
!     2-Dimensional Finite Element Analysis
!                   of Incompressible Viscous Flow
!
!      * Stabilized F.E.M.(SUPG/PSPG,T.E.Tezduyar,1992)
!      * P1/P1 Element
!      * Crank-Nicolson Method
!      * GPBi-CG Method with Diagonal Scaling System
!      * ELLPACK Matrix-Storage Format
!
!          Copyright (c) 2017.07.20 Seizo Tanaka
!                           and
!            Advanced Computational Mechanics Lab., 
!                   University of Tsukuba
!                    All rights reserved
!
!      * Japanege characters are written by UTF-8 encoding
!========================================================================
module indata
 implicit none
 integer :: node, nelm                        ! 節点数，要素数
 double precision, allocatable :: xx(:,:)     ! 節点座標
 integer, allocatable :: nc(:,:)              ! 要素結合情報
 integer :: iubc(1:2), iubc_max               ! 境界条件を課す節点数，その最大値
 integer, allocatable :: nubc(:,:)            ! 境界条件を課す節点番号
 double precision, allocatable :: fubc(:,:)   ! 境界条件を課す値
end module indata
module ellpack_info
 implicit none
 integer :: nrow_max                          ! 1自由度当たりの左辺非ゼロ成分数
 integer :: nltable(1:3,1:2)
 integer, allocatable :: infn(:)              ! 節点 n 周辺の要素総数
 integer, allocatable :: infe(:,:)            ! 節点 n 周辺の要素番号m
 integer, allocatable :: infc(:,:)            ! 要素 m における節点 n の要素内節点番号
 integer, allocatable :: infnc(:,:,:)         ! 左辺圧縮行列の非ゼロ成分位置
 integer, allocatable :: jcoef(:,:)           ! 左辺非ゼロ成分の全体系節点番号
 double precision, allocatable :: coef_x(:,:) ! 左辺行列(ELLPACK形式) (Mom(x))
 double precision, allocatable :: coef_y(:,:) ! 左辺行列(ELLPACK形式) (Mom(y))
 double precision, allocatable :: coef_c(:,:) ! 左辺行列(ELLPACK形式) (Cont)
 double precision, allocatable :: wkg_row(:)  ! ワーキングベクトル(1:nrow_max*3)
end module ellpack_info

program ns2d_sfem
!$ use OMP_LIB
   use indata
   implicit none
   double precision :: rei, dt, dti             ! 1/Re, 時間増分量，1/dt
   integer :: ista, iend, iout, istep           ! 解析開始Step，解析終了Step，結果出力間隔，現Step
   double precision :: ttime                    ! 現時刻
   double precision, allocatable :: bc(:,:,:), area(:)   ! dN/dx(dN/dy), 要素面積
   double precision, allocatable :: tau(:)               ! 安定化パラメータ
   double precision, allocatable :: uu(:,:),   ua(:,:)   ! 既知流速, 移流速度
   double precision, allocatable :: diag(:)              ! 左辺行列対角成分
   double precision, allocatable :: uvp(:),    bv(:)     ! 未知量ベクトル，右辺ベクトル
   integer :: ni(3)                                      ! 未知量ベクトル内の(u,v,p)の先頭アドレス
   double precision, allocatable :: &
                         w01(:), w02(:), w03(:), w04(:), w05(:), &
                         w06(:), w07(:), w08(:), w09(:), w10(:) ! GPBi-CG法でのワーキングベクトル
   integer :: i, kcg
   character(50) :: inpfile, mesfile, bdcfile, stafile, resfile, endfile
!
!$ double precision:: TIME_ALL
!$ write(6,'(a,i4,a)') 'OpenMP:',omp_get_max_threads(),'Threads'
!
!
! -- I/O File Control
      open(9, file = 'file.dat', status = 'old', action = 'read')
      read(9,'(a)') inpfile  ! 計算条件
      read(9,'(a)') mesfile  ! メッシュデータ
      read(9,'(a)') bdcfile  ! 境界条件データ
      read(9,'(a)') stafile  ! 初期条件データ
      read(9,'(a)') resfile  ! 結果出力ファイル(ioutステップごと)
      read(9,'(a)') endfile  ! 最終結果ファイル(iendステップ)
      open(10, file = inpfile, status = 'old',     action = 'read')
      open(11, file = mesfile, status = 'old',     action = 'read')
      open(12, file = bdcfile, status = 'old',     action = 'read')
      open(50, file = resfile, status = 'replace', action = 'write')
      open(51, file = endfile, status = 'replace', action = 'write')
! ---
!
! -- DATAINPUT
      call datain (ista, iend, iout, rei, dt, dti )
      call makNET( node, nelm, nc ) !make NET info 
!
!  --- Dynamic Memory Allocate
      allocate( uu(1:2,1:node), ua(1:2,1:node) )
      allocate( bc(1:2,1:3,1:nelm), area(1:nelm), tau(1:nelm) )
      allocate( diag(1:node*3), uvp(1:node*3), bv(1:node*3) )
      allocate( w01(1:node*3), w02(1:node*3), w03(1:node*3), w04(1:node*3), w05(1:node*3),&
                w06(1:node*3), w07(1:node*3), w08(1:node*3), w09(1:node*3), w10(1:node*3) )
      do i = 1, 3
        ni(i) = node * (i-1) ! Index of U,V,P
      enddo
      if(ista/=-1) open(30, file = stafile, status = 'old',     action = 'read')
      call initin ( ista, node, ni, uvp, uu, ua, iubc, nubc, fubc, iubc_max )
      call makeSF ( node, nelm, xx, nc, area, bc )
!
! =================================================================================================
!$    TIME_ALL = - omp_get_wtime()
      Time_loop : do istep=ista+1,iend
         ttime = dble(istep) * dt
!
         call maketau ( node, nelm, nc, area, bc, ua, dti, rei, tau )
         call makLHS ( node, nelm, nc, area, bc, ua, dti, rei, tau, diag, ni )
         call makRHS ( node, nelm, nc, area, bc, ua, dti, rei, tau, uu, bv,   ni )
         call GPBiCG ( node, uvp,  bv, diag, ni, iubc, iubc_max, nubc, &
                       w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, kcg )
         call stepch ( node, ni, uvp, uu, ua )
!
         write(*,*) '****** STEP INFO *****'
         write(*,*) 'STEP   =',istep
         write(*,*) 'TIME   =',ttime
         write(*,*) 'K(BiCG)=',kcg
!
         if(istep / iout * iout == istep)  then
            call output ( 50, istep, ttime, node, ni, uvp )
         endif
      enddo Time_loop
!$    TIME_ALL = TIME_ALL + omp_get_wtime()
!$    write(6,*) 'Elapsed Time:',TIME_ALL
! =================================================================================================
!
      call output ( 51, iend, ttime, node, ni, uvp )
      close(50)
      close(51)
!
end program ns2d_sfem
!
!
!
! -------------------------------------------------------------------------------------------------
!     Input MESH & B.C. DATA
      subroutine datain (ista, iend, iout, rei, dt, dti )
! -------------------------------------------------------------------------------------------------
      use indata
      implicit none
      integer, intent(out)          :: ista, iend, iout
      double precision, intent(out) :: rei, dti, dt
      integer :: n, m, i, j, k, istatus
      double precision :: re
!
      read(10,*) ista, iend, iout
      read(10,*) dt,   re
      close(10)
      dti = 1.0d0 / dt
      rei = 1.0d0 / re
!
! ---------- Mesh Data ----------
      read(11,*) node, nelm
         allocate( xx(2,node), nc(3,nelm), stat=istatus )
         if( istatus /= 0 ) then
           write(6,*) '001: Allocation Failure ( Mesh )'
         endif
      read(11,*) ( n, ( xx(j,n), j = 1, 2 ), i = 1, node )
      read(11,*) ( m, ( nc(j,m), j = 1, 3 ), i = 1, nelm )
      close(11)
!
! ---------- Boundary Condition Data ----------
      read(12,*) ( iubc(i), i = 1, 2 )
         iubc_max = maxval(iubc(:))
         allocate( nubc(2,iubc_max), fubc(2,iubc_max), stat=istatus )
         if( istatus /= 0 ) then
           write(6,*) '002: Allocation Failure ( BC )'
         endif
      do i = 1, 2
         if(iubc(i) /= 0) then
             read(12,*) ( j, nubc(i,j), fubc(i,j), k = 1, iubc(i) )
         end if
      enddo
      close(12)
!
      end subroutine datain
!
!
! -------------------------------------------------------------------------------------------------
!     Input I.C. DATA
      subroutine initin (ista, node, ni, uvp, uu, ua, iubc, nubc, fubc, iubc_max )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(inout) :: ista
      integer, intent(in) :: node, ni(3), iubc_max, iubc(2), nubc(2,iubc_max)
      double precision, intent(in)  :: fubc(2,iubc_max)
      double precision, intent(out) :: uvp(node*3), uu(2,node), ua(2,node)
      integer :: i, j, n
!
! ---------- Initial Condition Data ----------
      if(ista == -1) then ! Impulsive start!
         ista = 0
         uvp(:)  = 0.0d0
      else
         read(30,*) ista
         do i = 1, node
            read(30,*) n,   ( uvp(n+ni(j)), j = 1, 3 )
         end do
      end if
!
! Impose Boundary Condition
      do i = 1, 2
         do n = 1, iubc(i)
            uvp(nubc(i,n)+ni(i)) = fubc(i,n)
         enddo
      enddo
!
      do n = 1, node
         do i = 1, 2
            uu(i,n) = uvp(n+ni(i))
            ua(i,n) = uvp(n+ni(i))
         enddo
      enddo
!
      end subroutine initin
!
!
! -------------------------------------------------------------------------------------------------
!     Step Change
      subroutine stepch ( node, ni, uvp, uu, ua )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: node, ni(3)
      double precision, intent(in) :: uvp(node*3)
      double precision, intent(inout) :: uu(2,node)
      double precision, intent(out)   :: ua(2,node)
      integer :: i, n
!
      do n = 1, node
        do i = 1, 2
          ua(i,n) = 1.5d0 * uvp(n+ni(i)) - 0.5d0 * uu(i,n) ! Adams-Bashforce Approximation
          uu(i,n) = uvp(n+ni(i))
        enddo
      enddo
!
      end subroutine stepch
!
!
! -------------------------------------------------------------------------------------------------
!     Zero Clear on Dirichlet Boundary 
      subroutine bound0( node, ni, uvp, iubc_max, iubc, nubc )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: node, ni(3), iubc_max, iubc(2), nubc(2,iubc_max)
      double precision, intent(inout) :: uvp(node*3)
      integer :: i, ib
      do i = 1, 2
         do ib = 1, iubc(i)
            uvp(nubc(i,ib)+ni(i)) = 0.0d0
         enddo
      enddo
      end subroutine bound0
!
! -------------------------------------------------------------------------------------------------
!     Calcurate Area & b, c
      subroutine makeSF ( node, nelm, xx, nc, area, bc )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: node, nelm, nc(3,nelm)
      double precision, intent(in) :: xx(2,node)
      double precision, intent(out) :: area(nelm), bc(2,3,nelm)
      integer :: m, n1, n2, n3
      double precision :: x1, x2, x3, y1, y2, y3, a02
!
!$OMP parallel do default(none) &
!$OMP             private(m, n1, n2, n3, x1, x2, x3, y1, y2, y3, a02) &
!$OMP             shared( nelm, nc, xx, area, bc )
      do m = 1, nelm
         n1 = nc(1,m)
         n2 = nc(2,m)
         n3 = nc(3,m)
         x1 = xx(1,n1)
         x2 = xx(1,n2)
         x3 = xx(1,n3)
         y1 = xx(2,n1)
         y2 = xx(2,n2)
         y3 = xx(2,n3)
         a02 = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)
         if ( a02 <= 0.d0 ) then
            write(6,*) '003: Area Failure',m
         endif
         area(m) = a02 * 0.5d0
         a02 = 1.0d0 / a02
         bc(1,1,m) = (y2 - y3) * a02
         bc(1,2,m) = (y3 - y1) * a02
         bc(1,3,m) = (y1 - y2) * a02
         bc(2,1,m) = (x3 - x2) * a02
         bc(2,2,m) = (x1 - x3) * a02
         bc(2,3,m) = (x2 - x1) * a02
      end do
!$OMP end parallel do
!
      end subroutine makeSF
!
!
! -------------------------------------------------------------------------------------------------
!     Stabilization Parametar
      subroutine maketau ( node, nelm, nc, area, bc, ua, dti, rei, tau )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: node, nelm, nc(3,nelm)
      double precision, intent(in) :: bc(2,3,nelm), area(nelm), &
                                      ua(2,node), rei, dti
      double precision, intent(out) :: tau(nelm)
      integer :: m, n1, n2, n3
      double precision :: a01, b1, b2, b3, c1, c2, c3, u, v, uv, d, t1, t2, t3
!
      t1 = 4.0 * dti * dti
!
!$OMP parallel do default(none) &
!$OMP             private(m, n1, n2, n3, a01, b1, b2, b3, c1, c2, c3, u, v, uv, d, t2, t3) &
!$OMP             shared(nelm, nc, area, bc, ua, t1, rei, tau)
      do m = 1, nelm
         n1 = nc(1,m)
         n2 = nc(2,m)
         n3 = nc(3,m)
         a01 = area(m)
         b1 = bc(1,1,m)
         b2 = bc(1,2,m)
         b3 = bc(1,3,m)
         c1 = bc(2,1,m)
         c2 = bc(2,2,m)
         c3 = bc(2,3,m)
!
         u = ( ua(1,n1) + ua(1,n2) + ua(1,n3) ) / 3.0d0
         v = ( ua(2,n1) + ua(2,n2) + ua(2,n3) ) / 3.0d0
         uv = u * u + v * v
         d = dabs( u * b1 + v * c1 ) &
           + dabs( u * b2 + v * c2 ) &
           + dabs( u * b3 + v * c3 )
!
         t2 = d * d
         if(uv  < 1.d-10) then
            t3 = (2.0d0 * rei / a01) ** 2.0d0
         else
            t3 = (rei * d * d / uv) ** 2.0d0
         end if
         tau(m) = 1.0d0 / dsqrt(t1 + t2 + t3)
      enddo
!$OMP end parallel do
!
      end subroutine maketau
!
!
! -------------------------------------------------------------------------------------------------
!     Make L.H.S. Matrix (ELLPACK)
      subroutine makLHS ( node, nelm, nc, area, bc, ua, dti, rei, tau, diag, ni )
! -------------------------------------------------------------------------------------------------
      USE ellpack_info, only: nrow_max, infn, infe, infc, infnc, nltable, &
                              coef_x, coef_y, coef_c
      implicit none
      integer, intent(in) :: node, nelm, nc(1:3,1:nelm), ni(1:3)
      double precision, intent(in) :: dti, rei, ua(1:2,1:node), tau(1:nelm)
      double precision, intent(in) :: area(1:nelm), bc(1:2,1:3,1:nelm)
      double precision, intent(out):: diag(1:node*3)
      integer :: n, i, n1, n2, n3, ne, nt, m1, m2, m3
      double precision :: &
          a01,     a03,     b1,      b2,      b3,      c1,      c2,      c3,     &
          ts,      tp,      ua1,     ua2,     ua3,     va1,     va2,     va3,    &
          emd,     emu,     emu1,    emu2,    emu3,    emv1,    emv2,    emv3,   &
          eMs_11,  eMs_12,  eMs_13,  eMp1_1,  eMp2_1, eA_11,   eA_12,   eA_13,   &
          easu1,   easv1,   eAs_11,  eAs_12,  eAs_13, eAp1_11, eAp1_12, eAp1_13, &
          eAp2_11, eAp2_12, eAp2_13, eD11_11, eD11_12, eD11_13,                  &
          eD12_11, eD12_12, eD12_13, eD12_21, eD12_31,                           &
          eD22_11, eD22_12, eD22_13,                                             &
          eG1_1,   eG1_2,   eG1_3,   eG2_1,   eG2_2,   eG2_3,                    &
          eGs1_11, eGs1_12, eGs1_13, eGs2_11, eGs2_12, eGs2_13,                  &
          tua,     tva,     at1,     eGp_11,  eGp_12,  eGp_13
!
!$OMP parallel default(none)                                                                 &
!$OMP          private(n, i, n1, n2, n3, m1, m2, m3, ne, nt,                                 &
!$OMP                  a01, a03, b1, b2, b3, c1, c2, c3, ts, tp,                             &
!$OMP                  ua1, ua2, ua3, va1, va2, va3, emd, emu,                               &
!$OMP                  emu1, emu2, emu3, emv1, emv2, emv3,                                   &
!$OMP                  eMs_11, eMs_12, eMs_13, eMp1_1, eMp2_1, easu1, easv1,                 &
!$OMP                  eA_11, eA_12, eA_13, eAs_11, eAs_12, eAs_13,                          &
!$OMP                  eAp1_11, eAp1_12, eAp1_13, eAp2_11, eAp2_12, eAp2_13,                 &
!$OMP                  eD11_11, eD11_12, eD11_13, eD22_11, eD22_12, eD22_13,                 &
!$OMP                  eD12_11, eD12_12, eD12_13, eD12_21, eD12_31,                          &
!$OMP                  eG1_1, eG1_2, eG1_3, eG2_1, eG2_2, eG2_3, tua, tva, at1,              &
!$OMP                  eGs1_11, eGs1_12, eGs1_13, eGs2_11, eGs2_12, eGs2_13,                 &
!$OMP                  eGp_11, eGp_12, eGp_13 )                                              &
!$OMP          shared( node, ni, nc, infn, infe, infc, infnc, nltable, nrow_max,             &
!$OMP                  dti, rei, area, tau, bc, ua, diag, coef_x, coef_y, coef_c )
     !$OMP do
      do n = 1, node*3
        diag(n) = 0.0d0
      enddo
     !$OMP end do
     !$OMP do
      do n = 1, node
        do i = 1, nrow_max*3
          coef_x(i, n) = 0.0d0
          coef_y(i, n) = 0.0d0
          coef_c(i, n) = 0.0d0
        enddo
      enddo
     !$OMP end do
!
     !$OMP do
      NODE_LOOP:do n = 1, node
         ENCIRCLE_LOOP:do i = 1, infn(n)
          ne = infe(i,n)
          nt = infc(i,n)
          n1 = nc(nt,ne)
          n2 = nc(nltable(nt,1),ne)
          n3 = nc(nltable(nt,2),ne)
          a01 = area(ne)
          a03 = area(ne) / 3.0d0
          ts = tau(ne)
          tp = tau(ne)
          b1 = bc(1,nt,ne)
          b2 = bc(1,nltable(nt,1),ne)
          b3 = bc(1,nltable(nt,2),ne)
          c1 = bc(2,nt,ne)
          c2 = bc(2,nltable(nt,1),ne)
          c3 = bc(2,nltable(nt,2),ne)
          ua1 = ua(1,n1)
          ua2 = ua(1,n2)
          ua3 = ua(1,n3)
          va1 = ua(2,n1)
          va2 = ua(2,n2)
          va3 = ua(2,n3)
!
! ---- M Matrix ----------
          emd = a03 * 0.5d0
          emu = a03 * 0.25d0
! ---- Ms Matrix ----------
          emu1 = emd * ua1 + emu * ua2 + emu * ua3
          emu2 = emu * ua1 + emd * ua2 + emu * ua3
          emu3 = emu * ua1 + emu * ua2 + emd * ua3
          emv1 = emd * va1 + emu * va2 + emu * va3
          emv2 = emu * va1 + emd * va2 + emu * va3
          emv3 = emu * va1 + emu * va2 + emd * va3
          eMs_11 = ts * (b1 * emu1 + c1 * emv1)
          eMs_12 = ts * (b1 * emu2 + c1 * emv2)
          eMs_13 = ts * (b1 * emu3 + c1 * emv3)
!
! ---- Mp Matrix ----------
          eMp1_1 = tp * b1 * a03
          eMp2_1 = tp * c1 * a03
!
! ---- A Matrix ----------
          eA_11 = emu1 * b1 + emv1 * c1
          eA_12 = emu1 * b2 + emv1 * c2
          eA_13 = emu1 * b3 + emv1 * c3
!
! ---- As Matrix ----------
          easu1 = eMs_11 * ua1 + eMs_12 * ua2 + eMs_13 * ua3
          easv1 = eMs_11 * va1 + eMs_12 * va2 + eMs_13 * va3
          eAs_11 = easu1 * b1 + easv1 * c1
          eAs_12 = easu1 * b2 + easv1 * c2
          eAs_13 = easu1 * b3 + easv1 * c3
!
! ---- Ap Matrix ----------
          tua = tp * (ua1 + ua2 + ua3) * a03
          tva = tp * (va1 + va2 + va3) * a03
!
          eAp1_11 = tua * b1 * b1 + tva * b1 * c1
          eAp1_12 = tua * b1 * b2 + tva * b1 * c2
          eAp1_13 = tua * b1 * b3 + tva * b1 * c3
          eAp2_11 = tua * c1 * b1 + tva * c1 * c1
          eAp2_12 = tua * c1 * b2 + tva * c1 * c2
          eAp2_13 = tua * c1 * b3 + tva * c1 * c3
!
! ---- D Matrix ----------
          eD11_11 = a01 * rei * (b1 * b1 + b1 * b1 + c1 * c1)
          eD11_12 = a01 * rei * (b1 * b2 + b1 * b2 + c1 * c2)
          eD11_13 = a01 * rei * (b1 * b3 + b1 * b3 + c1 * c3)
          eD22_11 = a01 * rei * (c1 * c1 + c1 * c1 + b1 * b1)
          eD22_12 = a01 * rei * (c1 * c2 + c1 * c2 + b1 * b2)
          eD22_13 = a01 * rei * (c1 * c3 + c1 * c3 + b1 * b3)
!
          eD12_11 = a01 * rei * c1 * b1
          eD12_12 = a01 * rei * c1 * b2
          eD12_13 = a01 * rei * c1 * b3
          eD12_21 = a01 * rei * c2 * b1
          eD12_31 = a01 * rei * c3 * b1
!
! ---- G & C Matrix ----------
          eG1_1 = b1 * a03 
          eG1_2 = b2 * a03
          eG1_3 = b3 * a03
          eG2_1 = c1 * a03
          eG2_2 = c2 * a03
          eG2_3 = c3 * a03
!
! ---- Gs Matrix ----------
          tua = ts * (ua1 + ua2 + ua3) * a03
          tva = ts * (va1 + va2 + va3) * a03
          at1 = tua * b1 + tva * c1
          eGs1_11 = at1 * b1
          eGs1_12 = at1 * b2
          eGs1_13 = at1 * b3
          eGs2_11 = at1 * c1
          eGs2_12 = at1 * c2
          eGs2_13 = at1 * c3
!
! ---- Gp Matrix ----------
          eGp_11 = tp * (b1 * b1 + c1 * c1) * a01
          eGp_12 = tp * (b1 * b2 + c1 * c2) * a01
          eGp_13 = tp * (b1 * b3 + c1 * c3) * a01
!
! ---- Matrix form ----
          n1 = 0
          n2 = nrow_max * 1
          n3 = nrow_max * 2
          m1 = 1
          m2 = infnc(1,i,n)
          m3 = infnc(2,i,n)

! Momentum X
          coef_x(n1+m1,n) = coef_x(n1+m1,n) &
                          + dti * (emd + eMs_11)  +  0.5d0 * (eA_11 + eAs_11 + eD11_11)
          coef_x(n1+m2,n) = coef_x(n1+m2,n) &
                          + dti * (emu + eMs_12)  +  0.5d0 * (eA_12 + eAs_12 + eD11_12)
          coef_x(n1+m3,n) = coef_x(n1+m3,n) &
                          + dti * (emu + eMs_13)  +  0.5d0 * (eA_13 + eAs_13 + eD11_13)
          coef_x(n2+m1,n) = coef_x(n2+m1,n) + 0.5d0 * eD12_11
          coef_x(n2+m2,n) = coef_x(n2+m2,n) + 0.5d0 * eD12_12
          coef_x(n2+m3,n) = coef_x(n2+m3,n) + 0.5d0 * eD12_13
          coef_x(n3+m1,n) = coef_x(n3+m1,n) - eG1_1 + eGs1_11
          coef_x(n3+m2,n) = coef_x(n3+m2,n) - eG1_1 + eGs1_12
          coef_x(n3+m3,n) = coef_x(n3+m3,n) - eG1_1 + eGs1_13

! Momentum Y
          coef_y(n1+m1,n) = coef_y(n1+m1,n) + 0.5d0 * eD12_11
          coef_y(n1+m2,n) = coef_y(n1+m2,n) + 0.5d0 * eD12_21
          coef_y(n1+m3,n) = coef_y(n1+m3,n) + 0.5d0 * eD12_31
          coef_y(n2+m1,n) = coef_y(n2+m1,n) &
                          + dti * (emd + eMs_11)  +  0.5d0 * (eA_11 + eAs_11 + eD22_11)
          coef_y(n2+m2,n) = coef_y(n2+m2,n) &
                          + dti * (emu + eMs_12)  +  0.5d0 * (eA_12 + eAs_12 + eD22_12)
          coef_y(n2+m3,n) = coef_y(n2+m3,n) &
                          + dti * (emu + eMs_13)  +  0.5d0 * (eA_13 + eAs_13 + eD22_13)
          coef_y(n3+m1,n) = coef_y(n3+m1,n) - eG2_1 + eGs2_11
          coef_y(n3+m2,n) = coef_y(n3+m2,n) - eG2_1 + eGs2_12
          coef_y(n3+m3,n) = coef_y(n3+m3,n) - eG2_1 + eGs2_13

! Continuity
          coef_c(n1+m1,n) = coef_c(n1+m1,n) + eG1_1 + dti * eMp1_1 + 0.5d0 * eAp1_11
          coef_c(n1+m2,n) = coef_c(n1+m2,n) + eG1_2 + dti * eMp1_1 + 0.5d0 * eAp1_12
          coef_c(n1+m3,n) = coef_c(n1+m3,n) + eG1_3 + dti * eMp1_1 + 0.5d0 * eAp1_13
          coef_c(n2+m1,n) = coef_c(n2+m1,n) + eG2_1 + dti * eMp2_1 + 0.5d0 * eAp2_11
          coef_c(n2+m2,n) = coef_c(n2+m2,n) + eG2_2 + dti * eMp2_1 + 0.5d0 * eAp2_12
          coef_c(n2+m3,n) = coef_c(n2+m3,n) + eG2_3 + dti * eMp2_1 + 0.5d0 * eAp2_13
          coef_c(n3+m1,n) = coef_c(n3+m1,n) + eGp_11
          coef_c(n3+m2,n) = coef_c(n3+m2,n) + eGp_12
          coef_c(n3+m3,n) = coef_c(n3+m3,n) + eGp_13
        enddo ENCIRCLE_LOOP
! ---------- for Scaling ----------
        diag(n+ni(1)) = coef_x(1,           n)
        diag(n+ni(2)) = coef_y(1+nrow_max,  n)
        diag(n+ni(3)) = coef_c(1+nrow_max*2,n)
      enddo NODE_LOOP
     !$OMP end do
!$OMP end parallel
!st3      do n = 1, node*3
!st3        write(99,'(i7,f20.10)') n, diag(n)
!st3      enddo
!
! Diagonal Scaling
      call dscale ( node, nrow_max, ni, coef_x, coef_y, coef_c, diag )
!
      end subroutine makLHS
!
! -------------------------------------------------------------------------------------------------
!     Make R.H.S. Known Vector
      subroutine makRHS ( node, nelm, nc, area, bc, ua, dti, rei, tau, uu, bv, ni )
! -------------------------------------------------------------------------------------------------
      USE ellpack_info, only: nrow_max, infn, infe, infc, infnc, nltable
      implicit none
      integer, intent(in) :: node, nelm, nc(3,nelm), ni(3)
      double precision, intent(in) :: dti, rei, ua(2,node), uu(2,node),     &
                                      bc(2,3,nelm), area(nelm), tau(nelm)
      double precision, intent(out) :: bv(node*3)
      integer :: n, i, n1, n2, n3, ne, nt
      double precision :: a01, a03, b1, b2, b3, c1, c2, c3, ts, tp,         &
         ua1, ua2, ua3, va1, va2, va3, u1, u2, u3, v1, v2, v3,              &
         emd, emu, emua1, emua2, emua3, emva1, emva2, emva3,                &
         dxu, dyu, dxv, dyv, sxx, sxy, syy, tumu, tvmu, tumv, tvmv,         &
         tu, tv, atu, atv, stxu, stxv, styu, styv,                          &
         eMu1,  eMv1, eAu1, eAv1, eDu1, eDv1, eMsu1, eMsv1, eAsu1, eAsv1,   &
         eMp1,  eAp1
!
!$OMP parallel default(none) &
!$OMP          private(n, i, n1, n2, n3, ne, nt,                                  &
!$OMP                  a01, a03, b1, b2, b3, c1, c2, c3, ts, tp,                  &
!$OMP                  u1, u2, u3, v1, v2, v3, ua1, ua2, ua3, va1, va2, va3,      &
!$OMP                  emd, emu, emua1, emua2, emua3, emva1, emva2, emva3,        &
!$OMP                  dxu, dyu, dxv, dyv, sxx, sxy, syy, tumu, tvmu, tumv, tvmv, &
!$OMP                  tu, tv, atu, atv, stxu, stxv, styu, styv,                  &
!$OMP                  eMu1,  eMv1, eAu1, eAv1, eDu1, eDv1,                       &
!$OMP                  eMsu1, eMsv1, eAsu1, eAsv1, eMp1, eAp1)                    &
!$OMP          shared (node, ni, nc, infn, infe, infc, nltable, nrow_max,         &
!$OMP                  dti, rei, area, tau, bc, uu, ua, bv)
!      
     !$OMP do
      do n = 1, node*3
        bv(n) = 0.0d0
      enddo
     !$OMP enddo
     !$OMP do
      NODE_LOOP:do n = 1, node
         ENCIRCLE_LOOP:do i = 1, infn(n)
          ne = infe(i,n)
          nt = infc(i,n)
          n1 = nc(nt,ne)
          n2 = nc(nltable(nt,1),ne)
          n3 = nc(nltable(nt,2),ne)
          a01 = area(ne)
          a03 = area(ne) / 3.0d0
          b1 = bc(1,nt,ne)
          b2 = bc(1,nltable(nt,1),ne)
          b3 = bc(1,nltable(nt,2),ne)
          c1 = bc(2,nt,ne)
          c2 = bc(2,nltable(nt,1),ne)
          c3 = bc(2,nltable(nt,2),ne)
!
          u1  = uu(1,n1)
          u2  = uu(1,n2)
          u3  = uu(1,n3)
          v1  = uu(2,n1)
          v2  = uu(2,n2)
          v3  = uu(2,n3)
          ua1 = ua(1,n1)
          ua2 = ua(1,n2)
          ua3 = ua(1,n3)
          va1 = ua(2,n1)
          va2 = ua(2,n2)
          va3 = ua(2,n3)

          emd = a03 * 0.5d0
          emu = a03 * 0.25d0
!
          dxu = b1 * u1 + b2 * u2 + b3 * u3
          dyu = c1 * u1 + c2 * u2 + c3 * u3
          dxv = b1 * v1 + b2 * v2 + b3 * v3
          dyv = c1 * v1 + c2 * v2 + c3 * v3
!
! ---------- Galerkin term ----------
! M Term
          eMu1 = emd * u1 + emu * u2 + emu * u3
          eMv1 = emd * v1 + emu * v2 + emu * v3
!
! A Term
          emua1 = emd * ua1 + emu * ua2 + emu * ua3
          emua2 = emu * ua1 + emd * ua2 + emu * ua3
          emua3 = emu * ua1 + emu * ua2 + emd * ua3
          emva1 = emd * va1 + emu * va2 + emu * va3
          emva2 = emu * va1 + emd * va2 + emu * va3
          emva3 = emu * va1 + emu * va2 + emd * va3
          eAu1  = emua1 * dxu + emva1 * dyu
          eAv1  = emua1 * dxv + emva1 * dyv
!
! D Term
          sxx  = rei * (dxu + dxu) * a01
          sxy  = rei * (dyu + dxv) * a01
          syy  = rei * (dyv + dyv) * a01
          eDu1 = b1 * sxx + c1 * sxy
          eDv1 = b1 * sxy + c1 * syy
!
          bv(n+ni(1)) = bv(n+ni(1)) + eMu1 * dti  -  0.5d0 * (eAu1 + eDu1)
          bv(n+ni(2)) = bv(n+ni(2)) + eMv1 * dti  -  0.5d0 * (eAv1 + eDv1)
!
! ---------- Stabilized term ----------
          ts = tau(ne)
          tp = tau(ne)
!
! Ms Term
          tumu  = ts * (emua1 * u1 + emua2 * u2 + emua3 * u3)
          tvmu  = ts * (emva1 * u1 + emva2 * u2 + emva3 * u3)
          tumv  = ts * (emua1 * v1 + emua2 * v2 + emua3 * v3)
          tvmv  = ts * (emva1 * v1 + emva2 * v2 + emva3 * v3)
          eMsu1 = b1 * tumu + c1 * tvmu
          eMsv1 = b1 * tumv + c1 * tvmv
! Mp Term
          tu   = tp * (u1 + u2 + u3) * a03
          tv   = tp * (v1 + v2 + v3) * a03
          eMp1 = b1 * tu + c1 * tv
!
! As Term
          tumu  = ts * (ua1 * emua1 + ua2 * emua2 + ua3 * emua3)
          tumv  = ts * (ua1 * emva1 + ua2 * emva2 + ua3 * emva3)
          tvmv  = ts * (va1 * emva1 + va2 * emva2 + va3 * emva3)
          stxu  = tumu * dxu + tumv * dyu
          styu  = tumv * dxu + tvmv * dyu
          stxv  = tumu * dxv + tumv * dyv
          styv  = tumv * dxv + tvmv * dyv
          eAsu1 = b1 * stxu + c1 * styu
          eAsv1 = b1 * stxv + c1 * styv
!
! Ap Term
          tu   = tp * (ua1 + ua2 + ua3) * a03
          tv   = tp * (va1 + va2 + va3) * a03
          atu  = tu * dxu + tv * dyu
          atv  = tu * dxv + tv * dyv
          eAp1 = b1 * atu + c1 * atv
!
          bv(n+ni(1)) = bv(n+ni(1)) + eMsu1 * dti  -  0.5d0 * eAsu1
          bv(n+ni(2)) = bv(n+ni(2)) + eMsv1 * dti  -  0.5d0 * eAsv1
          bv(n+ni(3)) = bv(n+ni(3)) + eMp1  * dti  -  0.5d0 * eAp1
        enddo ENCIRCLE_LOOP
      enddo NODE_LOOP
     !$OMP end do
!$OMP end parallel

!st3      do n = 1, node*3
!st3        write(89,'(i7,f20.10)') n, bv(n)
!st3      enddo
!st3      stop
!
      end subroutine makRHS
!
!
! -------------------------------------------------------------------------------------------------
!     Diagonal Scaling System
      subroutine dscale ( node, nrow_max, ni, coef_x, coef_y, coef_c, diag )
! -------------------------------------------------------------------------------------------------
      USE ellpack_info, only: jcoef, d=>wkg_row
      implicit none
      integer, intent(in) :: node, nrow_max, ni(1:3)
      double precision, intent(inout) :: coef_x(1:nrow_max*3,1:node)
      double precision, intent(inout) :: coef_y(1:nrow_max*3,1:node)
      double precision, intent(inout) :: coef_c(1:nrow_max*3,1:node)
      double precision, intent(inout) :: diag(1:node*3)
      integer :: i, j
      double precision :: dx, dy, dc
!
!$OMP parallel default(none) &
!$OMP          private(i, j, dx, dy, dc, d ) &
!$OMP          shared (node, nrow_max, ni, jcoef, diag, coef_x, coef_y, coef_c )
!$    allocate( d(1:nrow_max*3) )
!$OMP do
      do i = 1, node*3
        diag(i) = 1.0d0 / dsqrt(dabs(diag(i)))
      enddo
!$OMP end do
!
!$OMP do
      do i = 1, node
        do j = 1, nrow_max
          d(j           ) = diag(jcoef(j,i)+ni(1))
          d(j+nrow_max  ) = diag(jcoef(j,i)+ni(2))
          d(j+nrow_max*2) = diag(jcoef(j,i)+ni(3))
        enddo
        dx = diag(i+ni(1))
        dy = diag(i+ni(2))
        dc = diag(i+ni(3))
        do j = 1, nrow_max*3
          coef_x(j,i) = coef_x(j,i) * dx * d(j)
          coef_y(j,i) = coef_y(j,i) * dy * d(j)
          coef_c(j,i) = coef_c(j,i) * dc * d(j)
        enddo
      enddo
!$OMP end do
!$    deallocate( d )
!$OMP end parallel
      end subroutine dscale
!
! -------------------------------------------------------------------------------------------------
!     Solver( GPBi-CG Method)
      subroutine GPBiCG ( node, x, b, d, ni, ibc, ibc_max, nbc, &
                          r, r0, p, t, y, u, z, w, Ap, At, k )
! -------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: node, ni(3), ibc_max, ibc(2), nbc(2,ibc_max)
      double precision, intent(in) :: d(node*3), b(node*3)
      double precision, intent(inout) :: x(node*3)
      double precision, intent(out)   :: r(node*3),  r0(node*3), p(node*3), t(node*3)
      double precision, intent(out)   :: y(node*3),  u(node*3),  z(node*3), w(node*3)
      double precision, intent(out)   :: Ap(node*3), At(node*3)
      integer, intent(out) :: k
      double precision :: r_r, r0_r, r0_r0, y_y, At_t, At_At, y_t, y_At
      double precision :: alph, beta, zeta, eta, pk, eps

      integer :: n
!
      data eps / 1.0d-6 /
      integer :: ndof

      ndof = node * 3
!
!$OMP parallel do default(none) private(n) shared(ndof, x,b,r,d)
      do n = 1, ndof
        x(n) = x(n) / d(n)
        r(n) = b(n) * d(n)
      enddo
!$OMP end parallel do
      call bound0 ( node, ni, r, ibc_max, ibc, nbc )
      call matvec ( node, ni, x, Ap, ibc_max, ibc, nbc )
      r0_r = 0.0d0
!$OMP parallel do default(none) private(n) shared(ndof, r,Ap,r0,p,u,t,w,z) &
!$OMP             reduction(+:r0_r)
      do n = 1, ndof
        r(n) = r(n) - Ap(n)
        r0(n) = r(n)
        r0_r = r0_r + r0(n) * r(n) !dot_product(r0,r)
        p(n)  = 0.0d0
        u(n)  = 0.0d0
        t(n)  = 0.0d0
        w(n)  = 0.0d0
        z(n)  = 0.0d0
      enddo
!$OMP end parallel do
      beta  = 0.0d0
      pk = 0.0d0
!================================================================
      k = 0
      if( dsqrt(r0_r) > eps ) then
        do  k = 1, ndof * 3
        
!$OMP parallel do default(none) private(n) shared(ndof,p,r,beta,u)
          do n = 1, ndof
            p(n) = r(n) + beta * ( p(n) - u(n) )
          enddo
!$OMP end parallel do
          call matvec ( node, ni, p, Ap, ibc_max, ibc, nbc )

          alph = 0.0d0
!$OMP parallel do default(none) private(n) shared(ndof,r0,Ap) reduction(+:alph)
          do n = 1, ndof
            alph = alph + r0(n) * Ap(n)
          enddo
!$OMP end parallel do
          alph = r0_r / alph
!$OMP parallel do default(none) private(n) shared(ndof,y,t,r,alph,w,Ap,u,beta)
          do n = 1, ndof
            y(n) = t(n) - r(n) - alph * w(n) + alph * Ap(n)
            u(n) = t(n) - r(n) + beta * u(n)
            t(n) = r(n) - alph * Ap(n)
          enddo
!$OMP end parallel do
!
          call matvec ( node, ni, t, At, ibc_max, ibc, nbc )
!
          y_y   = 0.0d0
          At_t  = 0.0d0
          At_At = 0.0d0
          y_t   = 0.0d0
          y_At  = 0.0d0
!$OMP parallel do default(none) private(n) shared(ndof,y,At,t) &
!$OMP             reduction(+:y_y,At_t,At_At,y_t,y_At)
          do n = 1, ndof
            y_y   = y_y   + y(n) * y(n)
            At_t  = At_t  + At(n)* t(n)
            At_At = At_At + At(n)* At(n)
            y_t   = y_t   + y(n) * t(n)
            y_At  = y_At  + y(n) * At(n)
          enddo
!$OMP end parallel do

          zeta = ( y_y   * At_t - y_t  * y_At * pk ) / ( At_At * y_y - y_At * y_At * pk )
          eta  = ( At_At * y_t  - y_At * At_t      ) / ( At_At * y_y - y_At * y_At ) * pk
          pk = 1.0d0
          r_r = 0.0d0
!$OMP parallel do default(none) private(n) shared(ndof,u,zeta,Ap,eta,z,r,x,alph,p,t,y,At) &
!$OMP             reduction(+:r_r)
          do n = 1, ndof
            u(n) = zeta * Ap(n) + eta * u(n)
            z(n) = zeta * r(n)  + eta * z(n) - alph * u(n)
            x(n) = x(n) + alph * p(n) + z(n)
            r(n) = t(n) - eta  * y(n) - zeta * At(n)
            r_r = r_r + r(n) * r(n) !dot_product(r,r)
          enddo
!$OMP end parallel do
!CHECK        write(6,*) k, dsqrt(r_r)
        if( dsqrt(r_r) <= eps ) exit      ! Convergence Check!
            r0_r0 = r0_r
            r0_r = 0.0d0
!$OMP parallel do default(none) private(n) shared(ndof,r0,r) reduction(+:r0_r)
          do n = 1, ndof
            r0_r = r0_r + r0(n) * r(n) !dot_product(r0,r)
          enddo
!$OMP end parallel do
            beta = alph / zeta * r0_r / r0_r0
!$OMP parallel do default(none) private(n) shared(ndof,w,At,beta,Ap)
          do n = 1, ndof
            w(n) = At(n) + beta * Ap(n)
          enddo
!$OMP end parallel do
        enddo
        if( k == ndof*3 + 1 ) then
           write(6,*) 'Not Convergence (GPBi-CG)'
           stop
        endif
      endif
!================================================================
!$OMP parallel do default(none) private(n) shared(ndof,x,d)
      do n = 1, ndof
        x(n) = x(n) * d(n)
      enddo
!$OMP end parallel do
!
      end subroutine GPBiCG
!
!
! --------------------------------------------------------------------------------------------------
!     Matrix-Vector Product
      subroutine matvec ( node, ni, x, r, ibc_max, ibc, nbc )
! --------------------------------------------------------------------------------------------------
      USE ellpack_info, only: nrow_max, coef_x, coef_y, coef_c, jcoef, xj=>wkg_row
      implicit none
      integer, intent(in) :: node, ni(1:3)
      integer, intent(in) :: ibc_max, ibc(1:2), nbc(1:2,1:ibc_max)
      double precision, intent(in)  :: x(1:node*3)
      double precision, intent(out) :: r(1:node*3)
      integer :: i, j
!
!$OMP parallel default(none) private(i,j,xj) &
!$OMP          shared(node,nrow_max,ni,x,jcoef,r,coef_x,coef_y,coef_c)
!$   allocate( xj(1:nrow_max*3) )
     !$OMP do
      do i = 1, node*3
        r(i) = 0.0d0
      enddo
     !$OMP enddo

     !$OMP do
      do i = 1, node
        do j = 1, nrow_max
          xj(j           ) = x(jcoef(j,i)+ni(1))
          xj(j+nrow_max  ) = x(jcoef(j,i)+ni(2))
          xj(j+nrow_max*2) = x(jcoef(j,i)+ni(3))
        enddo
        do j = 1, nrow_max * 3
          r(i+ni(1)) = r(i+ni(1)) + coef_x(j,i) * xj(j)
          r(i+ni(2)) = r(i+ni(2)) + coef_y(j,i) * xj(j)
          r(i+ni(3)) = r(i+ni(3)) + coef_c(j,i) * xj(j)
        enddo
      enddo
     !$OMP enddo
!$   deallocate( xj )
!$OMP end parallel
!
      call bound0 ( node, ni, r, ibc_max, ibc, nbc )
!
      end subroutine matvec
!
!
! --------------------------------------------------------------------------------------------------
!     Result Output
      subroutine output (iw, istep, ttime, node, ni, uvp)
! --------------------------------------------------------------------------------------------------
      implicit none
      integer, intent(in) :: iw, istep, node, ni(3)
      double precision, intent(in) :: ttime, uvp(node*3)
      integer :: n, i

      write(iw,500) istep, ttime
      do n = 1, node
         write(iw,500) n, (uvp(n+ni(i)), i=1,3)
      enddo
  500 format(i7,3e15.6)
!
      end subroutine output
!
!
!-----+---------+---------+---------+---------+---------+---------+---------+---------+
  subroutine makNET( node, nelm, nc ) !NET: Node-Element Table
!-----+---------+---------+---------+---------+---------+---------+---------+---------+
      USE ellpack_info
      implicit none
      integer, intent(in) :: node, nelm, nc(1:3,1:nelm)
      integer :: infn_max, infnn_max, infnn
      integer :: i, j, n, m, i1, i2, n1, n2, m1, m2, ne, nt
!
      allocate( infn(1:node) )
      infn(1:node) = 0
      do m = 1, nelm
         do i = 1, 3
            infn(nc(i,m)) = infn(nc(i,m)) + 1
         enddo
      enddo
      infn_max = maxval(infn)
      allocate( infe(infn_max,node), infc(infn_max,node) )
!$OMP parallel default(none) shared(infn,infe,infc)
     !$OMP workshare
      infn(:) = 0
     !$OMP end workshare
     !$OMP workshare
      infe(:,:) = 0
     !$OMP end workshare
     !$OMP workshare
      infc(:,:) = 0
     !$OMP end workshare
!$OMP end parallel
      do m = 1, nelm
         do i = 1, 3
            infn(nc(i,m)) = infn(nc(i,m)) + 1
            infe(infn(nc(i,m)), nc(i,m)) = m
            infc(infn(nc(i,m)), nc(i,m)) = i
         enddo
      enddo
!
      nltable(1,1) = 2; nltable(1,2) = 3
      nltable(2,1) = 3; nltable(2,2) = 1
      nltable(3,1) = 1; nltable(3,2) = 2
!
      nrow_max = 1
      infnn_max = infn_max + 2
      allocate( infnc(2,infn_max,node) )
 666  allocate( jcoef(infnn_max,node) )
      do n = 1, node
         infnn = 1
         jcoef(1,n) = n
         do i = 1, infn(n)
            ne = infe(i,n)
            nt = infc(i,n)
            n1 = nc(nltable(nt,1),ne)
            n2 = nc(nltable(nt,2),ne)
            i1 = 0; i2 = 0
            do j = 1, i - 1
               m1 = nc(nltable(infc(j,n),1),infe(j,n))
               m2 = nc(nltable(infc(j,n),2),infe(j,n))
               if( n1 == m2 ) then
                  i1 = 1
                  infnc(1,i,n) = infnc(2,j,n)
               endif
               if( n2 == m1 ) then
                  i2 = 1
                  infnc(2,i,n) = infnc(1,j,n)
               endif
            enddo
            if( i1 == 0 ) then
               if(infnn >= infnn_max) then
                  infnn_max = infnn_max + 2
                  deallocate( jcoef )
                  goto 666
               endif
               infnn = infnn + 1
               jcoef(infnn,n) = n1
               infnc(1,i,n) = infnn
            endif
            if( i2 == 0 ) then
               if(infnn >= infnn_max) then
                  infnn_max = infnn_max + 2
                  deallocate( jcoef )
                  goto 666
               endif
               infnn = infnn + 1
               jcoef(infnn,n) = n2
               infnc(2,i,n) = infnn
            endif
         enddo
         nrow_max = max(nrow_max, infnn)
         jcoef(infnn+1:infnn_max,n) = n
      enddo

      allocate( coef_x(1:nrow_max*3,1:node) )
      allocate( coef_y(1:nrow_max*3,1:node) )
      allocate( coef_c(1:nrow_max*3,1:node) )
      allocate( wkg_row(1:nrow_max*3) )
!$    deallocate( wkg_row )
!
  end subroutine makNET 
!
!-----+---------+---------+---------+---------+---------+---------+---------+---------+
