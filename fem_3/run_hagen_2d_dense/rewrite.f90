module indata
  implicit none
  integer :: node, nelm ! 節点数、要素数
  double precision, allocatable :: xx(:,:) ! 節点座標
  integer, allocatable :: nc(:,:) ! 要素結合情報
  integer :: iubc(1:2), iubc_max ! 境界条件を課す節点数、その最大値
  integer, allocatable :: nubc(:,:) ! 境界条件を課す節点番号
  double precision, allocatable :: fubc(:,:) ! 境界条件を課す値
end module indata

module ellpack_info
  implicit none
  integer :: nrow_max ! 1自由度あたりの左辺非ゼロ成分数
  integer :: nltable(1:3,1:2) ! これなに？
  integer, allocatable :: infn(:) ! 節点n周辺の要素総数 ! 割り付け配列
  integer, allocatable :: infe(:,:) ! 節点n周辺の要素番号m
  integer, allocatable :: infc(:,:) ! 要素mにおける節点nの要素内節点番号
  integer, allocatable :: infnc(:,:,:) ! 左辺圧縮行列の非ゼロ成分位置
  integer, allocatable :: jcoef(:,:) ! 左辺非ゼロ成分の全体系節点番号
  double precision, allocatable :: coef_x(:,:) ! 左辺行列(ELLPACK形式) (Mom(x))
  double precision, allocatable :: coef_y(:,:) ! 左辺行列(ELLPACK形式) (Mom(x))
  double precision, allocatable :: coef_c(:,:) ! 左辺行列(ELLPACK形式) (Cont)
  double precision, allocatable :: wkg_row(:)  ! ワーキングベクトル(1:nrow_max*3)
end module ellpack_info
!
!
!
!
program ns2d_sfem
!$ use omp_lib
  use indata
  implicit none
  double precision st, en
  double precision :: rei, dt, dti ! 1/Re, 時間増分量, 1/dt
  integer :: ista, iend, iout, istep ! 解析開始Step, 解析終了Step, 結界出力間隔、現在Step
  double precision :: ttime ! 現時刻
  double precision, allocatable :: bc(:,:,:), area(:) ! dN/dx(dN/dy), 要素面積
  double precision, allocatable :: tau(:) ! 安定化パラメータ
  double precision, allocatable :: uu(:,:), ua(:,:) ! 既知流速、移流速度
  double precision, allocatable :: diag(:) ! 左辺行列対角成分
  double precision, allocatable :: uvp(:), bv(:) ! 未治療ベクトル、右舷ベクトル
  integer :: ni(3) ! 未治療ベクトル内の(u,v,p)の先頭アドレス
  double precision, allocatable :: &
                  w01(:), w02(:), w03(:), w04(:), w05(:), &
                  w06(:), w07(:), w08(:), w09(:), w10(:) ! GPBi-CG法でのワーキングベクトル
  integer :: i,kcg
  character(50) :: inpfile, meshfile, bdcfile, stafile, endfile
  character(50) :: resfile
  character filename * 128

!
!$ double precision :: time_all
!$ write(6,"(a,i4,a)") "OpenMP:", omp_get_max_threads(), "Threads"

!$ st = omp_get_wtime()

!
!
! === I?O file control ===
  open(9,file="file.dat",status="old",action="read")
  read(9,"(a)") inpfile ! 計算条件のデータがどこにあるかを文字列としてinpfileに格納(読み取る)
  read(9,"(a)") meshfile ! メッシュデータがどこにあるかを文字列としてmesfileに格納(読み取る)
  read(9,"(a)") bdcfile ! 境界条件データがどこにあるかを文字列としてbdvfileに格納(読み取る)
  read(9,"(a)") stafile ! 初期条件データがどこにあるかを文字列としてstafileに格納(読み取る)
  ! read(9,"(a)") resfile ! 結果出力ファイル(ioutステップごと)
  read(9,"(a)") endfile ! 最終結果ファイル(iendステップ)
  open(10,file=inpfile,status="old",action="read")
  open(11,file=meshfile,status="old",action="read")
  open(12,file=bdcfile,status="old",action="read")
  ! opne(50,file=resfile,status="replace",action="write")
  open(51,file=endfile,status="replace",action="write")
! ===
!
! === datainput
  call datain(ista, iend, iout, rei, dt, dti)
  call makNET(node, nelm, nc) ! make net infl
!
  stop "end program"
! === dynamic memory allocate
  allocate(uu(1:2, 1:node), ua(1:2, 1:node))
  allocate(bc(1:2, 1:3, 1:nelm), area(1:nelm), tau(1:nelm))
  allocate(diag(1:node*3), uvp(1:node*3), bv(1:node*3))
  allocate(w01(1:node*3), w02(1:node*3), w03(1:node*3), w04(1:node*3), w05(1:node*3), &
           w06(1:node*3), w07(1:node*3), w08(1:node*3), w09(1:node*3), w10(1:node*3))
  do i = 1, 3
    ni(i) = node * (i - 1) ! index of u, v, p
  end do
  if(ista /= -1) open(30,file=stafile,status="old",action="read")
  call initin(ista, node, ni, uvp, uu, ua, iubc, nubc, fubc, iubc_max)
  call makeSP(node, nelm, xx, nc, area, bc)
!
! ==========================================================
!$ time_all = - omp_get_wtime()
  time_loop : do istep = ista + 1, iend
    ttime = dble(istep) * dt
!
    call maktau(node, nelm, nc, area, bc, ua, dti, rei, tau)
    call makLHS(node, nelm, nc, area, bc, ua, dti, rei, tau, diag, ni)
    call makRHS(node, nelm, nc, area, bc, ua, dti, rei, tau, uu, bv,  ni)
    call GPBiCG(node, uvp, bc, diag, ni, iubc, iubc_max, nubc, &
                w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, kcg)
    call stepch(node, ni, uvp, uu, ua)
!
    write(*,*) "=== STEP INFO ==="
    write(*,*) "STEP   =", istep
    write(*,*) "TIME   =", ttime
    write(*,*) "K(BiCG)=", kcg
!
    ! if(istep / iout * iout == istep) then
    !   call output(50, istep, ttime, node, ni, uvp, xx)
    ! end if
    if(mod(istep,10) == 0) then
      write(filename,'("res_omp/cylinder_",i4.4,".vtk")') istep / 10
      open(50,file=filename,status="replace",action="write")
        call output(50, istep, ttime, node, ni, uvp, xx)
      close(50)
    end if
  end do time_loop
!$ time_all = time_all + omp_get_wtime()
!$ write(6,*) "Elapsed Time:", time_all
! ==========================================================
!
  write(filename,'("res_omp/cylinder_",i4.4,".vtk")') istep / 10
  open(50,file=filename,status="replace",action="write")
    call output(50, istep, ttime, node, ni, uvp, xx)
  close(50)
! call output(51, iend, ttime, node, ni, uvp, xx)
  close(51)
!
!$ en = omp_get_wtime()
  write(*,*) "Elapsed time in second is:", en - st
!
end program ns2d_sfem
!
! ここでmain文は終了
! ここからは関数(subroutine)
!
!
!
!
! =========================================================
! input mesh & b.c. data
  subroutine datain(ista, iend, iout, rei, dt, dti)
! =========================================================
    use indata
    implicit none
    integer, intent(out) :: ista, iend, iout
    double precision, intent(out) :: rei, dti, dt
    integer :: n, m, i, j, k, istatus
    double precision :: re
!
    read(10,*) ista, iend, iout
    read(10,*) dt, re
    close(10)
    dti = 1.0d0 / dt
    rei = 1.0d0 / re
!
    read(11,*) node, nelm
      allocate(xx(2,node), nc(3,nelm), stat=istatus)
      if(istatus /= 0) then
        write(6,*) "001: Allocation Failure (Mesh)"
      end if
    read(11,*) (n, (xx(j, n), j = 1, 2), i = 1, node)
    read(11,*) (m, (nc(j, m), j = 1, 3), i = 1, nelm)
    close(11)
!
! === Boundary Condition Data ===
    read(12,*) (iubc(i), i = 1, 2)
    iubc_max = maxval(iubc(:))
    allocate(nubc(2, iubc_max), fubc(2, iubc_max), stat=istatus)
    if(istatus /= 0) then
      write(6,*) "002: Allocation Failure (BC)"
    end if
    do i = 1, 2
      if(iubc(i) /= 0) then
        read(12,*) (j, nubc(i, j), fubc(i, j), k = 1, iubc(i))
      end if
    end do
    close(12)
!
  end subroutine datain
!
!
!
! =============================================================
! input i.c. data
  subroutine initin(ista, node, ni, uvp, uu, ua, iubc, nubc, fubc, iubc_max)
! =====================================================================
    implicit none
    integer, intent(inout) :: ista
    integer, intent(in) :: node, ni(3), iubc_max, iubc(2), nubc(2, iubc_max)
    double precision, intent(in) :: fubc(2, iubc_max)
    double precision, intent(out) :: uvp(node * 3), uu(2, node), ua(2, node)
    integer :: i, j, n
!
! === initial condition data ===
    if(ista == -1) then
      ista = 0
      uvp(:) = 0.0d0
    else
      read(30,*) ista
      do i = 1, node
        read(30,*) n, (uvp(n + ni(j)), j = 1, 3)
      end do
    end if
!
! inpose boundary condition
    do i = 1, 2
      do n = 1, iubc(i)
        uvp(nubc(i, n) + ni(i)) = fubc(i, n)
      end do
    end do
!
    do n = 1, node
      do i = 1, 2
        uu(i, n) = uvp(n + ni(i))
        ua(1, n) = uvp(n + ni(i))
      end do
    end do
!
  end subroutine initin
!
!
!
!
!
! ===================================
  subroutine makNET(node, nelm, nc) ! NET: Node-Element Table
! ===================================
    use ellpack_info ! モジュールの読み込み
    implicit none
    integer, intent(in) :: node, nelm, nc(1:3, 1:nelm)
    integer :: infn_max, infnn_max, infnn
    integer :: i, j, m, n, i1, i2, n1, n2, m1, m2, ne, nt

    allocate(infn(1:node))
    infn(1:node) = 0
    do m = 1, nelm
      do i = 1, 3
        infn(nc(i, m)) = infn(nc(i, m)) + 1
      end do
    end do
    infn_max = maxval(infn)
    allocate(infe(infn_max, node), infc(infn_max, node))
!$ omp parallel default(none) shared(infn, infe, infc)
    !$ omp workshare
    infn(:) = 0
    !$ omp end workshare
    !$ omp workshare
    infe(:, :) = 0
    !$ omp end workshare
    !$ omp workshare
    infc(:, :) = 0
    !$ omp end workshare
!$ omp end parallel
    do m = 1, nelm
      do i = 1, 3
        infn(nc(i, m)) = infn(nc(i, m)) + 1
        infe(infn(nc(i, m)), nc(i, m)) = m
        infc(infn(nc(i, m)), nc(i, m)) = i
      end do
    end do
!
    nltable(1, 1) = 2;
    nltable(1, 2) = 3;
    nltable(2, 1) = 3;
    nltable(2, 2) = 1;
    nltable(3, 1) = 1;
    nltable(3, 2) = 2;
!
    nrow_max = 1
    infnn_max = infn_max + 2
    allocate(infnc(2, infn_max, node))
666 allocate(jcoef(infnn_max, node))
    do n = 1, node
      infnn = 1
      jcoef(1, n) = n
      do i = 1, infn(n)
        ne = infe(i, n)
        nt = infc(i ,n)
        n1 = nc(nltable(nt, 1), ne)
        n2 = nc(nltable(nt, 2), ne)
        i1 = 0
        i2 = 0
        do j = 1 , infn(n)
          m1 = nc(nltable(infc(j, n), 1), infe(j, n))
          m2 = nc(nltable(infc(j, n), 2), infe(j, n))
          if(n1 == m2) then
            i1 = 1
            infnc(1, i, n) = infnc(2, j, n)
          end if
          if(n2 == m1) then
            i2 = 1
            infnc(2, i, n) = infnc(1, j, n)
          end if
        end do
      if(i1 == 0) then
        if(infnn_max <= infnn) then
          infnn_max = infnn_max + 2
          deallocate(jcoef)
          goto 666
        end if
        infnn = infnn + 1
        jcoef(infnn, n) = n1
        infnc(1, i, n) = infnn
      end if
      if(i2 == 0) then
        if(infnn_max <= infnn) then
          deallocate(jcoef)
          goto 666
        end if
        infnn = infnn + 1
        jcoef(infnn, n) = n2
        infnc(2, i, n) = infnn
      end if
    end do
    nrow_max = max(nrow_max, infnn)
    jcoef(infnn + 1:infnn_max, n) = n
  end do
!
  allocate(coef_x(1:nrow_max * 3, 1:node))
  allocate(coef_y(1:nrow_max * 3, 1:node))
  allocate(coef_c(1:nrow_max * 3, 1:node))
  allocate(wkg_row(1:nrow_max * 3))
!$ deallocate(wkg_row)
end subroutine makNET

