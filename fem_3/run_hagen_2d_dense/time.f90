program main
    use,intrinsic :: iso_fortran_env
    !$ use omp_lib
    implicit none

     enum, bind(c)
        enumerator :: Year = 1
        enumerator :: Month
        enumerator :: Day
        enumerator :: TimeDifference_min
        enumerator :: Hour
        enumerator :: Minute
        enumerator :: Second
        enumerator :: Millisecond
    end enum

    integer(int32),parameter :: N = 2**13
    real(real64),allocatable :: a(:,:),b(:,:),c(:,:)
    integer(int32) :: i,j

    integer(int64) :: time_begin_c,time_end_c,CountPerSec
    real(real32)   :: time_begin_s,time_end_s
    !$ real(real64)   :: time_begin_ws,time_end_ws     ! 変数名の重複を避けるため，wall-clock timeのwを追加
    integer :: time_begin_values(8),time_end_values(8) !標準種別

    allocate(a(N,N))
    allocate(b(N,N))
    allocate(c(N,N))

    ! 起動するスレッド数
    !$ call omp_set_num_threads(32)

    !$omp parallel　
    ! 配列の初期化
    !$omp do
    do i = 1,N
    do j = 1,N
        a(i,j) = 1._real64
        b(i,j) = 2._real64
        c(i,j) = 0._real64
    end do
    end do
    !$omp end do

    ! system_clock
    !$omp master
    call system_clock(time_begin_c)
    !$omp end master
    !$omp do
    do i = 1,N
    do j = 1,N
        c(i,j) = a(i,j) + b(i,j)
    end do
    end do
    !$omp end do
    !$omp master
    call system_clock(time_end_c,CountPerSec)
    print *,real(time_end_c - time_begin_c)/CountPerSec,"sec",sum(c)/N/N
    !$omp end master

    ! cpu_time
    !$omp master
    call cpu_time(time_begin_s)
    !$omp end master
    !$omp do
    do i = 1,N
    do j = 1,N
        c(i,j) = a(i,j) + b(i,j)
    end do
    end do
    !$omp end do
    !$omp master
    call cpu_time(time_end_s)
    print *,time_end_s - time_begin_s,"sec",sum(c)/N/N
    !$omp end master

    ! date_and_time
    !$omp master
    call date_and_time(values = time_begin_values)
    !$omp end master
    !$omp do
    do i = 1,N
    do j = 1,N
        c(i,j) = a(i,j) + b(i,j)
    end do
    end do
    !$omp end do
    !$omp master
    call date_and_time(values = time_end_values)
    print *,time_end_values(     Second)-time_begin_values(     Second) &
          +(time_end_values(Millisecond)-time_begin_values(Millisecond))/1000.0,"sec",sum(c)/N/N
    !$omp end master

    ! omp_get_wtime
    !$omp master
    !$ time_begin_ws = omp_get_wtime()
    !$omp end master
    !$omp do
    do i = 1,N
    do j = 1,N
        c(i,j) = a(i,j) + b(i,j)
    end do
    end do
    !$omp end do
    !$omp master
    !$ time_end_ws = omp_get_wtime()
    !$ print *,real(time_end_ws - time_begin_ws,real32),"sec",sum(c)/N/N
    !$omp end master
    !$omp end parallel

    deallocate(a)
    deallocate(b)
    deallocate(c)
end program main
