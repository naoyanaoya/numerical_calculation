/* �ꎟ���o�[�K�[�X�������̈ꎟ���x�L���̐ϖ@ (Harten �\���o�[) */
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define f_flux(x) (0.5*x*x)  //�����֐��̒�`�F�o�[�K�[�X������
#define i_max 100           //�i�q�Z����
#define XL -1.0            //�v�Z�̈捶�[�̍��W
#define XR 1.0            //�v�Z�̈�E�[�̍��W
#define tstop 0.3            //�v�Z��~����

FILE *fp;				 // �`��\�t�ggnuplot�̎��s�t�@�C���̃|�C���^

int i;    //�Z���ԍ��G (i�ԖڃZ���̃Z�������E�̔ԍ���i�A�E�Ӌ��E�̔ԍ���i+1�Ƃ���)
double x[i_max +1];    //�Z�����E�̍��W
double u[i_max];       //�Z�����ϒl
double ue[i_max];       //�Z�����ϒl
double ul[i_max+1];    //�Z�����E�����̕ϐ��l
double ur[i_max+1];    //�Z�����E�E���̕ϐ��l
double f[i_max+1];    //�Z�����E�̗���
double dx;            //�i�q�Ԋu�i���Ԋu�Ƃ���j
double dt;            //���ԍ���
int n;            //���ԃX�e�b�v
double t;            //�v�Z����


/*
�@    �v�Z�ϐ��Ɛ��l�����̔z�u

	 �Z�����E�@�@�@�@�@ �Z�����E
  -------|-----------------|-------
	   f[i]     u[i]    f[i+1]�@�@�@==> �v�Z�R�[�h�̕ϐ���`

	  f_i-1/2   u_i     f_i+1/2     ==> �e�L�X�g�̕W�L


�@  �@���E����
	u[0]     u[1]                    u[i_max-2]  u[i_max-1]
 ---------|-------|  ---- ---  ----|-----------|---------
		 [1]     [2]           [i_max-2]    [i_max-1]
			  �v�Z���E		    �v�Z���E

*/


void graph(); //gnuplot��p���ĕ`�悷��֐�
void graph1(); //gnuplot��p���ĕ`�悷��֐��ioutput.eps�֏o�́j
void initc(int); //����������ݒ肷��֐�
void reconstruction_pc();//�č\�z�֐��i�O����ԁj
void riemann_harten();//���[�}���E�\���o�[�֐��iHarten�X�L�[���j
void update();//���ԑO�i���v�Z����֐�
void bc();//���E������ݒ肷��֐��i�������E�����j
void exact(int);//���������v�Z����֐�

int main() //�@main�@�֐�
{
	int sw1;
	fp = _popen("C:/gnuplot/bin/gnuplot", "w"); //�`��c�[��gnuplot�̃t�@�C���|�C���^�w��
	fprintf(fp, "set terminal windows title 'Solution plots' size 800,600 \n");//�����̏o�͐�w��iPC�̉�ʂɕ\���j

	printf("�o�[�K�[�X�������̏����l��I��\n");
	printf("���炩�ȕ��z�i�����g�j�F�P�@\n");
	printf("�s�A���ȕ��z�i��`�g�j�F�Q�@\n");
	scanf("%d", &sw1);

	n = 0;
	t = 0.0;
	initc(sw1);	// �v�Z�i�q�C���ԍ��݁C����������ݒ肷��
	exact(sw1);
	graph();	// �v�Z���ʂ��O���t�ŕ\������
	getchar();

	while (t <= tstop) //�@���C�����[�v
	{
		n = n + 1;
		t = t + dt;
		reconstruction_pc();	// ��ԍč\�z
		riemann_harten();	// ���[�}���\���o�[
		update();	// ���Ԑϕ�
		bc();	// ���E����
		exact(sw1);// ������
		graph();	// �v�Z���ʂ��O���t�ŕ\������
		printf("n=%d, t=%f, \n", n, t);
	}
    //*****************************************
    graph1();	// �v�Z���ʂ̃O���t��ۑ�����
    //*****************************************
	printf("Press Enter key to finish.\n");
    getchar();
}


void initc(int sw1) { // �v�Z�i�q�C���ԍ��݁C����������ݒ肷��
	dx = (XR - XL) / ((double)i_max - 4.0);  //�@�i�q�Ԋu
	dt = 0.2 * dx; //���ԍ���
	x[0] = XL - 2.0 * dx;
	for (i = 1; i <= i_max; i++) {
		x[i] = x[i - 1] + dx;  //�@�i�q�_�̍��W
	}

	for (i = 0; i <= i_max; i++) {
		ul[i] = 0.0;  //�Z�����E�����̕ϐ��l��������
		ur[i] = 0.0;  //�Z�����E�E���̕ϐ��l��������
		f[i] = 0.0;  //�Z�����E�̐��l������������
	}

	for (i = 0; i <= i_max - 1; i++) {
		u[i] = 0.0;  //�@�ϐ��l���O�ɏ�����
	}

	switch (sw1)
	{
	case 1:		//�@�����̕ϐ��l�i���炩�ȕ��z�j
		for (i = 0; i <= i_max - 1; i++) {
			u[i] = 0.5 * (1.1 + sin(2. * M_PI * (x[i] - x[2])));
		}
		break;
	case 2:		//�@�����̕ϐ��l�i�s�A���ȕ��z�j
		for (i = 0; i <= i_max - 1; i++) {
			u[i] = 0.1;
		}
		for (i = i_max / 2 - 10; i <= i_max / 2 + 10; i++) {
			u[i] = 1.0;
		}
		break;
	default:
		printf("�����l�𐳂����I������Ă��܂���B");
		break;
	}
}

void reconstruction_pc() {
	for (i = 1; i <= i_max - 3; i++) {
		ul[i + 1] = u[i];  //�Z�����E(i+1/2)�����̒l
		ur[i + 1] = u[i + 1];  //�Z�����E(i+1/2)�E���̒l
	}
}

void riemann_harten() {
	//	�������v�Z����
	double alpha_12, nu_12, eps = 0.25;
	for (i = 2; i <= i_max - 2; i++) {
		alpha_12 = 1.0 / 2.0 * (ur[i] + ul[i]);
		nu_12 = fabs(alpha_12);
		if (nu_12 < 2.0 * eps) nu_12 = nu_12 * nu_12 / 4.0 / eps + eps;
		f[i] = 1.0 / 2.0 * (f_flux(ul[i]) + f_flux(ur[i])) -
			1.0 / 2.0 * nu_12 * (ur[i] - ul[i]);
	}
}

void update() {
	for (i = 2; i <= i_max - 3; i++) {
		u[i] = u[i] - dt / dx * (f[i + 1] - f[i]);//�v�Z�ϐ����X�V����
	}
}

void exact(int sw1) {
	double xc, xl, xr;
	double f, df, c;
	switch (sw1)
	{
	case 1:		//�@�����̕ϐ��l�i���炩�ȕ��z�j
		for (i = 0; i <= i_max - 1; i++) {
			c = 2. * M_PI * (i_max) / i_max;
			f = ue[i] - 0.5 * (1.1 + sin(c * ((x[i] - x[2]) - ue[i] * t)));
			df = 1.0 + 0.5 * c * cos(c * ((x[i] - x[2]) - ue[i] * t)) * t;
			while (fabs(f) >= 1.0e-4) {
				ue[i] = ue[i] - f / df; /*�j���[�g���@�̌v�Z��*/
				f = ue[i] - 0.5 * (1.1 + sin(c * ((x[i] - x[2]) - ue[i] * t)));
				df = 1.0 + 0.5 * c * cos(c * ((x[i] - x[2]) - ue[i] * t)) * t;
			}
		}
		break;
	case 2:		//�@�����̕ϐ��l�i�s�A���ȕ��z�j
		xc = -dx * 10. + t;
		xl = -dx * 10. + 0.1 * t;
		xr = dx * 10. + 0.55 * t;
		for (i = 0; i <= i_max - 1; i++) {
			if (x[i] <= xl) ue[i] = 0.1;
			if (x[i] >= xl && x[i] <= xc) ue[i] = (x[i] - xl) / (xc - xl) * 0.9 + 0.1;
			if (x[i] >= xc && x[i] <= xr) ue[i] = 1.0;
			if (x[i] >= xr) ue[i] = 0.1;
		}
		break;
	}
}

void bc() {    //�������E����
	u[0] = u[i_max - 4];  //�@�v�Z�̈捶�[�̋��E����
	u[1] = u[i_max - 3];  //�@�v�Z�̈捶�[�̋��E����
	u[i_max - 2] = u[2];    //�@�v�Z�̈�E�[�̋��E����
	u[i_max - 1] = u[3];      //�@�v�Z�̈�E�[�̋��E����
}

void graph() //�@�v�Z���ʂ̉����O���t��[���ɕ\��
{

	fprintf(fp, "set xrange [-1:1] \n"); ;//�����\���͈�
	fprintf(fp, "set yrange [-0.25:1.25] \n"); //�c���\���͈�

	fprintf(fp, "set xtics -1,0.5,1  \n"); //�����ڐ�
	fprintf(fp, "set ytics 0,0.5,1  \n"); //�c���ڐ�

	fprintf(fp, "plot 0 title 'X-axis'lw 3.5 lc rgb 'black',"); // x���i�����j
	fprintf(fp, " '-'title 'Exact sol.' w line lw 2.5  lc rgb 'red',");// �������i�Ԑ��j
	fprintf(fp, " '-'title 'Numerical sol.' w point pt 7 ps 1 lc rgb 'blue'\n");// ���l���i�_�j
	for (i = 1; i <= i_max - 1; i++) {
		fprintf(fp, "%f %f \n", x[i], ue[i]);  // �������̃f�[�^�Ƒ���
	}
	fprintf(fp, "e \n");
	for (i = 1; i <= i_max - 1; i++) {
		fprintf(fp, "%f %f \n", x[i], u[i]);  // ���l���̃f�[�^�Ƒ���
	}
	fprintf(fp, "e \n");
	fflush(fp);
}

void graph1() //�@�v�Z���ʂ̉����O���t���t�@�C���ɕۑ�
{
	fprintf(fp, "set terminal postscript eps \n");//�����̏o�͐�w��ieps�t�@�C���ɏo�́j
	fprintf(fp, "set output 'output.eps \n");//�o�̓t�@�C�����w��
	fprintf(fp, "set xrange [-1:1] \n"); ;//�����\���͈�
	fprintf(fp, "set yrange [-0.25:1.25] \n"); //�c���\���͈�
	fprintf(fp, "set xtics -1,0.5,1  \n"); //�����ڐ�
	fprintf(fp, "set ytics 0,0.5,1  \n"); //�c���ڐ�
	fprintf(fp, "plot 0 title 'X-axis'lw 3.5 lc rgb 'black',"); // x���i�����j
	fprintf(fp, " '-'title 'Exact sol.' w line lw 2.5  lc rgb 'red',");// �������i�Ԑ��j
	fprintf(fp, " '-'title 'Numerical sol.' w point pt 7 ps 1 lc rgb 'blue'\n");// ���l���i�_�j
	for (i = 1; i <= i_max - 1; i++) {
		fprintf(fp, "%f %f \n", x[i], ue[i]);  // �������̃f�[�^�Ƒ���
	}
	fprintf(fp, "e \n");
	for (i = 1; i <= i_max - 1; i++) {
		fprintf(fp, "%f %f \n", x[i], u[i]);  // ���l���̃f�[�^�Ƒ���
	}
	fprintf(fp, "e \n");
	fflush(fp);
}