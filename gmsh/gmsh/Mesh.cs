﻿using System;
using System.Diagnostics;

namespace gmsh
{
	public class Mesh
	{
        public float[] Nodes { get; set; }
        public Cell[] Cells { get; set; }
        public Node[] Node { get; set; }
        public Dictionary<int, string> PhysicalIDs { get; set; }

        public Mesh(string[] lines)
		{
			Console.WriteLine("start input msh file");

            (this.Nodes, var cellsJugArray, this.PhysicalIDs) = InterpretLinesOnMesh(lines);

            this.Cells = new Cell[cellsJugArray.Length];
            for (int i = 0; i < cellsJugArray.Length; i++)
            {
                var line = cellsJugArray[i];

                // ここで単体のcellを定義
                var cell = new Cell()
                {
                    CellType = (CellType)line[1],
                    PhysicalNameID = line[3],
                    EntityID = line[4]
                };
                

                if (cell.CellType == CellType.Triangle)
                {
                    cell.NodesIndex = new int[]
                    {
                        line[5],
                        line[6],
                        line[7]
                    };
                }
                else if (cell.CellType == CellType.Quadrilateral)
                {
                    cell.NodesIndex = new int[]
                    {
                        line[5],
                        line[6],
                        line[7],
                        line[8]
                    };
                }
                else if (cell.CellType == CellType.Tetrahedron)
                {
                    cell.NodesIndex = new int[]
                    {
                        line[5],
                        line[6],
                        line[7],
                        line[8]
                    };
                }
                else if (cell.CellType == CellType.Prism)
                {
                    cell.NodesIndex = new int[]
                    {
                        line[5],
                        line[6],
                        line[7],
                        line[8],
                        line[9],
                        line[10]
                    };
                }

                // ここで複数のCellsにCellを入れる
                this.Cells[i] = cell;
            }

            int nodeHashSetLength = this.Nodes.Length / 3 + 1;
            var nodeHashSet = new List<HashSet<int>>(nodeHashSetLength);
            //sample
            for (int i = 0; i < nodeHashSetLength; i++)
            {
                nodeHashSet.Add(new HashSet<int> { 1 });
            }
            for (int i = 0; i < nodeHashSetLength; i++)
            {
                nodeHashSet[i].Clear();
            }

　           for (int k = 0; k < this.Cells.Length; k++)
                {
                var c = this.Cells[k];
                for (int i = 0; i < c.NodesIndex.Length; i++)
                {
                    for (int j = 0; j < c.NodesIndex.Length; j++)
                    {
                        if (i != j)
                        {
                            nodeHashSet[c.NodesIndex[i]].Add(c.NodesIndex[j]);
                        }
                    }

                }
            }
            //Console.WriteLine($"==========================");

            this.Node = new Node[nodeHashSetLength];
            for (int i = 0; i < nodeHashSetLength; i++)
            {
                this.Node[i] = new Node(i, nodeHashSet[i], this.Nodes);
            }


            Console.WriteLine($"{this.Node.Count()}");

            

            Console.WriteLine("finish input msh file.");
        }

        private (float[], int[][], Dictionary<int, string>) InterpretLinesOnMesh(string[] lines)
        {
            float[] nodes = null;
            int[][] elements = null;
            Dictionary<int, string> PhysicalNamesCorrespondence = null;
            // Interpret lines.
            for (int currentLine = 0; currentLine < lines.Length; currentLine++)
            {
                if (lines[currentLine] == "$MeshFormat")
                {
                    //Debug.WriteLine("This is MeshFormat.");
                    currentLine += 2;
                }
                else if (lines[currentLine] == "$PhysicalNames")
                {
                    // TODO: PhysicalNamesが定義されていないときには対応できていない
                    currentLine += 1;
                    var physicalNameNumber = int.Parse(lines[currentLine]);
                    PhysicalNamesCorrespondence = new Dictionary<int, string>();
                    for (int index = 0; index < physicalNameNumber; index++)
                    {
                        currentLine += 1;
                        //Debug.WriteLine($"{i}行目は{test[i]}");
                        string[] cols = lines[currentLine].Split(" ");
                        var id = int.Parse(cols[1]);
                        var name = cols[2].Replace("\"", "");
                        PhysicalNamesCorrespondence.Add(id, name);
                    }
                }
                else if (lines[currentLine] == "$Nodes")
                {
                    //Debug.WriteLine($"Nodes");
                    currentLine += 1;
                    var nodesNumber = int.Parse(lines[currentLine]);
                    nodes = new float[nodesNumber * 3];
                    for (int index = 0; index < nodesNumber; index++)
                    {
                        currentLine += 1;
                        string[] cols = lines[currentLine].Split(" ");
                        nodes[(3 * index) + 0] = float.Parse(cols[1]); //x
                        nodes[(3 * index) + 1] = float.Parse(cols[2]); //y
                        nodes[(3 * index) + 2] = float.Parse(cols[3]); //z
                    }
                }
                else if (lines[currentLine] == "$Elements")
                {
                    //Debug.WriteLine("Elements Yes!!");
                    currentLine += 1;
                    var elementsNumber = int.Parse(lines[currentLine]);
                    //Debug.WriteLine($"ElementsNumber is {elementsNumber}");
                    elements = new int[elementsNumber][];
                    for (int index = 0; index < elementsNumber; index++)
                    {
                        currentLine += 1;
                        string[] splittedLine = lines[currentLine].Split(" ");
                        var array = new int[splittedLine.Length];
                        for (int c = 0; c < splittedLine.Length; c++)
                        {
                            array[c] = int.Parse(splittedLine[c]);
                        }
                        elements[index] = array;
                        //Debug.WriteLine($"elements[{index}] is {elements[index][0]}");
                    }
                }


            }
            if (elements == null)
            {
                Debug.WriteLine("No ---------------");
            }
            return (nodes, elements, PhysicalNamesCorrespondence);
        }
    }
}

