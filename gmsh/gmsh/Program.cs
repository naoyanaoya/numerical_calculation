﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

// See https://aka.ms/new-console-template for more information

namespace gmsh
{
    public class Test
    {
        static public void Main(string[] args)
        {
            Console.WriteLine("start c# gmsh code.");

            //Console.WriteLine(args.Length);
            string[] lines = null;
            if (args.Length == 0)
            {
                return;
            }
            else
            {
                Console.WriteLine($"input file is {args[0]}");
                lines = File.ReadAllLines(args[0]);
            }

            if (lines == null)
            {
                Console.WriteLine("うまく読み込めませんでした。");
                return;
            }


            var mesh = new Mesh(lines);

            foreach (var p in mesh.PhysicalIDs)
            {
                Console.WriteLine($"{p.Key} {p.Value}");
            }

            //var output = new Output(mesh);
            var output = new Output();
            output.output_vtk(mesh);

            var matrix = new double[,]
            {
                { 3, 2, 1 },
                { 1, 4, 1 },
                { 2, 2, 5 }
            };
            var b = new double[] { 10, 12, 21 };
            var eps = 1e-15;
            //var result = GaussSeidel(matrix, b, 1000, eps);

            //Console.WriteLine("Error:" + result.Error);
            //Console.WriteLine("Count:" + result.Iterator);

            //var xyz = new[] { "x", "y", "z" };
            //for (int i = 0; i < result.Solution.Length; i++)
            //{
            //    Console.WriteLine(xyz[i] + " = " + result.Solution[i]);
            //}
            //Console.WriteLine("finish c# gmsh code.");
        }

        public static IterativeResult GaussSeidel(double[,] squareMatrix, double[] constantVector, int maxIterator, double eps, bool AbsoluteError = true)
        {
            if (squareMatrix.GetLength(0) != squareMatrix.GetLength(1))
            {
                throw new ArgumentException("引き算の係数行列が正方行列ではありません。", "A");
            }
            if (squareMatrix.GetLength(0) != constantVector.Length)
            {
                throw new ArgumentException("引き算の定数項行列が係数行列の大きさと一致しません。");
            }

            // 行列の大きさ
            int n = squareMatrix.GetLength(0);
            // 解。初期値はすべて0
            double[] solution = new double[n];
            // 誤差
            double e = 0.0;
            // 現在の反復回数
            int k;

            double tmp;

            for (k = 0; k < maxIterator; k++)
            {
                // 現在の値を代入して次の解候補を計算
                e = 0.0;
                for (int i = 0; i < n; i++)
                {
                    tmp = solution[i];
                    solution[i] = constantVector[i];
                    for (int j = 0; j < n; j++)
                    {
                        if (j != i)
                        {
                            solution[i] -= squareMatrix[i, j] * solution[j];
                        }
                        else
                        {
                            solution[i] -= 0.0;
                        }
                    }
                    solution[i] /= squareMatrix[i, i];

                    if (AbsoluteError)
                    {
                        // 絶対誤差
                        e += Math.Abs(tmp - solution[i]);
                    }
                    else
                    {
                        // 相対誤差
                        e += Math.Abs((tmp - solution[i]) / tmp);
                    }
                }
                // 収束判定
                if (e <= eps)
                {
                    Console.WriteLine("break");
                    break;
                }

            }

            return new IterativeResult(solution, k, e);
        }

        public struct IterativeResult
        {
            /// <summary>
            /// solution
            /// </summary>
            public double[] Solution { get; set; }

            /// <summary>
            /// number of iterations
            /// </summary>
            public int Iterator { get; set; }

            /// <summary>
            /// Error
            /// </summary>
            public double Error { get; set; }

            public IterativeResult(double[] solution, int iterator, double error)
            {
                this.Solution = solution;
                this.Iterator = iterator;
                this.Error = error;
            }
        }
    }
}
