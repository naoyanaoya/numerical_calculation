﻿using System;

namespace gmsh
{
	public class Cell
	{
        public int[] NodesIndex { get; set; }
        public CellType CellType { get; set; }
        public int PhysicalNameID { get; set; }
        public int EntityID { get; set; }
	}
    public enum CellType
    {
        Triangle = 2,
        Quadrilateral = 3,
        Tetrahedron = 4,
        Prism = 6
    }
}

