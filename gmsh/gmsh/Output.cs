﻿using System;
using System.Reflection;

namespace gmsh
{
	public class Output
	{
        //public Output(Mesh mesh)
        //{
        //    output_vtk(mesh);
        //}

        public void output_vtk(Mesh mesh)
        {
            Console.WriteLine("start output_vtk.");

            var nodes = mesh.Nodes;
            var cells = mesh.Cells;
            var node = mesh.Node;
            var numberOfNodes = nodes.Count() / 3;

            using (var sw = new StreamWriter(@"/Users/imainaoya/gitlab-personal/numerical_calculation/gmsh/100-spere/test.vtk"))
            {
                sw.WriteLine("# vtk DataFile Version 2.0");
                sw.WriteLine("Wall, Created by Gmsh 4.10.5 ");
                sw.WriteLine("ASCII");
                sw.WriteLine("DATASET UNSTRUCTURED_GRID");
                sw.WriteLine($"POINTS {numberOfNodes} double");
                for (int i = 0; i < numberOfNodes; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Write($"{nodes[3 * i + j]}");
                        if (j != 3)
                        {
                            sw.Write(" ");
                        }
                    }
                    sw.WriteLine("");
                }
                sw.WriteLine($"CELLS {cells.Count()} {cells.Count() * 4}");
                for (int i = 0; i < cells.Count(); i++)
                {
                    sw.Write($"{cells[i].NodesIndex.Length}");
                    sw.Write(" ");
                    for (int j = 0; j < cells[i].NodesIndex.Length; j++)
                    {
                        sw.Write($"{cells[i].NodesIndex[j] - 1}");
                        if (j != cells[i].NodesIndex.Length - 1)
                        {
                            sw.Write(" ");
                        }
                    }
                    sw.WriteLine("");
                }
                sw.WriteLine($"CELL_TYPES {cells.Count()}");
                for (int i = 0; i < cells.Count(); i++)
                {
                    sw.WriteLine("5");
                }
                sw.WriteLine($"POINT_DATA {numberOfNodes}");
                sw.WriteLine("SCALARS discreteGussCurvature float");
                sw.WriteLine("LOOKUP_TABLE default");
                for (int i = 0; i < numberOfNodes; i++)
                {
                    sw.WriteLine($"{node[i + 1].discreteGussCurvature}");
                }
            }
            Console.WriteLine("finish output_vtk.");
        }
    }
}

