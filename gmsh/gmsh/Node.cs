﻿using System;
namespace gmsh
{
	public class Node
	{
		public int NodeOwn { get; set; }
		public HashSet<int> Adjacent { get; set; }
		public float discreteGussCurvature { get; set; }

        public Node(int nodeOwn, HashSet<int> adjacent, float[] nodes)
		{
			this.NodeOwn = nodeOwn;
			this.Adjacent = adjacent;
            calculateDiscreteGaussCurvature(nodes);
        }

        private void calculateDiscreteGaussCurvature(float[] nodes)
        {
            if (this.NodeOwn == 0)
            {
                return;
            }
            // xc mean x center
            var xyz = new double[3];
            var xyzSquare = new double[3];
            for (int i = 0; i < 3; i++)
            {
                xyz[i] = nodes[(this.NodeOwn - 1) * 3 + i];
                xyzSquare[i] = xyz[i] * xyz[i];
            }

            // row major
            var matrix = new double[3, 3];
            var rvec = new double[3];

            foreach (var a in this.Adjacent)
            {
                var xyzTmp = new double[3];
                var xyzTmpSquare = new double[3];
                for (int i = 0; i < 3; i++)
                {
                    xyzTmp[i] = nodes[(a - 1) * 3 + i];
                    xyzTmpSquare[i] = xyzTmp[i] * xyzTmp[i];
                }

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        matrix[i, j] += (xyz[i] - xyzTmp[i]) * (xyz[j] - xyzTmp[j]);
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    double tmp = 0.0;
                    for (int j = 0; j < 3; j++)
                    {
                        tmp += (xyzTmpSquare[j] - xyzSquare[j]);
                    }
                    rvec[i] = (xyz[i] - xyzTmp[i]) * tmp * (-0.5);
                }
            }

            var eps = 1e-15;
            var result = GaussSeidel(matrix, rvec, 1000, eps);

            Console.WriteLine("Error:" + result.Error);
            Console.WriteLine("Count:" + result.Iterator);

            var xyzSolution = new[] { "x", "y", "z" };
            for (int i = 0; i < result.Solution.Length; i++)
            {
                Console.WriteLine($"{xyzSolution[i]} = {result.Solution[i]} {xyz[i]}");
            }

            var tmptmp = 0.0;
            for (int i = 0; i < 3; i++)
            {
                tmptmp += (result.Solution[i] - xyz[i]) * (result.Solution[i] - xyz[i]);
            }
            var tmptmptmp = Math.Sqrt(tmptmp);

            this.discreteGussCurvature = (float)(1 / tmptmptmp);
        }

        public static IterativeResult GaussSeidel(double[,] squareMatrix, double[] constantVector, int maxIterator, double eps, bool AbsoluteError = true)
        {
            if (squareMatrix.GetLength(0) != squareMatrix.GetLength(1))
            {
                throw new ArgumentException("引き算の係数行列が正方行列ではありません。", "A");
            }
            if (squareMatrix.GetLength(0) != constantVector.Length)
            {
                throw new ArgumentException("引き算の定数項行列が係数行列の大きさと一致しません。");
            }

            // 行列の大きさ
            int n = squareMatrix.GetLength(0);
            // 解。初期値はすべて0
            double[] solution = new double[n];
            // 誤差
            double e = 0.0;
            // 現在の反復回数
            int k;

            double tmp;

            for (k = 0; k < maxIterator; k++)
            {
                // 現在の値を代入して次の解候補を計算
                e = 0.0;
                for (int i = 0; i < n; i++)
                {
                    tmp = solution[i];
                    solution[i] = constantVector[i];
                    for (int j = 0; j < n; j++)
                    {
                        if (j != i)
                        {
                            solution[i] -= squareMatrix[i, j] * solution[j];
                        }
                        else
                        {
                            solution[i] -= 0.0;
                        }
                    }
                    solution[i] /= squareMatrix[i, i];

                    if (AbsoluteError)
                    {
                        // 絶対誤差
                        e += Math.Abs(tmp - solution[i]);
                    }
                    else
                    {
                        // 相対誤差
                        e += Math.Abs((tmp - solution[i]) / tmp);
                    }
                }
                // 収束判定
                if (e <= eps)
                {
                    Console.WriteLine("break");
                    break;
                }

            }

            return new IterativeResult(solution, k, e);
        }

        public struct IterativeResult
        {
            /// <summary>
            /// solution
            /// </summary>
            public double[] Solution { get; set; }

            /// <summary>
            /// number of iterations
            /// </summary>
            public int Iterator { get; set; }

            /// <summary>
            /// Error
            /// </summary>
            public double Error { get; set; }

            public IterativeResult(double[] solution, int iterator, double error)
            {
                this.Solution = solution;
                this.Iterator = iterator;
                this.Error = error;
            }
        }
    }
}

