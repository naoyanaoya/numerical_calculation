import gmsh
import sys
import math

gmsh.initialize(sys.argv)
surface_real_wall = []
surface_fake_wall = []
surface_inlet_outlet = []

def OptionSetting():
    gmsh.option.setNumber("Mesh.SurfaceFaces", 1)
    # gmsh.option.setNumber("Mesh.VolumeFaces", 1)
    gmsh.option.setNumber("Mesh.Lines", 1)
    gmsh.option.setNumber("Geometry.PointLabels", 1)
    gmsh.option.setNumber("General.MouseInvertZoom", 1)
    gmsh.option.setNumber("Mesh.LineWidth", 4)
    gmsh.option.setNumber("Mesh.OptimizeThreshold", 0.1)
    gmsh.option.setNumber("General.Axes", 3)
    gmsh.option.setNumber("General.Trackball", 0)
    gmsh.option.setNumber("General.RotationX", 0)
    gmsh.option.setNumber("General.RotationY", 0)
    gmsh.option.setNumber("General.RotationZ", 0)
    gmsh.option.setNumber("General.Terminal", 1)


def ShapeCreation():
    gmsh.model.add("sphere")
    gmsh.model.occ.addSphere(0, 0, 0, 1)
    Syncronize()
    s_first = gmsh.model.getEntities(2)
    print(s_first)
    for i in range(len(s_first)):
        surface_real_wall.append(s_first[i][1])
    print(surface_real_wall)




def OutputMshVtk():
    gmsh.option.setNumber("Mesh.MshFileVersion", 2.2)
    gmsh.write("model-before.msh")
    gmsh.write("model-before.vtk")

def Meshing():
    gmsh.model.mesh.optimize('Netgen', True)

    # 2次元メッシュのメッシュ作成アルゴリズムの選択
    gmsh.option.setNumber('Mesh.Algorithm', 1)
    # Meshing algorithms can changed globally using options:
    # gmsh.option.setNumber("Mesh.Algorithm", 3)  # Frontal-Delaunay
    # 1: MeshAdapt, 2: Automatic, 3: Initial mesh only, 5: Delaunay, 6: Frontal-Delaunay, 7: BAMG,
    # 8: Frontal-Delaunay for Quads, 9: Packing of Parallelograms, 11: Quasi-structured Quad
    # default value 6

    # 3次元メッシュのメッシュ作成アルゴリズムの選択
    # gmsh.option.setNumber("Mesh.Algorithm3D", 1)
    # 1: Delaunay, 3: Initial mesh only, 4: Frontal, 7: MMG3D, 9: R-tree, 10: HXT
    # default value 1

    # Finally, while the default "Frontal-Delaunay" 2D meshing algorithm
    # (Mesh.Algorithm = 6) usually leads to the highest quality meshes, the
    # "Delaunay" algorithm (Mesh.Algorithm = 5) will handle complex mesh size
    # fields better - in particular size fields with large element size gradients:

    Syncronize()
    gmsh.option.setNumber("Mesh.MeshSizeMin", 0.01)
    gmsh.option.setNumber("Mesh.MeshSizeMax", 1.0)
    gmsh.model.mesh.generate(2)
    print("finish meshing")

def Syncronize():
    gmsh.model.geo.synchronize()
    gmsh.model.occ.synchronize()

def GetNormalAndCurvature():
    normals = []
    curvatures = []

    # for each surface in the model:
    for e in gmsh.model.getEntities(2):
        # このモデルだとsurfaceは7個ある
        # それぞれのsurfaceに対して処理を行う
        s = e[1]
        # print(s)

        tags, coord, param = gmsh.model.mesh.getNodes(2, s, True)

        norm = gmsh.model.getNormal(s, param)
        curv = gmsh.model.getCurvature(2, s, param)
        # print("=================")
        # print(len(coord))
        # print(len(norm))
        # print(len(curv))
        sum_x = 0
        sum_y = 0
        sum_z = 0
        if (len(coord) == len(norm)) and (len(coord) / 3 == len(curv)):
            for i in range(0, len(coord), 3):
                normals.append(coord[i])
                normals.append(coord[i + 1])
                normals.append(coord[i + 2])
                normals.append(norm[i])
                # print(norm[i])
                sum_x = sum_x + norm[i]
                normals.append(norm[i + 1])
                # print(norm[i + 1])
                sum_y = sum_y + norm[i + 1]
                normals.append(norm[i + 2])
                # print(norm[i + 2])
                sum_z = sum_z + norm[i + 2]
                curvatures.append(coord[i])
                curvatures.append(coord[i + 1])
                curvatures.append(coord[i + 2])
                curvatures.append(curv[i // 3])



    vn = gmsh.view.add("normals")
    gmsh.view.addListData(vn, "VP", (len(normals) // 6), normals)
    vc = gmsh.view.add("curvatures")
    gmsh.view.addListData(vc, "SP", (len(curvatures) // 4), curvatures)
    # curvatureは1(0index)だからView[1].PointSizeに設定
    gmsh.option.setNumber("View[1].PointSize", 20)

def ConfirmMesh():
    if "-nopopup" not in sys.argv:
        gmsh.fltk.run()

def NamingBoundary():
    s_second = gmsh.model.getEntities(2)
    surface_all = []
    for i in range(len(s_second)):
        surface_all.append(s_second[i][1])

    # gmsh.model.addPhysicalGroup(2, surface_all, 1)
    # gmsh.model.setPhysicalName(2, 1, "surface_all")

    # gmsh.model.addPhysicalGroup(2, surface_real_wall, 2)
    # gmsh.model.setPhysicalName(2, 2, "surface_real_wall")

    # gmsh.model.addPhysicalGroup(2, surface_fake_wall, 3)
    # gmsh.model.setPhysicalName(2, 3, "surface_fake_wall")

    # gmsh.model.addPhysicalGroup(2, surface_inlet_outlet, 4)
    # gmsh.model.setPhysicalName(2, 4, "surface_inlet_outlet")

    surface_another = list(set(surface_all) - set(surface_real_wall) - set(surface_fake_wall) - set(surface_inlet_outlet))

    # gmsh.model.addPhysicalGroup(2, surface_another, 5)
    # gmsh.model.setPhysicalName(2, 5, "surface_another")

    something = list(set(surface_another) ^ set(surface_fake_wall))
    # gmsh.model.addPhysicalGroup(2, something, 99)
    # gmsh.model.setPhysicalName(2, 99, "something")

    # wall = list(
    #     set(surface_real_wall) & set(surface_all))
    print(f"wall is {surface_real_wall}")
    gmsh.model.addPhysicalGroup(2, surface_real_wall, 10)
    gmsh.model.setPhysicalName(2, 10, "wall")

    # v = gmsh.model.getEntities(3)
    # three_dimension_list = []
    # for i in range(len(v)):
    #     three_dimension_list.append(v[i][1])
    # gmsh.model.addPhysicalGroup(3, three_dimension_list, 100)
    # gmsh.model.setPhysicalName(3, 100, "internal")

    # 2次元の「inlet,outlet」に関しては完璧な手作業
    # 別にコードを書いて、stlと組み合わせて「inlet,outlet」を特定できるようにする必要がある
    # gmsh.model.addPhysicalGroup(2, [678, 704], 15)
    # gmsh.model.setPhysicalName(2, 15, "outlet4")
    # gmsh.model.addPhysicalGroup(2, [598, 702], 13)
    # gmsh.model.setPhysicalName(2, 13, "outlet2")
    # gmsh.model.addPhysicalGroup(2, [510, 700], 12)
    # gmsh.model.setPhysicalName(2, 12, "outlet1")
    # gmsh.model.addPhysicalGroup(2, [498, 698], 11)
    # gmsh.model.setPhysicalName(2, 11, "inlet")
    # gmsh.model.addPhysicalGroup(2, [129, 696], 14)
    # gmsh.model.setPhysicalName(2, 14, "outlet3")

    Syncronize()


OptionSetting()
ShapeCreation()
NamingBoundary()
Meshing()
GetNormalAndCurvature()
OutputMshVtk()
ConfirmMesh()

gmsh.finalize()

