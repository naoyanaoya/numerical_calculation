import gmsh
import math
import os
import sys
import numpy as np

gmsh.initialize(sys.argv)

# p => point
# s => surface
# v => volume

surface_real_wall = []
surface_fake_wall = []
surface_inlet_outlet = []

def OptionSetting():
    gmsh.option.setNumber("Mesh.SurfaceFaces", 1)
    # gmsh.option.setNumber("Mesh.VolumeFaces", 1)
    gmsh.option.setNumber("Mesh.Lines", 1)
    gmsh.option.setNumber("Geometry.PointLabels", 1)
    gmsh.option.setNumber("General.MouseInvertZoom", 1)
    gmsh.option.setNumber("Mesh.LineWidth", 4)
    gmsh.option.setNumber("Mesh.OptimizeThreshold", 0.1)
    gmsh.option.setNumber("General.Axes", 3)
    gmsh.option.setNumber("General.Trackball", 0)
    gmsh.option.setNumber("General.RotationX", 0)
    gmsh.option.setNumber("General.RotationY", 0)
    gmsh.option.setNumber("General.RotationZ", 0)
    gmsh.option.setNumber("General.Terminal", 1)

def ImportStl():
    path = os.path.dirname(os.path.abspath(__file__))
    # gmsh.merge(os.path.join(path, "./aneurysm_data_0.1_smoothed.stl"))
    gmsh.merge(os.path.join(path, "./Wall.stl"))
    # gmsh.merge(os.path.join(path, "./aneurysm_data_smooth.stl"))
    gmsh.model.mesh.classifySurfaces(angle=math.pi, boundary=True, forReparametrization=True)
    gmsh.model.mesh.createGeometry()
    # gmsh.model.mesh.createTopology()

    s_first = gmsh.model.getEntities(2)
    for i in range(len(s_first)):
        surface_real_wall.append(s_first[i][1])

    Syncronize()

def ShapeCreation():
    gmsh.option.setNumber("Geometry.ExtrudeReturnLateralEntities", 0)

    N = 5 # number of layers
    r = 1.2 # ration
    h = 0.05 # first_layer_thickness
    n = np.linspace(1, 1, N) # [1, 1, 1, 1, 1]
    t = np.full(N, h) # distance from the reference line
    for i in range(0, N):
        t[i] = t[i] * r ** i
    for i in range(1, N):
        t[i] += t[i - 1]

    # gmsh.model.geo.extrudeBoundaryLayer(gmsh.model.getEntities(2), [4], [0.5], True)
    e = gmsh.model.geo.extrudeBoundaryLayer(gmsh.model.getEntities(2), n, -t, True)
    top_ent = [s for s in e if s[0] == 2]
    for t in top_ent:
        surface_fake_wall.append(t[1])
    Syncronize()
    bnd_ent = gmsh.model.getBoundary(top_ent)
    bnd_curv = [c[1] for c in bnd_ent]
    loops = gmsh.model.geo.addCurveLoops(bnd_curv)
    for i in loops:
        surface_fake_wall.append(gmsh.model.geo.addPlaneSurface([i]))
        surface_inlet_outlet.append(gmsh.model.geo.addPlaneSurface([i]))
    gmsh.model.geo.addVolume([gmsh.model.geo.addSurfaceLoop(surface_fake_wall)])

    print(f"surface_fake_wall is {surface_fake_wall}")
    print(f"surface_inlet_outlet is {surface_inlet_outlet}")

    Syncronize()

def NamingBoundary():
    s_second = gmsh.model.getEntities(2)
    surface_all = []
    for i in range(len(s_second)):
        surface_all.append(s_second[i][1])

    # gmsh.model.addPhysicalGroup(2, surface_all, 1)
    # gmsh.model.setPhysicalName(2, 1, "surface_all")

    # gmsh.model.addPhysicalGroup(2, surface_real_wall, 2)
    # gmsh.model.setPhysicalName(2, 2, "surface_real_wall")

    # gmsh.model.addPhysicalGroup(2, surface_fake_wall, 3)
    # gmsh.model.setPhysicalName(2, 3, "surface_fake_wall")

    # gmsh.model.addPhysicalGroup(2, surface_inlet_outlet, 4)
    # gmsh.model.setPhysicalName(2, 4, "surface_inlet_outlet")

    surface_another = list(set(surface_all) - set(surface_real_wall) - set(surface_fake_wall) - set(surface_inlet_outlet))

    # gmsh.model.addPhysicalGroup(2, surface_another, 5)
    # gmsh.model.setPhysicalName(2, 5, "surface_another")

    something = list(set(surface_another) ^ set(surface_fake_wall))
    # gmsh.model.addPhysicalGroup(2, something, 99)
    # gmsh.model.setPhysicalName(2, 99, "something")

    wall = list(
        set(surface_real_wall) & set(surface_all))
    print(f"wall is {wall}")
    gmsh.model.addPhysicalGroup(2, wall, 10)
    gmsh.model.setPhysicalName(2, 10, "wall")

    # v = gmsh.model.getEntities(3)
    # three_dimension_list = []
    # for i in range(len(v)):
    #     three_dimension_list.append(v[i][1])
    # gmsh.model.addPhysicalGroup(3, three_dimension_list, 100)
    # gmsh.model.setPhysicalName(3, 100, "internal")

    # 2次元の「inlet,outlet」に関しては完璧な手作業
    # 別にコードを書いて、stlと組み合わせて「inlet,outlet」を特定できるようにする必要がある
    # gmsh.model.addPhysicalGroup(2, [678, 704], 15)
    # gmsh.model.setPhysicalName(2, 15, "outlet4")
    # gmsh.model.addPhysicalGroup(2, [598, 702], 13)
    # gmsh.model.setPhysicalName(2, 13, "outlet2")
    # gmsh.model.addPhysicalGroup(2, [510, 700], 12)
    # gmsh.model.setPhysicalName(2, 12, "outlet1")
    # gmsh.model.addPhysicalGroup(2, [498, 698], 11)
    # gmsh.model.setPhysicalName(2, 11, "inlet")
    # gmsh.model.addPhysicalGroup(2, [129, 696], 14)
    # gmsh.model.setPhysicalName(2, 14, "outlet3")

    Syncronize()

def ConfirmMesh():
    if "-nopopup" not in sys.argv:
        gmsh.fltk.run()

def OutputMshVtk():
    gmsh.option.setNumber("Mesh.MshFileVersion", 2.2)
    gmsh.write("model-before.msh")
    gmsh.write("model-before.vtk")

def Meshing():
    gmsh.model.mesh.optimize('Netgen', True)

    # 2次元メッシュのメッシュ作成アルゴリズムの選択
    gmsh.option.setNumber('Mesh.Algorithm', 1)
    # Meshing algorithms can changed globally using options:
    # gmsh.option.setNumber("Mesh.Algorithm", 3)  # Frontal-Delaunay
    # 1: MeshAdapt, 2: Automatic, 3: Initial mesh only, 5: Delaunay, 6: Frontal-Delaunay, 7: BAMG,
    # 8: Frontal-Delaunay for Quads, 9: Packing of Parallelograms, 11: Quasi-structured Quad
    # default value 6

    # 3次元メッシュのメッシュ作成アルゴリズムの選択
    # gmsh.option.setNumber("Mesh.Algorithm3D", 1)
    # 1: Delaunay, 3: Initial mesh only, 4: Frontal, 7: MMG3D, 9: R-tree, 10: HXT
    # default value 1

    # Finally, while the default "Frontal-Delaunay" 2D meshing algorithm
    # (Mesh.Algorithm = 6) usually leads to the highest quality meshes, the
    # "Delaunay" algorithm (Mesh.Algorithm = 5) will handle complex mesh size
    # fields better - in particular size fields with large element size gradients:

    Syncronize()
    gmsh.option.setNumber("Mesh.MeshSizeMin", 0.01)
    gmsh.option.setNumber("Mesh.MeshSizeMax", 0.5)
    gmsh.model.mesh.generate(3)
    print("finish meshing")

def Syncronize():
    gmsh.model.geo.synchronize()

def GetNormalAndCurvature():
    normals = []
    curvatures = []

    # for each surface in the model:
    for e in gmsh.model.getEntities(2):
        # このモデルだとsurfaceは7個ある
        # それぞれのsurfaceに対して処理を行う
        s = e[1]
        # print(s)

        tags, coord, param = gmsh.model.mesh.getNodes(2, s, True)

        norm = gmsh.model.getNormal(s, param)
        curv = gmsh.model.getCurvature(2, s, param)
        # print("=================")
        # print(len(coord))
        # print(len(norm))
        # print(len(curv))
        sum_x = 0
        sum_y = 0
        sum_z = 0
        if (len(coord) == len(norm)) and (len(coord) / 3 == len(curv)):
            for i in range(0, len(coord), 3):
                normals.append(coord[i])
                normals.append(coord[i + 1])
                normals.append(coord[i + 2])
                normals.append(norm[i])
                # print(norm[i])
                sum_x = sum_x + norm[i]
                normals.append(norm[i + 1])
                # print(norm[i + 1])
                sum_y = sum_y + norm[i + 1]
                normals.append(norm[i + 2])
                # print(norm[i + 2])
                sum_z = sum_z + norm[i + 2]
                curvatures.append(coord[i])
                curvatures.append(coord[i + 1])
                curvatures.append(coord[i + 2])
                curvatures.append(curv[i // 3])

        # print(sum_x)
        # print(sum_y)
        # print(sum_z)



    vn = gmsh.view.add("normals")
    gmsh.view.addListData(vn, "VP", (len(normals) // 6), normals)
    vc = gmsh.view.add("curvatures")
    gmsh.view.addListData(vc, "SP", (len(curvatures) // 4), curvatures)
    # curvatureは1(0index)だからView[1].PointSizeに設定
    gmsh.option.setNumber("View[1].PointSize", 20)

OptionSetting()
ImportStl()
ShapeCreation()
NamingBoundary()
Meshing()
# GetNormalAndCurvature()
OutputMshVtk()
ConfirmMesh()

gmsh.finalize()
