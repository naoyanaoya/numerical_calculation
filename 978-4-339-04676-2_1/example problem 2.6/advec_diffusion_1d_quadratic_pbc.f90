!****************************************************************************
!   finite element discretization for the 1d advection-diffusion equation   *
!   without using the lumped mass matrix (periodic b.c.)                    *
!                                                                           *
!    - piecewise quadratic interpolation                                    *
!                                                                           *
!                         edited by haruhiko kohno                          *
!****************************************************************************

      program advec_diffusion_1d_quadratic_pbc

      implicit none
      integer :: i,ie,ii,j,jj,k,kk,nu
      integer :: np1,np2,np3,i1,i2,i3,j1,j2,j3
      integer :: ll,n,nnode
      integer :: id
      real(8) :: dt,u,alpha,xl,xr,pi,dx
      real(8) :: gg,ah,w,ppp,q,s
      integer, dimension(1000,3) :: nbool
      real(8), dimension(1000) :: x,phib,b,c,phi
      real(8), dimension(3) :: xi
      real(8), dimension(3,3) :: inn,ind,idd
      real(8), dimension(1000,1000) :: a

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number: ll'
      read(*,*) ll

      write(*,*) 'input the time increment: dt'
      read(*,*) dt

      write(*,*) 'input the number of steps: n'
      read(*,*) n

      write(*,*) 'input the advection velocity: u'
      read(*,*) u

      write(*,*) 'input the diffusion coefficient: alpha'
      read(*,*) alpha

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=1.0d0

!--- number of nodes and pi

      nnode=2*ll
      pi=dacos(-1.0d0)

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,ll
       x(i)=dx*dfloat(i-1)+xl
      enddo

      do ie=1,ll-1
       np1=ie
       np2=np1+1

       nbool(ie,1)=np1
       nbool(ie,2)=np2

       i1=nbool(ie,1)
       i2=nbool(ie,2)

       np3=ll+ie
       x(np3)=0.5d0*(x(i1)+x(i2))
       nbool(ie,3)=np3
      enddo

      nbool(ll,1)=ll
      nbool(ll,2)=1
      nbool(ll,3)=2*ll
      x(2*ll)=0.5d0*(x(ll)+xr)

!//////////////////////////////////////////
!/// coefficients in the shape function
!//////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0
      xi(3)= 0.0d0

!//////////////////////////////////
!/// initial profile (at t = 0)
!//////////////////////////////////

      do i=1,nnode
       phib(i)=dsin(2.0d0*pi*x(i))
      enddo

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,3
       do j=1,3
         inn(i,j)=xi(i)*xi(j)/6.0d0 &
                 +(32.0d0           &
                  -28.0d0*xi(i)**2  &
                  -28.0d0*xi(j)**2  &
                  +27.0d0*xi(i)**2*xi(j)**2)/30.0d0

         ind(i,j)=2.0d0/3.0d0*(xi(j)-xi(i)) &
                 +xi(i)*xi(j)/2.0d0         &
                 *(2.0d0*xi(j)-xi(i))

         idd(i,j)=xi(i)*xi(j)/2.0d0      &
                 +2.0d0/3.0d0            &
                 *(2.0d0-3.0d0*xi(i)**2) &
                 *(2.0d0-3.0d0*xi(j)**2)
       enddo
      enddo

!/////////////////////////////////
!/// time marching calculation
!/////////////////////////////////

      do k=1,n

!--- construction of the global matrix A

       a=0.0d0

       do i=1,3
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)
          j3=nbool(ie,3)

          a(nu,j1)=a(nu,j1)+0.5d0*dx*inn(i,1)
          a(nu,j2)=a(nu,j2)+0.5d0*dx*inn(i,2)
          a(nu,j3)=a(nu,j3)+0.5d0*dx*inn(i,3)
        enddo
       enddo

!--- sum of the advection and diffusion terms in the discretized equation

       b=0.0d0

       do i=1,3
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)
          j3=nbool(ie,3)

          b(nu)=b(nu)-u*(ind(i,1)*phib(j1)              &
                        +ind(i,2)*phib(j2)              &
                        +ind(i,3)*phib(j3))             &
                     -2.0d0/dx*alpha*(idd(i,1)*phib(j1) &
                                     +idd(i,2)*phib(j2) &
                                     +idd(i,3)*phib(j3))
        enddo
       enddo

!--- solve a system of linear equations using Gaussian elimination

       do kk=1,nnode-1
        id=kk
        gg=dabs(a(kk,kk))
        do ii=kk+1,nnode
         ah=dabs(a(ii,kk))
         if(ah > gg) then
          id=ii
          gg=ah
         endif
        enddo
        do jj=kk,nnode
         w=a(kk,jj)
         a(kk,jj)=a(id,jj)
         a(id,jj)=w
        enddo
        w=b(kk)
        b(kk)=b(id)
        b(id)=w

        ppp=a(kk,kk)
        do i=kk+1,nnode
         q=a(i,kk)/ppp
         do jj=kk,nnode
           a(i,jj)=a(i,jj)-q*a(kk,jj)
         enddo
         b(i)=b(i)-q*b(kk)
        enddo
       enddo

       c(nnode)=b(nnode)/a(nnode,nnode)

       do kk=nnode-1,1,-1
        s=b(kk)
        do jj=kk+1,nnode
         s=s-a(kk,jj)*c(jj)
        enddo
        c(kk)=s/a(kk,kk)
       enddo

       b=c

!--- calculate phi at the n+1 time step

       do i=1,nnode
        phi(i)=phib(i)+dt*b(i)
       enddo

       phib=phi
      enddo

!////////////////////////////////////////////////////
!/// output of the numerical solution at t = n*dt
!////////////////////////////////////////////////////

      do ie=1,ll
       i1=nbool(ie,1)
       i2=nbool(ie,2)
       i3=nbool(ie,3)

       write(11,*) x(i1),phi(i1)
       write(11,*) x(i3),phi(i3)

       if(ie == ll) then
        write(11,*) xr,phi(i2)
       endif
      enddo

      stop
      end program advec_diffusion_1d_quadratic_pbc
