!****************************************************************************
!   finite element discretization for the 1d advection-diffusion equation   *
!   without using the lumped mass matrix (periodic b.c.)                    *
!                                                                           *
!    - piecewise linear interpolation                                       *
!                                                                           *
!                         edited by haruhiko kohno                          *
!****************************************************************************

      program advec_diffusion_1d_linear_pbc

      implicit none
      integer :: i,ie,ii,j,jj,k,kk,nu
      integer :: j1,j2
      integer :: ll,n
      integer :: id
      real(8) :: dt,u,alpha,xl,xr,pi,dx
      real(8) :: gg,ah,w,ppp,q,s
      integer, dimension(1000,2) :: nbool
      real(8), dimension(1000) :: x,phib,b,c,phi
      real(8), dimension(2) :: xi
      real(8), dimension(2,2) :: inn,ind,idd
      real(8), dimension(1000,1000) :: a

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number: ll'
      read(*,*) ll

      write(*,*) 'input the time increment: dt'
      read(*,*) dt

      write(*,*) 'input the number of steps: n'
      read(*,*) n

      write(*,*) 'input the advection velocity: u'
      read(*,*) u

      write(*,*) 'input the diffusion coefficient: alpha'
      read(*,*) alpha

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=1.0d0

!--- pi

      pi=dacos(-1.0d0)

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,ll
       x(i)=dx*dfloat(i-1)+xl
      enddo

      do ie=1,ll-1
       nbool(ie,1)=ie
       nbool(ie,2)=ie+1
      enddo

      nbool(ll,1)=ll
      nbool(ll,2)=1

!//////////////////////////////////////////
!/// coefficients in the shape function
!//////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0

!//////////////////////////////////
!/// initial profile (at t = 0)
!//////////////////////////////////

      do i=1,ll
       phib(i)=dsin(2.0d0*pi*x(i))
      enddo

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,2
       do j=1,2
         inn(i,j)=0.5d0*(1.0d0+xi(i)*xi(j)/3.0d0)
         ind(i,j)=0.5d0*xi(j)
         idd(i,j)=0.5d0*xi(i)*xi(j)
       enddo
      enddo

!/////////////////////////////////
!/// time marching calculation
!/////////////////////////////////

      do k=1,n

!--- construction of the global matrix A

       a=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)

          a(nu,j1)=a(nu,j1)+0.5d0*dx*inn(i,1)
          a(nu,j2)=a(nu,j2)+0.5d0*dx*inn(i,2)
        enddo
       enddo

!--- sum of the advection and diffusion terms in the discretized equation

       b=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)

          b(nu)=b(nu)-u*(ind(i,1)*phib(j1)              &
                        +ind(i,2)*phib(j2))             &
                     -2.0d0/dx*alpha*(idd(i,1)*phib(j1) &
                                     +idd(i,2)*phib(j2))
        enddo
       enddo

!--- solve a system of linear equations using Gaussian elimination

       do kk=1,ll-1
        id=kk
        gg=dabs(a(kk,kk))
        do ii=kk+1,ll
         ah=dabs(a(ii,kk))
         if(ah > gg) then
          id=ii
          gg=ah
         endif
        enddo
        do jj=kk,ll
         w=a(kk,jj)
         a(kk,jj)=a(id,jj)
         a(id,jj)=w
        enddo
        w=b(kk)
        b(kk)=b(id)
        b(id)=w

        ppp=a(kk,kk)
        do i=kk+1,ll
         q=a(i,kk)/ppp
         do jj=kk,ll
           a(i,jj)=a(i,jj)-q*a(kk,jj)
         enddo
         b(i)=b(i)-q*b(kk)
        enddo
       enddo

       c(ll)=b(ll)/a(ll,ll)

       do kk=ll-1,1,-1
        s=b(kk)
        do jj=kk+1,ll
         s=s-a(kk,jj)*c(jj)
        enddo
        c(kk)=s/a(kk,kk)
       enddo

       b=c

!--- calculate phi at the n+1 time step

       do i=1,ll
        phi(i)=phib(i)+dt*b(i)
       enddo

       phib=phi
      enddo

!////////////////////////////////////////////////////
!/// output of the numerical solution at t = n*dt
!////////////////////////////////////////////////////

      do i=1,ll
       write(11,*) x(i),phi(i)
      enddo

      write(11,*) xr,phi(1)

      stop
      end program advec_diffusion_1d_linear_pbc
