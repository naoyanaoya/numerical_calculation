!****************************************************************************
!   finite element discretization for the 1d advection-diffusion equation   *
!   using the lumped mass matrix (periodic b.c.)                            *
!                                                                           *
!    - piecewise quadratic interpolation                                    *
!                                                                           *
!                         edited by haruhiko kohno                          *
!****************************************************************************

      program advec_diffusion_1d_quadratic_lumped_pbc

      implicit none
      integer :: i,ie,j,k,nu
      integer :: np1,np2,np3,i1,i2,i3,j1,j2,j3
      integer :: ll,n,nnode
      real(8) :: dt,u,alpha,xl,xr,pi,dx
      integer, dimension(1000,3) :: nbool
      real(8), dimension(1000) :: x,phib,fm,pp,phi
      real(8), dimension(3) :: xi
      real(8), dimension(3,3) :: ind,idd

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number: ll'
      read(*,*) ll

      write(*,*) 'input the time increment: dt'
      read(*,*) dt

      write(*,*) 'input the number of steps: n'
      read(*,*) n

      write(*,*) 'input the advection velocity: u'
      read(*,*) u

      write(*,*) 'input the diffusion coefficient: alpha'
      read(*,*) alpha

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=1.0d0

!--- number of nodes and pi

      nnode=2*ll
      pi=dacos(-1.0d0)

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,ll
       x(i)=dx*dfloat(i-1)+xl
      enddo

      do ie=1,ll-1
       np1=ie
       np2=np1+1

       nbool(ie,1)=np1
       nbool(ie,2)=np2

       i1=nbool(ie,1)
       i2=nbool(ie,2)

       np3=ll+ie
       x(np3)=0.5d0*(x(i1)+x(i2))
       nbool(ie,3)=np3
      enddo

      nbool(ll,1)=ll
      nbool(ll,2)=1
      nbool(ll,3)=2*ll
      x(2*ll)=0.5d0*(x(ll)+xr)

!//////////////////////////////////////////
!/// coefficients in the shape function
!//////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0
      xi(3)= 0.0d0

!//////////////////////////////////
!/// initial profile (at t = 0)
!//////////////////////////////////

      do i=1,nnode
       phib(i)=dsin(2.0d0*pi*x(i))
      enddo

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,3
       do j=1,3
         ind(i,j)=2.0d0/3.0d0*(xi(j)-xi(i)) &
                 +xi(i)*xi(j)/2.0d0         &
                 *(2.0d0*xi(j)-xi(i))

         idd(i,j)=xi(i)*xi(j)/2.0d0      &
                 +2.0d0/3.0d0            &
                 *(2.0d0-3.0d0*xi(i)**2) &
                 *(2.0d0-3.0d0*xi(j)**2)
       enddo
      enddo

!///////////////////////////////////////////////////
!/// diagonal elements of the lumped mass matrix
!///////////////////////////////////////////////////

      fm=0.0d0

      do i=1,3
       do ie=1,ll
         nu=nbool(ie,i)
         fm(nu)=fm(nu)+dx*(-0.5d0*xi(i)**2+2.0d0/3.0d0)
       enddo
      enddo

!/////////////////////////////////
!/// time marching calculation
!/////////////////////////////////

      do k=1,n

!--- sum of the advection and diffusion terms in the discretized equation

       pp=0.0d0

       do i=1,3
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)
          j3=nbool(ie,3)

          pp(nu)=pp(nu)-u*(ind(i,1)*phib(j1)              &
                          +ind(i,2)*phib(j2)              &
                          +ind(i,3)*phib(j3))             &
                       -2.0d0/dx*alpha*(idd(i,1)*phib(j1) &
                                       +idd(i,2)*phib(j2) &
                                       +idd(i,3)*phib(j3))
        enddo
       enddo

!--- calculate phi at the n+1 time step

       do i=1,nnode
        phi(i)=phib(i)+dt*pp(i)/fm(i)
       enddo

       phib=phi
      enddo

!////////////////////////////////////////////////////
!/// output of the numerical solution at t = n*dt
!////////////////////////////////////////////////////

      do ie=1,ll
       i1=nbool(ie,1)
       i2=nbool(ie,2)
       i3=nbool(ie,3)

       write(11,*) x(i1),phi(i1)
       write(11,*) x(i3),phi(i3)

       if(ie == ll) then
        write(11,*) xr,phi(i2)
       endif
      enddo

      stop
      end program advec_diffusion_1d_quadratic_lumped_pbc
