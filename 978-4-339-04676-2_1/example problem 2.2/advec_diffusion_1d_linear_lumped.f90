!****************************************************************************
!   finite element discretization for the 1d advection-diffusion equation   *
!   using the lumped mass matrix                                            *
!                                                                           *
!                         edited by haruhiko kohno                          *
!****************************************************************************

      program advec_diffusion_1d_linear_lumped

      implicit none
      integer :: i,ie,j,k,nu
      integer :: j1,j2
      integer :: ll,n,l1
      real(8) :: dt,u,alpha,xl,xr,pi,dx
      integer, dimension(1000,2) :: nbool
      real(8), dimension(1000) :: x,phib,fm,pp,phi
      real(8), dimension(2) :: xi
      real(8), dimension(2,2) :: ind,idd

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number: ll'
      read(*,*) ll

      write(*,*) 'input the time increment: dt'
      read(*,*) dt

      write(*,*) 'input the number of steps: n'
      read(*,*) n

      write(*,*) 'input the advection velocity: u'
      read(*,*) u

      write(*,*) 'input the diffusion coefficient: alpha'
      read(*,*) alpha

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=3.0d0

!--- number of nodes l1 and pi

      l1=ll+1
      pi=dacos(-1.0d0)

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,l1
       x(i)=dx*dfloat(i-1)+xl
       if(i == l1) x(l1)=xr
      enddo

      do ie=1,ll
       nbool(ie,1)=ie
       nbool(ie,2)=ie+1
      enddo

!//////////////////////////////////////////
!/// coefficients in the shape function
!//////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0

!//////////////////////////////////
!/// initial profile (at t = 0)
!//////////////////////////////////

      do i=1,l1
       if(x(i) <= 0.75d0) then
        phib(i)=0.0d0
       elseif((x(i) > 0.75d0).and.(x(i) <= 1.25d0)) then
        phib(i)=(dcos(2.0d0*pi*(x(i)-1.0d0)))**2
       else
        phib(i)=0.0d0
       endif
      enddo

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,2
       do j=1,2
         ind(i,j)=0.5d0*xi(j)
         idd(i,j)=0.5d0*xi(i)*xi(j)
       enddo
      enddo

!///////////////////////////////////////////////////
!/// diagonal elements of the lumped mass matrix
!///////////////////////////////////////////////////

      fm=0.0d0

      do i=1,2
       do ie=1,ll
         nu=nbool(ie,i)
         fm(nu)=fm(nu)+0.5d0*dx
       enddo
      enddo

!/////////////////////////////////
!/// time marching calculation
!/////////////////////////////////

      do k=1,n

!--- sum of the advection and diffusion terms in the discretized equation

       pp=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)

          pp(nu)=pp(nu)-u*(ind(i,1)*phib(j1)              &
                          +ind(i,2)*phib(j2))             &
                       -2.0d0/dx*alpha*(idd(i,1)*phib(j1) &
                                       +idd(i,2)*phib(j2))
        enddo
       enddo

!--- calculate phi at the n+1 time step

       do i=2,ll
        phi(i)=phib(i)+dt*pp(i)/fm(i)
       enddo

       phi(1)=phib(1)
       phi(l1)=phib(l1)

       phib=phi
      enddo

!////////////////////////////////////////////////////
!/// output of the numerical solution at t = n*dt
!////////////////////////////////////////////////////

      do i=1,l1
       write(11,*) x(i),phi(i)
      enddo

      stop
      end program advec_diffusion_1d_linear_lumped
