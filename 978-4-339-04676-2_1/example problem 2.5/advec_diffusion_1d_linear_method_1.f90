!****************************************************************************
!   finite element discretization for the 1d advection-diffusion equation   *
!   without using the lumped mass matrix                                    *
!                                                                           *
!    - The Neumann BC is substituted into the boundary integral term.       *
!                                                                           *
!                         edited by haruhiko kohno                          *
!****************************************************************************

      program advec_diffusion_1d_linear_method_1

      implicit none
      integer :: i,ie,ii,j,jj,k,kk,nu
      integer :: j1,j2
      integer :: ll,n,l1
      integer :: id
      real(8) :: dt,u,alpha,xl,xr,pi,dx
      real(8) :: gg,ah,w,ppp,q,s
      integer, dimension(1000,2) :: nbool
      real(8), dimension(1000) :: x,phib,b,c,phi
      real(8), dimension(2) :: xi
      real(8), dimension(2,2) :: inn,ind,idd
      real(8), dimension(1000,1000) :: a

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number: ll'
      read(*,*) ll

      write(*,*) 'input the time increment: dt'
      read(*,*) dt

      write(*,*) 'input the number of steps: n'
      read(*,*) n

      write(*,*) 'input the advection velocity: u'
      read(*,*) u

      write(*,*) 'input the diffusion coefficient: alpha'
      read(*,*) alpha

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=2.0d0

!--- number of nodes l1 and pi

      l1=ll+1
      pi=dacos(-1.0d0)

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,l1
       x(i)=dx*dfloat(i-1)+xl
       if(i == l1) x(l1)=xr
      enddo

      do ie=1,ll
       nbool(ie,1)=ie
       nbool(ie,2)=ie+1
      enddo

!//////////////////////////////////////////
!/// coefficients in the shape function
!//////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0

!//////////////////////////////////
!/// initial profile (at t = 0)
!//////////////////////////////////

      do i=1,l1
       if(x(i) <= 0.75d0) then
        phib(i)=0.0d0
       elseif((x(i) > 0.75d0).and.(x(i) <= 1.25d0)) then
        phib(i)=(dcos(2.0d0*pi*(x(i)-1.0d0)))**2
       else
        phib(i)=0.0d0
       endif
      enddo

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,2
       do j=1,2
         inn(i,j)=0.5d0*(1.0d0+xi(i)*xi(j)/3.0d0)
         ind(i,j)=0.5d0*xi(j)
         idd(i,j)=0.5d0*xi(i)*xi(j)
       enddo
      enddo

!/////////////////////////////////
!/// time marching calculation
!/////////////////////////////////

      do k=1,n

!--- construction of the global matrix A

       a=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)

          a(nu,j1)=a(nu,j1)+0.5d0*dx*inn(i,1)
          a(nu,j2)=a(nu,j2)+0.5d0*dx*inn(i,2)
        enddo
       enddo

       do j=1,l1
        a(1,j)=0.0d0
       enddo

       a(1,1)=1.0d0

!--- sum of the advection and diffusion terms in the discretized equation

       b=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          j1=nbool(ie,1)
          j2=nbool(ie,2)

          b(nu)=b(nu)-u*(ind(i,1)*phib(j1)              &
                        +ind(i,2)*phib(j2))             &
                     -2.0d0/dx*alpha*(idd(i,1)*phib(j1) &
                                     +idd(i,2)*phib(j2))
        enddo
       enddo

       b(1)=0.0d0

!--- solve a system of linear equations using Gaussian elimination

       do kk=1,ll
        id=kk
        gg=dabs(a(kk,kk))
        do ii=kk+1,l1
         ah=dabs(a(ii,kk))
         if(ah > gg) then
          id=ii
          gg=ah
         endif
        enddo
        do jj=kk,l1
         w=a(kk,jj)
         a(kk,jj)=a(id,jj)
         a(id,jj)=w
        enddo
        w=b(kk)
        b(kk)=b(id)
        b(id)=w

        ppp=a(kk,kk)
        do i=kk+1,l1
         q=a(i,kk)/ppp
         do jj=kk,l1
           a(i,jj)=a(i,jj)-q*a(kk,jj)
         enddo
         b(i)=b(i)-q*b(kk)
        enddo
       enddo

       c(l1)=b(l1)/a(l1,l1)

       do kk=ll,1,-1
        s=b(kk)
        do jj=kk+1,l1
         s=s-a(kk,jj)*c(jj)
        enddo
        c(kk)=s/a(kk,kk)
       enddo

       b=c

!--- calculate phi at the n+1 time step

       do i=2,l1
        phi(i)=phib(i)+dt*b(i)
       enddo

       phi(1)=phib(1)
       phib=phi
      enddo

!////////////////////////////////////////////////////
!/// output of the numerical solution at t = n*dt
!////////////////////////////////////////////////////

      do i=1,l1
       write(11,*) x(i),phi(i)
      enddo

      stop
      end program advec_diffusion_1d_linear_method_1
