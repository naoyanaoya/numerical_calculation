!******************************************************************
!   finite element discretization for the 1d Helmholtz equation   *
!   without using the lumped mass matrix                          *
!                                                                 *
!                    edited by haruhiko kohno                     *
!******************************************************************

      program helmholtz_1d_linear

      implicit none
      integer :: i,ie,ii,j,jj,kk,nu
      integer :: j1,j2
      integer :: ll,l1
      integer :: id
      real(8) :: kdp1,kdp2,phil,dphir_dx,xl,xr,pi,dx
      real(8) :: gg,ah,w,ppp,q,s
      integer, dimension(1000,2) :: nbool
      integer, dimension(1000) :: ncod
      real(8), dimension(1000) :: kw,x,b,c,phi
      real(8), dimension(2) :: xi
      real(8), dimension(2,2) :: inn,idd
      real(8), dimension(1000,1000) :: a

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number (even number): ll'
      read(*,*) ll

      write(*,*) 'input two different parameters: k1/pi, k2/pi'
      read(*,*) kdp1,kdp2

      write(*,*) 'phi1 at x = xl'
      read(*,*) phil

      write(*,*) 'dphi2/dx at x = xr'
      read(*,*) dphir_dx

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=2.0d0

!--- number of nodes, pi, and the coefficients of the equations

      l1=ll+1
      pi=dacos(-1.0d0)

      do ie=1,ll/2
       kw(ie)=(kdp1*pi)**2
      enddo
      do ie=ll/2+1,ll
       kw(ie)=(kdp2*pi)**2
      enddo

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,l1
       x(i)=dx*dfloat(i-1)+xl
       if(i == l1) x(l1)=xr
      enddo

      do ie=1,ll
       nbool(ie,1)=ie
       nbool(ie,2)=ie+1
      enddo

!/////////////////////////
!/// condition numbers
!/////////////////////////

      ncod=0

      ncod(1)=1
      ncod(l1)=2

!///////////////////////////////////////////
!/// coefficients in the shape functions
!///////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,2
       do j=1,2
         inn(i,j)=0.5d0*(1.0d0+xi(i)*xi(j)/3.0d0)
         idd(i,j)=0.5d0*xi(i)*xi(j)
       enddo
      enddo

!////////////////////////////////////////////////////
!/// solving a system of linear equations, Ax = b
!////////////////////////////////////////////////////

!--- construction of the global matrix A

       a=0.0d0

       do i=1,2
        do ie=1,ll
          nu=nbool(ie,i)
          if(ncod(nu) /= 1) then
           j1=nbool(ie,1)
           j2=nbool(ie,2)

           a(nu,j1)=a(nu,j1)-2.0d0/dx*idd(i,1) &
                            +0.5d0*dx*kw(ie)*inn(i,1)
           a(nu,j2)=a(nu,j2)-2.0d0/dx*idd(i,2) &
                            +0.5d0*dx*kw(ie)*inn(i,2)
          endif
        enddo
       enddo

       a(1,1)=1.0d0

!--- the vector b on the right-hand side of the equation

       b=0.0d0

       b(1)=phil
       b(l1)=-dphir_dx

!--- solve a system of linear equations using Gaussian elimination

       do kk=1,ll
        id=kk
        gg=dabs(a(kk,kk))
        do ii=kk+1,l1
         ah=dabs(a(ii,kk))
         if(ah > gg) then
          id=ii
          gg=ah
         endif
        enddo
        do jj=kk,l1
         w=a(kk,jj)
         a(kk,jj)=a(id,jj)
         a(id,jj)=w
        enddo
        w=b(kk)
        b(kk)=b(id)
        b(id)=w

        ppp=a(kk,kk)
        do i=kk+1,l1
         q=a(i,kk)/ppp
         do jj=kk,l1
           a(i,jj)=a(i,jj)-q*a(kk,jj)
         enddo
         b(i)=b(i)-q*b(kk)
        enddo
       enddo

       c(l1)=b(l1)/a(l1,l1)

       do kk=ll,1,-1
        s=b(kk)
        do jj=kk+1,l1
         s=s-a(kk,jj)*c(jj)
        enddo
        c(kk)=s/a(kk,kk)
       enddo

       phi=c

!////////////////////////////////////////
!/// output of the numerical solution
!////////////////////////////////////////

      do i=1,l1
       write(11,*) x(i),phi(i)
      enddo

      stop
      end program helmholtz_1d_linear
