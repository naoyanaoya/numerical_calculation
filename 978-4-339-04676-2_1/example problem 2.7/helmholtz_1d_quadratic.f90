!******************************************************************
!   finite element discretization for the 1d Helmholtz equation   *
!   without using the lumped mass matrix                          *
!                                                                 *
!                    edited by haruhiko kohno                     *
!******************************************************************

      program helmholtz_1d_quadratic

      implicit none
      integer :: i,ie,ii,j,jj,kk,nu
      integer :: i1,i2,i3,j1,j2,j3
      integer :: ll,l1,nnode
      integer :: id
      real(8) :: kdp1,kdp2,phil,dphir_dx,xl,xr,pi,dx
      real(8) :: gg,ah,w,ppp,q,s
      integer, dimension(1000,3) :: nbool
      integer, dimension(1000) :: ncod
      real(8), dimension(1000) :: kw,x,b,c,phi
      real(8), dimension(3) :: xi
      real(8), dimension(3,3) :: inn,idd
      real(8), dimension(1000,1000) :: a

!////////////////////////
!/// input parameters
!////////////////////////

      write(*,*) 'input the division number (even number): ll'
      read(*,*) ll

      write(*,*) 'input two different parameters: k1/pi, k2/pi'
      read(*,*) kdp1,kdp2

      write(*,*) 'phi1 at x = xl'
      read(*,*) phil

      write(*,*) 'dphi2/dx at x = xr'
      read(*,*) dphir_dx

!--- coordinates at the left and right edges of the domain: xl, xr

      xl=0.0d0
      xr=2.0d0

!--- number of nodes, pi, and the coefficients of the equations

      l1=ll+1
      nnode=l1+ll
      pi=dacos(-1.0d0)

      do ie=1,ll/2
       kw(ie)=(kdp1*pi)**2
      enddo
      do ie=ll/2+1,ll
       kw(ie)=(kdp2*pi)**2
      enddo

!///////////////////////////////////////////////////
!/// coordinates of the spatial points and nbool
!///////////////////////////////////////////////////

      dx=(xr-xl)/dfloat(ll)

      do i=1,l1
       x(i)=dx*dfloat(i-1)+xl
       if(i == l1) x(l1)=xr
      enddo

      do ie=1,ll
       nbool(ie,1)=ie
       nbool(ie,2)=ie+1

       i1=nbool(ie,1)
       i2=nbool(ie,2)

       x(l1+ie)=0.5d0*(x(i1)+x(i2))
       nbool(ie,3)=l1+ie
      enddo

!/////////////////////////
!/// condition numbers
!/////////////////////////

      ncod=0

      ncod(1)=1
      ncod(l1)=2

!///////////////////////////////////////////
!/// coefficients in the shape functions
!///////////////////////////////////////////

      xi(1)=-1.0d0
      xi(2)= 1.0d0
      xi(3)= 0.0d0

!///////////////////////////////////////////////////////////////
!/// coefficient matrices independent of the element lengths
!///////////////////////////////////////////////////////////////

      do i=1,3
       do j=1,3
         inn(i,j)=xi(i)*xi(j)/6.0d0 &
                 +(32.0d0           &
                  -28.0d0*xi(i)**2  &
                  -28.0d0*xi(j)**2  &
                  +27.0d0*xi(i)**2*xi(j)**2)/30.0d0

         idd(i,j)=xi(i)*xi(j)/2.0d0      &
                 +2.0d0/3.0d0            &
                 *(2.0d0-3.0d0*xi(i)**2) &
                 *(2.0d0-3.0d0*xi(j)**2)
       enddo
      enddo

!////////////////////////////////////////////////////
!/// solving a system of linear equations, Ax = b
!////////////////////////////////////////////////////

!--- construction of the global matrix A

       a=0.0d0

       do i=1,3
        do ie=1,ll
          nu=nbool(ie,i)
          if(ncod(nu) /= 1) then
           j1=nbool(ie,1)
           j2=nbool(ie,2)
           j3=nbool(ie,3)

           a(nu,j1)=a(nu,j1)-2.0d0/dx*idd(i,1) &
                            +0.5d0*dx*kw(ie)*inn(i,1)
           a(nu,j2)=a(nu,j2)-2.0d0/dx*idd(i,2) &
                            +0.5d0*dx*kw(ie)*inn(i,2)
           a(nu,j3)=a(nu,j3)-2.0d0/dx*idd(i,3) &
                            +0.5d0*dx*kw(ie)*inn(i,3)
          endif
        enddo
       enddo

       a(1,1)=1.0d0

!--- the vector b on the right-hand side of the equation

       b=0.0d0

       b(1)=phil
       b(l1)=-dphir_dx

!--- solve a system of linear equations using Gaussian elimination

       do kk=1,nnode-1
        id=kk
        gg=dabs(a(kk,kk))
        do ii=kk+1,nnode
         ah=dabs(a(ii,kk))
         if(ah > gg) then
          id=ii
          gg=ah
         endif
        enddo
        do jj=kk,nnode
         w=a(kk,jj)
         a(kk,jj)=a(id,jj)
         a(id,jj)=w
        enddo
        w=b(kk)
        b(kk)=b(id)
        b(id)=w

        ppp=a(kk,kk)
        do i=kk+1,nnode
         q=a(i,kk)/ppp
         do jj=kk,nnode
           a(i,jj)=a(i,jj)-q*a(kk,jj)
         enddo
         b(i)=b(i)-q*b(kk)
        enddo
       enddo

       c(nnode)=b(nnode)/a(nnode,nnode)

       do kk=nnode-1,1,-1
        s=b(kk)
        do jj=kk+1,nnode
         s=s-a(kk,jj)*c(jj)
        enddo
        c(kk)=s/a(kk,kk)
       enddo

       phi=c

!////////////////////////////////////////
!/// output of the numerical solution
!////////////////////////////////////////

      do ie=1,ll
       i1=nbool(ie,1)
       i2=nbool(ie,2)
       i3=nbool(ie,3)

       write(11,*) x(i1),phi(i1)
       write(11,*) x(i3),phi(i3)

       if(ie == ll) then
        write(11,*) x(i2),phi(i2)
       endif
      enddo

      stop
      end program helmholtz_1d_quadratic
