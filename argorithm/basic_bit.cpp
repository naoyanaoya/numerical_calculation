#include <iostream>
#include <vector>

int main(){
    int n = 3;

    // {0, 1, ..., n-1}の部分集合の全探索
    // (1<<n)は2^nと覚える
    for(int bit = 0;bit<(1<<n);bit++){
        std::vector<int> S;
        for(int i = 0;i<n;i++){
            if(bit & (1<<i)){ // 列挙にiが含まれるか
                S.push_back(i);
            }
        }

        std::cout << bit << ": {";
        for(int i=0;i<(int)S.size();i++){
            std::cout << S[i] << " ";
        }
        std::cout << "}" << std::endl;
    }
}