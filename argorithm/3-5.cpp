// 部分和問題に対するビット演算を持ちいる全探索解法
#include <iostream>
#include <vector>

/*
4
10
1 5 8 11
*/

int main(){
    int N, W;
    std::cin >> N >> W;
    std::vector<int> a(N);
    for(int i=0;i<N;i++){
        std::cin >> a[i];
    }
    

    // bitは2^N通りの部分集合を動く
    bool exist = false;
    for(int bit=0;bit<(1<<N);bit++){
        std::cout << "================" << std::endl;
        int sum = 0; // 部分集合に含まれる要素の和
        for(int i=0;i<N;i++){
            // i番目の要素a[i]が部分集合に含まれるかどうか
            if(bit & (1 << i)){
                std::cout << (1<<i) << std::endl;
                sum += a[i];
            }
        }

        // sumがWに一致するかどうか
        std::cout << "sum : " << sum << std::endl;
        if(sum == W){
            exist = true;
        }
    }

    if(exist){
        std::cout << "yes" << std::endl;
    }else{
        std::cout << "no" << std::endl;
    }
}