! gfortran call_lapack.f90 -o call_lapack -llapack
program call_lapack
  implicit none
  integer n, i, j, info
  double precision, allocatable :: a(:,:), b(:), ipiv(:)
  
  ! データを読み込む＆配列の領域確保
  read *, n
  allocate (a(n,n))
  allocate (b(n))
  allocate (ipiv(n))
  read *, ((a(i,j),j=1,n),i=1,n), (b(i),i=1,n)
  
  ! ライブラリルーチンの呼び出し
  call dgesv(n,1,a,n,ipiv,b,n,info)
  
  ! 結果を出力
  print *
  print *, "Solution"
  print "((f9.4))", b

end program call_lapack

