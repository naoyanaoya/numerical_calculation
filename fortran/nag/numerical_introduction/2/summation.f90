program summation
  implicit none
  integer i
  integer, parameter :: n = 20
  real total, a(n)

  ! read numbers
  open (11, file='summation-data.txt', status='old')
  read (11, *, end=999)(a(i), i=1, n)
  close (11)
  999 print *, "Done reading!"

  ! compute sum
  total = 0
  do i = 1, n
    total = total + a(i)
  end do

  ! print result
  print '(''true value     = '',f5.0)', n/2.0
  print '(''computed value = '',f5.0)', total
end program
