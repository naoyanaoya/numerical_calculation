program summation_sorted
  implicit none
  integer i, j
  integer, parameter :: n = 20
  real total, tmp, a(n)

  ! read numbers
  open (11, file='summation-data.txt', status='old')
  read (11, *)(a(i), i=1, n)
  close (11)

  ! sort numbers by magnitude (selection sort)
  do i = 1, n - 1
    do j = i + 1, n
      if (abs(a(i))>abs(a(j))) then
        tmp = a(i)
        a(i) = a(j)
        a(j) = tmp
      end if
    end do
  end do

  ! compute sum
  total = 0
  do i = 1, n
    total = total + a(i)
  end do

  ! print result
  print '(''true value     = '',f5.0)', n/2.0
  print '(''computed value = '',f5.0)', total
end program