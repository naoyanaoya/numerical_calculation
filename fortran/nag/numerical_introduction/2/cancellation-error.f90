! 桁落ち
program cancellation
  implicit none
  real a, b
  a = 0.1234
  b = 0.1231
  print '(F20.12)', a-b
  print '(F20.12)', 0.0003
end program cancellation