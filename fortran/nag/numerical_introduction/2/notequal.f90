program not_equal
  implicit none
  real :: a = 0.3
  real :: b = 7
  real :: c = 2.1
  if( c == a*b ) then
    print *, 'a x b = c'
  else
    print *, 'a x b <> c'
  end if
end program