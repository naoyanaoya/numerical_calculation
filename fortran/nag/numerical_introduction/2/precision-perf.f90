program precision_perf
  implicit none
  integer, parameter :: wp = selected_real_kind(6) ! 6=SP, 14=DP, 28=QP
  integer, parameter :: n = 1000
  integer t1, t2, t_rate
  integer i, j, k
  real(wp), allocatable :: a(:, :)
  real(wp) val
  allocate(a(n,n))
  a = 1.0_wp
  val = 1.23_wp
  call system_clock(t1, t_rate)
  do i = 1, n
    do j = 1, n
      do k = 1, 100
        a(j, i) = a(j, i) + val
        a(j, i) = a(j, i) * val
        a(j, i) = a(j, i) - val
        a(j, i) = a(j, i) / val
      end do
    end do
  end do
  call system_clock(t2)
  print '(A, F10.3)', 'time it took was:',(t2-t1)/dble(t_rate)
end program