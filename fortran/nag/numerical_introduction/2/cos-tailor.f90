program taylor
  implicit none
  double precision x, mycos, factorial
  integer i, j
  print *, "Enter x:"
  read *, x
  mycos = 0d0
  do i = 0, 3
    ! compute(2i)!
    factorial = 1d0
    do j = 2, i*2
      factorial = factorial * j
    end do
    mycos = mycos +(-1)**i * x**(i*2) / factorial
  end do
  print *, mycos
end program