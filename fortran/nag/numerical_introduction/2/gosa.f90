program gosa
  implicit none
  integer i
  double precision delta, time
  delta = 1.0d0
  time = 0d0
  do i = 1, 1000
    time = time + delta
    print *, time
  end do
  print '("time = ", f18.13)', time
end program gosa