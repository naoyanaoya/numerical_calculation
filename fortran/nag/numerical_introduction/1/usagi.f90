program usagi
  implicit none
  integer :: u0 = 1
  integer :: u1 = 1
  integer i, num_usagi_pairs
  do i=2, 12
    num_usagi_pairs = u0 + u1
    print *, "Month =", i, "  num_usagi_pairs =", num_usagi_pairs
    u0 = u1
    u1 = num_usagi_pairs
  end do
end program usagi