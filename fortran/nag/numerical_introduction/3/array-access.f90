program array_access
  implicit none
  integer,parameter :: n = 512
  integer a(n,n,n)
  integer val, i, j, k
  real t1, t2
  print *, "Enter a value:"
  read *, val
  call cpu_time(t1)
  do k=1, n
    do j=1, n
      do i=1, n
        ! a(k, j, i) = val
        a(i, j, k) = val
      end do
    end do
  end do
  call cpu_time(t2)
  print *, "Time was:", t2-t1
end program array_access