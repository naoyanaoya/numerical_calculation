program trapezoidal_integration
  implicit none
  integer n, i
  double precision a, b, sum, delta, x1, x2
  a = 0d0
  b = 1d0
  n = 1000
  sum = 0d0
  delta = (b-a)/n
  do i = 1, n
    x1 = a+delta*(i-1)
    x2 = a+delta*i
    sum = sum + f(x1) + f(x2)
  end do
  print *, "Result =", sum * delta / 2
contains
  function f(x)
    double precision f, x
    f = 4/(1+x**2)
  end function f
end program trapezoidal_integration