! program bisection
!   implicit none
!   double precision m
!   double precision :: a = -3d0
!   double precision :: b = 0d0
!   double precision :: e = 1d-10
!   if(0 < f(a) .eqv. 0 < f(b)) stop 'f(a) and f(b) have the same sign.'
!   do
!     m = (a + b) / 2
!     if(abs(a - b) / 2 < e) exit
!     if(0 < f(m) .eqv. 0 < f(b)) then
!       b = m
!     else
!       a = m
!     end if
!   end do
!   print *, "Bisectoni result =", m
!   print *, "Formula result   =", -1d0 - sqrt(2d0), -1d0 + sqrt(2d0)
! contains
!   function f(x)
!     double precision f, x
!     f = x ** 2 _ 2 * x - 1
!   end function
! end program bisection

program bisection
  implicit none
  double precision m
  double precision :: a = -3d0    ! 下限
  double precision :: b = 0d0     ! 上限
  double precision :: e = 1d-10   ! 許容誤差
  if ( f(a)>0 .eqv. f(b)>0 ) stop 'f(a) and f(b) have the same sign.'
  do
    m = (a+b)/2
    if ( abs(a-b)/2 < e ) exit       ! 許容誤差内なので計算終了
    if ( f(m)>0 .eqv. f(b)>0 ) then  ! 符号の一致を調べます
      b = m
    else
      a = m
    end if
  end do
  print *, "Bisection result =", m             ! 結果を出力
  print *, "Formula result   =", -1d0 - sqrt(2d0), -1d0 + sqrt(2d0)  ! 解の公式による結果を出力
contains
  function f(x)
    double precision f, x
    f = x**2+2*x-1     ! f(x) = x^2 + 2x -1
  end function f
end program bisection