program gauss
  implicit none
  integer n, i, j, k
  double precision, allocatable :: a(:,:), b(:)
  double precision c, sum
  
  ! データを読み込む＆配列の領域確保
  read *, n
  allocate (a(n,n))
  allocate (b(n))
  read *, ((a(i,j),j=1,n),i=1,n), (b(i),i=1,n)

  !前進消去 (forward elimination)
  do k = 1, n - 1
    do i = k + 1, n
      c = a(i,k)/a(k,k)
      do j = k + 1, n
        a(i,j) = a(i,j) - c*a(k,j)
      end do
      b(i) = b(i) - c*b(k)
    end do
  end do

  !後退代入 (backward substitution) 
  b(n) = b(n)/a(n,n)
  do i = n - 1, 1, -1
    sum = 0.0d0
    do k = i + 1, n
      sum = sum + a(i,k)*b(k)
    end do
    b(i) = (b(i)-sum)/a(i,i)
  end do
  
  !結果を出力
  print *
  print *, "Solution"
  print "((f9.4))", b

end program gauss

