! 経過時間計測のサンプル
! program sys_clock
!   implicit none
!   integer t1, t2, t_rate, t_max, diff
!   call system_clock(t1)   ! 開始時を記録
!   !
!   ! 計算コード
!   !
!   call system_clock(t2, t_rate, t_max)   ! 終了時を記録
!   if ( t2 < t1 ) then
!     diff = (t_max - t1) + t2 + 1
!   else
!     diff = t2 - t1
!   endif
!   print "(A, F10.3)", "time it took was:", diff/dble(t_rate)
! end program sys_clock

! CPU時間計測のサンプル
program cputime
  implicit none
  real :: t1, t2
  call cpu_time( t1 )
  !
  ! 計算コード
  !
  call cpu_time( t2 )
  print *, "cpu time:", t2-t1, "seconds."
end program cputime