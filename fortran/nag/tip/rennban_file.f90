program write_numbered_files
  implicit none
  character filename*128
  integer i, j
  do i = 1, 5
    write (filename, '("xyz", i4.4, ".csv")') i
    open (17, file=filename, status='replace')
    do j = 1, 10
      write (17, '(i0,",",i0)') j, j*2
    end do
    close (17)
  end do
end program write_numbered_files