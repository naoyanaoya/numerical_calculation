! シンプルなcfvファイルを読み込むサンプル
program readSimpleCSV
  implicit none
  integer, parameter :: n = 5
  ! real x, y, z
  double precision x, y, z
  integer i
  open (17, file='mydata.csv', status='old')
  read (17, '()')       ! ヘッダ行の読み飛ばし
  do i = 1, n
    read (17, *) x, y, z
    print *, x, y, z
  end do
  close (17)
end program readSimpleCSV