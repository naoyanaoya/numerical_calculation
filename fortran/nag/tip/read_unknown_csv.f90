program readCSV
  implicit none
  integer n
  real x, y, z
  integer i
  open (17, file='mydata.csv', status='old')
  ! === レコード数を調べる ===
  n = 0
  read (17, '()')
  do
    read (17, *, end=100) x, y, z  ! ファイル終端ならば999に飛ぶ
    n = n + 1
  end do
100 continue
  rewind (17)  ! ファイルの最初に戻る
  print *, 'NumRec =', n
  ! === 読み込む ===
  read (17, '()')
  do i = 1, n
    read (17, *) x, y, z
    print *, x, y, z
  end do
  close (17)
end program readCSV