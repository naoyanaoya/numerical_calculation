program main
!use omp_lib
  implicit none
  double precision t1, t2
  call cpu_time( t1 )
  call cpu_time( t2 )
  print *, 'cpu time:', t2-t1, 'seconds.'
end program