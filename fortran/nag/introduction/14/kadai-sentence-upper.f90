program to_upper
  implicit none
  integer i
  character(len = 100) line
  read "(a)", line
  do i = 1, len_trim(line)
    if('a' <= line(i:i) .and. line(i:i) <= 'z') then
      line(i:i) = char(ichar(line(i:i)) - 32)
    end if
  end do
  print "(a)", trim(line)
end program to_upper