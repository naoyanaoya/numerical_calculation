program kadai_sosu
  implicit none
  integer i, j, n
  logical sosu_desu
  print *, "Please enter N:"
  read *, n
  do i = 2, n
    sosu_desu = .true.
    do j = 2, i - 1
      if( mod(i, j) == 0) then
        sosu_desu = .false.
        exit
      end if
    end do
    if(sosu_desu) print *, i
  end do
end program kadai_sosu