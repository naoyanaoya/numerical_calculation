! program main
!   real r
!   real, parameter :: pi = 3.14
!   r = 5.0
!   print *, r * r * pi
! end program main

! program main
!   character(3) a
!   character(len = 4) :: b = "ABCD"
!   a = "xyz"
!   print *, a
!   print *, b
! end program main

! program parameter_attr
!   implicit none
!   real real "" p = 3.14

!   p = 1.0

!   print *, "Enter radius:"
!   read *,

! ! 構造体を含む構造体の定義
! type point
!   real :: x, y
! end type point
! type line
!   type(point) :: p1, p2
! end type line

program type_example
  implicit none

  ! point型を定義
  type point
    real :: x, y
  end type point

  ! line型を定義
  type line
    type(point) :: p1, p2
  end type line

  ! line型の変数mを宣言
  type(line) m

  m = line(point(0.0,0.0),point(1.0,1.0))
  print *, m
end program type_example