program count_lines
  implicit none
  character(1000) filename
  integer counter

  print *, "Enter file name to count lines: "
  read "(a)", filename
  open(11, file=filename, status="old")
  counter = 0
  do
    read(11, "()", end=100)
    counter = counter + 1
  end do
100 close(11)
  print *, "Number of lines is", counter
end program count_lines