! 授受特性(intent)の使用意義を示すサンプル
program intent_error
  implicit none
  real x, y
  x = 1.0
  y = 2.0
  call mysub(x, y)
  print *, x, y
contains
  subroutine mysub(a, b)
    real, intent(in) :: a
    real, intent(out) :: b
    a = 5.0
    print *, b
  end subroutine
end program intent_error