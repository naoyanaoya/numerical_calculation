program kadai_kcal
  implicit none
  real kg, minute
  print *, "Please Enter taiju(kg)"
  read *, kg
  print *, "Please Enter Jogging Duration(minutes)"
  read *, minute
  print *, "Congulatulations! You have just burned", calc_kcal(kg, minute), "kcal!"
contains
  real function calc_kcal(weight, duration)
    real, intent(in) :: weight, duration
    calc_kcal = weight * (duration / 60.0) * 8.0 ! 戻り値を帰す場合には関数名と同じ変数にする
  end function calc_kcal
end program