! f90だと実行できない？

module mod_global
  implicit none

  integer, save :: i ! グローバル変数

end module mod_global

program main
  use mod_global
  implicit none

  ! グローバル変数iでループを回す
  do i = 1, 2
    call sub() ! この中でiが更新されてしまう。
  end do

  stop
contains
  subroutine sub()
    implicit none
    
    ! ここでもグローバル変数iでループを回す
    do i = 1, 3
      write(*,*) i
    end do

  end subroutine sub
end program main
