module mod_integrator
  implicit none

contains
  !
  ! 台形公式による数値積分
  !
  function trapezoid(f, dx) result(ret)
    implicit none
    real(8), intent(in) :: f(:)
    real(8), intent(in) :: dx
    real(8) :: ret

    integer :: i, n

    n = size(f)

    ret = 0.5_8 * (f(1) + f(n))
    do i = 2, n-1
       ret = ret + f(i)
    end do
    ret = ret * dx

  end function trapezoid

  !
  ! Simpson公式による数値積分
  !
  function simpson(f, dx) result(ret)
    implicit none
    real(8), intent(in) :: f(:)
    real(8), intent(in) :: dx
    real(8) :: ret

    integer :: i, n

    n = size(f)

    ! 端点を含めた配列サイズが奇数でなければエラー
    if( mod(n, 2) == 0 ) then
       write(*,*) 'array size must be odd'
       stop
    end if

    ret = f(1) + f(n)
    ! even
    do i = 2, n-1, 2
       ret = ret + 4.0_8 * f(i)
    end do
    ! odd
    do i = 3, n-2, 2
       ret = ret + 2.0_8 * f(i)
    end do
    ret = ret * dx / 3.0_8

  end function simpson
end module mod_integrator