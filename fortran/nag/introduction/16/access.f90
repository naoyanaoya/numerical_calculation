module mod
  implicit none
  integer,private :: numIteration  ! モジュール内でのみ利用可能
  private showNumIteration         ! モジュール内でのみ利用
  public init, calc                ! モジュール外から参照可能
contains
  subroutine showNumIteration()
    print *, "Number of iteration is", numIteration
  end subroutine showNumIteration
  subroutine init()
    numIteration = 0
  end subroutine init
  subroutine calc
    numIteration = numIteration + 1
    call showNumIteration()
  end subroutine calc
end module mod

program access_example
  use mod
  implicit none
  integer i
  call init
  do i=1, 5
    call calc
  end do
end program access_example