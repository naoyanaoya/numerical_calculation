program string_len
  implicit none
  character a*6
  a = "AB"
  print *, "len =", len(a)
  print *, "len_trim =", len_trim(a) ! 空白部分はカウントしない
  print *, 'trim(a) = "', trim(a), '"' ! 空白部分を取り去る
end program string_len