program find_bmi
  implicit none
  real bmi, h, w
  print *, "Enter SHINCHO(cm):"
  read *, h
  print *, "Enter TAIJU(kg):"
  read *, w
  h = h/100.0   ! cm を m に変換
  bmi = w/(h*h)
  print *, "BMI =", bmi
  if (bmi<18.5) then
    print *, "Yasegimidesu"
  else if (bmi<25.0) then
    print *, "Futsuudesu"
  else if (bmi<30.0) then
    print *, "Futorigimidesu"
  else
    print *, "Futorisugidesu"
  end if
end program find_bmi