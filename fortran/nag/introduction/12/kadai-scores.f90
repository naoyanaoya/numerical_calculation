program kadai_scores
  implicit none
  integer i, n
  integer, dimension(10) :: scores
  print *, "Please enther number of students:"
  read *, n
  do i = 1, n
    print *, "Enter score of student #", i
    read *, scores(i)
  end do
  do i = 1, n
    print *, "Student #", i, "=", scores(i)
  end do
end program kadai_scores

