! 主プログラムが内部副プログラムを含む場合
program main ! 主プログラム(プログラム単位)
  implicit none
  integer i ! 宣言部
  i = 99 ! 実行部
  call mysub(i) ! 内部副プログラムを呼び出している
  call my2ndSub(i) ! 内部副プログラムを呼び出している

contains ! contains文で区切る
  subroutine mysub(a) ! 内部副プログラム
    integer a
    print *, a
  end subroutine mysub
  subroutine my2ndSub(a) ! 複数定義する場合はこのように続ければ良い
    integer a
    print *, a * 2
  end subroutine my2ndSub
end program main


! 外部副プログラムが内部副プログラムを含む例
subroutine myextsub(x) ! 外部副プログラム(プログラム単位の一つ)
  implicit none
  integer x
  print *, myfunc(x) ! 内部副プログラムを呼び出している
contains
  function myfunc(b) ! 内部副プログラム
    integer myfunc, b
    myfunc = b + 1
  end function myfunc
end subroutine myextsub