! program kadaiSections
! !$ use omp_lib
!   implicit none
!   integer, parameter :: N = 10000
!   integer myid
!   real, allocatable :: a(:), b(:)
!   allocate( a(N), b(N))
!   call random_number(a)
!   call random_number(b)
! !$omp parallel
! !$omp sections
! !$omp section
!   myid = omp_get_thread_num()
!   print *, myid
!   call sort(a)
! !$omp section
!   myid = omp_get_thread_num()
!   print *, myid
!   call sort(b)
! !$omp end sections
! !$omp end parallel
!   print '("first 4 numbers in a : ",4f10.7)', a(1:4)
!   print '("first 4 numbers in b : ",4f10.7)', b(1:4)
! contains
!   subroutine sort(x)
!     real x(:), tmp
!     integer i, j
!     do i = 1, N - 1
!       do j = i + 1, N
!         if (x(j) < x(i)) then
!           tmp = x(i)
!           x(i) = x(j)
!           x(j) = tmp
!         end if
!       end do
!     end do
!   end subroutine sort
! end program

program kadaiSections
!$ use omp_lib
  implicit none
  integer, parameter :: N = 10000
  integer myid
  real,allocatable :: a(:), b(:)
  allocate( a(N), b(N) )
  call random_number(a)
  call random_number(b)
!$omp parallel
!$omp sections
!$omp section
  myid = omp_get_thread_num()
  print *, myid
  call sort(a)
!$omp section
  myid = omp_get_thread_num()
  print *, myid
  call sort(b)
!$omp end sections
!$omp end parallel
  print '("first 4 numbers in a : ",4f10.7)', a(1:4)
  print '("first 4 numbers in b : ",4f10.7)', b(1:4)
contains
  subroutine sort(x)
    real x(:), tmp
    integer i, j
    do i = 1, N - 1
      do j = i + 1, N
        if (x(i)>x(j)) then
          tmp = x(i)
          x(i) = x(j)
          x(j) = tmp
        end if
      end do
    end do
  end subroutine sort
end program