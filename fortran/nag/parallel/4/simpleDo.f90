program simpleDO
  implicit none
  integer,parameter :: N = 100
  integer i
  double precision x(N)
!$omp parallel
!$omp do
  do i=1,N
    ! print *, dble(i)
    x(i) = dble(i) / N
    ! print *, x(i)
  end do
!$omp end do
!$omp end parallel
  print *, sum(x)
end program