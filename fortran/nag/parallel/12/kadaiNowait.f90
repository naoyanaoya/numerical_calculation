program kadaiNowait
!$ use omp_lib
  implicit none
  integer i, a(100), b(50)
!$omp parallel
  !-------------------1st Loop-----------------
  print *, "My thread number is ", omp_get_thread_num()
!$omp do
  do i = 1, 100
    a(i) = i
  end do
!  !$omp end do !<--ここで同期がなされる
!$omp end do nowait!<--ここで同期がなされる
  print *, sum(a)
  !-----------------ENS 1st Loop--------------
  !-------------------2nd Loop-----------------
  print *, "My thread number is ", omp_get_thread_num()
!$omp do
  do i = 1, 50
    b(i) = i * 2
  end do
!$omp end do !<--ここで同期がなされる
  !-----------------ENS 2nd Loop--------------

!$omp end parallel
  print *, sum(b)
end program