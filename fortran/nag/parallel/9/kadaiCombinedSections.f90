program kadaiCombinedSections
!$ use omp_lib
  implicit none
  integer,parameter :: N = 100
  real x(N), y(N), z(N), xsum, ysum, zsum
  call random_number(x)
  call random_number(y)
  call random_number(z)
!$omp parallel sections
!$omp section
  xsum = sum(x)
!$omp section
  ysum = sum(y)
!$omp section
  zsum = sum(z)
!$omp end parallel sections
  print *, xsum, ysum, zsum
end program