! program exampleBarrier
! !$ use omp_lib
!   implicit none
!   integer id
!   real a(4), b(4), avg
!   a = 0
!   b = (/3, 2, 5, 1/)
! !$omp parallel num_threads(4) private(id)
!   id = omp_get_thread_num()
!   a(id + 1) = id
! !$omp barrier ! 明示的に同期を行う(バリアー)
! !$omp single
!   avg = sum(a)/ 4
!   print *, "Average =", avg
! !$omp end single
!   b(id + 1) = b(id + 1) - avg
! !$omp end parallel
!   print *, b
! end program

program kadaiBarrier
!$ use omp_lib
  implicit none
  integer a(5), b(5), c(10), max_a, max_b, i
  a = (/10, 15, 7, 3, 22/)
  b = (/20, 17, 1, 30, 1/)
  max_a = 0
  max_b = 0
!$omp parallel num_threads(2)
  if( omp_get_thread_num() == 0) then
    max_a = maxval(a)
  else
    max_b = maxval(b)
  end if
!$omp barrier
!$omp do
  do i = 1, 10
    c(i) = (max_a + max_b) * i
  end do
!$omp end do
!$omp end parallel
  print *, "maxA + maxB =", max_a + max_b
  print *, "C has :", c
end program