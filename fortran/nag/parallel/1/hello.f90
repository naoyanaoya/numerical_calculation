program helloOpenMP
  !$ use omp_lib
  implicit none
  write(*,"(a)") "START"
!$omp parallel ! 指示文(OpenMP指示文以降に現れた!以降はすべてコメントとして扱われる。)
  print *, "Hello! N =",omp_get_num_threads(), " and I am ",omp_get_thread_num()
  ! write(*,"(a)") "Hello! N =",omp_get_num_threads(), " and I am ",omp_get_thread_num()
!$omp end parallel ! 指示文
  write(*,"(a)") "END"
end