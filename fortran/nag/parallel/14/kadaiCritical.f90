! program exampleCritical 
! !$ use omp_lib
!   implicit none
!   integer, parameter :: N = 10
!   integer myTask, nextTask
!   nextTask = 1
! !$omp parallel private(mytask)
!   do
! !$omp critical
!     myTask = nextTask
!     nextTask = nextTask + 1
! !$omp end critical
!     if( N < myTask) exit
!     print *, "Processing task =", myTask
!   end do
! !$omp end parallel
!   print *, "Done..."
! end program

program kadaiCritical
!$ use omp_lib
  implicit none
  integer, parameter :: N = 10000
  integer i, totalParallel, totalSerial
  !逐次計算で合計を求める
  totalSerial = 0
  do i = 1, N
    totalSerial = TotalSerial + i
  end do
  ! 並列計算で合計を求める
  totalParallel = 0
!$omp parallel
!$omp do
  do i = 1, N
!$omp critical
    totalParallel = totalParallel + i
!$omp end critical
  end do
!$omp end do
!$omp end parallel
  print *, "Total(serial)   :", totalSerial
  print *, "Total(parallel) :", totalParallel
end program