program kadaiTime
!$ use omp_lib
  implicit none
  integer, parameter :: N = 1000 * 1000, M = 100
  integer i,j
  !$ double precision dts, dte
  double precision, allocatable :: a(:)
  double precision x
  allocate(a(N))
  !$ dts = omp_get_wtime()
!$omp parallel private(x) num_threads(8) ! スレッド数を指定(環境変数のスレッド指定より優先)
!$omp do
  do i = 1, N
    x = 0
    do j = 1, M
      x = x + log(dble(i + j))
    end do
    a(i) = x / M
  end do
!$omp end do
!$omp end parallel
  !$ dte = omp_get_wtime()
  print *, nint(sum(a))
  !$ print *, "Elapse time [sec.] = ", dte - dts
end program