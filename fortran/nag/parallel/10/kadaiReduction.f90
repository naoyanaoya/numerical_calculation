! program reduction
! !$ use omp_lib
!   implicit none
!   integer i, total
!   total = 100
! !$omp parallel
! !$omp do reduction(+:total)
!   do i = 1, 10000
!     total = total + i
!   end do
! !$omp end do
! !$omp end parallel
!   print *, "Total =", total
! end program

program kadaiReduction
!$ use omp_lib
  implicit none
  integer i, prod
  prod = 1
!$omp parallel
  print *, "My thread number is ", omp_get_thread_num()
!$omp do reduction(*:prod)
  do i = 1, 10
    prod = prod * i
    print *, prod
  end do
!$omp end do
!$omp end parallel
  print *, "Product =", prod
end program