! program exampleSingle
! !$ use omp_lib
!   implicit none
! !$omp parallel
! !$omp single
!   print *, "IN SINGLE: My Thread Number is ", omp_get_thread_num()
! !$omp end single
!   print *, "NOT IN SINGLE: My Thread Number is ", omp_get_thread_num()
! !$omp end parallel
! end program

program kadaiSingle
!$ use omp_lib
  implicit none
  integer, parameter :: N = 1000 * 1000
  integer i
  double precision total
  double precision, allocatable :: a(:)
  allocate(a(N))
  print *, "Starting..."
!$omp parallel
!$omp do
  do i = 1, N
    a(i) = i
  end do
!$omp end do
  total = sum(a)
!$omp single
  print *, "1st phase done..."
!$omp end single
!$omp do
  do i = 1, N
    a(i) = a(i) / total
  end do
!$omp end do
!$omp end parallel
  print *, "All done... Check value =", sum(a)
end program