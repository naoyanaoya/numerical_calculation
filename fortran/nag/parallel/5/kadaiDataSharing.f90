program kadaiDataSharing
!$ use omp_lib
  implicit none
  integer i,j
  integer,parameter :: N = 10000
  double precision dts,dte
  integer,allocatable :: a(:),b(:)
  allocate(a(N),b(N))
  a = (/(i,i=1,N)/)
  b = 0
! 変数の属性をすべて明示的に書く
! doループの反復制御変数は共有されない(private)で、それ以外は共有する(shared)
! defaultは環境によっては実装されていないこともあるのであまり使わないほうが良い
  dts = omp_get_wtime()
!$omp parallel default(none),private(i,j),shared(a,b)
!$omp do
  do i=1,N
    j = N - i + 1
    b(j) = a(i)
  end do
!$omp end do
!$omp end parallel
  dte = omp_get_wtime()
  print *, sum(a), "=", sum(b)
  print *, "Elapse time [sec.] = ", dte - dts
end program