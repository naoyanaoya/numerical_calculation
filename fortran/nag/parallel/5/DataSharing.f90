program dataSharing
  implicit none
  integer i,j
  integer,parameter :: N = 10000
  integer,allocatable :: a(:)
  allocate(a(N))
  a = 0
!$omp parallel
!$omp do private(j)
  do i=1,10000
    j = i
    a(j) = i
  end do
!$omp end do
!$omp end parallel
  print *, sum(a)
end program