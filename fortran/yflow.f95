!======================================================================!
!                                                                      !
!  FORTRAN SOURCE CODE TO SOLVE THE                                    !
!  NAVIER-STOKES EQUATION & SCALAR TRANSPORT EQUATION                  !
!                                                                      !
!  TWO-DIMENSIONAL INCOMPRESSIBLE FLOW SOLVER                          !
!  VERSION WITH DRIVER                                                 !
!                                                                      !
!                                          WRITTEN BY CHANGYOUNG YUHN  !
!                                                  @OSHIMA LABORATORY  !
!                                            20 AUG 2016 - 2 SEP 2016  !
!                                                                      !
!======================================================================!

!----------------------------------------------------------------------!
!  ** DISCRETIZATION **                                                !
!----------------------------------------------------------------------!
!  DISCRETIZATION METHOD      : FINITE DIFFERENCE METHOD               !
!  GRID TYPE                  : NON-UNIFORM STAGGERED GRID             !
!  VELOCITY-PRESSURE COUPLING : MAC METHOD                             !
!                                                                      !
!  DISCRETIZATION SCHEME (NAVIER-STOKES EQ.)                           !
!    TIME VARIATION TERM      : FORWARD EULAER METHOD                  !
!    CONVECTION TERM          : 1ST UPWIND SCHEME                      !
!    DIFFUSION TERM           : 2ND CENTRAL DIFFERENCE SCHEME          !
!    PRESSURE TERM            : 2ND STG CENTRAL DIFFERENCE SCHEME      !
!                                                                      !
!  DISCRETIZATION SCHEME (PRESSURE POISSON EQ.)                        !
!    PRESSURE LAPLACIAN       : 2ND STG CENTRAL DIFFERENCE SCHEME      !
!    VELOCITY DIVERGENCE      : 2ND STG CENTRAL DIFFERENCE SCHEME      !
!                                                                      !
!  DISCRETIZATION SCHEME (SCALAR TRANSPORT EQ.)                        !
!    TIME VARIATION TERM      : FORWARD EULAER METHOD                  !
!    CONVECTION TERM          : 1ST UPWIND SCHEME                      !
!    DIFFUSION TERM           : 2ND CENTRAL DIFFERENCE SCHEME          !
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!  ** PROGRAM FLOWCHART **                                             !
!----------------------------------------------------------------------!
!  PHYSICAL CONDITIONS                                                 !
!  CALCULATION CONDITIONS                                              !
!  READ GRID CONDITIONS                                                !
!  CALL DRIVER_PRECAL         (SUBROUTINE)                             !
!  CALL INITIAL_CONDITIONS    (SUBROUTINE)                             !
!  CALL BOUNDARY_CONDITIONS   (SUBROUTINE)                             !
!  CALL OUTPUT_POST           (SUBROUTINE)                             !
!    CALL OUTPUT_RESULTS      (SUBROUTINE)                             !
!                                                                      !
!  DO STEP=STEP+1,STEP_MAX                                             !
!    CALL SOLVE_SCALAR        (SUBROUTINE)                             !
!    CALL SOLVE_P             (SUBROUTINE)                             !
!      CALL SOLVE_MATRIX      (SUBROUTINE; PARALLELIZED)               !
!    CALL SOLVE_U             (SUBROUTINE)                             !
!    CALL SOLVE_V             (SUBROUTINE)                             !
!    CALL BOUNDARY_CONDITIONS (SUBROUTINE)                             !
!    CALL OUTPUT_POST         (SUBROUTINE)                             !
!      CALL OUTPUT_RESULTS    (SUBROUTINE)                             !
!    CALL OUTPUT_RESTART      (SUBROUTINE)                             !
!  END DO                                                              !
!----------------------------------------------------------------------!

  !MEMO: Y-FLOW LATEST VER. (UPDATED ON 2 SEP 2016 02:06)
  PROGRAM INCOMPRESSIBLE_FLOW_WITH_SCALAR_TRANSPORT
    IMPLICIT NONE
    INTEGER :: I, J
    INTEGER :: NX, NY, STEP, STEP_MAX, USE_RESTART
    INTEGER :: STEP_OUTPUT_RESULT, STEP_OUTPUT_RESTART
    INTEGER :: PARA_NO
    DOUBLE PRECISION :: TIME, DT, RHO, NU
    DOUBLE PRECISION :: INLET_VELOCITY, CHARACTERISTIC_LENGTH
    DOUBLE PRECISION :: REYNOLDS_NO, CFL_NO, DIFF_NO, PECRET_NO
    DOUBLE PRECISION :: DIFF_COEFF_X, DIFF_COEFF_Y
    DOUBLE PRECISION,ALLOCATABLE :: XP(:), YP(:), DX(:), DY(:)
    DOUBLE PRECISION,ALLOCATABLE :: U(:,:), V(:,:), P(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: U_OLD(:,:), V_OLD(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: SCALAR(:,:)
    CHARACTER(LEN=50) :: GRID_X_FILENAME, GRID_Y_FILENAME,&
                        &RESULT_FILENAME, RESTART_FILENAME
    CHARACTER(LEN=60) :: READ_RESTART_FILENAME

    !DRIVER
    INTEGER :: NX_D, NY_D, STEP_DRIVER_MAX, USE_DRIVER,&
              &USE_RESTART_DRIVER
    DOUBLE PRECISION :: DT_PRECAL
    DOUBLE PRECISION,ALLOCATABLE :: XP_D(:), YP_D(:), DX_D(:), DY_D(:)
    DOUBLE PRECISION,ALLOCATABLE :: U_D(:,:), V_D(:,:), P_D(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: U_D_OLD(:,:), V_D_OLD(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: SCALAR_D(:,:)
    CHARACTER(LEN=50) :: GRID_X_FILENAME_DRIVER,&
                        &GRID_Y_FILENAME_DRIVER,&
                        &RESULT_FILENAME_DRIVER,&
                        &RESTART_FILENAME_DRIVER
    CHARACTER(LEN=60) :: READ_RESTART_FILENAME_DRIVER

!-----------------------------
!  PHYSICAL CONDITIONS
!-----------------------------

    !FLOW
    RHO=9.9822D2 ![kg/m3] ;water @293K, 1atm
    NU=1.004D-6 ![m2/s] ;water @293K, 1atm
!    RHO=1.205D0 ![kg/m3] ;air @293K, 1atm
!    NU=1.512D-5 ![m2/s] ;air @293K, 1atm
    INLET_VELOCITY=1.D0 ![m/s]
    CHARACTERISTIC_LENGTH=2.D0*2.D-2 ![m]

    !SCALAR TRANSPORT
    DIFF_COEFF_X=2.0D-9 ![m2/s] ;N2 in water @298K ~O(D-9)
    DIFF_COEFF_Y=2.0D-9 ![m2/s]
!    DIFF_COEFF_X=1.6D-5 ![m2/s] ;CO2 in air @293K, 1atm ~O(D-9)
!    DIFF_COEFF_Y=1.6D-5 ![m2/s]

!-----------------------------
!  CALCULATION CONDITIONS
!-----------------------------

    STEP_MAX=25000
    DT=2.D-4 ![s]
    NX=347
    NY=86

    !GRID
    GRID_X_FILENAME="grid_x.csv"
    GRID_Y_FILENAME="grid_y.csv"

    !RESTART
    USE_RESTART=0 !0:DO NOT USE RESTART
    READ_RESTART_FILENAME="yflow_main_restart_002000.csv"

    !OUTPUT
    STEP_OUTPUT_RESULT=100
    STEP_OUTPUT_RESTART=1000
    RESULT_FILENAME="yflow_main_result_"
    RESTART_FILENAME="yflow_main_restart_"

    !PARALLELIZATION
    PARA_NO=6 !NO. OF THREADS TO USE

    !DRIVER
    USE_DRIVER=1 !0:DO NOT USE DRIVER
    USE_RESTART_DRIVER=1 !0:DO NOT USE RESTART FOR DRIVER
    STEP_DRIVER_MAX=8000 !NO. OF STEPS FOR PRECAL
    DT_PRECAL=2.D-4 ![s]
    NX_D=102
    NY_D=22
    GRID_X_FILENAME_DRIVER="grid_driver_x.csv"
    GRID_Y_FILENAME_DRIVER="grid_driver_y.csv"
    RESULT_FILENAME_DRIVER="yflow_driver_result_"
    RESTART_FILENAME_DRIVER="yflow_driver_restart_"
    READ_RESTART_FILENAME_DRIVER="yflow_driver_restart_000000.csv"

!-----------------------------
!  READ GRID CONDITIONS
!-----------------------------

    ALLOCATE(XP(0:NX-1), YP(0:NY-1), DX(0:NX-2), DY(0:NY-2))
    ALLOCATE(U(0:NX-1,0:NY-1), V(0:NX-1,0:NY-1), P(0:NX-1,0:NY-1))
    ALLOCATE(U_OLD(0:NX-1,0:NY-1), V_OLD(0:NX-1,0:NY-1))
    ALLOCATE(SCALAR(0:NX-1,0:NY-1))

    !READ GRID CONDITIONS FROM EXTERNAL FILE
    OPEN(11,FILE=TRIM(GRID_X_FILENAME),STATUS='OLD')
    OPEN(12,FILE=TRIM(GRID_Y_FILENAME),STATUS='OLD')
    DO I=0,NX-1
      READ(11,*) XP(I)
    END DO
    DO I=0,NY-1
      READ(12,*) YP(I)
    END DO
    CLOSE(11)
    CLOSE(12)

    DO I=0,NX-2
      DX(I)=XP(I+1)-XP(I)
    END DO
    DO I=0,NY-2
      DY(I)=YP(I+1)-YP(I)
    END DO

    !DRIVER
    IF (USE_DRIVER/=0) THEN

      ALLOCATE(XP_D(0:NX_D-1), YP_D(0:NY_D-1),&
              &DX_D(0:NX_D-2), DY_D(0:NY_D-2))
      ALLOCATE(U_D(0:NX_D-1,0:NY_D-1), V_D(0:NX_D-1,0:NY_D-1),&
              &P_D(0:NX_D-1,0:NY_D-1))
      ALLOCATE(U_D_OLD(0:NX_D-1,0:NY_D-1), V_D_OLD(0:NX_D-1,0:NY_D-1))
      ALLOCATE(SCALAR_D(0:NX_D-1,0:NY_D-1))

      OPEN(13,FILE=TRIM(GRID_X_FILENAME_DRIVER),STATUS='OLD')
      OPEN(14,FILE=TRIM(GRID_Y_FILENAME_DRIVER),STATUS='OLD')
      DO I=0,NX_D-1
        READ(13,*) XP_D(I)
      END DO
      DO I=0,NY_D-1
        READ(14,*) YP_D(I)
      END DO
      CLOSE(13)
      CLOSE(14)

      DO I=0,NX_D-2
        DX_D(I)=XP_D(I+1)-XP_D(I)
      END DO
      DO I=0,NY_D-2
        DY_D(I)=YP_D(I+1)-YP_D(I)
      END DO

    END IF

!-----------------------------
!  PRINT CONDITIONS
!-----------------------------

    REYNOLDS_NO=INLET_VELOCITY*CHARACTERISTIC_LENGTH/NU
    CFL_NO=INLET_VELOCITY*DT/MINVAL(DX)
    DIFF_NO=MAX( NU/MINVAL(DX)/MINVAL(DX),NU/MINVAL(DY)/MINVAL(DY),&
                &DIFF_COEFF_X/MINVAL(DX)/MINVAL(DX),&
                &DIFF_COEFF_Y/MINVAL(DY)/MINVAL(DY) )*DT
    PECRET_NO=INLET_VELOCITY*MAXVAL(DX)/NU

    !PHYSICAL CONDITIONS
    WRITE(*,*) '***** PHYSICAL CONDITIONS *****'
    WRITE(*,*)
    WRITE(*,*) '[FLOW]'
    WRITE(*,*) 'RHO=', RHO
    WRITE(*,*) 'NU=', NU
    WRITE(*,*) 'INLET VELOCITY=', INLET_VELOCITY
    WRITE(*,*) 'CHARACTERISTIC LENGTH=', CHARACTERISTIC_LENGTH
    WRITE(*,*) 'REYNOLDS NO.=', REYNOLDS_NO
    WRITE(*,*)
    WRITE(*,*) '[SCALAR TRANSPORT]'
    WRITE(*,*) 'DIFF COEFF(X)=', DIFF_COEFF_X
    WRITE(*,*) 'DIFF COEFF(Y)=', DIFF_COEFF_Y
    WRITE(*,*)

    !CALCULATION CONDITIONS
    WRITE(*,*) '***** CALCULATION CONDITIONS *****'
    WRITE(*,*)
    WRITE(*,*) '[TIME STEP]'
    WRITE(*,*) 'MAX NO. OF STEPS=', STEP_MAX
    WRITE(*,*) 'DT=', DT
    WRITE(*,*) 'TIME DURATION=', STEP_MAX*DT
    IF (USE_RESTART==0) THEN
      WRITE(*,*) 'USE RESTART= NO'
    ELSE
      WRITE(*,*) 'USE RESTART= YES'
    END IF
    IF (USE_DRIVER==0) THEN
      WRITE(*,*) 'USE DRIVER= NO'
    ELSE
      WRITE(*,*) 'USE DRIVER= YES'
    END IF
    WRITE(*,*)
    WRITE(*,*) '[GRID]'
    WRITE(*,*) 'NO. OF GRID POINTS(X)=', NX
    WRITE(*,*) 'NO. OF GRID POINTS(Y)=', NY
    WRITE(*,*) 'NO. OF GRID POINTS(TOTAL)=', NX*NY
    WRITE(*,*) 'DX MAX=', MAXVAL(DX)
    WRITE(*,*) 'DX MIN=', MINVAL(DX)
    WRITE(*,*) 'DY MAX=', MAXVAL(DY)
    WRITE(*,*) 'DY MIN=', MINVAL(DY)
    WRITE(*,*)
    WRITE(*,*) '[CALCULATION STABILITY]'
    WRITE(*,*) 'CFL NO.=', CFL_NO
    WRITE(*,*) 'DIFFUSION NO.=', DIFF_NO
    WRITE(*,*) 'PECRET NO.=', PECRET_NO
    WRITE(*,*)

    IF (CFL_NO>1.D0) THEN
      WRITE(*,*) '!!! CFL NO. EXCEEDS 1.0 !!!'
      WRITE(*,*)
    END IF
    IF (DIFF_NO>5.D-1) THEN
      WRITE(*,*) '!!! DIFFUSION NO. EXCEEDS 0.5 !!!'
      WRITE(*,*)
    END IF
    IF (PECRET_NO>2.D0) THEN
      WRITE(*,*) '!!! PECRET NO. EXCEEDS 2.0 !!!'
      WRITE(*,*)
    END IF

!-----------------------------
!  I.C. & B.C.
!-----------------------------

    !DRIVER PRECAL FOR BOUNDARY CONDITIONS
    IF (USE_DRIVER/=0) THEN
      CALL DRIVER_PRECAL(U_D, V_D, P_D, U_D_OLD, V_D_OLD, DT_PRECAL,&
                        &DX_D, DY_D, SCALAR_D, XP_D, YP_D, NX_D, NY_D,&
                        &STEP_DRIVER_MAX, RHO, NU, INLET_VELOCITY,&
                        &DIFF_COEFF_X, DIFF_COEFF_Y,&
                        &USE_RESTART_DRIVER,&
                        &READ_RESTART_FILENAME_DRIVER, PARA_NO)
      !OUTPUT PRECAL RESTART FILE
      IF (USE_RESTART_DRIVER==0) THEN
        STEP=0
        CALL OUTPUT_RESTART(U_D, V_D, P_D, SCALAR_D, NX_D, NY_D,&
                           &STEP, TIME, RESTART_FILENAME_DRIVER)
      END IF
    END IF

    !INITIAL CONDITIONS
    CALL INITIAL_CONDITIONS(U, V, P, SCALAR, NX, NY, STEP, TIME,&
                           &USE_RESTART, READ_RESTART_FILENAME)

    !BOUNDARY CONDITIONS
    CALL BOUNDARY_CONDITIONS(U, V, P, SCALAR, XP, YP, NX, NY,&
                            &INLET_VELOCITY, U_D, V_D, P_D, SCALAR_D,&
                            &XP_D, YP_D, NX_D, NY_D)

    !OUTPUT INITIAL CONDITIONS
    CALL OUTPUT_POST(U, V, P, SCALAR, XP, YP, DX, DY, NX, NY, STEP,&
                    &RESULT_FILENAME)
    IF (USE_DRIVER/=0) THEN
      CALL OUTPUT_POST(U_D, V_D, P_D, SCALAR_D, XP_D, YP_D, DX_D, DY_D,&
                      &NX_D, NY_D, STEP, RESULT_FILENAME_DRIVER)
    END IF

!-----------------------------
!  MAIN CALCULATION
!-----------------------------

    WRITE(*,*) '***** CALCULATION START *****'
    WRITE(*,*)

    DO STEP=STEP+1,STEP_MAX

      TIME=TIME+DT

      !-----------------------------
      !  DRIVER CAL
      !-----------------------------

      IF (USE_DRIVER/=0) THEN

        !SOLVE SCALAR TRANSPORT EQ
        CALL SOLVE_SCALAR(U_D, V_D, SCALAR_D, DT, DX_D, DY_D, NX_D,&
                         &NY_D, DIFF_COEFF_X, DIFF_COEFF_Y)

        !SOLVE N-S EQ USING MAC METHOD
        DO J=0,NY_D-1
        DO I=0,NX_D-1
          U_D_OLD(I,J)=U_D(I,J)
          V_D_OLD(I,J)=V_D(I,J)
        END DO
        END DO
        CALL SOLVE_P(U_D, V_D, P_D, U_D_OLD, V_D_OLD, DT, DX_D, DY_D,&
                    &NX_D, NY_D, STEP, RHO, NU, PARA_NO)
        CALL SOLVE_U(U_D, P_D, DT, DX_D, NX_D, NY_D, RHO)
        CALL SOLVE_V(V_D, P_D, DT, DY_D, NX_D, NY_D, RHO)

        !RECALL BOUNDARY CONDITIONS
        CALL DRIVER_BOUNDARY_CONDITIONS(U_D, V_D, P_D, SCALAR_D, XP_D,&
             &YP_D, NX_D, NY_D, INLET_VELOCITY)

      END IF

      !-----------------------------
      !  MAIN CAL
      !-----------------------------

      !SOLVE SCALAR TRANSPORT EQ
      CALL SOLVE_SCALAR(U, V, SCALAR, DT, DX, DY, NX, NY,&
                       &DIFF_COEFF_X, DIFF_COEFF_Y)

      !SOLVE N-S EQ USING MAC METHOD
      DO J=0,NY-1
      DO I=0,NX-1
        U_OLD(I,J)=U(I,J)
        V_OLD(I,J)=V(I,J)
      END DO
      END DO
      CALL SOLVE_P(U, V, P, U_OLD, V_OLD, DT, DX, DY, NX, NY, STEP,&
                  &RHO, NU, PARA_NO)
      CALL SOLVE_U(U, P, DT, DX, NX, NY, RHO)
      CALL SOLVE_V(V, P, DT, DY, NX, NY, RHO)

      !RECALL BOUNDARY CONDITIONS
      CALL BOUNDARY_CONDITIONS(U, V, P, SCALAR, XP, YP, NX, NY,&
                              &INLET_VELOCITY, U_D, V_D, P_D, SCALAR_D,&
                              &XP_D, YP_D, NX_D, NY_D)

      !-----------------------------
      !  OUTPUT RESULTS & RESTART
      !-----------------------------

      !OUTPUT RESULTS
      IF (MOD(STEP,STEP_OUTPUT_RESULT)==0) THEN
        !MAIN
        CALL OUTPUT_POST(U, V, P, SCALAR, XP, YP, DX, DY, NX, NY, STEP,&
                        &RESULT_FILENAME)
        !DRIVER
        IF (USE_DRIVER/=0) THEN
          CALL OUTPUT_POST(U_D, V_D, P_D, SCALAR_D, XP_D, YP_D, DX_D,&
               &DY_D, NX_D, NY_D, STEP, RESULT_FILENAME_DRIVER)
        END IF
      END IF

      !OUTPUT RESTART FILE
      IF (MOD(STEP,STEP_OUTPUT_RESTART)==0) THEN
        !MAIN
        CALL OUTPUT_RESTART(U, V, P, SCALAR, NX, NY, STEP, TIME,&
                           &RESTART_FILENAME)
        !DRIVER
        IF (USE_DRIVER/=0) THEN
          CALL OUTPUT_RESTART(U_D, V_D, P_D, SCALAR_D, NX_D, NY_D,&
                             &STEP, TIME, RESTART_FILENAME_DRIVER)
        END IF
      END IF

    END DO

    WRITE(*,*) '***** CALCULATION FINISHED *****'

    STOP
  END PROGRAM INCOMPRESSIBLE_FLOW_WITH_SCALAR_TRANSPORT


!----------------------------------------------------------------------!
!  ** SUBROUTINE LIST **                                               !
!----------------------------------------------------------------------!
!  SUBROUTINE INITIAL_CONDITIONS                                       !
!  SUBROUTINE BOUNDARY_CONDITIONS                                      !
!  SUBROUTINE DRIVER_INITIAL_CONDITIONS                                !
!  SUBROUTINE DRIVER_BOUNDARY_CONDITIONS                               !
!  SUBROUTINE DRIVER_PRECAL                                            !
!  SUBROUTINE SOLVE_SCALAR                                             !
!  SUBROUTINE SOLVE_P                                                  !
!  SUBROUTINE SOLVE_MATRIX                                             !
!  SUBROUTINE SOLVE_U                                                  !
!  SUBROUTINE SOLVE_V                                                  !
!  SUBROUTINE OUTPUT_POST                                              !
!  SUBROUTINE OUTPUT_RESULTS                                           !
!  SUBROUTINE OUTPUT_RESTART                                           !
!----------------------------------------------------------------------!

  SUBROUTINE INITIAL_CONDITIONS(U, V, P, SCALAR, NX, NY, STEP, TIME,&
                               &USE_RESTART, READ_RESTART_FILENAME)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY, USE_RESTART
    INTEGER,INTENT(OUT) :: STEP
    DOUBLE PRECISION,INTENT(OUT) :: TIME
    DOUBLE PRECISION,INTENT(INOUT) :: U(0:NX-1,0:NY-1),&
    &V(0:NX-1,0:NY-1), P(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(INOUT) :: SCALAR(0:NX-1,0:NY-1)
    CHARACTER(LEN=60),INTENT(IN) :: READ_RESTART_FILENAME

    !LOCAL VARIABLES
    INTEGER :: I, J

    !-----------------------------
    !  SET I.C. WO USING RESTART FILE
    !-----------------------------

    IF (USE_RESTART==0) THEN

      STEP=0
      TIME=0.D0 ![s]

      DO J=0,NY-1
      DO I=0,NX-1
        U(I,J)=0.D0 ![m/s]
        V(I,J)=0.D0 ![m/s]
        P(I,J)=0.D0 ![Pa]
        SCALAR(I,J)=0.D0
      END DO
      END DO

    !-----------------------------
    !  READ I.C. FROM RESTART FILE
    !-----------------------------

    ELSE

      OPEN(21,FILE=TRIM(READ_RESTART_FILENAME),STATUS='OLD')

      !SKIP HEADER
      DO I=1,3
        READ(21,'()')
      END DO

      !READ TIME STEP INFO
      READ(21,*) STEP, TIME

      !READ MATRIX
      DO J=0,NY-1
      DO I=0,NX-1
        READ(21,*) U(I,J), V(I,J), P(I,J), SCALAR(I,J)
      END DO
      END DO

      CLOSE(21)

    END IF

    RETURN
  END SUBROUTINE INITIAL_CONDITIONS
!----------------------------------------------------------------------!
  SUBROUTINE BOUNDARY_CONDITIONS(U, V, P, SCALAR, XP, YP, NX, NY,&
                                &INLET_VELOCITY, U_D, V_D, P_D,&
                                &SCALAR_D, XP_D, YP_D, NX_D, NY_D)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY
    DOUBLE PRECISION,INTENT(IN) :: INLET_VELOCITY
    DOUBLE PRECISION,INTENT(IN) :: XP(0:NX-1), YP(0:NY-1)
    DOUBLE PRECISION,INTENT(INOUT) :: U(0:NX-1,0:NY-1),&
    &V(0:NX-1,0:NY-1), P(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(INOUT) :: SCALAR(0:NX-1,0:NY-1)

    !DRIVER
    INTEGER,INTENT(IN) :: NX_D, NY_D
    DOUBLE PRECISION,INTENT(IN) :: XP_D(0:NX_D-1), YP_D(0:NY_D-1)
    DOUBLE PRECISION,INTENT(IN) :: U_D(0:NX_D-1,0:NY_D-1),&
    &V_D(0:NX_D-1,0:NY_D-1), P_D(0:NX_D-1,0:NY_D-1)
    DOUBLE PRECISION,INTENT(IN) :: SCALAR_D(0:NX_D-1,0:NY_D-1)

    !LOCAL VARIABLES
    INTEGER :: I, J, K

    !LEFT
    !0.04<=YP<=0.06 INLET; U(0,J)=INLET, V_WALL(1/2)=INLET,
    !                      P_WALL(1/2)=INLET, SCALAR_WALL(1/2)=INLET
    !ELSE NO-SLIP WALL; U(0,J)=0, V_WALL(1/2)=0, dP/dX=0, dSCALAR/dX=0
    DO J=1,NY-2
      K=J
      IF (YP(J)==YP_D(1)) EXIT
    END DO

    DO J=1,NY-2
      IF (J<K .OR. J>K+NY_D-3) THEN !NO-SLIP WALL
        U(0,J)=0.D0
        V(0,J)=-V(1,J)
        P(0,J)=P(1,J) !DUMMY
        SCALAR(0,J)=SCALAR(1,J) !DUMMY
      ELSE !INLET FROM DRIVER
        U(0,J)=U_D(NX_D-2,J-K+1)
        V(0,J)=-V(1,J)+( V_D(NX_D-2,J-K+1)+V_D(NX_D-1,J-K+1) )
        P(0,J)=-P(1,J)+( P_D(NX_D-2,J-K+1)+P_D(NX_D-1,J-K+1) )
        SCALAR(0,J)=-SCALAR(1,J)+( SCALAR_D(NX_D-2,J-K+1)&
                                 &+SCALAR_D(NX_D-1,J-K+1) )
      END IF
    END DO

    !RIGHT
    !OUTLET; dU/dX=0, dV/dX=0, P=0, dSCALAR/dX=0
    DO J=1,NY-2
      U(NX-1,J)=U(NX-2,J)
      V(NX-1,J)=V(NX-2,J)
      P(NX-1,J)=-P(NX-2,J) !DUMMY
      SCALAR(NX-1,J)=SCALAR(NX-2,J)
    END DO

    !BOTTOM
    !NO-SLIP WALL; U_WALL(1/2)=0, V(I,0)=0, dP/dY=0, dSCALAR/dY=0
    DO I=0,NX-1
      U(I,0)=-U(I,1)
      V(I,0)=0.D0
      P(I,0)=P(I,1) !DUMMY
      SCALAR(I,0)=SCALAR(I,1) !DUMMY
    END DO

    !TOP
    !NO-SLIP WALL; U_WALL(1/2)=0, V(I,NY-2)=0, V(I,NY-1)=DUMMY, dP/dY=0,
    !              dSCALAR/dY=0
    DO I=0,NX-1
      U(I,NY-1)=-U(I,NY-2)
      V(I,NY-2)=0.D0
      V(I,NY-1)=0.D0 !DUMMY
      P(I,NY-1)=P(I,NY-2) !DUMMY
      SCALAR(I,NY-1)=SCALAR(I,NY-2) !DUMMY
    END DO

    RETURN
  END SUBROUTINE BOUNDARY_CONDITIONS
!----------------------------------------------------------------------!
  SUBROUTINE DRIVER_INITIAL_CONDITIONS(U_D, V_D, P_D, SCALAR_D, NX_D,&
                                      &NY_D, INLET_VELOCITY)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX_D, NY_D
    DOUBLE PRECISION,INTENT(IN) :: INLET_VELOCITY
    DOUBLE PRECISION,INTENT(OUT) :: U_D(0:NX_D-1,0:NY_D-1),&
    &V_D(0:NX_D-1,0:NY_D-1), P_D(0:NX_D-1,0:NY_D-1)
    DOUBLE PRECISION,INTENT(OUT) :: SCALAR_D(0:NX_D-1,0:NY_D-1)

    !LOCAL VARIABLES
    INTEGER :: I, J

    DO J=0,NY_D-1
    DO I=0,NX_D-1
      U_D(I,J)=4.D0 ![m/s]
      V_D(I,J)=0.D0 ![m/s]
      P_D(I,J)=0.D0 ![Pa]
      SCALAR_D(I,J)=1.D0
    END DO
    END DO

    RETURN
  END SUBROUTINE DRIVER_INITIAL_CONDITIONS
!----------------------------------------------------------------------!
  SUBROUTINE DRIVER_BOUNDARY_CONDITIONS(U_D, V_D, P_D, SCALAR_D,&
                                       &XP_D, YP_D, NX_D, NY_D,&
                                       &INLET_VELOCITY)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX_D, NY_D
    DOUBLE PRECISION,INTENT(IN) :: INLET_VELOCITY
    DOUBLE PRECISION,INTENT(IN) :: XP_D(0:NX_D-1), YP_D(0:NY_D-1)
    DOUBLE PRECISION,INTENT(INOUT) :: U_D(0:NX_D-1,0:NY_D-1),&
    &V_D(0:NX_D-1,0:NY_D-1), P_D(0:NX_D-1,0:NY_D-1)
    DOUBLE PRECISION,INTENT(INOUT) :: SCALAR_D(0:NX_D-1,0:NY_D-1)

    !LOCAL VARIABLES
    INTEGER :: I, J

    !LEFT
    !PERIODIC INLET; U,V,P,SCALAR(0,J)=U,V,P,SCALAR(NX-2,J)
    DO J=1,NY_D-2
      U_D(0,J)=U_D(NX_D-2,J)
      V_D(0,J)=V_D(NX_D-2,J)
      P_D(0,J)=P_D(NX_D-2,J)
      SCALAR_D(0,J)=SCALAR_D(NX_D-2,J)
    END DO

    !RIGHT
    !PERIODIC OUTLET; U,V,P,SCALAR(NX-1,J)=U,V,P,SCALAR(1,J)
    DO J=1,NY_D-2
      U_D(NX_D-1,J)=U_D(1,J)
      V_D(NX_D-1,J)=V_D(1,J)
      P_D(NX_D-1,J)=P_D(1,J)
      SCALAR_D(NX_D-1,J)=SCALAR_D(1,J)
    END DO

    !BOTTOM
    !NO-SLIP WALL; U_WALL(1/2)=0, V(I,0)=0, dP/dY=0, dSCALAR/dY=0
    DO I=0,NX_D-1
      U_D(I,0)=-U_D(I,1)
      V_D(I,0)=0.D0
      P_D(I,0)=P_D(I,1) !DUMMY
      SCALAR_D(I,0)=SCALAR_D(I,1) !DUMMY
    END DO

    !TOP
    !NO-SLIP WALL; U_WALL(1/2)=0, V(I,NY-2)=0, V(I,NY-1)=DUMMY, dP/dY=0,
    !              dSCALAR/dY=0
    DO I=0,NX_D-1
      U_D(I,NY_D-1)=-U_D(I,NY_D-2)
      V_D(I,NY_D-2)=0.D0
      V_D(I,NY_D-1)=0.D0 !DUMMY
      P_D(I,NY_D-1)=P_D(I,NY_D-2) !DUMMY
      SCALAR_D(I,NY_D-1)=SCALAR_D(I,NY_D-2) !DUMMY
    END DO

    RETURN
  END SUBROUTINE DRIVER_BOUNDARY_CONDITIONS
!----------------------------------------------------------------------!
  SUBROUTINE DRIVER_PRECAL(U_D, V_D, P_D, U_D_OLD, V_D_OLD, DT_PRECAL,&
                          &DX_D, DY_D, SCALAR_D, XP_D, YP_D, NX_D,&
                          &NY_D, STEP_DRIVER_MAX, RHO, NU,&
                          &INLET_VELOCITY, DIFF_COEFF_X, DIFF_COEFF_Y,&
                          &USE_RESTART_DRIVER,&
                          &READ_RESTART_FILENAME_DRIVER, PARA_NO)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX_D, NY_D, STEP_DRIVER_MAX,&
                         &USE_RESTART_DRIVER
    INTEGER,INTENT(IN) :: PARA_NO
    DOUBLE PRECISION,INTENT(IN) :: DT_PRECAL, RHO, NU
    DOUBLE PRECISION,INTENT(IN) :: INLET_VELOCITY
    DOUBLE PRECISION,INTENT(IN) :: DIFF_COEFF_X, DIFF_COEFF_Y
    DOUBLE PRECISION,INTENT(IN) :: XP_D(0:NX_D-1), YP_D(0:NY_D-1),&
                                  &DX_D(0:NX_D-2), DY_D(0:NY_D-2)
    DOUBLE PRECISION,INTENT(OUT) :: U_D(0:NX_D-1,0:NY_D-1),&
    &V_D(0:NX_D-1,0:NY_D-1), P_D(0:NX_D-1,0:NY_D-1)
    DOUBLE PRECISION,INTENT(OUT) :: U_D_OLD(0:NX_D-1,0:NY_D-1),&
    &V_D_OLD(0:NX_D-1,0:NY_D-1)
    DOUBLE PRECISION,INTENT(OUT) :: SCALAR_D(0:NX_D-1,0:NY_D-1)
    CHARACTER(LEN=60),INTENT(IN) :: READ_RESTART_FILENAME_DRIVER

    !LOCAL VARIABLES
    INTEGER :: I, J, STEP

    !-----------------------------
    !  DRIVER PRECAL
    !-----------------------------

    IF (USE_RESTART_DRIVER==0) THEN

      !INITIAL CONDITIONS
      CALL DRIVER_INITIAL_CONDITIONS(U_D, V_D, P_D, SCALAR_D, NX_D,&
                                    &NY_D, INLET_VELOCITY)

      !BOUNDARY CONDITIONS
      CALL DRIVER_BOUNDARY_CONDITIONS(U_D, V_D, P_D, SCALAR_D, XP_D,&
                                     &YP_D, NX_D, NY_D, INLET_VELOCITY)

      WRITE(*,*) '***** DRIVER PRECAL START *****'
      WRITE(*,*)

      DO STEP=1,STEP_DRIVER_MAX

        !SOLVE SCALAR TRANSPORT EQ
        CALL SOLVE_SCALAR(U_D, V_D, SCALAR_D, DT_PRECAL, DX_D, DY_D,&
                         &NX_D, NY_D, DIFF_COEFF_X, DIFF_COEFF_Y)

        !SOLVE N-S EQ USING MAC METHOD
        DO J=0,NY_D-1
        DO I=0,NX_D-1
          U_D_OLD(I,J)=U_D(I,J)
          V_D_OLD(I,J)=V_D(I,J)
        END DO
        END DO
        CALL SOLVE_P(U_D, V_D, P_D, U_D_OLD, V_D_OLD, DT_PRECAL, DX_D,&
                    &DY_D, NX_D, NY_D, STEP, RHO, NU, PARA_NO)
        CALL SOLVE_U(U_D, P_D, DT_PRECAL, DX_D, NX_D, NY_D, RHO)
        CALL SOLVE_V(V_D, P_D, DT_PRECAL, DY_D, NX_D, NY_D, RHO)

        !RECALL BOUNDARY CONDITIONS
        CALL DRIVER_BOUNDARY_CONDITIONS(U_D, V_D, P_D, SCALAR_D, XP_D,&
                                       &YP_D, NX_D, NY_D,&
                                       &INLET_VELOCITY)

        !PRINT LOG
        IF (MOD(STEP,1000)==0) THEN
          WRITE(*,*) 'DRIVER PRECAL:', STEP, 'STEPS FINISHED'
          WRITE(*,*) 'U_MAX=', MAXVAL( 5.D-1*(U_D(:,10)+U_D(:,11)) )
          WRITE(*,*)
        END IF

      END DO

      WRITE(*,*) '***** DRIVER PRECAL FINISHED *****'
      WRITE(*,*)

    !-----------------------------
    !  READ DRIVER RESTART FILE
    !-----------------------------

    ELSE

      WRITE(*,*) '***** READING DRIVER RESTART *****'
      WRITE(*,*)

      OPEN(31,FILE=TRIM(READ_RESTART_FILENAME_DRIVER),STATUS='OLD')

      !SKIP HEADER AND TIME STEP INFO
      DO I=1,4
        READ(31,'()')
      END DO

      !READ MATRIX
      DO J=0,NY_D-1
      DO I=0,NX_D-1
        READ(31,*) U_D(I,J), V_D(I,J), P_D(I,J), SCALAR_D(I,J)
      END DO
      END DO

      CLOSE(31)

    END IF

    RETURN
  END SUBROUTINE DRIVER_PRECAL
!----------------------------------------------------------------------!
  SUBROUTINE SOLVE_SCALAR(U, V, SCALAR, DT, DX, DY, NX, NY,&
                         &DIFF_COEFF_X, DIFF_COEFF_Y)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY
    DOUBLE PRECISION,INTENT(IN) :: DT
    DOUBLE PRECISION,INTENT(IN) :: DIFF_COEFF_X, DIFF_COEFF_Y
    DOUBLE PRECISION,INTENT(IN) :: DX(0:NX-2), DY(0:NY-2)
    DOUBLE PRECISION,INTENT(IN) :: U(0:NX-1,0:NY-1), V(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(INOUT) :: SCALAR(0:NX-1,0:NY-1)

    !LOCAL VARIABLES
    INTEGER :: I, J
    DOUBLE PRECISION :: U_CNT(0:NX-1,0:NY-1), V_CNT(0:NX-1,0:NY-1)
    DOUBLE PRECISION :: SCALAR_OLD(0:NX-1,0:NY-1)

    !-----------------------------
    !  INTERPOLATE U,V AT GRID POINT
    !-----------------------------

    DO J=1,NY-2
    DO I=1,NX-2
      U_CNT(I,J)=5.D-1*(U(I,J)+U(I-1,J)) !LINEAR INTERPOLATION
      V_CNT(I,J)=5.D-1*(V(I,J)+V(I,J-1))
    END DO
    END DO

    !-----------------------------
    !  SOLVE SCALAR TRANSPORT EQ
    !-----------------------------

    DO J=0,NY-1
    DO I=0,NX-1
      SCALAR_OLD(I,J)=SCALAR(I,J)
    END DO
    END DO

    DO J=1,NY-2
    DO I=1,NX-2

      !CONVECTION X (1ST UPWIND)
      SCALAR(I,J)=SCALAR_OLD(I,J)&
                &-DT*MAX(U_CNT(I,J),0.D0)&
                &*(SCALAR_OLD(I,J)-SCALAR_OLD(I-1,J))/DX(I-1)&
                &-DT*MIN(U_CNT(I,J),0.D0)&
                &*(SCALAR_OLD(I+1,J)-SCALAR_OLD(I,J))/DX(I)

      !CONVECTION Y
      SCALAR(I,J)=SCALAR(I,J)&
                &-DT*MAX(V_CNT(I,J),0.D0)&
                &*(SCALAR_OLD(I,J)-SCALAR_OLD(I,J-1))/DY(J-1)&
                &-DT*MIN(V_CNT(I,J),0.D0)&
                &*(SCALAR_OLD(I,J+1)-SCALAR_OLD(I,J))/DY(J)

      !DIFFUSION X (1ST CENTRAL FOR NON-UNIFORM GRID; O(DX(I-1)-DX(I)))
      SCALAR(I,J)=SCALAR(I,J)&
                &+DT*DIFF_COEFF_X&
                &*2.D0*( (SCALAR_OLD(I+1,J)-SCALAR_OLD(I,J))*DX(I-1)&
                       &-(SCALAR_OLD(I,J)-SCALAR_OLD(I-1,J))*DX(I) )&
                &/( DX(I)*DX(I-1)*(DX(I)+DX(I-1)) )

      !DIFFUSION Y
      SCALAR(I,J)=SCALAR(I,J)&
                &+DT*DIFF_COEFF_Y&
                &*2.D0*( (SCALAR_OLD(I,J+1)-SCALAR_OLD(I,J))*DY(J-1)&
                       &-(SCALAR_OLD(I,J)-SCALAR_OLD(I,J-1))*DY(J) )&
                &/( DY(J)*DY(J-1)*(DY(J)+DY(J-1)) )

    END DO
    END DO

    RETURN
  END SUBROUTINE SOLVE_SCALAR
!----------------------------------------------------------------------!
  SUBROUTINE SOLVE_P(U, V, P, U_OLD, V_OLD, DT, DX, DY, NX, NY, STEP,&
                    &RHO, NU, PARA_NO)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY, STEP
    INTEGER,INTENT(IN) :: PARA_NO
    DOUBLE PRECISION,INTENT(IN) :: DT, RHO, NU
    DOUBLE PRECISION,INTENT(IN) :: DX(0:NX-2), DY(0:NY-2)
    DOUBLE PRECISION,INTENT(INOUT) :: U(0:NX-1,0:NY-1),&
    &V(0:NX-1,0:NY-1), P(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: U_OLD(0:NX-1,0:NY-1),&
    &V_OLD(0:NX-1,0:NY-1)

    !LOCAL VARIABLES
    INTEGER :: I, J
    DOUBLE PRECISION :: U_STG, V_STG
    DOUBLE PRECISION :: AP(1:NX-2,1:NY-2), AE(1:NX-2,1:NY-2),&
    &AW(1:NX-2,1:NY-2), AN(1:NX-2,1:NY-2), AS(1:NX-2,1:NY-2),&
    &B(1:NX-2,1:NY-2)

    DO J=1,NY-2
    DO I=1,NX-2

    !-----------------------------
    !  CALCULATE U_FRACTIONAL
    !-----------------------------

      !CONVECTION X (1ST UPWIND)
      U(I,J)=U_OLD(I,J)&
           &-DT*MAX(U_OLD(I,J),0.D0)*(U_OLD(I,J)-U_OLD(I-1,J))/DX(I-1)&
           &-DT*MIN(U_OLD(I,J),0.D0)*(U_OLD(I+1,J)-U_OLD(I,J))/DX(I)

      !CONVECTION Y
      V_STG=(V_OLD(I,J)+V_OLD(I+1,J)+V_OLD(I,J-1)+V_OLD(I+1,J-1))/4.D0
      U(I,J)=U(I,J)&
           &-DT*MAX(V_STG,0.D0)*(U_OLD(I,J)-U_OLD(I,J-1))/DY(J-1)&
           &-DT*MIN(V_STG,0.D0)*(U_OLD(I,J+1)-U_OLD(I,J))/DY(J)

      !DIFFUSION X (2ND CENTRAL FOR NON-UNIFORM GRID; O(DX(I-1)-DX(I)))
      U(I,J)=U(I,J)&
            &+2.D0*DT*NU*U_OLD(I+1,J)/DX(I)/(DX(I)+DX(I-1))&
            &-2.D0*DT*NU*U_OLD(I,J)/DX(I)/DX(I-1)&
            &+2.D0*DT*NU*U_OLD(I-1,J)/DX(I-1)/(DX(I)+DX(I-1))

      !DIFFUSION Y
      U(I,J)=U(I,J)&
            &+2.D0*DT*NU*U_OLD(I,J+1)/DY(J)/(DY(J)+DY(J-1))&
            &-2.D0*DT*NU*U_OLD(I,J)/DY(J)/DY(J-1)&
            &+2.D0*DT*NU*U_OLD(I,J-1)/DY(J-1)/(DY(J)+DY(J-1))

    !-----------------------------
    !  CALCULATE V_FRACTIONAL
    !-----------------------------

      !CONVECTION X (1ST UPWIND)
      U_STG=(U_OLD(I,J)+U_OLD(I,J+1)+U_OLD(I-1,J)+U_OLD(I-1,J+1))/4.D0
      V(I,J)=V_OLD(I,J)&
           &-DT*MAX(U_STG,0.D0)*(V_OLD(I,J)-V_OLD(I-1,J))/DX(I-1)&
           &-DT*MIN(U_STG,0.D0)*(V_OLD(I+1,J)-V_OLD(I,J))/DX(I)

      !CONVECTION Y
      V(I,J)=V(I,J)&
           &-DT*MAX(V_OLD(I,J),0.D0)*(V_OLD(I,J)-V_OLD(I,J-1))/DY(J-1)&
           &-DT*MIN(V_OLD(I,J),0.D0)*(V_OLD(I,J+1)-V_OLD(I,J))/DY(J)

      !DIFFUSION X (2ND CENTRAL FOR NON-UNIFORM GRID; O(DX(I-1)-DX(I)))
      V(I,J)=V(I,J)&
            &+2.D0*DT*NU*V_OLD(I+1,J)/DX(I)/(DX(I)+DX(I-1))&
            &-2.D0*DT*NU*V_OLD(I,J)/DX(I)/DX(I-1)&
            &+2.D0*DT*NU*V_OLD(I-1,J)/DX(I-1)/(DX(I)+DX(I-1))

      !DIFFUSION Y
      V(I,J)=V(I,J)&
            &+2.D0*DT*NU*V_OLD(I,J+1)/DY(J)/(DY(J)+DY(J-1))&
            &-2.D0*DT*NU*V_OLD(I,J)/DY(J)/DY(J-1)&
            &+2.D0*DT*NU*V_OLD(I,J-1)/DY(J-1)/(DY(J)+DY(J-1))

    END DO
    END DO

    !-----------------------------
    !  SOLVE PRESSURE POISSON EQ
    !-----------------------------

    !COEFFICIENT MATRIX
    DO J=1,NY-2
    DO I=1,NX-2

      !2ND CENTRAL FOR NON-UNIFORM STG GRID; O(DX(I-1)-DX(I))
      AE(I,J)=2.D0*DT/RHO/DX(I)/(DX(I)+DX(I-1))
      AW(I,J)=2.D0*DT/RHO/DX(I-1)/(DX(I)+DX(I-1))
      AN(I,J)=2.D0*DT/RHO/DY(J)/(DY(J)+DY(J-1))
      AS(I,J)=2.D0*DT/RHO/DY(J-1)/(DY(J)+DY(J-1))
      AP(I,J)=-2.D0*DT/RHO/DX(I)/DX(I-1)-2.D0*DT/RHO/DY(J)/DY(J-1)

      !2ND STAGGERED CENTRAL FOR NON-UNIFORM STG GRID
      B(I,J)=2.D0*( U(I,J)-U(I-1,J) )/( DX(I-1)+DX(I) )&
           &+2.D0*( V(I,J)-V(I,J-1) )/( DY(J-1)+DY(J) )
!      B(I,J)=(U(I,J)-U(I-1,J))/DX(I-1)+(V(I,J)-V(I,J-1))/DY(J-1)

    END DO
    END DO

    !SOLVE MATRIX
    CALL SOLVE_MATRIX(P, AP, AE, AW, AN, AS, B, NX, NY, STEP, PARA_NO)

    RETURN
  END SUBROUTINE SOLVE_P
!----------------------------------------------------------------------!
  SUBROUTINE SOLVE_MATRIX(P, AP, AE, AW, AN, AS, B, NX, NY, STEP,&
                         &PARA_NO)
    !$ USE OMP_LIB
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY, STEP
    INTEGER,INTENT(IN) :: PARA_NO
    DOUBLE PRECISION,INTENT(IN) :: AP(1:NX-2,1:NY-2),&
    &AE(1:NX-2,1:NY-2), AW(1:NX-2,1:NY-2), AN(1:NX-2,1:NY-2),&
    &AS(1:NX-2,1:NY-2), B(1:NX-2,1:NY-2)
    DOUBLE PRECISION,INTENT(INOUT) :: P(0:NX-1,0:NY-1)

    !LOCAL VARIABLES
    INTEGER :: I, J, ITER, ITER_MAX, ITER_NO
    DOUBLE PRECISION :: RELAX_FACTOR, EPS, ERROR
    DOUBLE PRECISION :: P_OLD(0:NX-1,0:NY-1)

    !-----------------------------
    !  SOLVE MATRIX BY SOR METHOD
    !-----------------------------

    RELAX_FACTOR=1.8D0 !0<RELAX_FACTOR<2 (1.D0: JACOBI METHOD)
    ITER_MAX=1000000 !MAX NO. OF ITERATION
    EPS=1.D-3 !CONVERGENCE CRITERION

    ITER_NO=0 !ITER NO. COUNTER

    DO ITER=1,ITER_MAX

      ITER_NO=ITER
      ERROR=0.D0

      DO J=1,NY-2
      DO I=1,NX-2
        P_OLD(I,J)=P(I,J)
      END DO
      END DO

      !$OMP PARALLEL NUM_THREADS(PARA_NO)
      !$OMP DO REDUCTION(MAX:ERROR)
      DO J=1,NY-2
      DO I=1,NX-2

        P(I,J)=( B(I,J)&
                &-AE(I,J)*P_OLD(I+1,J)-AW(I,J)*P_OLD(I-1,J)&
                &-AN(I,J)*P_OLD(I,J+1)-AS(I,J)*P_OLD(I,J-1) )/AP(I,J)
!                &*RELAX_FACTOR&
!                &+P_OLD(I,J)*(1.D0-RELAX_FACTOR)

        ERROR=MAX( ERROR, ABS((P(I,J)-P_OLD(I,J))/P_OLD(I,J)) )

      END DO
      END DO
      !$OMP END DO
      !$OMP END PARALLEL

      IF (ERROR<=EPS) EXIT

    END DO

    IF (ERROR>EPS .OR. ERROR==0.D0) THEN
      WRITE(*,*) '!!! ERROR EXCEEDED ACCEPTABLE VALUE !!!'
      WRITE(*,*) 'STEP=', STEP
      WRITE(*,*) 'NO. ITER=', ITER_NO
      WRITE(*,*) 'REL_ERROR=', ERROR
      WRITE(*,*)
    END IF

!--*-(ITERATION LOG)-*--*--*--*--*--*--*--!
!    WRITE(*,*) 'STEP=', STEP
!    WRITE(*,*) 'NO. ITER=', ITER_NO
!    WRITE(*,*) 'REL_ERROR=', ERROR
!    WRITE(*,*)
!--*--*--*--*--*--*--*--*--*--*--*--*--*--!

    RETURN
  END SUBROUTINE SOLVE_MATRIX
!----------------------------------------------------------------------!
  SUBROUTINE SOLVE_U(U, P, DT, DX, NX, NY, RHO)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY
    DOUBLE PRECISION,INTENT(IN) :: DT, RHO
    DOUBLE PRECISION,INTENT(IN) :: DX(0:NX-2)
    DOUBLE PRECISION,INTENT(INOUT) :: U(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: P(0:NX-1,0:NY-1)

    !LOCAL VARIABLES
    INTEGER :: I, J

    DO J=1,NY-2
    DO I=1,NX-2
      U(I,J)=U(I,J)& !U_FRACTIONAL (ALREADY CALCULATED IN SOLVE_P)
           &-DT/RHO*(P(I+1,J)-P(I,J))/DX(I) !2ND STAGGERED CENTRAL
    END DO
    END DO

    RETURN
  END SUBROUTINE SOLVE_U
!----------------------------------------------------------------------!
  SUBROUTINE SOLVE_V(V, P, DT, DY, NX, NY, RHO)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY
    DOUBLE PRECISION,INTENT(IN) :: DT, RHO
    DOUBLE PRECISION,INTENT(IN) :: DY(0:NY-2)
    DOUBLE PRECISION,INTENT(INOUT) :: V(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: P(0:NX-1,0:NY-1)

    !LOCAL VARIABLES
    INTEGER :: I, J

    DO J=1,NY-2
    DO I=1,NX-2
      V(I,J)=V(I,J)& !V_FRACTIONAL (ALREADY CALCULATED IN SOLVE_P)
           &-DT/RHO*(P(I,J+1)-P(I,J))/DY(J) !2ND STAGGERED CENTRAL
    END DO
    END DO

    RETURN
  END SUBROUTINE SOLVE_V
!----------------------------------------------------------------------!
  SUBROUTINE OUTPUT_POST(U, V, P, SCALAR, XP, YP, DX, DY, NX, NY, STEP,&
                        &RESULT_FILENAME)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY, STEP
    DOUBLE PRECISION,INTENT(IN) :: XP(0:NX-1), YP(0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: DX(0:NX-2), DY(0:NY-2)
    DOUBLE PRECISION,INTENT(IN) :: U(0:NX-1,0:NY-1), V(0:NX-1,0:NY-1),&
                                  &P(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: SCALAR(0:NX-1,0:NY-1)
    CHARACTER(LEN=50),INTENT(IN) :: RESULT_FILENAME

    !LOCAL VARIABLES
    INTEGER :: I, J
    DOUBLE PRECISION :: XP_POST(0:NX-1), YP_POST(0:NY-1)
    DOUBLE PRECISION :: U_POST(0:NX-1,0:NY-1), V_POST(0:NX-1,0:NY-1),&
                       &P_POST(0:NX-1,0:NY-1)
    DOUBLE PRECISION :: SCALAR_POST(0:NX-1,0:NY-1)

    !COPY XP(I)
    DO I=0,NX-1
      XP_POST(I)=XP(I)
    END DO

    !COPY YP(J)
    DO J=0,NY-1
      YP_POST(J)=YP(J)
    END DO

    !COPY P(I,J) & SCALAR(I,J)
    DO J=0,NY-1
    DO I=0,NX-1
      P_POST(I,J)=P(I,J)
      SCALAR_POST(I,J)=SCALAR(I,J)
    END DO
    END DO

    !-----------------------------
    !  REMOVE DUMMY GRID POINTS
    !-----------------------------

    !REMOVE LEFT DUMMY GRIDS
    XP_POST(0)=5.D-1*(XP(0)+XP(1))

    !REMOVE RIGHT DUMMY GRIDS
    XP_POST(NX-1)=5.D-1*(XP(NX-1)+XP(NX-2))

    !REMOVE LOWER DUMMY GRIDS
    YP_POST(0)=5.D-1*(YP(0)+YP(1))

    !REMOVE UPPER DUMMY GRIDS
    YP_POST(NY-1)=5.D-1*(YP(NY-1)+YP(NY-2))

    !-----------------------------
    !  INTERPOLATE U,V AT GRID POINT
    !-----------------------------

    DO J=1,NY-2
    DO I=1,NX-2
      U_POST(I,J)=5.D-1*(U(I,J)+U(I-1,J)) !LINEAR INTERPOLATION
      V_POST(I,J)=5.D-1*(V(I,J)+V(I,J-1))
    END DO
    END DO

    !-----------------------------
    !  INTERPOLATE U,V,P,SCALAR AT BOUNDARY
    !-----------------------------

    !INTERPOLATE LEFT BOUNDARY
    DO J=1,NY-2
      U_POST(0,J)=U(0,J)
      V_POST(0,J)=5.D-1*(V(0,J)+V(1,J))
      P_POST(0,J)=5.D-1*(P(0,J)+P(1,J))
      SCALAR_POST(0,J)=5.D-1*(SCALAR(0,J)+SCALAR(1,J))
    END DO

    !INTERPOLATE RIGHT BOUNDARY
    DO J=1,NY-2
      U_POST(NX-1,J)=U(NX-2,J)
      V_POST(NX-1,J)=5.D-1*(V(NX-1,J)+V(NX-2,J))
      P_POST(NX-1,J)=5.D-1*(P(NX-1,J)+P(NX-2,J))
      SCALAR_POST(NX-1,J)=5.D-1*(SCALAR(NX-1,J)+SCALAR(NX-2,J))
    END DO

    !INTERPOLATE LOWER BOUNDARY
    DO I=1,NX-2
      U_POST(I,0)=5.D-1*(U(I,0)+U(I,1))
      V_POST(I,0)=V(I,0)
      P_POST(I,0)=5.D-1*(P(I,0)+P(I,1))
      SCALAR_POST(I,0)=5.D-1*(SCALAR(I,0)+SCALAR(I,1))
    END DO

    !INTERPOLATE UPPER BOUNDARY
    DO I=1,NX-2
      U_POST(I,NY-1)=5.D-1*(U(I,NY-1)+U(I,NY-2))
      V_POST(I,NY-1)=V(I,NY-2)
      P_POST(I,NY-1)=5.D-1*(P(I,NY-1)+P(I,NY-2))
      SCALAR_POST(I,NY-1)=5.D-1*(SCALAR(I,NY-1)+SCALAR(I,NY-2))
    END DO

    !LFFT BOTTOM VERTEX GRID
    U_POST(0,0)=5.D-1*(U(0,0)+U(0,1))
    V_POST(0,0)=5.D-1*(V(0,0)+V(1,0))
    P_POST(0,0)=( P(0,0)+P(1,0)+P(0,1)+P(1,1) )/4.D0
    SCALAR_POST(0,0)=( SCALAR(0,0)+SCALAR(1,0)&
                     &+SCALAR(0,1)+SCALAR(1,1) )/4.D0

    !RIGHT BOTTOM VERTEX GRID
    U_POST(NX-1,0)=5.D-1*(U(NX-2,0)+U(NX-2,1))
    V_POST(NX-1,0)=5.D-1*(V(NX-1,0)+V(NX-2,0))
    P_POST(NX-1,0)=( P(NX-2,0)+P(NX-1,0)+P(NX-2,1)+P(NX-1,1) )/4.D0
    SCALAR_POST(NX-1,0)=( SCALAR(NX-2,0)+SCALAR(NX-1,0)&
                        &+SCALAR(NX-2,1)+SCALAR(NX-1,1) )/4.D0

    !LEFT TOP VERTEX GRID
    U_POST(0,NY-1)=5.D-1*(U(0,NY-1)+U(0,NY-2))
    V_POST(0,NY-1)=5.D-1*(V(0,NY-2)+V(1,NY-2))
    P_POST(0,NY-1)=( P(0,NY-2)+P(1,NY-2)+P(0,NY-1)+P(1,NY-1) )/4.D0
    SCALAR_POST(0,NY-1)=( SCALAR(0,NY-2)+SCALAR(1,NY-2)&
                        &+SCALAR(0,NY-1)+SCALAR(1,NY-1) )/4.D0

    !RIGHT TOP VERTEX GRID
    U_POST(NX-1,NY-1)=5.D-1*(U(NX-2,NY-1)+U(NX-2,NY-2))
    V_POST(NX-1,NY-1)=5.D-1*(V(NX-1,NY-2)+V(NX-2,NY-2))
    P_POST(NX-1,NY-1)=( P(NX-2,NY-2)+P(NX-1,NY-2)&
                       &+P(NX-2,NY-1)+P(NX-1,NY-1) )/4.D0
    SCALAR_POST(NX-1,NY-1)=( SCALAR(NX-2,NY-2)+SCALAR(NX-1,NY-2)&
                           &+SCALAR(NX-2,NY-1)+SCALAR(NX-1,NY-1) )/4.D0

    !-----------------------------
    !  OUTPUT RESULTS AS VTK FORMAT
    !-----------------------------

    CALL OUTPUT_RESULTS(U_POST, V_POST, P_POST, SCALAR_POST,&
                       &XP_POST, YP_POST, NX, NY, STEP, RESULT_FILENAME)

    RETURN
  END SUBROUTINE OUTPUT_POST
!----------------------------------------------------------------------!
  SUBROUTINE OUTPUT_RESULTS(U_POST, V_POST, P_POST, SCALAR_POST,&
                           &XP_POST, YP_POST, NX, NY, STEP,&
                           &RESULT_FILENAME)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: NX, NY, STEP
    DOUBLE PRECISION,INTENT(IN) :: XP_POST(0:NX-1), YP_POST(0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: U_POST(0:NX-1,0:NY-1),&
    &V_POST(0:NX-1,0:NY-1), P_POST(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: SCALAR_POST(0:NX-1,0:NY-1)
    CHARACTER(LEN=50),INTENT(IN) :: RESULT_FILENAME

    !LOCAL VARIABLES
    INTEGER :: I, J
    CHARACTER(LEN=60) :: FILENAME

    !-----------------------------
    !  VTK FORMAT FOR PARAVIEW
    !-----------------------------

    WRITE(FILENAME,'(A,I6.6,".vtk")') TRIM(RESULT_FILENAME), STEP
    OPEN(99,FILE=TRIM(FILENAME),STATUS='REPLACE',FORM='FORMATTED',&
    &POSITION='REWIND')

    WRITE(99,"('# vtk DataFile Version 3.0')")
    WRITE(99,"('2D INCOMPRESSIBLE FLOW')")
    WRITE(99,"('ASCII')")
    WRITE(99,"('DATASET STRUCTURED_GRID')")
    WRITE(99,"('DIMENSIONS ',3(1X,I5))") NX, NY, 1

    !GRID POINTS
    WRITE(99,"('POINTS ',I9,' float')") NX*NY
    DO J=0,NY-1
    DO I=0,NX-1
      WRITE(99,"(3(F9.4,1X))") XP_POST(I), YP_POST(J), 0.D0
    END DO
    END DO

    WRITE(99,"('POINT_DATA ',I9)") NX*NY

    !VELOCITY VECTOR
    WRITE(99,"('VECTORS velocity float')")
    DO J=0,NY-1
    DO I=0,NX-1
      WRITE(99,"(3(F9.4,1X))") U_POST(I,J), V_POST(I,J), 0.D0
    END DO
    END DO

    !PRESSURE
    WRITE(99,"('SCALARS pressure float')")
    WRITE(99,"('LOOKUP_TABLE default')")
    DO J=0,NY-1
    DO I=0,NX-1
      WRITE(99,"(F9.4)") P_POST(I,J)
    END DO
    END DO

    !SCALAR
    WRITE(99,"('SCALARS scalar float')")
    WRITE(99,"('LOOKUP_TABLE default')")
    DO J=0,NY-1
    DO I=0,NX-1
      WRITE(99,"(F9.4)") SCALAR_POST(I,J)
    END DO
    END DO

    CLOSE(99)

    RETURN
  END SUBROUTINE OUTPUT_RESULTS
!----------------------------------------------------------------------!
  SUBROUTINE OUTPUT_RESTART(U, V, P, SCALAR, NX, NY, STEP, TIME,&
                           &RESTART_FILENAME)
    INTEGER,INTENT(IN) :: NX, NY, STEP
    DOUBLE PRECISION,INTENT(IN) :: TIME
    DOUBLE PRECISION,INTENT(IN) :: U(0:NX-1,0:NY-1), V(0:NX-1,0:NY-1),&
                                  &P(0:NX-1,0:NY-1)
    DOUBLE PRECISION,INTENT(IN) :: SCALAR(0:NX-1,0:NY-1)
    CHARACTER(LEN=50),INTENT(IN) :: RESTART_FILENAME

    !LOCAL VARIABLES
    INTEGER :: I, J
    CHARACTER(LEN=60) :: FILENAME

    !-----------------------------
    !  OUTPUT RESTART FILE
    !-----------------------------

    WRITE(FILENAME,'(A,I6.6,".csv")') TRIM(RESTART_FILENAME), STEP
    OPEN(89,FILE=TRIM(FILENAME),STATUS='REPLACE',FORM='FORMATTED',&
    &POSITION='REWIND')

    !HEADER
    WRITE(89,*) '***** RESTART FILE ******'
    WRITE(89,*) 'NX=', NX, ', ', 'NY=', NY, ', ', 'TOTAL GP=', NX*NY
    WRITE(89,*)

    !TIME STEP INFO
    WRITE(89,'(I9,","E18.10)') STEP, TIME

    !OUTPUT MATRIX
    DO J=0,NY-1
    DO I=0,NX-1
      WRITE(89,'(E18.10,9(",",E18.10))') U(I,J), V(I,J), P(I,J),&
                                        &SCALAR(I,J)
    END DO
    END DO

    CLOSE(89)

    RETURN
  END SUBROUTINE OUTPUT_RESTART
!----------------------------------------------------------------------!