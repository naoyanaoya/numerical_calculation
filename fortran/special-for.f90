program dowrite

  implicit none

  integer, parameter :: im = 3, jm = 5 ! 配列の大きさの宣言　
  integer            :: i, j
  integer            :: array(im, jm) ! 2 * 3 の配列の宣言

  do j = 1, jm ! 1 ~ 5
    do i = 1, im ! 1 ~ 3
      array(i, j) = i + 10 * j
    end do
  end do

  ! fortranの多次元配列は列優先(Column Major)
  ! # cは行優先
  ! [
  !   11 21 31 41 51
  !   12 22 32 42 52
  !   13 23 33 43 53
  ! ]

  do i = 1, im
    write(6, *) (array(i, j), j = 1, jm) ! それぞれのiに対してjを1からjmまで繰り返す
  enddo
  write(*, *) "==========================================="
  do j = 1, jm
    write(6, *) (array(i, j), i = 1 , im)
  enddo

end program dowrite
