program explicit
parameter( nmax=2000 )
parameter( mmax=10 )
parameter( kmax=56, vmin=0, vmax=1 )
real u(0:nmax,0:mmax,0:mmax), v(0:mmax,0:mmax)
real x(0:mmax), y(0:mmax)
real pi(2,kmax+1)
! *-- 出力ファイルをダンプ (保存) するための命令 ---
CALL SWpSET( 'LDUMP', .TRUE. )
CALL SWpSET( 'LWAIT', .FALSE. )
CALL SWpSET( 'LWAIT1', .FALSE. )

! *-- 空間領域の設定 ---
xmin=0.0
xmax=1.0
ymin=0.0
ymax=1.0
! *-- 空間きざみ幅の設定 ---
! *-- ここでは, 空間分解能をx,y方向で同じ間隔とした ---
dx=(xmax-xmin)/mmax
dy=(ymax-ymin)/mmax        ! これは y 座標を与えるのみの定義
! *-- 時間ステップの設定 ---
dt=1.0/nmax
! *-- 初期条件の設定 ---
do 10 i=1,mmax-1
do 20 j=1,mmax-1
    u(0,i,j)=1.0
    v(i,j)=u(0,i,j)
20  continue
10  continue
do 30 i=0,mmax
    u(0,i,0)=0.0
    u(0,i,mmax)=0.0
    v(i,0)=u(0,i,0)
    v(i,mmax)=u(0,i,mmax)
30  continue
do 40 j=1,mmax-1
    u(0,0,j)=0.0
    u(0,mmax,j)=0.0
    v(0,j)=u(0,0,j)
    v(mmax,j)=u(0,mmax,j)
40  continue

! *-- グラフの出力装置の指定 (IWS=4 は GTK 指定) ----
WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
CALL SGPWSN
! *      IWS=4
READ (*,*) IWS

! *-- 出力装置の呼び出し
CALL GROPN( IWS )

! *-- トーン塗り分けの設定 1 ---
! *-- UESTLV は 100 以下しか呼び出せないので, 苦肉の策 ---
    dp=(vmax-vmin)/kmax
    do 110 k=1,kmax
      TLEV1=(k-1)*dp
      TLEV2=TLEV1+dp
      IPAT=(29+k)*1000+999
      CALL UESTLV( TLEV1, TLEV2, IPAT )
110  continue

! *-- 時間ステップの計算 ---
do 100 i=0,nmax-1
! *-- 境界条件の設定 ---
do 50 j=0,mmax
    u(i+1,j,0)=0.0
    u(i+1,j,mmax)=0.0
    v(j,0)=u(0,j,0)
    v(j,mmax)=u(0,j,mmax)
50  continue
do 60 j=1,mmax-1
    u(i+1,0,j)=0.0
    u(i+1,mmax,j)=0.0
    v(0,j)=u(0,0,j)
    v(mmax,j)=u(0,mmax,j)
60  continue
    x(0)=xmin
    x(mmax)=xmax
    y(0)=ymin
    y(mmax)=ymax
    do 70 j=1,mmax-1
      x(j)=xmin+dx*j
      y(j)=ymin+dy*j
      do 80 k=1,mmax-1
          u(i+1,j,k)=u(i,j,k)+(dt/(dx**2)) * (u(i,j+1,k)+u(i,j-1,k)+u(i,j,k+1) + u(i,j,k-1) - 4.0*u(i,j,k))
          v(j,k)=u(i+1,j,k)
80  continue
70  continue
! *-- t=100 ごとで描画する設定 ---
if(i.gt.20)then
    go to 200
else
    CALL GRFRM
    CALL GRSWND( xmin, xmax, ymin, ymax )
    CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
    CALL USPFIT
    CALL GRSTRF

    CALL USSTTL( 'X-AXIS', '', 'Y-AXIS', '' )
    CALL UETONE( v, mmax+1, mmax+1, mmax+1 )  ! 格子点の個数を与える
    CALL USDAXS
    CALL UDCNTR( v, mmax+1, mmax+1, mmax+1 )
! *-- トーンバー（凡例）の設定 2 ---
    CALL GRSWND( 0.0, 1.0, vmin, vmax )
    CALL GRSVPT( 0.85, 0.9, 0.2, 0.8 )
    CALL GRSTRN( 1 )
    CALL GRSTRF

    do 120 k=1,kmax+1
      pi(1,k)=vmin+(k-1)*dp
      pi(2,k)=vmin+(k-1)*dp
120  continue

    CALL UETONE( pi, 2, 2, kmax+1 )

    CALL SLPVPR( 3 )            ! これは GRSTRN より後ろで宣言される
    CALL UZLSET( 'LABELYR', .TRUE. )
! *         CALL UZFACT( 0.8 )
    CALL UYSFMT( '(F4.1)' )
    CALL UYAXDV( 'R', 0.1, 0.2 )
end if
100  continue
200  CALL GRCLS
stop
end