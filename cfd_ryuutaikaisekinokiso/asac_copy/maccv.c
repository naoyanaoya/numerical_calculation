#include <stdio.h>
#include <math.h>
#include <time.h>

#define MX 21
#define MY 21
#define MX1 MX + 1
#define MY1 MY + 1

double u[MY][MX], v[MY][MX], p[MY][MX], q[MY][MX], d[MY][MX], r[MY][MX], ul[MX], h[MX];
void out(int i21, int j21);


int main(){
    int i, j, k, l, t;
    int iteration_max, timestep_max;
    int i19, i20, i21, j19, j20, j21;
    double a1, a2, a3, divv, dt, dt_inverse, dx, dy, g2, re, re_inverse;
    double u1, u2, v1, v2, un, uu, vn, vv;
    double ua, ub, va, vb;
    double dx_inverse,dx_inverse_half, dy_inverse, dy_inverse_half, uli;
    clock_t start_clock, end_clock;
    // start_clock = clock();
    
    FILE *fp17;

    printf("dt, re, timestep_max (0.01, 40, 300) \n");
    dt = 0.01;
    re = 40;
    timestep_max = 300;
    // scanf("%lf,%lf,%d", &dt, &re, &timestep_max);
    i21 = MX;
    j21 = MY;
    dx = 1.0 / (double)(i21 - 3);
    dy = 1.0 / (double)(j21 - 3);
    dx_inverse = 1.0 / dx;
    dy_inverse = 1.0 / dy;
    dx_inverse_half = dx / 2.0;
    dy_inverse_half = dy / 2.0;
    dt_inverse = 1.0 / dt;
    re_inverse = 1.0 / re;
    i20 = i21 - 1;
    i19 = i21 - 2;
    j20 = j21 - 1;
    j19 = j21 - 2;
    iteration_max = 20;
    printf("iteration_max? (50) \n");
    iteration_max = 50;
    // scanf("%d", &iteration_max);
    a1 = dy * dy / (dx * dx + dy * dy) / 2.0;
    a2 = dx * dx / (dx * dx + dy * dy) / 2.0;
    a3 = dy * dy / (1.0 + dy * dy / (dx * dx)) / 2.0;

    // printf("%lf,%lf,%lf \n", a1, a2, a3);
    // return 0;

    // インクリメントに時間がかかるのか、こっちのほうがおそい
    // for(i=1;i<=i21;i++){
    //     for(j=1;j<=j21;j++){
    //         u[i][j] = 0.0;
    //     }
    // }
    // for(i=1;i<=i21;i++){
    //     for(j=1;j<=j21;j++){
    //         v[i][j] = 0.0;
    //     }
    // }
    // for(i=1;i<=i21;i++){
    //     for(j=1;j<=j21;j++){
    //         p[i][j] = 0.0;
    //     }
    // }
    for(i=1;i<=i21;i++){
        for(j=1;j<=j21;j++){
            u[i][j] = 0.0;
            v[i][j] = 0.0;
            p[i][j] = 0.0;
        }
    }

    // time loop
    for(t=1;t<=timestep_max;t++){

        for(i=1;i<=i21;i++){
            u[i][j20] = 1.0;
            v[i][j21] = v[i][j19];
            v[i][j20] = 0.0;
            v[i][1] = v[i][3];
            v[i][2] = 0.0;
            u[i][1] = -u[i][2];
        }
        for(j=1;j<=21;j++){
            u[i21][j] = u[i19][j];
            u[i20][j] = 0.0;
            v[i20][j] = -v[i19][j];
            u[1][j] = u[3][j];
            u[2][j] = 0.0;
            v[1][j] = -v[2][j];
        }
        divv = 0.0;
        for(j=2;j<=19;j++){
            for(i=2;i<=19;i++){
                u1 = (u[i + 1][j] - u[i][j]) * dx_inverse;
                v2 = (v[i][j + 1] - v[i][j]) * dx_inverse;
                d[i][j] = u1 + v2;
                divv = divv + fabs(u1 + v2);
                ua = 0.25 * (u[i][j] + u[i + 1][j] + u[i + 1][j + 1] + u[i][j + 1]);
                ub = 0.25 * (u[i][j] + u[i + 1][j] + u[i + 1][j - 1] + u[i][j - 1]);
                va = 0.25 * (v[i][j] + v[i][j + 1] + v[i + 1][j + 1] + v[i + 1][j]);
                vb = 0.25 * (v[i][j] + v[i][j + 1] + v[i - 1][j + 1] + v[i - 1][j]);
    	        r[i][j] = -u1 * u1- 2.0 * (ua - ub) * (va - vb) * dx_inverse * dy_inverse - v2 * v2 + dt_inverse * (u1+v2);
            }
        }

        // solving possion equation by SOR method
        for(k=1;k<=iteration_max;k++){
            g2 = 0.0;

            // pressure boundary condition
            for(j=1;j<=j21;j++){
                p[1][j] = p[2][j];
                p[i20][j] = p[i19][j];
            }
            for(i=1;i<=i21;i++){
                p[i][1] = p[i][2];
                p[i][j20] = p[i][j19];
            }

            // SOR method
            for(j=2;j<=j19;j++){
                for(i=2;i<=i19;i++){
                    uli = a1 * (p[i + 1][j] + p[i - 1][j]) + a2 * (p[i][j + 1] + p[i][j - 1]) - a3 * r[i][j] - p[i][j];
                    g2 = g2 + uli * uli;
                    p[i][j] = uli + p[i][j];
                }
            }
            if(g2 <= 0.000001){
                goto ESCAPE;
            }
        }

        ESCAPE:

        if((t % 20) == 0){
            printf("%d %d %lf \n", t, k, g2);

        }
        for(j=2;j<=j19;j++){
            for(i=3;i<=i19;i++){
                v1 = 0.25 * (v[i][j] + v[i][j + 1] + v[i - 1][j + 1] + v[i - 1][j]);
                un = u[i][j] * (u[i + 1][j] - u[i - 1][j]) * dx_inverse_half + v1 * (u[i][j + 1] - u[i][j - 1]) * dy_inverse_half;
                uu = (u[i + 1][j] - 2.0 * u[i][j] + u[i - 1][j]) * dx_inverse * dx_inverse + (u[i][j + 1] - 2.0 * u[i][j] + u[i][j - 1]) * dy_inverse * dy_inverse;
                u[i][j] = u[i][j] + dt * (-un - (p[i][j] - p[i - 1][j]) * dx_inverse + re_inverse * uu);
            }
        }
        for(j=3;j<=j19;j++ ){
            for(i=2;i<=i19;i++){
                u1 = 0.25 * (u[i][j] + u[i + 1][j] + u[i + 1][j - 1] + u[i][j - 1]);
                vn = u1 * (v[i + 1][j] - v[i - 1][j]) * dx_inverse_half + v[i][j] * (v[i][j + 1] - v[i][j - 1]) * dy_inverse_half;
                vv = (v[i + 1][j] - 2.0 * v[i][j] + v[i - 1][j]) * dx_inverse * dx_inverse + (v[i][j + 1] - 2.0 * v[i][j] + v[i][j - 1]) * dy_inverse * dy_inverse;
                v[i][j] = v[i][j] + dt * (-vn - (p[i][j] - p[i][j - 1]) * dy_inverse + re_inverse * vv);
            }
        }
    }
	printf("---U---\n");
	for(j=2;j<=20;j+=2){
        for(i=2;i<=20;i+=2){
        printf("%6.3f",u[i][j]);
        }
        printf("\n");
	}
	printf("\n");
	printf("---V---\n");
	for(j=2;j<=20;j+=2){
	    for(i=2;i<=20;i+=2){
	        printf("%6.3f",v[i][j]);
	    }
	    printf("\n");
	}
	printf("\n");
	printf("---P---\n");
	for(j=2;j<=20;j+=2){
	    for(i=2;i<=20;i+=2){
	        printf("%6.3f",p[i][j]);
	    }
	    printf("\n");
	}
	printf("\n");
	for(i=1;i<=i21;i++){
		q[i][1]=0.0;
	}
	for(j=2;j<=j21;j++){
        for(i=1;i<=i21;i++){
            q[i][j]=q[i][j-1]+dy*u[i][j];
        }
    }
	out(i21,j21);
	fp17=fopen( "file.17", "w");
	for(j=1;j<=j21;j++){
		for(i=1;i<=i21;i++){
			fprintf( fp17,"%e %e %e\n",u[i][j],v[i][j],q[i][j]);
        }
    }
    // end_clock = clock();
    // printf("clock:%lf\n", (double)(end_clock - start_clock) / CLOCKS_PER_SEC);
    // printf("end \n");
    return 0;
}

void out(int i21,int j21)
{
    int i,ind[100],j;
    float qma, qmi;

    qma=q[1][1];
    qmi=q[1][1];
    for(j=1;j<=j21;j++){
        for(i=1;i<=i21;i++){
            if(qma<q[i][j]){
                qma = q[i][j];
            }
            if(qmi>q[i][j]){
                qmi = q[i][j];
                }
        }
    }
    for(j=j21-1;j>=2;j+=-1){
        for(i=2;i<=i21-1;i++){
            ind[i]=(int)((q[i][j]-qmi)/(qma-qmi)*9.9999);
            ind[i]=ind[i]*11;
        }
        for(i=2;i<=i21-1;i++){
            printf("%2d ",ind[i]);
        }
        printf("\n");
    }
}