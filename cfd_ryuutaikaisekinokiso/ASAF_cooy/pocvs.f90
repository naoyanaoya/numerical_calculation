! ==================================================
! steady flow in cubic cavity
! ==================================================

program main
  implicit none
  integer, parameter :: mx = 101, my = 101 ! プログラムの配列の大きさを指定するパラメータ
  double precision psi(mx, my) ! 流れ関数ψを記憶する配列
  double precision omg(mx, my) ! 渦度ωを記憶する配列
  double precision tmp(mx, my) ! 流れ関数ψや渦度ωを一次的に記憶する配列
  double precision re ! レイノルズ数
  integer nn ! 格子数
  integer na ! 格子点数
  integer iteration_max ! 差分方程式を(連立代数方程式)を反復法で解くときの最大反復回数
  double precision h ! 格子幅
  double precision h_inverse ! 格子幅の逆数
  double precision const1 ! 渦度に関する方程式をSOR法で解くときに加速係数
  double precision const2 ! 流れ関数に関する方程式をSOR法で解くときに加速係数
  double precision EPS ! 反復法の許容最大誤差(収束判定条件)
  double precision aa ! ある格子点での前のステップでの渦度または流れ関数
  double precision bb ! ある格子点での渦度または流れ関数ψの相対誤差
  double precision err1 ! 全格子点での渦度の最大相対誤差(aaの最大値)
  double precision err2 ! 全格子点での渦度の最大相対誤差(bbの最大値)
  double precision rhs
  integer ii, isave
  integer i, j, t

  ! write(*,*) 0.0
  ! write(*,*) 0.d0

! read and calculate parameters
99 write(*,"(a)") "input number of mesh (<100) (20)"
  na = 80 ! 格子数
  ! read(*,*) na
  nn = na + 1 ! 格子点数
  ! if(nn <= 1 .or. 52 < nn) then
  !   go to 99
  ! end if
  write(*,"(a)") "input reynolds number (40)"
  re = 40
  ! read(*,*) re
  write(*,*) "input maximum number of iteration (500)"
  iteration_max = 10000
  ! read(*,*) iteration_max
  write(*,"(a)") "input accelaration two parameters (1.0, 1.0)"
  write(*,"(a)") "if accelaration is 1.0, it is equal to gauss-seidel"
  write(*,"(a)") "for omega eq and psi eq"
  const1 = 1.5
  const2 = 1.5
  ! read(*,*) const1, const2
  write(*,"(a)") "input maximum error eps (0.000001)"
  EPS = 0.00001
  ! read(*,*) EPS

  h = 1.d0 / dble(nn - 1)
  h_inverse = 1.d0 / h
  ! write(*,*) h
  ! write(*,*) h_inverse

! initial condiiton for psi and omega
  do j = 1, nn
    do i = 1, nn
      psi(i, j) = 0.d0
      omg(i, j) = 0.d0
    end do
  end do

  ! do i = 1, nn
  !   do j = 1, nn
  !     write(*,"(f7.3)",advance="no") omg(i, j)
  !   end do
  !   write(*,*)
  ! end do

! main loop
  do t = 1, iteration_max
! boundary condition
    ! left and right
    do j = 1, nn
      omg(1, j) = -2.d0 * psi(2, j) * h_inverse * h_inverse
      omg(nn, j) = -2.d0 * psi(nn - 1, j) * h_inverse * h_inverse
    end do
    ! bottom and top
    do i = 1, nn
      omg(i, 1) = -2.d0 * psi(i, 2) * h_inverse * h_inverse
      omg(i, nn) = -2.d0 * (psi(i, nn - 1) + h) * h_inverse * h_inverse
    end do
    ! ここの要素は不変

    ! write(*,*) "======================"
    ! do i = 1, nn
    !   do j = 1, nn
    !     write(*,"(f7.3)",advance="no") omg(i, j)
    !   end do
    !   write(*,*)
    ! end do
    ! stop

    ! calculate new omega (step 2)
    do j = 2, nn - 1
      do i = 2, nn - 1
        tmp(i, j) = omg(i, j) ! 一時的に保存
        rhs = (omg(i + 1, j) + omg(i - 1, j) + omg(i, j + 1) + omg(i, j - 1)) / 4.d0 + ((psi(i + 1, j) - psi(i - 1, j)) * &
        (omg(i, j + 1) - omg(i, j - 1)) - (psi(i, j + 1) - psi(i, j - 1)) * (omg(i + 1, j) - omg(i - 1, j))) * re / 16.d0
        omg(i, j) = omg(i, j) * (1.d0 - const1) + rhs * const1
      end do
    end do

    err1 = 0.d0
    do j = 2, nn - 1
      do i = 2, nn - 1
        bb = abs(omg(i, j) - tmp(i, j))
        if(err1 <= bb) then
          err1 = bb
        end if
      end do
    end do

    ! calculate new psi (step 3)
    do j = 2, nn - 1
      do i = 2, nn - 1
        tmp(i, j) = psi(i, j) ! 一時的に保存
        rhs = (psi(i + 1, j) + psi(i - 1, j) + psi(i, j + 1) + psi(i, j - 1)) / 4.d0 + omg(i, j) * h * h / 4.d0
        psi(i, j) = psi(i, j) * (1.d0 - const2) + rhs * const2
      end do
    end do

    err2 = 0.d0
    do j = 2, nn - 1
      do i = 2, nn - 1
        aa = max(1e-8, abs(tmp(i, j)))
        bb = abs(psi(i, j) - tmp(i, j)) / aa
        if(err2 <= bb) then
          err2 = bb
        end if
      end do
    end do

    if(mod(t, 50) == 0) then
      write(*,*) t, "error(omg)=", err1, "error(psi)=", err2
    end if
    if(max(err1, err2) <= eps) then
      go to 80
    end if
  end do
! end of main loop

  write(*,*) "not converge! do you want continue? (yes = 1)"
  read(*,*) ii
  if(ii == 1) then
    go to 99
  end if

  80 call output(psi, mx, my, nn, nn)
  write(*,"(a)") "saving in file? (yes=1)"
  read(*,*) isave
  open(17,file="pocvs.txt",status="replace")
  if(isave == 1) then
    write(17,*) nn, nn
    do j = 1, NN
      write(17,*) (PSI(i, j), I = 1, NN)
    end do
    do j = 1, NN
      write(17,*) (OMG(I, J), I = 1, NN)
    end do
  end if
  close(17)

end program main

! subroutine for output
subroutine output(u, nx, ny, mx, my)
  implicit none
  integer mx, my, nx, ny
  double precision, intent(in) :: u(nx, ny)
  character*2 z(101)
  integer index(101)
  double precision umax, umin
  integer ind
  double precision cc
  integer i, j

  umax = u(1, 1)
  umin = u(1, 1)
  do j = 1, my
    do i = 1, mx
      if(umax < u(i, j)) then
        umax = u(i, j)
      end if
      if(u(i, j) < umin) then
        umin = u(i, j)
      end if
    end do
  end do

  do j = my, 1, -1
    do i = 1, mx
      ind = int((u(i, j) - umin) / (umax - umin) * 7.9999) * 11
      if(mod(ind, 2) == 0) then
        z(i) = "**"
      end if
      if(mod(ind, 2) == 1) then
        z(i) = "  "
      end if
    end do
    z(1) = "| "
    z(mx) = " |"
    if(j == 1 .or. j == my) then
      do i =1 , mx
        z(i) = "--"
      end do
    end if
    write(*,601) (z(i), i = 1, mx)
  end do
  601 format(100a2)

  write(*,"(a)") "input any number (1)"
  read(*,*) cc

  do j = my, 1, -1
    do i = 1, mx
      index(i) = int((u(i, j) - umin) / (umax - umin) * 9.9999) * 11
    end do
    write(*,600) (index(i), i = 1, mx)
  end do
  600 format(100I2)

end subroutine output


! ＜Ｃａｖｉｔｙ問題、ψ－ω法　－－－定常解＞

! 　このプログラムはＣａｖｉｔｙ流れを定常問題に対する流れ関数－渦度法を用いて計算するプログラムであり、本文の手続きをそのままプログラムしたものである。配列PSI、OMGはそれぞれψ、ωを表す。プログラムのはじめの部分でｘ方向の格子数NN（ｙ方向も同数）、レイノルズ数RE、最大反復回数NMAX、反復法の加速係数CONST1、CONST2、反復法の収束判定条件に用いるEPS1、EPS2を読み込む。なお反復法は次の条件が成立したとき、収束したとみなした。
! 　　max｜ψ(υ+1)j,k －ψ(υ)j,k｜≦EPS1
!     max｜ω(υ+1)j,k －ω(υ)j,k｜≦EPS2
!   DO 10 のループで反復法の出発値を０とおいたが、例えばレイノルズ数を変化させて複数の計算を行う場合には、あるケースの収束値を次のケースの出発値にとることにより収束を速めることができる。DO 100のループでψ、ωの反復計算を行う。DO 20、DO 30のループは(3.18)を用いて壁面上のωを内部のψを用いて変化させる境界条件の部分であり、反復過程でψが変化するためDO 100のループの内側にいれておく必要がある。なお境界上のψの値は常に０で変化しないため、DO 10 のループで初期条件を兼ねて一度与えておけば十分である。したがって境界条件としては改めて課していない。DO 40 のループで(3.20)、DO 60 のループで(3.19)を計算する。収束の判定はDO 50、DO 70のループで行っている。出力結果の表示はサブルーチンOUT で行っている。

! 　メインプログラムの仕様

! 　　配列　PSI ----  流れ関数ψを記憶する配列
! 　　配列　OMG ----  渦度ωを記憶する配列
! 　　配列　TMP ----  流れ関数や渦度ωを一時的に記憶する配列
! 　　変数　MX,MY --  プログラムの配列の大きさを指定するパラメータ
! 　　変数　NA  ----  格子数（Ｘ、Ｙ方向同数）
! 　　変数　NN  ----  格子点数（Ｘ、Ｙ方向同数）
! 　　変数　RE  ----  レイノルズ数
! 　　変数　NMAX ---  差分方程式（連立代数方程式）を反復法で解く時の
! 　　　　　　　　　　最大反復回数
! 　　変数　CONST1 -  渦度に関する方程式をＳＯＲ法で解くときの加速係数
! 　　変数　CONST2 -  流れ関数に関する方程式をＳＯＲ法で解くときの加速係数
! 　　変数　EPS  ---  反復法の許容最大誤差（収束判定条件）
! 　　変数　H   ----  格子幅
! 　　変数　HI   ---  １／（格子幅）
! 　　変数　AA  ----  ある格子点の前のステップでの渦度または流れ関数
! 　　変数　BB  ----  ある格子点での渦度または流れ関数の相対誤差
! 　　変数　ERR1 ---  全格子点での渦度の最大相対誤差（BBの最大値）
! 　　変数　ERR2 ---  全格子点での流れ関数の最大相対誤差（BBの最大値）

! 　サブルーチン　OUT の仕様（ラプラス方程式の解法(LAP.FOR)と同じ）