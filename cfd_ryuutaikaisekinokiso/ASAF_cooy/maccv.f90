program main
  implicit none
  integer, parameter :: mx = 7001, my = 7001 ! プログラムの配列の大きさを指定するパラメータ
  double precision u(mx, my) ! 速度uを記憶する配列
  double precision v(mx, my) ! 速度vを記憶する配列
  double precision p(mx, my) ! 圧力pを記憶する配列
  double precision r(mx, my) ! ポアソン方程式(3.47)の右辺を記憶する配列
  double precision d(mx, my) ! divVを記憶する配列
  double precision u1 ! 
  double precision u2
  double precision v1
  double precision v2 ! 
  double precision un
  double precision uu
  double precision vn
  double precision vv
  double precision dt ! 時間刻み
  double precision dt_inverse ! 時間幅刻み幅の逆数
  double precision re ! レイノルズ数
  double precision re_inverse ! レイノルズ数の逆数
  integer timestep_max ! 全体のステップ数
  integer iteration_max ! ポアソン方程式の反復回数の上限
  integer nx, ny ! x,y方向の格子数
  integer i19, j19 ! x,y方向の内側?
  integer i20, j20 ! x, y方向の格子数
  integer i21, j21 ! x,y方向の格子点数
  double precision dx, dy ! x,y方向の格子幅
  double precision dx_inverse, dy_inverse ! x,y方向の格子幅の逆数
  double precision dx_inverse_half, dy_inverse_half ! x,y方向の格子幅の逆数の1/2
  double precision a1, a2, a3 ! ポアソン方程式を差分化したときに出てくる係数
  double precision g2 ! 
  double precision uli !
  double precision divv ! 
  double precision ua, ub, va, vb
  double precision isave
  double precision t1, t2

  integer i, j, k, t

  write(*,"(a)") "input number of mesh (<51) (20, 20)"
  nx = 20
  ny = 20
  ! read(*,*) nx, ny
  i21 = nx + 1
  j21 = ny + 1
  write(*,"(a)") "input time inclement dt (0.01)"
  dt = 0.01
  ! read(*,*) dt
  write(*,"(a)") "input reynolds number (40)"
  re = 40
  ! read(*,*) re
  write(*,"(a)") "input number of time step (300)"
  timestep_max = 300
  ! read(*,*) timestep_max
  write(*,"(a)") "input maximum number of iteration for poisson equation (50)"
  iteration_max = 50
  ! read(*,*) iteration_max
  dx = 1.d0 / dble(i21 - 3)
  dy = 1.d0 / dble(j21 - 3)
  dx_inverse = 1.d0 / dx
  dy_inverse = 1.d0 / dy
  dx_inverse_half = dx_inverse / 2.d0
  dy_inverse_half = dy_inverse / 2.d0
  dt_inverse = 1.d0 / dt
  re_inverse = 1.d0 / re
  i20 = i21 - 1
  j20 = j21 - 1
  i19 = i21 - 2
  j19 = j21 - 2
  a1 = dy * dy / (dx * dx + dy * dy) / 2.d0
  a2 = dx * dx / (dx * dx + dy * dy) / 2.d0
  a3 = dy * dy / (1.d0 + dy * dy / (dx * dx)) / 2.d0


  ! call cpu_time(t1)
! initail condition
  do j = 1, j21
    do i = 1, j21
      u(i, j) = 0.d0
      v(i, j) = 0.d0
      p(i, j) = 0.d0
    end do
  end do
  ! call cpu_time(t2)
  ! write(*,*) "cpu time:", t2 - t1, "seconds."
  ! stop
  ! time step
  do t = 1, timestep_max

    ! boundary condition
    do i = 1, i21
      u(i, j20) = 1.d0
      v(i, j21) = v(i, j19)
      v(i, j20) = 0.d0
      v(i, 1) = v(i, 3)
      v(i, 2) = 0.d0
      u(i, 1) = -u(i, 2)
    end do
    do j = 1, j21
      u(i21, j) = u(i19, j)
      u(i20, j) = 0.d0
      v(i20, j) = -v(i19, j)
      u(1, j) = u(3, j)
      u(2, j) = 0.d0
      v(1, j) = -v(2, j)
    end do

    ! calculate right hand side of possion equation
    divv = 0.d0
    do j = 2, j19
      do i = 2, i19
        u1 = (u(i + 1, j) - u(i, j)) * dx_inverse
        ! u2 = (u(i, i + 1) - u(i, j - 1)) * dx_inverse_half
        ! v1 = (v(i + 1, j) - v(i - 1, j)) * dy_inverse_half
        v2 = (v(i, j + 1) - v(i, j)) * dy_inverse
        d(i, j) = u1 + v2
        divv = divv + abs(u1 + v2)
        ua = 0.25 * (u(i, j) + u(i + 1, j) + u(i + 1, j + 1) + u(i, j + 1))
        ub = 0.25 * (u(i, j) + u(i + 1, j) + u(i + 1, j - 1) + u(i, j - 1))
        va = 0.25 * (v(i, j) + v(i, j + 1) + v(i + 1, j + 1) + v(i + 1, j))
        vb = 0.25 * (v(i, j) + v(i, j + 1) + v(i - 1, j + 1) + v(i - 1, j))
        r(i, j) = dt_inverse * (u1 + v2) - u1 * u1 - v2 * v2 - 2.d0 * (ua - ub) * (va - vb) * dx_inverse * dy_inverse
      end do
    end do

    ! solving poisson equation by SOR method
    do k = 1, iteration_max
      g2 = 0.d0

      ! pressure boundary condition
      do j = 1, j21
        p(1, j) = p(2, j)
        p(i20, j) = p(i19, j)
      end do
      do i = 1, i21
        p(i, 1) = p(i, 2)
        p(i, j20) = p(i, j19)
      end do

      ! SOR method
      do j = 2, j19
        do i = 2, i19
          uli = a1 * (p(i + 1, j) + p(i - 1, j)) + a2 * (p(i, j + 1) + p(i, j - 1)) - a3 * r(i, j) - p(i, j)
          g2 = g2 + uli * uli
          p(i, j) = uli + p(i, j)
        end do
      end do

      if(g2 <= 0.000001) then
        go to 54
      end if

    end do

    ! write(*, *) "========p========"
    ! write(*,600) ((p(i, j), i = 2, 20, 2), j = 2, 20, 2)

    if(t == 1) then
      write(*,"(a)") "t   :   k   :   g2"
    end if
    54 if(mod(t, 1) == 0) then
        write(*,*) t, k, g2
    end if

    ! time integration for NS equation
    do j = 2, j19
      do i = 3, i19
        vb = 0.25 * (v(i, j) + v(i, j + 1) + v(i - 1, j + 1) + v(i - 1, j))
        un = u(i, j) * (u(i + 1, j) - u(i - 1, j)) * dx_inverse_half + vb * (u(i, j + 1) - u(i, j - 1)) * dy_inverse_half
        uu = (u(i + 1, j) - 2.d0 * u(i, j) + u(i - 1, j)) * dx_inverse * dx_inverse + (u(i, j + 1) - 2.d0 * u(i, j) + u(i, j - 1)) &
           * dy_inverse * dy_inverse
        d(i, j) = u(i, j) + dt * (-un - (p(i, j) - p(i - 1, j)) * dx_inverse + re_inverse * uu)
        ! dx_inverseは(p(i, j) - p(i - 1, j))に掛かってる
      end do
    end do
    do j = 3, j19
      do i = 2, i19
        ub = 0.25 * (u(i, j) + u(i + 1, j) + u(i + 1, j - 1) + u(i, j - 1))
        vn = ub * (v(i + 1,j) - v(i - 1, j)) * dx_inverse_half + v(i, j) * (v(i, j + 1) - v(i, j - 1)) * dy_inverse_half
        vv = (v(i + 1, j) - 2.d0 * v(i ,j) + v(i - 1, j)) * dx_inverse * dx_inverse + (v(i, j + 1) - 2.d0 * v(i ,j) + v(i, j - 1)) &
           * dy_inverse * dy_inverse
        r(i, j) = v(i, j) + dt * (-vn - (p(i, j) - p(i, j - 1)) * dy_inverse + re_inverse * vv)
      end do
    end do

    do j = 2 , j19
      do i = 3, i19
        u(i, j) = d(i, j)
      end do
    end do
    do j = 3, j19
      do i = 2, i19
        v(i, j) = r(i, j)
      end do
    end do

  ! write(*, *) "========u========"
  ! write(*,600) ((u(i, j), i = 2, 20, 2), j = 2, 20, 2)
  ! write(*, *) "========v========"
  ! write(*,600) ((v(i, j), i = 2, 20, 2), j = 2, 20, 2)
  ! write(*, *) "========p========"
  ! write(*,600) ((p(i, j), i = 2, 20, 2), j = 2, 20, 2)
  ! if(t == 2) then
  !   stop
  ! end if

  end do ! time loop はここで終わり

  write(*, *) "========u========"
  write(*,600) ((u(i, j), i = 2, 20, 2), j = 2, 20, 2)
  write(*, *) "========v========"
  write(*,600) ((v(i, j), i = 2, 20, 2), j = 2, 20, 2)
  write(*, *) "========p========"
  write(*,600) ((p(i, j), i = 2, 20, 2), j = 2, 20, 2)
  600 format(10f6.3)

  ! calcualte psi
  do i = 1, i21
    r(i, 1) = 0.d0
  end do
  do j = 2, j21
    do i = 1, i21
      r(i, j) = r(i, j - 1) + dy * u(i, j)
    end do
  end do

  call output(r, mx, my, i21, j21)
  call output_vtk(u, v, p, i21, j21)

  write(*,"(a)") "save data? yes=1, no=0"
  read(*,*) isave
  if(isave == 0) then
    do j = 1, j21
      do i = 1, i21
        write(17,*) u(i, j), v(i, j), p(i, j)
      end do
    end do
  end if

end program main


subroutine output_vtk(u, v, p, i21, j21)
  implicit none
  integer i21, j21
  double precision, intent(in) :: u(i21, j21), v(i21, j21), p(i21, j21)
  open(20,file="maccv.vtk")
  write(20,"(a)") "# vtk DataFile Version 2.0"
  write(20,"(a)") "Header"
  write(20,"(a)") "ASCII"
  write(20,"(a)") "DATASET UNSTRUCTURED_GRID"
  write(20,"(a)",advance="no") "POINTS "
  write(20,"(i4)",advance="no") i21 * j21
  write(20,"(a)",advance="no") " float"
  close(20)


end subroutine output_vtk



! subroutine for output
subroutine output(u, nx, ny, mx, my)
  implicit none
  integer mx, my, nx, ny
  double precision, intent(in) :: u(nx, ny)
  character*2 z(80)
  integer index(80)
  double precision umax, umin
  integer ind
  double precision cc
  integer i, j

  umax = u(1, 1)
  umin = u(1, 1)
  do j = 1, my
    do i = 1, mx
      if(umax < u(i, j)) then
        umax = u(i, j)
      end if
      if(u(i, j) < umin) then
        umin = u(i, j)
      end if
    end do
  end do

  do j = my, 1, -1
    do i = 1, mx
      ind = int((u(i, j) - umin) / (umax - umin) * 7.9999) * 11
      if(mod(ind, 2) == 0) then
        z(i) = "**"
      end if
      if(mod(ind, 2) == 1) then
        z(i) = "  "
      end if
    end do
    z(1) = "| "
    z(mx) = " |"
    if(j == 1 .or. j == my) then
      do i =1 , mx
        z(i) = "--"
      end do
    end if
    write(*,601) (z(i), i = 1, mx)
  end do
  601 format(80a2)

  write(*,"(a)") "input any number (1)"
  read(*,*) cc

  do j = my, 1, -1
    do i = 1, mx
      index(i) = int((u(i, j) - umin) / (umax - umin) * 9.9999) * 11
    end do
    write(*,600) (index(i), i = 1, mx)
  end do
  600 format(80I2)

end subroutine output


! ＜Ｃａｖｉｔｙ問題、ＭＡＣ法＞

! 　このプログラムはＣａｖｉｔｙ問題を非保存形ナビエ・ストークス方程式を用いて解くプログラムである。配列Ｕ、Ｖ、Ｐ、Ｒは速度ｕ、ｖ、圧力ｐおよびポアソン方程式(3.40)（または(3.47)）の右辺を記憶する配列である。またＤは  div V を記憶する配列である。なおＲ、Ｄはｕ、ｖの一次的な記憶場所としても用いる。I20、J20はｘ、ｙ方向の格子数である。またＤＴはΔｔ、ＲＥはレイノルズ数であり、ＬＭは全体のタイムステップ数、ＫＭはポアソン方程式の反復回数の上限である。Ａ１、Ａ２、Ａ３は(3.47)を次のように変形したときの係数である。

! Ｐi,j＝Ａ１*（Ｐi+1,j＋Ｐi-1,j）＋Ａ２*（Ｐi,j+1＋Ｐi,j-1）－Ａ３*Ｒi,j

! ただし、Ｒは(3.47)の右辺に相当する。DO 20 のループでｕ、ｖ、ｐの初期条件（今の場合すべて０）を与えている。DO 30 のループにおいてＭＡＣ法の時間発展的な解法を構成する。DO 40 のループは左右の壁面での速度の境界条件であり、DO 41 のループは上下の壁面での速度の境界条件である。DO 22 のループで div Ｖ の値およびポアソン方程式の右辺の値Ｒを計算している。 div Ｖ の値を記憶する必要はないが、連続の式がどの程度満足されているかということは、ＭＡＣ法の大きなポイントであるから、計算結果の検討のため一応記憶している。DO 50 のループでガウス－ザイデル法を用いて圧力のポアソン方程式を解いている。圧力の境界条件はノイマン条件（微係数に関する条件）であるから、反復ごとに値が変化する。したがって境界条件も反復のループ内に含める必要がある。収束の判定は残差（上式の右辺－左辺）の全格子点での二乗和がEPS より小さいかどうかで行っている。ただし反復回数の上限をもうけ、上限を越えれば収束していなくても次のステップに進むようにしている。DO 60、61 のループで速度ｕ、ｖをひとつの時間ステップ進める。新しいｕをＤ、ｖをＲに一度記憶した後に DO 62、63のループでＤ、ＲをＵ、Ｖに入れ換えている。計算終了後、DO 71 のループで流れ関数を
! 　　　　ψ　＝　∫ｕ　ｄｙ
! から計算し、Ｒに記憶したあと、Ｒの等値線を簡単な表示プログラムＯＵＴ（プログラムLAP.FOR と同じ）を用いて表示している。なお div Ｖ の値Ｄも計算途中で出力するとよい。

! 　メインプログラムの仕様

! 　　配列　Ｕ　----  Ｘ方向速度Ｕを記憶する配列
! 　　配列　Ｖ　----  Ｙ方向速度Ｖを記憶する配列
! 　　配列　Ｐ　----  圧力Ｐを記憶する配列
! 　　配列　Ｒ　----  ポアソン方程式の右辺（ソース項）を記憶する配列、
! 　　　　　　　　　　Ｖの一時記憶場所や計算終了後の流れ関数の記憶場所
! 　　　　　　　　　　としても用いる。
! 　　配列　Ｄ　----  div v を記憶する配列、Ｕの一時記憶場所
! 　　変数　NX,NY --  プログラムの配列の大きさを指定するパラメータ
! 　　変数　NA,NB --  Ｘ、Ｙ方向の格子数
! 　　変数　I21,J21-  Ｘ、Ｙ方向の格子点数
! 　　変数　DT  ----  時間間隔
! 　　変数　RE  ----  レイノルズ数
! 　　変数　LM  ----  計算打ち切りのタイムステップ数
! 　　変数　KM  ----  流れ関数に関するポアソン方程式の近似差分方程式を
! 　　　　　　　　　　ガウス－ザイデル法で解くときの最大反復回数
! 　　変数　DX,DY --  Ｘ、Ｙ方向の格子幅
! 　　変数　XD,YD --  Ｘ、Ｙ方向の格子幅の逆数
! 　　変数　XDH,YDH-  Ｘ、Ｙ方向の格子幅の逆数の１／２
! 　　変数　TD  ----  時間間隔の逆数
! 　　変数　R1  ----  レイノルズ数の逆数
! 　　変数　I20,I19-  それぞれ I21-1、I21-2
! 　　変数　J20,J19-  それぞれ J21-1、J21-2
! 　　変数　A1,A2,A3  ポアソン方程式を差分化したときにでてくる係数
! 　　変数　DIVV ---  div V の絶対値の各格子点での総和
! 　　変数　G2  ----  すべての内部格子点での圧力の誤差の二乗和

! 　サブルーチン　OUT の仕様（ラプラス方程式の解法(LAP.FOR)と同じ）