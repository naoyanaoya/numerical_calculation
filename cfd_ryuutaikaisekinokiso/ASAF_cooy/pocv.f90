! ==================================================
! unsteady flow in cubic cavity
! ==================================================

program main
  implicit none
  integer, parameter :: mx = 51, my = 51 ! プログラムの配列の大きさを指定するパラメータ
  double precision, allocatable :: psi(:,:) ! 流れ関数ψを記憶する配列
  double precision, allocatable :: omg(:,:) ! 渦度ωを記憶する配列
  double precision, allocatable :: tmp(:,:) ! 流れ関数ψや渦度ωを一時的に記憶する配列
  double precision re ! レイノルズ数
  integer nn ! 格子数
  integer na ! 格子点数
  integer time_step ! 計算打ち切りのタイムステップ数
  double precision dt ! 時間間隔
  double precision h ! 格子幅
  double precision hh ! 格子幅の二条
  double precision h_inverse ! 格子幅の逆数
  double precision const ! 流れ関数ψに関するポアソン方程式の近似差分方程式をSOR法で解くときの加速係数
  double precision EPS ! SOR方の許容最大誤差(収束条件)
  double precision err2 ! 全講師店での流れ関数ψの最大誤差(bbの最大値)
  double precision rhs ! 一時的な変数
  double precision bb ! ある格子点での流れ関数の誤差
  integer isave ! 1のとき計算結果をファイルに記憶する、それ以外のときは記憶しない
  integer i, j, k, t

! read and calculate parameters
  99 write(*,"(a)") "input number of mesh (<51) (20)"
  na = 30 ! メッシュ数
  ! read(*,*) na
  nn = na + 1 ! 節点数
  if(nn <= 1 .or. 52 < nn) then
    go to 99
  end if
  write(*,"(a)") "input reynolds number (40)"
  re = 40
  ! read(*,*) re
  write(*,"(a)") "input time increment dt (0.01)"
  dt = 0.01
  ! read(*,*) dt
  write(*,*) "input number of time step (100)"
  time_step = 1000
  ! read(*,*) iteration_max
  write(*,"(a)") "input accelaration two parameters (1.0, 1.0)"
  write(*,"(a)") "if accelaration is 1.0, it is equal to gauss-seidel"
  write(*,"(a)") "for omega eq and psi eq"
  const = 1.0
  ! read(*,*) const1, const2
  write(*,"(a)") "input maximum error eps (0.000001)"
  EPS = 0.00001
  ! read(*,*) EPS

  h = 1.d0 / dble(nn - 1)
  h_inverse = 1.d0 / h
  hh = h * h

  ! allocate(psi(nn,nn))
  ! allocate(omg(nn,nn))
  ! allocate(tmp(nn,nn))

  allocate(psi(mx,mx))
  allocate(omg(mx,mx))
  allocate(tmp(mx,mx))

  ! initial condition for psi and omega
  do j = 1, nn
    do i = 1, nn
      psi(i, j) = 0.d0
      omg(i, j) = 0.d0
    end do
  end do

  ! main loop
  do t = 1, time_step
    ! boundary condition (step 1)
    do j = 1, nn
      omg(1, j) = -2.d0 * psi(2, j) * h_inverse * h_inverse
      omg(nn, j) = -2.d0 * psi(nn - 1, j) * h_inverse * h_inverse
    end do
    do i = 1, nn
      omg(i, 1) = -2.d0 * psi(i, 2) * h_inverse * h_inverse
      omg(i, nn) = -2.d0 * (psi(i, nn - 1) + h) * h_inverse * h_inverse
    end do

    ! calculate new psi (step 2)
    do k = 1, 10000
      do j = 2, nn - 1
        do i = 2, nn - 1
          tmp(i, j) = psi(i, j)
          rhs = (psi(i + 1, j) + psi(i - 1, j) + psi(i, j + 1) + psi(i, j - 1)) / 4.d0 + omg(i, j) * h * h / 4.d0
          psi(i, j) = (1.d0 - const) * psi(i, j) + rhs * const
          ! psi(i, j) = (1.d0 - const) * psi(i, j) + rhs * const
        end do
      end do

      err2 = 0.d0
      do j = 2, nn - 1
        do i = 2, nn - 1
          bb = abs(psi(i, j) - tmp(i, j))
          if(err2 <= bb) then
            err2 = bb
          end if
        end do
      end do
      
      if(err2 <= EPS) then
        go to 75
      end if
    end do

    75 continue
    write(*,*) t, k, err2

    ! calculate new omega (step 3)
    do j = 2, nn - 1
      do i = 2, nn - 1
        tmp(i, j) = omg(i, j)
        rhs = ((psi(i + 1, j) - psi(i - 1, j)) * (omg(i, j + 1) - omg(i, j - 1)) - (psi(i, j + 1) - psi(i, j - 1)) * &
            (omg(i + 1, j) - omg(i - 1, j)))/ 4.d0 + (omg(i + 1, j) + omg(i - 1, j) + omg(i, j + 1) + omg(i, j - 1) - &
            4.d0 * omg(i, j)) / re
        omg(i, j) = omg(i, j) + dt * rhs / hh
      end do
    end do

    ! err1 = 0.d0
    ! do j = 2, nn - 1
    !   do i = 2, nn - 1
    !     bb = abs(omg(i, j) - tmp(i, j))
    !     if(err1 <= bb) then
    !       err1 = bb
    !     end if
    !   end do
    ! end do
    ! write(*,*) t, k, err1

  end do

  call output(psi, mx, my, nn, nn)
  write(*,"(a)") "saving in file? (yes=1)"
  read(*,*) isave
  open(17,file="pocv.txt",status="replace")
  if(isave == 1) then
    write(17,*) nn, nn
    do j = 1, NN
      write(17,*) (PSI(i, j), I = 1, NN)
    end do
    do j = 1, NN
      write(17,*) (OMG(I, J), I = 1, NN)
    end do
  end if
  close(17)

end program main


  ! subroutine for output
  subroutine output(u, nx, ny, mx, my)
    implicit none
    integer mx, my, nx, ny
    double precision, intent(in) :: u(nx, ny)
    character*2 z(80)
    integer index(80)
    double precision umax, umin
    integer ind
    double precision cc
    integer i, j

    umax = u(1, 1)
    umin = u(1, 1)
    do j = 1, my
      do i = 1, mx
        if(umax < u(i, j)) then
          umax = u(i, j)
        end if
        if(u(i, j) < umin) then
          umin = u(i, j)
        end if
      end do
    end do

    do j = my, 1, -1
      do i = 1, mx
        ind = int((u(i, j) - umin) / (umax - umin) * 7.9999) * 11
        if(mod(ind, 2) == 0) then
          z(i) = "**"
        end if
        if(mod(ind, 2) == 1) then
          z(i) = "  "
        end if
      end do
      z(1) = "| "
      z(mx) = " |"
      if(j == 1 .or. j == my) then
        do i =1 , mx
          z(i) = "--"
        end do
      end if
      write(*,601) (z(i), i = 1, mx)
    end do
    601 format(80a2)

    write(*,"(a)") "input any number (1)"
    read(*,*) cc

    do j = my, 1, -1
      do i = 1, mx
        index(i) = int((u(i, j) - umin) / (umax - umin) * 9.9999) * 11
      end do
      write(*,600) (index(i), i = 1, mx)
    end do
    600 format(80I2)

  end subroutine output



! ＜Ｃａｖｉｔｙ問題、ψ－ ω法　－－－非定常解＞

! 　このプログラムは(3.23),(3.24) を用いて非定常的にＣａｖｉｔｙ問題を解くプログラムである。プログラムは本文の手順をそのまま記述すればよい。配列の名前はプログラムPOCVS.FOR と同じである。プログラムのはじめの部分で計算に必要なパラメータを読み込んでいる。NMAXは時間ステップ数、REはレイノルズ数、CONST はＳＯＲ法の加速係数、EPS1はポアソン方程式の収束条件である。DO 10 のループは初期条件であり、初期に流れは静止しているとした。DO 100のループで流れを時間発展させる。DO 20、DO 30のループはωの境界条件であり、領域内のψの変化によりωは各時間ステップで変化するため、DO 100のループの内側におかれている。ψの境界条件はψ＝０で一定なのですでに初期条件で与えられており、改めて指定していない。DO 55 のループでψのポアソン方程式(3.23)を  ＳＯＲ法で解いている。DO 60 のループでPSI の値を更新している。そして残差の最大値がEPS より小さくなったとき収束したとみなしている。DO 40 のループで(3.24)を用いてωを時間発展させている。
!   定常になるまでの途中の結果を出力すれば、流れが静止状態から時間的に変化する様子を調べることができる。出力ルーチンはプログラム LAP.FORと同じである。

! 　メインプログラムの仕様

! 　　配列　PSI ----  流れ関数ψを記憶する配列
! 　　配列　OMG ----  渦度ωを記憶する配列
! 　　配列　TMP ----  流れ関数や渦度ωを一時的に記憶する配列
! 　　変数　MX,MY --  プログラムの配列の大きさを指定するパラメータ
! 　　変数　NA  ----  格子数（Ｘ、Ｙ方向同数）
! 　　変数　NN  ----  格子点数（Ｘ、Ｙ方向同数）
! 　　変数　DT  ----  時間間隔
! 　　変数　RE  ----  レイノルズ数
! 　　変数　NMAX ---  計算打ち切りのタイムステップ数
! 　　変数　CONST --  流れ関数に関するポアソン方程式の近似差分方程式を
! 　　　　　　　　　　ＳＯＲ法で解くときの加速係数
! 　　変数　EPS  ---  ＳＯＲ法の許容最大誤差（収束判定条件）
! 　　変数　H   ----  格子幅
! 　　変数　HI   ---  １／（格子幅）
! 　　変数　HH   ---  H の二乗
! 　　変数　BB  ----  ある格子点での流れ関数の誤差
! 　　変数　ERR2 ---  全格子点での流れ関数の最大誤差（BBの最大値）
! 　　変数　ISAVE --  １のとき計算結果をファイルに記録する。それ以外の
! 　　　　　　　　　　ときは記録しない。

! 　サブルーチン　OUT の仕様（ラプラス方程式の解法(LAP.FOR)と同じ）