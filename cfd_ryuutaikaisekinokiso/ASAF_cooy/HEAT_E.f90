! ==================================================
! diffusion equation euler explicit method
! ==================================================
program main
  implicit none
  integer, parameter :: NX = 51
  double precision u(NX), uu(NX)
  integer KX, KM, MX
  integer i, j, k
  double precision DX, R, X
  integer IH, I5
  double precision :: dt

! input & calculate parameters
  KX =20
  KM = 250
  dt = 0.001
  write(*,"(a)") "input number of mesh. (20)"
  ! read(*,*) KX
  MX = KX + 1
  write(*,"(a)") "input number of time steps. (250)"
  ! read(*,*) KM
  write(*,"(a)") "input time increment delta t. (0.001)"
  ! read(*,*) dt

  dx = 1.0/dble(MX - 1)
  R = dt / dx ** 2
  IH = (MX + 1) / 2
  I5 = 5 / dt / 100
  if(I5 == 0) then
    I5 = 1
  end if
  write(*,*) dt
  write(*,*) I5
  call output(U, NX, MX, 1)
  call output_main(u, nx, mx, dx, 1)

! initial condition
  do i = 1, MX
    X = dble(i - 1) / dble(MX - 1)
    if(i <= IH) then
      U(i) = X
    else
      U(i) = 1.0 - X
    end if
  end do

! main loop
  do k = 1, KM
    U(1) = 0.0
    U(MX) = 0.0
    if(mod(k, I5) == 1) then
      call output(U, NX, MX, 2)
      call output_main(u, nx, mx, dx, 2)
    end if
    do i = 2, MX - 1
      UU(i) = R * U(i - 1) + (1.0 - 2 * R) * U(i) + R * U(i + 1)
    end do
    do i = 2, MX - 1
      U(i) = UU(I)
    end do
    if(abs(U(IH)) >= 10000) then
      write(*,*) "diverge!"
      stop
    end if
  end do
  
  call output(U, NX, MX, 3)
  write(*,"(a)") "mine"
end program main


subroutine output_main(u, nx, mx, dx, mm)
  implicit none
  integer nx, mx, mm
  double precision dx
  double precision, intent(in) :: u(nx)
  character*2 z(mx, 23)
  integer i, j
  if(MM == 1) then
    do j = 1, 23
      do i = 2, (MX - 1)
        z(i, j) = " "
      end do
    end do
    do j = 1, 23
      z(1, j) = ":"
      z(MX, j) = ":"
    end do
    do i = 1, MX
      z(i, 1) = "-"
      z(i, 23) = "-"
    end do
  else if(MM == 2) then
    do i = 1, MX
      j = u(i) * 40 + 0.001
      if(j > 23 .or. j <= 0) then
        go to 40
      end if
      z(i, j) = "*"
      40 continue
    end do
  else
    do j = 23, 1, -1
      write(*,*) (z(i, j), i = 1, 60)
    end do
  end if
end subroutine output_main

subroutine output(u, NX, MX, MM)
  implicit none
  integer NX, MX, MM
  integer i, j
  character :: z(60, 23)
  double precision, intent(in) :: u(NX)

  if(MM == 1) then
    do j = 1, 23
      do i = 2, (MX - 1)
        z(i,j) = " "
      end do
    end do
    do j = 1, 23
      z(1, j) = ":"
      z(MX, j) = ":"
    end do
    do i = 1, MX
      z(i, 1) = "-"
      z(i, 23) = "-"
    end do
  else if(MM == 2) then
    do i = 1, MX
      j = u(i) * 40 + 0.001
      if(j > 23 .or. j <= 0) then
        go to 40
      end if
      z(i, j) = "*"
    40 end do
  else
    do j = 23, 1, -1
      write(*,600) (z(i, j), i = 1, 60)
    end do
  end if
600 format(60a1)

end subroutine output
