import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)

x_list = []
temperature_list = []


with open("output.dat","rt") as f:
    for line in f:
        data = line[:-1].split(" ")
        x_list.append(float(data[0]))
        temperature_list.append(float(data[1]))


ax.plot(x_list,temperature_list,label="temperature")

titlename = "1d_heat_conduction_equation"
ax.set_title(titlename)

ax.set_xlabel("x")
ax.set_ylabel("temperature")

ax.legend()

filename = "1d_heat_conduction_equation" + ".pdf"
fig.savefig(filename)

plt.show()
