#include <iostream>
#include "pfem_util.hpp"
#define GLOBAL_VALUE_DEFINE
// #include "pfem_util.hpp"
// #include "allocate.hpp"

int main(){
    // double *X;
    // X = (double*)allocate_vector(sizeof(double),10);
    // for(int i=0;i<10;i++){
    //     X[i] = i;
    // }
    // for(int i=0;i<10;i++){
    //     std::cout << X[i] << std::endl;
    // }

    double *U;
    U = (double*)calloc(10,sizeof(double));
    if(U == NULL){
        std::cout << "can not secure memory" << std::endl;
    }
    for(int i=0;i<10;i++){
        U[i] = i;
    }
    for(int i=0;i<10;i++){
        std::cout << U[i] << " ";
    }
    std::cout << std::endl;

    double **XY;
    int row = 5;
    int col = 3;
    XY = (double**)calloc(row,sizeof(double));
    for(int i=0;i<row;i++){
        XY[i] = (double*)calloc(col,sizeof(double));
    }
    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
            XY[i][j] = i + j;
        }
    }
    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
            std::cout << XY[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // deallocate_vector(X);
    free(U);

    for(int i=0;i<row;i++){
        free(XY[i]);
    }
    free(XY);


    return 0;

}



