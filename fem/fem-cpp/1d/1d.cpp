#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include "allocate.h"

int main(){
    int NE; // 要素数
    int N; // 節点数
    int NPLU; // 非零非対角成分数
    int IterMax; // 最大反復回数
    int R; // CGベクトル名
    int Z; // CGベクトル名
    int Q; // CGベクトル名
    int P; // CGベクトル名
    int DD; // CGベクトル数
    int iter;

    double dX; // 要素長さ
    double Resid; // CG法残債
    double Eps; // CG法反復打切り誤差
    double Area; // 要素断面積
    double QV; // 体積当たり発熱量Q
    double COND; // 熱伝導率
    double X1; // 節点座標
    double X2; // 節点座標
    double U1; // 節点温度
    double U2; // 節点温度
    double Strain;
    double Sigma;
    double Ck; // Area*COND/DL
    double QN; // 0.5*QV*Area*dX
    double XL;
    double C2;
    double Xi;
    double PHIa;

    // std::vector<std::vector<double>> W(4,std::vector<double>(N));
    // とりあえず要素数を指定せず宣言
    std::vector<double> PHI; // 温度
    std::vector<double> Rhs; // 右辺ベクトル
    std::vector<double> X; // 節点座標
    std::vector<double> Diag; // 全体マトリクスの対角成分
    std::vector<double> AMat; // 係数マトリクス非零非対角成分要素番号用一次元圧縮配列（非対角成分）
    std::vector<std::vector<double>> W; // CG法のwork配列
    std::vector<int> Index; // 係数マトリクス非零非対角成分要素番号一次元圧縮配列（非対角成分数）
    std::vector<int> Item; // 係数マトリクス非零非対角成分要素番号一次元圧縮配列（非零非対角成分要素（列番号））
    std::vector<int> Icelnod; // 各要素節点番号

    std::vector<std::vector<double>> Kmat(2,std::vector<double>(2)); // 要素マトリクス
    std::vector<std::vector<double>> Emat(2,std::vector<double>(2)); // 要素マトリクス

    std::string filename_reading = "input.dat";
    std::ifstream ifs(filename_reading);
    if(!ifs){
        std::cout << "==================can't open the file.==============" << std::endl;
        std::exit(1);
    }
    ifs >> NE;
    ifs >> QV >> Area >> COND;
    ifs >> IterMax;
    ifs >> Eps;
    ifs.close();

    N = NE + 1;
    dX = 1.0 / NE;

    PHI.resize(N);
    X.resize(N);
    Diag.resize(N);
    AMat.resize(2 * N - 2);
    Rhs.resize(N);
    Index.resize(N + 1);
    Item.resize(2 * N - 2);
    Icelnod.resize(2 * NE);
    
    W.resize(4);
    for(int i = 0;i < 4;i++){
        W[i].resize(N);
    }

    for(int i = 0;i < N;i++){
        X[i] = i * dX;
    }
    for(int i = 0;i < NE;i++){
        Icelnod[2 * i] = i;
        Icelnod[2 * i + 1] = i + 1;
    }
    for(int i = 0;i < 2 * NE;i++){
        std::cout << "Icelnod[" << i << "]=" << Icelnod[i] << std::endl;
    }

    Kmat[0][0] = 1.0;
    Kmat[0][1] = -1.0;
    Kmat[1][0] = -1.0;
    Kmat[1][1] = 1.0;

    for(int i=0;i<N+1;i++){
        Index[i] = 2;
    }
    Index[0] = 0;
    Index[1] = 1;
    Index[N] = 1;
    for(int i=0;i<N;i++){
        Index[i + 1] = Index[i + 1] + Index[i];
    }
    for(int i=0;i<N+1;i++){
        std::cout << "Index[" << i << "]=" << Index[i] << std::endl;
    }

    NPLU = Index[N]; // 非零非対角成分の総数

    for(int i=0;i<N;i++){
        int jS = Index[i];
        if(i == 0){
            Item[jS] = i + 1;
        }else if(i == N - 1){
            Item[jS] = i - 1;
        }else{
            Item[jS] = i - 1;
            Item[jS + 1] = i + 1;
        }
    }
    for(int i=0;i<2 * N - 2;i++){
        std::cout << "Item[" << i << "]=" << Item[i] << std::endl;
    }

    int in1;
    int in2;
    double DL;
    int k1;
    int k2;
    for(int i=0;i<NE;i++){
        in1 = Icelnod[2 * i];
        in2 = Icelnod[2 * i + 1];
        X1 = X[in1];
        X2 = X[in2];
        DL = fabs(X2 - X1);
        Ck = Area * COND / DL;
        Emat[0][0] = Ck * Kmat[0][0];
        Emat[0][1] = Ck * Kmat[0][1];
        Emat[1][0] = Ck * Kmat[1][0];
        Emat[1][1] = Ck * Kmat[1][1];

        Diag[in1] = Diag[in1] + Emat[0][0];
        Diag[in2] = Diag[in2] + Emat[1][1];

        if(i == 0){
            k1 = Index[in1];
        }else{
            k1 = Index[in1] + 1;
        }
        int k2 = Index[in2];

        AMat[k1] = AMat[k1] + Emat[0][1];
        AMat[k2] = AMat[k2] + Emat[1][0];

        QN = 0.5 * QV * Area * dX;
        Rhs[in1] = Rhs[in1] + QN;
        Rhs[in2] = Rhs[in2] + QN;
    }

    for(int i=0;i<N;i++){
        std::cout << "Diag[" << i << "]=" << Diag[i] << std::endl;
    }
    for(int i=0;i<N;i++){
        std::cout << "AMat[" << i << "]=" << AMat[i] << std::endl;
    }

    // Boundary Condition
    // x = xmin
    int xmin = 0;
    int jS = Index[xmin];
    AMat[jS] = 0.0;
    Diag[xmin] = 1.0;
    Rhs[xmin] = 0.0;

    std::cout << "==============After Boundary Condition================" << std::endl;
    std::cout << "NPLU=" << NPLU << std::endl;
    for(int i=0;i<NPLU;i++){
        if(Item[i] == 0){
            // Rhs[i] = Rhs[i] - 
            AMat[i] = 0.0;
        }
    }
    for(int i=0;i<N;i++){
        std::cout << "Diag[" << i << "]=" << Diag[i] << std::endl;
    }
    for(int i=0;i<2 * N - 2;i++){
        std::cout << "AMat[" << i << "]=" << AMat[i] << std::endl;
    }

    R = 0;
    Z = 1;
    Q = 1;
    P = 2;
    DD = 3;

    for(int i=0;i<N;i++){
        W[DD][i] = 1.0 / Diag[i];
    }

    for(int i=0;i<N;i++){
        W[R][i] = Diag[i] * PHI[i];
        for(int j=Index[i];j<Index[i + 1];j++){
            W[R][i] += AMat[j] * PHI[Item[j]];
        }
    }
    double BNorm2 = 0.0;
    for(int i=0;i<N;i++){
        BNorm2 += Rhs[i] * Rhs[i];
        W[R][i] = Rhs[i] - W[R][i];
    }

    std::cout << "stop" << std::endl;

    double Rho1 = 0.0;
    for(int k=1;k<=IterMax;k++){
        for(int i=0;i<N;i++){
            W[Z][i] = W[DD][i] * W[R][i];
        }
       double Rho = 0.0;
       for(int i=0;i<N;i++){
           Rho += W[R][i] * W[Z][i];
       }
       if(k == 1){
           for(int i=0;i<N;i++){
               W[P][i] = W[Z][i];
           }
       }else{
           double Beta = Rho / Rho1;
           for(int i=0;i<N;i++){
               W[P][i] = W[Z][i] + Beta * W[P][i];
           }
       }
       for(int i=0;i<N;i++){
           W[Q][i] = Diag[i] * W[P][i];
           for(int j=Index[i];j<Index[i + 1];j++){
               W[Q][i] += AMat[j] * W[P][Item[j]];
           }
       }
       double C1 = 0.0;
       for(int i=0;i<N;i++){
           C1 += W[P][i] * W[Q][i];
       }
       double Alpha = Rho / C1;

       for(int i=0;i<N;i++){
           PHI[i] += Alpha * W[P][i];
           W[R][i] -= Alpha * W[Q][i];
       }

       double DNorm2 = 0.0;
       for(int i=0;i<N;i++){
           DNorm2 += W[R][i] * W[R][i];
       }
       double Resid = sqrt(DNorm2/BNorm2);

       if((k)%1000 == 0){
           std::cout << k << " iters,RESID=" << Resid << std::endl;
       }
       
       iter = k;

        if(Resid <= Eps){
            double ierr = 0;
            break;
        }

        Rho1 = Rho;
    }

    std::cout << iter << " iters,RESID=" << Resid << std::endl;
    std::cout << std::endl;
    std::cout << "### TEMPERATURE" << std::endl;
    XL = NE * dX;
    C2 = QV * XL;

    std::string filename_writing = "output.dat";
    std::ofstream ofs(filename_writing);
    if(!ifs){
        std::cout << "==================can't open the output.dat.==============" << std::endl;
        std::exit(1);
    }
    for(int i=0;i<N;i++){
        ofs << X[i] << " " << PHI[i] << std::endl;
    }

    // for(int i=0;i<N;i++){
    //     Xi = X[i];
    //     PHIa = (-0.5 * QV * Xi * Xi + C2 * Xi) / COND;
    //     std::cout << i + 1 << " " << PHI[i] << " " << PHIa << std::endl;
    // }


    return 0;
}