!C
!C***
!C*** pfem_util
!C***
      module pfem_util
      include 'precision.inc'
!C
!C +--------------+
!C | file settings |
!C +--------------+
!C===
      character(len= 80) ::  fname ! メッシュファイル名
!C===

!C
!C +-------------------------+
!C | DISTRIBUTED MESH FILE's |
!C +-------------------------+
!C===

!C
!C-- CONNECTIVITIES & BOUNDARY nodes
      integer:: ICELTOT ! 総要素数
      integer:: NODGRPtot ! 節点グループ数

      real(kind=kreal)  , dimension(:,:),allocatable :: XYZ ! 節点座標
      integer(kind=kint), dimension(:,:),allocatable :: iCELNOD ! 要素コネクティビティ

      integer(kind=kint), dimension(:),  allocatable :: NODGRP_INDEX ! 各節点グループに含まれる節点数の配列
      integer(kind=kint), dimension(:),  allocatable :: NODGRP_ITEM ! 節点グループに含まれる節点
      character(len=80), dimension(:),  allocatable :: NODGRP_NAME ! 節点グループ名
!C===

!C
!C +-----------------+
!C | MATRIX & SOLVER |
!C +-----------------+
!C===

!C
!C-- MATRIX SCALARs
      integer(kind=kint) :: N ! 節点数
      integer(kind=kint) :: NP ! 節点数
      integer(kind=kint) :: N2
      integer(kind=kint) :: NLU ! 各節点非大悪成分数
      integer(kind=kint) :: NPLU ! 非対角成分総数

!C
!C-- MATRIX arrays
      real(kind=kreal), dimension(:), allocatable :: D ! 全体行列の対角ブロック
      real(kind=kreal), dimension(:), allocatable :: B ! 右辺ベクトル、未知ベクトル
      real(kind=kreal), dimension(:), allocatable :: X ! 右辺ベクトル、未知ベクトル
      real(kind=kreal), dimension(:), allocatable :: AMAT ! 全体行列の非零非対角成分

      integer(kind=kint), dimension(:), allocatable :: index ! 全体行列の非零非対角成分数
      integer(kind=kint), dimension(:), allocatable :: item ! 全体行列の非零非対角成分（列番号）
      integer(kind=kint), dimension(:),  allocatable :: INLU ! 全体行列の非零非対角成分数
      integer(kind=kint), dimension(:,:),allocatable :: IALU ! 全体行列の非零非対角成分数（列番号）

      integer(kind=kint), dimension(:,:),allocatable :: IWKX ! ワーク用配列
!C
!C-- PARAMETER's for LINEAR SOLVER
      integer(kind=kint) :: ITER ! 反復回数の上限
      integer(kind=kint) :: ITERactual ! 実際の反復回数
      real   (kind=kreal) :: RESID ! 打切り誤差
      real   (kind=kint) :: SIGMA_DIAG
      real   (kind=kreal) :: SIGMA
!C===

!C
!C +-------------+
!C | PARAMETER's |
!C +-------------+
!C===

!C
!C-- GENERAL PARAMETER's
      integer(kind=kint ), dimension(100) :: pfemIarray ! 諸定数（整数）
      real   (kind=kreal), dimension(100) :: pfemRarray ! 諸定数（実数）

      real   (kind=kreal), parameter :: O8th= 0.125d0 ! 0.125
!C
!C-- PARAMETER's for FEM
      real(kind=kreal), dimension(2,2,8) :: PNQ ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2,2,8) :: PNE ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2,2,8) :: PNT ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2)     :: WEI ! 各ガウス積分点の座標
      real(kind=kreal), dimension(2)     :: POS ! 各ガウス積分点の重み係数
      integer(kind=kint), dimension(100) :: NCOL1 ! ソート用ワーク配列
      integer(kind=kint), dimension(100) :: NCOL2 ! ソート用ワーク配列

      real(kind=kreal), dimension(2,2,2,8) :: SHAPE ! 各ガウス積分点における形状関数
      real(kind=kreal), dimension(2,2,2,8) :: PNX ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2,2,2,8) :: PNY ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2,2,2,8) :: PNZ ! 各ガウス積分点における微分値
      real(kind=kreal), dimension(2,2,2  ) :: DETJ ! 各ガウス積分点におけるヤコビアン行列式

!C
!C-- PROBLEM PARAMETER's
      real(kind=kreal) :: COND ! 熱伝導率
      real(kind=kreal) :: QVOL ! 体積当たり発熱量係数
!C===
      end module pfem_util
