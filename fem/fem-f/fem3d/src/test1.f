! メインプログラム
      program heat3D

      use solver11
      use pfem_util

      implicit REAL*8(A-H,O-Z)
!C
!C +-------+
!C | INIT. |
!C +-------+
!C=== 
      call INPUT_CNTL
      call INPUT_GRID
!C===

!C
!C +---------------------+
!C | matrix connectivity |
!C +---------------------+
!C===
      call MAT_CON0
      call MAT_CON1
!C===
      
!C
!C +-----------------+
!C | MATRIX assemble |
!C +-----------------+
!C===
      call MAT_ASS_MAIN 
      call MAT_ASS_BC
!C===

!C
!C +--------+
!C | SOLVER |
!C +--------+
!C===
      call SOLVE11
!C===

!C
!C +--------+
!C | OUTPUT |
!C +--------+
!C===
      call OUTPUT_UCD
      do i= 1, N
        if (XYZ(i,1).eq.0.d0.and.XYZ(i,2).eq.0.d0
     &                      .and.XYZ(i,3).eq.0.d0) then
          write (*,'(i8,1pe16.6)') i, X(i)
        endif
      enddo

!C===

      end program heat3D
      

