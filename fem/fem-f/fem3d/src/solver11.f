      module SOLVER11

      contains
        subroutine SOLVE11

        use pfem_util
        use solver_CG

        implicit REAL*8 (A-H,O-Z)

        integer :: ERROR, ICFLAG
        character(len=char_length) :: BUF

        data ICFLAG/0/

!C
!C +------------+
!C | PARAMETERs |
!C +------------+
!C===
        ITER      = pfemIarray(1)
        RESID     = pfemRarray(1)
!C===

!C
!C +------------------+
!C | ITERATIVE solver |
!C +------------------+
!C===
        call CG                                                         &
     &     ( N, NPLU, D, AMAT, index, item, B, X, RESID, ITER, ERROR )

      ITERactual= ITER
!C===

      end subroutine SOLVE11
      end module SOLVER11
