!C
!C***
!C*** INPUT_CNTL
!C***
!C
      subroutine INPUT_CNTL
      use pfem_util

      implicit REAL*8 (A-H,O-Z)

        open (11,file= 'INPUT.DAT', status='unknown')
        read (11,'(a80)') fname
        read (11,*) ITER
        read (11,*) COND, QVOL
        read (11,*) RESID
        close (11)

      pfemIarray(1)= ITER
      pfemRarray(1)= RESID

      return
      end
