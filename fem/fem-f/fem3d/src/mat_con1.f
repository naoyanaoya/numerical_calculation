!C
!C***
!C*** MAT_CON1
!C***
!C
! index,itme生成
      subroutine MAT_CON1
      use pfem_util
      implicit REAL*8 (A-H,O-Z)

      allocate (index(0:N))
      index= 0

      do i= 1, N
        index(i)= index(i-1) + INLU(i)
      enddo

      NPLU= index(N)

      allocate (item(NPLU))

      do i= 1, N
        do k= 1, INLU(i)
               kk = k + index(i-1)
          item(kk)=      IALU(i,k)
        enddo
      enddo

      deallocate (INLU, IALU)

      end subroutine MAT_CON1
