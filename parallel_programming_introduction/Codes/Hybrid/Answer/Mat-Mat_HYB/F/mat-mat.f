       program main
       include 'mpif.h'
       include 'mat-mat.inc'
       common /mpienv/myid,numprocs

       integer DEBUG
       parameter (DEBUG=1)
       double precision EPS
       parameter (EPS=1.0e-18)

       double precision  A(NN, NN)
       double precision  B(NN, NN)
       double precision  C(NN, NN)

       double precision  t0, t1, t2, t_w
       double precision  d_mflops, dtemp
    

       integer  myid, numprocs
       integer  ierr
       integer  i, j
       integer  iflag, iflag_t
       integer  i_start, i_end, ib


       call MPI_INIT(ierr)
       call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
       call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)


c      === matrix generation --------------------------
       if (DEBUG .eq. 1) then
          do j=1, NN
            do i=1, NN
              A(i, j) = 1.0;
              B(i, j) = 1.0;
              C(i, j) = 0.0;
            enddo
          enddo
       else 
          call RANDOM_SEED
          do j=1, NN
            do i=1, NN
              call RANDOM_NUMBER(dtemp)   
              A(i, j) = dtemp
              call RANDOM_NUMBER(dtemp)   
              B(i, j) = dtemp
              C(i, j) = 0.0
            enddo
          enddo
       endif
c      === end of matrix generation ------------------------

c      === Start of mat-vec routine ----------------------------
       call  MPI_BARRIER(MPI_COMM_WORLD, ierr)
       t1 = MPI_Wtime(ierr)

       call MyMatMat(C, A, B, NN)

       call MPI_BARRIER(MPI_COMM_WORLD, ierr)
       t2 = MPI_Wtime(ierr)
       t0 =  t2 - t1 
       call MPI_REDUCE(t0, t_w, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 
     &        0, MPI_COMM_WORLD, ierr)
c      === End of mat-vec routine ---------------------------

       if (myid .eq. 0) then

         print *, "NN  = ", NN
         print *, "Mat-Mat time[sec.] = ", t_w

         d_mflops = 2.0*dble(NN)*dble(NN)*dble(NN)/t_w
         d_mflops = d_mflops * 1.0e-6
         print *, "MFLOPS = ", d_mflops
       endif

       if (DEBUG .eq. 1) then
c      === Verification routine ----------------- 
         iflag = 0

         ib = NN / numprocs
         i_start = 1 + myid*ib
         if (myid .ne. (numprocs-1)) then
           i_end = (myid+1)*ib
         else
           i_end = NN
         endif

         do j=1, NN
           do i=i_start, i_end 
             if (dabs(C(i,j) - dble(NN)) > EPS) then
               print *, " Error! in (", i, ",", j, ") th argument."
               iflag = 1
               goto 10
             endif 
           enddo
         enddo
 10      continue
c        -----------------------------------------

         call MPI_REDUCE(iflag, iflag_t, 1, MPI_INTEGER, 
     &         MPI_SUM, 0, MPI_COMM_WORLD, ierr)

         if (myid .eq. 0) then
           if (iflag_t .eq. 0) then
             print *, " OK!"
           endif
         endif

       endif
c      -----------------------------------------

       call MPI_FINALIZE(ierr)

       stop
       end



       subroutine MyMatMat(C ,A, B, n)
       include 'mpif.h'
       include 'mat-mat.inc'
       common /mpienv/myid,numprocs

       double precision C(NN, NN)
       double precision A(NN, NN)
       double precision B(NN, NN)
       integer n

       integer  i, j, k
       integer  i_start, i_end, ib

       ib = n / numprocs
       i_start = 1 + myid*ib
       if (myid .ne. (numprocs-1)) then
         i_end = (myid+1)*ib
       else
         i_end = n
       endif

!$omp parallel do private(j,k) 
       do i=i_start, i_end
         do j=1, n
           do k=1, n 
             C(i, j) = C(i, j) + A(i, k) * B(k, j) 
           enddo
         enddo
       enddo
!$omp end parallel do

      return
      end
