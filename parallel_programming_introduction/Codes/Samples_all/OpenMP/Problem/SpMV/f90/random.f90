program random_seed_example
  implicit none
  integer,parameter :: n = 5
  real :: x
  integer i, seedsize
  integer,allocatable:: seed(:)

  write(*,*) "seedsize is not", seedsize, "?"
  call random_seed(size = seedsize)  ! シードの格納に必要なサイズを取得する
  allocate(seed(seedsize))         ! シード格納領域を確保
  call random_seed(get = seed)       ! 次回同じ乱数を発生できるように

  print *, "Size of seed array is", seedsize

  print *, "1st try..."
  do i = 1, n
    call random_number(x)
    print *, i, x
  end do

  call random_seed(put=seed)       ! 前回と同じシードを設定

  print *, "2nd try..."
  do i = 1, n
    call random_number(x)
    print *, i, x
  end do

end program random_seed_example