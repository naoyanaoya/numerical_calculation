!      == number of maximum dimension of matrix
       integer  NN
       parameter (NN=5)

!      == number of maximum number of non-zero elements
       integer  NNNZ
       parameter (NNNZ=20)

!      == number of number of non-zero elements per low
       integer  NZPR
       parameter (NZPR=3)

