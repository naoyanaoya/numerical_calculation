program main
!$ use omp_lib
  include "spmv.inc"

  integer, parameter :: DEBUG = 1
  integer, parameter :: MATRIX_VISUALIZE_FLAG = 1
  double precision :: EPS = 1.0e-8
  integer, parameter :: MAX_ITER = 100

  double precision, allocatable, dimension(:) :: x, y , val
  double precision t0, t1, t2, t_w
  double precision d_mflops, dtemp

  integer, allocatable, dimension(:) :: irp, icol, iseed, icol_list
  integer i, j, k, n, kk
  integer iflag, iflag_t

! === NN is number of maximum dimension of matrix ===
! === NNNZ is number of maximum number of non_zero elements ===
! === NZPR is number of number of non-zero elements per low ===
  allocate(irp(NN + 1))
  allocate(icol(NNNZ))
  allocate(x(NN))
  allocate(y(NN))
  allocate(val(NNNZ))
  allocate(icol_list(NN))

! === sparse matrix generation ===
  call random_seed (size = i)
  allocate(iseed(i))
  iseed = 1

  call random_seed(put = iseed(1:i))

  k = 1
  irp(1) = 1
  do i = 1, NN
    do j = 1, NN
      icol_list(j) = -1
    end do


    ! if(MATRIX_VISUALIZE_FLAG == 1) then
    !   write(*,fmt="(a)") "icol = "
    !   do k = 1, NN * NZPR
    !     write(*,fmt="(i3,a)",advance="no") icol(i), " "
    !   end do
    !   write(*,*)
    ! end if

    do j = 1, NZPR
1     continue
      call random_number(dtemp)
      kk = int(dble(NN) * dtemp)
      if (kk == 0) kk = 1
      if (icol_list(kk) /= -1 ) then
        goto 1
      else
        icol_list(kk) = 1
      end if
    end do
      do j = 1, NN 
        if(icol_list(j) == 1) then
          icol(k) = j
          val(k) = 1.0d0
          k = k + 1
        end if
      end do
      irp(i + 1) = k
      x(i) = 1.0d0
      y(i) = 0.0d0
    end do
! === end of sparse matrix generation ===



    
  if(MATRIX_VISUALIZE_FLAG == 1) then
    write(*,fmt="(a)") "val = "
    do i = 1, NN * NZPR
      write(*,fmt="(f5.3,a)",advance="no") val(i), " "
    end do
    write(*,*)
    write(*,fmt="(a)") "icol = "
    do i = 1, NN * NZPR
      write(*,fmt="(i3,a)",advance="no") icol(i), " "
    end do
    write(*,*)
    write(*,fmt="(a)") "icol_list = "
    do i = 1, NN
      write(*,fmt="(i3,a)",advance="no")  icol_list(i), " "
    end do
    write(*,*)
    write(*,fmt="(a)") "irp = "
    do i = 1, NN
      write(*,fmt="(i3,a)",advance="no")  irp(i), " "
    end do
    write(*,*)

    ! write(*,fmt="(a)") "c = "
    ! do i = 1, NN
    !   do j = 1, NN
    !     write(*,fmt='(f6.3)',advance="no") c(i, j)
    !   end do
    !   write(*,*)
    ! end do
  end if



! === start of mat-vet routine ===
    t1 = omp_get_wtime()

    do i = 1, MAX_ITER
      ! call MySpMV(y, irp, icol, val, x, NN, NNNZ)
      call MySpMV(y, irp, icol, val, x)
    end do

    t2 = omp_get_wtime()
    t_w = t2 - t1
! === end of mat-vec routine ===

    write(*,*) "NN = ", NN
    write(*,*) "NNNZ = ", NN * NZPR
    write(*,*) "NZPR = ", NZPR
    write(*,*) "MAX_ITER = ", MAX_ITER

    write(*,*) "SpMV time [sec.] = ", t_w

    d_mflops = (2.0*dble(NZPR*NN)*dble(MAX_ITER)) /t_w
    d_mflops = d_mflops * 1.0e-6
    write(*,*) "MFLOPS = ", d_mflops

  if(MATRIX_VISUALIZE_FLAG == 1) then
    write(*,fmt="(a)") "y = "
    do i = 1, NN
      write(*,fmt="(f5.3,a)",advance="no") y(i), " "
    end do
    write(*,*)
  end if

    if(DEBUG == 1) then
! === verification routine ===
      iflag = 0
      do i = 1, NN
        if(EPS < dabs(y(i) - dble(NZPR))) then
          write(*,*) " Error! in (", i, ") th argument.", y(i)
          iflag = 1
          go to 10
        end if
      end do
10    continue
! ===============================

    if (iflag == 0) then
      write(*,*) "OK!"
    end if

  end if

! ===============================

  deallocate(irp)
  deallocate(icol)
  deallocate(x)
  deallocate(y)
  deallocate(val)
  deallocate(iseed)
  deallocate(icol_list)
end program

  ! subroutine MySpMV(y, irp, icol, val, x, n, nnz)
  subroutine MySpMV(y, irp, icol, val, x)
    !$ use omp_lib
    include "spmv.inc"

    integer irp(NN + 1)
    integer icol(NNNZ)
    double precision x(NN)
    double precision y(NN)
    double precision val(NNNZ)

    ! integer n, nnz

    double precision s
    integer i, j_ptr

!$omp parallel do private(i, s, j_ptr) num_threads(8)
    do i = 1, NN
      s = 0.0d0
      do j_ptr = irp(i), irp(i + 1) - 1
        s = s + val(j_ptr) * x(icol(j_ptr))
      end do
      y(i) = s
    end do
!$omp end parallel do 

  end subroutine MySpMV