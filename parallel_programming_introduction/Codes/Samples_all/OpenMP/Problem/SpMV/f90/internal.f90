program internal
  implicit none
  real(4) :: x, y, z
  x = 0.0
  y = 0.0
  z = 0.0
  call sub(x)
  write(*,*) "x = ", x, " y = ", y, " z = ", z

  contains
    subroutine sub(x)
      real(4) :: x
      real(4) :: z = 1.0
      y = y + z
      x = y + z
    end subroutine sub
end program internal