program main
!$ use omp_lib
  implicit none
  integer i
!$ integer use_thread, max_threads
!$ double precision dts, dte
!$ dts = omp_get_wtime()
!$ max_threads = omp_get_max_threads()
!$ print *, 'we can use threads number is ', max_threads
 
!$omp parallel num_threads(8)
 
!$omp do
 
!$ use_thread = omp_get_thread_num()
    print *, 'Using thread number is ', use_thread
 
!$omp end do
 
!$omp end parallel
 
!$ dte = omp_get_wtime()
!$ print *, 'Elapse time [sec.] = ', dte - dts
end program