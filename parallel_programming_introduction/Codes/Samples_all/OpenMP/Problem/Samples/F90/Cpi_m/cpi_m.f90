!********************************************
! pi.c - conpute pi by integrating f(x) = 4 / (1 + x ** 2)
!
! Each node:
!  1) receivves the number of rectangles used in the approximation.
!  2) calculates the areas of it's rectangles.
!  3) Synchronizes for a global summation.
! Node 0 prints the result.
!
! Variables:
!  pi the calculated result
!  n number of points of integration.
!  x midpoint of each rectagle's interval
!  f function to integrate
!  sum,pi area of rectangles
!  tmp temporary scratch space for global summaiton
!  i do loop index
!*********************************************

program main
  !$ use omp_lib

  double precision, parameter :: PI25DT = 3.141592653589793238462643
  double precision pi, h, sum, x
  !$ double precision dts, dte
  integer n, i

  write(*, "(a)") "Enter the number of intervals"
  read(*,*) n
  write(*,*) "n = ", n

! === calculate the interval size ===
  if (n <= 0) then
    stop
  end if

  !$ dts = omp_get_wtime()

! === calculate the interval size ===
  h = 1.0 / dble(n)

  sum = 0.0

!$omp parallel private(x) num_threads(8)
!$omp do reduction(+:sum)
  do i = 1, n
    x = h * (dble(i) - 0.5)
    sum = sum + 4.0 / (1.0 + x * x)
  end do
!$omp end do
!$omp end parallel

  pi = h * sum

  !$ dte = omp_get_wtime()

! === prints the answer ===
  write(*, *) "pi is approximately:", pi, "Erroris:", dabs(pi - PI25DT)
  !$ write(*, *) "time = ", dte - dts, "[sec.]"

end program