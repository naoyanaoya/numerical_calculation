!********************************************
! pi.c - conpute pi by integrating f(x) = 4 / (1 + x ** 2)
!
! Each node:
!  1) receivves the number of rectangles used in the approximation.
!  2) calculates the areas of it's rectangles.
!  3) Synchronizes for a global summation.
! Node 0 prints the result.
!
! Variables:
!  pi the calculated result
!  n number of points of integration.
!  x midpoint of each rectagle's interval
!  f function to integrate
!  sum,pi area of rectangles
!  tmp temporary scratch space for global summaiton
!  i do loop index
!*********************************************

program main
!$ use omp_lib
  implicit none
  integer i

  double precision, parameter :: PI25DT = 3.141592653589793238462643
  double precision pi, h, sum, x
  integer n

!$ double precision dts, dte
!$ dts = omp_get_wtime()

  write(*, "(a)") "Enter the number of intervals"
  read(*,*) n
  write(*,*) "n = ", n

! === check for quit signal ===
  if ( n <= 0 ) then
    write(*, "(a)") "Please enter the integer number, it is bigger than 0"
    stop
  end if

! === calculate the interval size ===
  h = 1.0 / dble(n)
  sum = 0.0

!$omp parallel private(x)
!$omp do reduction(+:sum)
  do i = 1, n
    x = h * (dble(i) - 0.5)
    sum = sum + 4.0 / (1.0 + x * x)
  end do
!$omp end do
!$omp end parallel

  pi = h * sum

! === prints the answer ===
  write(*,*) "pi is approximately:", pi, "Error is:", dabs(pi - PI25DT)

!$ dte = omp_get_wtime()
!$ print *, "Elapse time [sec.] = ", dte - dts

end program

! program main
! !$ use omp_lib
!   implicit none
!   integer i
 
! !$ integer use_thread, max_threads
! !$ double precision dts, dte
! !$ dts = omp_get_wtime()
! !$ max_threads = omp_get_max_threads()
! !$ print *, 'we can use threads number is ', max_threads
 
! !$omp parallel num_threads(8)
 
! !$omp do
 
! !$ use_thread = omp_get_thread_num()
!     print *, "Using thread number is ", use_thread
 
! !$omp end do
 
! !$omp end parallel
 
! !$ dte = omp_get_wtime()
! !$ print *, "Elapse time [sec.] = ", dte - dts
! end program