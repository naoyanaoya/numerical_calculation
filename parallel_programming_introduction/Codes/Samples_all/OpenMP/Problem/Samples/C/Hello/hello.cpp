#include <iostream>
#include "omp.h"

int main(int argc,
        char* argv[]
)
{
    int threads_num = omp_get_max_threads();
    std::cout << "===============All Threads number is " << threads_num << ".==================" << std::endl;
    
    #pragma omp parallel for
    for (int i=0; i<10; i++) {
    // printf("Hello parallel world!  i:%d \n", i);
        #pragma omp critical
        std::cout << "Hello parallel world! i=" << i << std::endl;
    }

}

