program main
!$ use omp_lib
  ! include "cg.inc"

  integer, parameter :: DEBUG = 1
  double precision, parameter :: EPS = 1.0e-8
  double precision, parameter :: EPS_CG = 1.0e-8
  integer, parameter :: MAX_ITER = 100

  double precision, allocatable, dimension(:) :: x, b, val
  double precision, allocatable, dimension(:) :: r, p, ax, ap, adiag
  double precision t0, t1, t2, t_w
  double precision d_mflops, dtemp
  double precision alpha, beta, rdot, rdot_old, pAp
  double precision xnorm, bnorm

  integer, allocatable, dimension(:) :: irp, icol, iseed
  integer i, j, k, n
  integer iflag, iflat_t

  allocate(irp(NN + 1))
  allocate(icol(NNNZ))
  allocate(x(NN))
  allocate(b(NN))
  allocate(val(NNNZ))

  allocate(r(NN))
  allocate(p(NN))
  allocate(ax(NN))
  allocate(ap(NN))
  allocate(adiag(NN))

! === sparse matrix generation ==================
! === 疎行列作成 =================================
  irp(1) = 1
  val(1) = 4.0d0
  icol(1) = 1
  val(2) = -1.0d0
  icol(2) = 2
  irp(2) = 3

  k = 3
  do i = 2, NN - 1
    icol(k) = i - 1
    val(k) = -1.0d0
    icol(k + 1) = i 
    val(k + 1) = 4.0d0
    icol(k + 2) = i + 1 
    val(k + 2) = -1.0d0

    k = k + 3
    irp(i + 1) = k
  end do
    val(k) = -1.0d0
    icol(k) = NN - 1
    val(k + 1) = 4.0d0
    icol(k + 1) = NN 
    irp(NN + 1) = k + 2

! === set diag elements of A ===
    do i = 1, NN
      adiag(i) = 4.0d0
    end do
! === end of set diag elements of A ===

! === end of sparse matrix generation

! === make right hand vector b ===
    do i = 1, NN
      x(i) = 1.0d0
    end do
    call MySpMV(b, irp, icol, val, x, NN, NNNZ)
! === end of right hand vecotr b ===




