program main
!$ use omp_lib
  include "mat-mat.inc"

  integer, parameter :: DEBUG = 0
  integer, parameter :: MATRIX_VISUALIZE_FLAG = 1
  double precision, parameter :: EPS = 1.0e-18

  double precision, allocatable, dimension(:,:) :: a, b, c

  double precision t0, t1, t2, t_w
  double precision d_mflops, dtemp

  integer i, j, k, n
  integer iflag, iflag_t

  allocate(a(NN, NN))
  allocate(b(NN, NN))
  allocate(c(NN, NN))

! === matrix generatino ===

  if(DEBUG == 1) then
    do j = 1, NN
      do i = 1, NN
        a(i, j) = 1.0
        b(i, j) = 1.0
        c(i, j) = 0
      end do
    end do
  else
    call RANDOM_SEED
    do j = 1, NN
      do i = 1, NN
        call RANDOM_NUMBER(dtemp)
        a(i, j) = dtemp
        call RANDOM_NUMBER(dtemp)
        b(i, j) = dtemp
        c(i, j) = 0.0
      end do
    end do
  end if
! === end of matrix generation ===

! === start of mat_vec routine ===
  t1 = omp_get_wtime()

  call MyMatMat(c, a, b, NN)

  t2 = omp_get_wtime()
  t_w = t2 - t1
! === end of mat_vet routine ===

  if(MATRIX_VISUALIZE_FLAG == 1) then
    write(*,fmt="(a)") "a = "
    do i = 1, NN
      do j = 1, NN
        write(*,fmt='(f6.3)',advance="no") a(i, j)
      end do
      write(*,*)
    end do

    write(*,fmt="(a)") "b = "
    do i = 1, NN
      do j = 1, NN
        write(*,fmt='(f6.3)',advance="no") b(i, j)
      end do
      write(*,*)
    end do

    write(*,fmt="(a)") "c = "
    do i = 1, NN
      do j = 1, NN
        write(*,fmt='(f6.3)',advance="no") c(i, j)
      end do
      write(*,*)
    end do
  end if

  write(*,*) "NN = ", NN
  write(*,*) "Mat-Mat time[sec.] = ", t_w

  d_mflops = 2.0 * dble(NN) * dble(NN) * dble(NN) / t_w
  d_mflops = d_mflops * 1.0e-6
  write(*,*) "MFLOPS = ", d_mflops

  if(DEBUG == 1) then
! === verification routine ===
    iflag = 0
    do j = 1, NN
      do i = 1, NN
        ! write(*,fmt="(i4)",advance="no") i
        ! write(*,fmt="(i4)") j
        ! write(*,fmt='(f6.3)',advance="no") c(i, j)
        ! write(*,fmt='(f6.3)') dble(NN)
        if(EPS < dabs(c(i, j) - dble(NN))) then
          write(*,*) "Error! in (", i, ",", j, ") th argument."
          iflag = 1
          goto 10
        end if
      end do
    end do
10 continue

      if(iflag == 0) then
        write(*,*) "OK!"
      end if

    end if


    deallocate(a)
    deallocate(b)
    deallocate(c)

  end program

  subroutine MyMatMat(c, a, b, n)
    !$ use omp_lib
    include "mat-mat.inc"

    double precision c(NN, NN)
    double precision a(NN, NN)
    double precision b(NN, NN)
    integer n
    integer i, j, k

    !$omp parallel do private(i, j, k) num_threads(8)
    do i = 1, n
      do j = 1, n
        do k = 1, n
          c(i, j) = c(i , j) + a(i, k) * b(k, j)
        end do
      end do
    end do
    !$omp end parallel do
  end subroutine MyMatMat
