! ==============================================
!  Poisson equation with k = 1, f = sin(i * j)
! ==============================================

program main
  !$ use omp_lib
  include "poisson.inc"

  integer, parameter :: DEBUG = 1
  double precision, parameter :: EPS = 1.0e-4
  integer, parameter :: MAX_ITER = 10000
  integer, parameter :: MAX_HEAT = 100.0

  double precision, allocatable, dimension(:,:) :: u, u_old
  double precision t0, t1, t2, t_w
  double precision h, h_pow, dmax

  integer i, j
  integer ii, jj

  allocate(u(0:M + 1, 0:M + 1))
  allocate(u_old(0:M + 1, 0:M + 1))

  do j = 0, M + 1
    do i = 0, M + 1
      u(i, j) = 0.0d0
      u_old(i, j) = 0.0d0
    end do
  end do

! === set given temperature ===
  do j = 0, M + 1
    u(0, j) = MAX_HEAT
  end do

  do i = 1, M 
    u(i, 0) = MAX_HEAT - dble(i) / dble(M) * MAX_HEAT
    u(i, M + 1) = dble(i) / dble(M) * MAX_HEAT
  end do

  do j = 0, M + 1
    u(M + 1, j) = 0.0d0
  end do
! === end of setting given temperature ===

! === start of solving poisson equation ===
  t1 = omp_get_wtime()

  h = 1.0d0 / (dble(M + 1))
  h_pow = h * h

! === main loop ===
  do i = 1, MAX_ITER

! === perfoam explicit method ( Gauss-Seidel Method )
    call MyPoisson(u, h_pow, M)
! === compute maximum differences ===
    call CalcErr(u, u_old, M, dmax)

    if(mod(i, 100) == 0) then
      write(*,*) "iter = ", i, " dmax = ", dmax
    end if

    if(dmax < EPS) then
      write(*,*) i, "Iteration is converged in residual ", EPS
      goto 10
    end if

! === copy back to u_old
    do jj = 1, M 
      do ii = 1, M 
        u_old(ii, jj) = u(ii, jj)
      end do
    end do

  end do
! === End of main loop ===

  write(*,*) "Iteration is not converged within ", MAX_ITER, " times."

10 continue

  t2 = omp_get_wtime()
  t_w = t2 - t1

! === End of Solving Poisson equation ===

  write(*,*) "M = ", M 
  write(*,*) "MAX_ITER = ", MAX_ITER
  write(*,*) "time[sec.] = ", t_w

  deallocate(u)
  deallocate(u_old)

end program

  subroutine MyPoisson(u, h_pow, M)
    !$ use omp_lib
    implicit none
    double precision u(0:M + 1,0:M + 1)
    double precision h_pow
    integer M
    integer i, j
! === u_{i,j} = 1/4 (h^2 f_{i,j} + u_{i,j-1} +u_{i-1,j}+u_{i+1,j}+u_{i,j+1})
    do j = 1, M
      do i = 1, M
        u(i, j) = 0.25d0 * (h_pow * dsin(dble(i) * dble(j)) + u(i,j - 1) + u(i - 1,j) + u(i + 1,j) + u(i,j + 1))
      end do
    end do
  end subroutine

  subroutine CalcErr(u, u_old, M, dmax)
    !$ use omp_lib
    implicit none
    double precision u(0:M + 1, 0:M + 1)
    double precision u_old(0:M + 1, 0:M + 1)
    double precision dmax
    integer M

    integer i, j
    double precision dtemp

    dmax = 0.0
    do j = 1, M
      do i = 1, M
        dtemp = dabs(u(i,j) - u_old(i,j))
        if(dtemp .gt. dmax) dmax = dtemp
      end do
    end do
  end subroutine