#include <math.h>
// 複素数クラスの定義(宣言部:ヘッダファイル)
class Complex{
	private: // 非公開メンバ
		double realPart, imagPart;
	public: // 公開メンバ
		Complex(double re = 0, double im = 0)
			:realPart(re), imagPart(im) {}
		double Real()
			{return realPart;}
		double Imag()
			{return imagPart;}
		double Abs()
			{return sqrt(realPart * realPart + imagPart * imagPart); }
		double Arg()
			{return atan2(imagPart, realPart); }
		void Plus(Complex);
		void Minus(Complex);
		void Multiply(Complex);
		void Divide(Complex);
};
