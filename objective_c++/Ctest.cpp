#include <iostream>
#include "Complex.h"

int main()
{
	Complex a(2, 1);
	std::cout << "(" << a.Real() << " + " << a.Imag() << "i)" << std::endl;
	Complex b(5, 3);
	std::cout << "(" << b.Real() << " + " << b.Imag() << "i)" << std::endl;
	a.Multiply(b);
	std::cout << "(" << a.Real() << " + " << a.Imag() << "i)" << std::endl;
}
