#include "Complex.h" // クラスの宣言部をインクルード
#include <iostream>
// 複素数クラスの定義(実現部)
// オブジェクトの値を和、差、積、商で置き換える
void Complex::Plus(Complex z){
	realPart += z.realPart;
	imagPart += z.imagPart;
}
void Complex::Minus(Complex z){
	realPart -= z.realPart;
	imagPart -= z.imagPart;
}
void Complex::Multiply(Complex z){
	double re = realPart, im = imagPart;
	realPart = re * z.realPart - im * z.imagPart;
	imagPart = re * z.imagPart + im * z.realPart;
    std::cout << re << " " << im << std::endl;
    std::cout << z.realPart << " " << z.imagPart << std::endl;
    std::cout << realPart << " " << imagPart << std::endl;
}
void Complex::Divide(Complex z){
	double re = realPart, im = imagPart;
	double d = z.realPart * z.realPart + z.imagPart * z.imagPart;
	realPart = (re * z.realPart + im * z.imagPart) / d;
	imagPart = - (re * z.imagPart - im * z.realPart) / d;
}

