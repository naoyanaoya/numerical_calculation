import numpy as np
from scipy import sparse

data_np = np.array([[0, 0, 0, 2, 5],
                    [9, 0, 1, 8, 0],
                    [0, 0, 6, 0, 0],
                    [0, 4, 7, 0, 3]])

print("元のデータ配列")
print(data_np)


print("COO形式(座標形式)")
data_coo = sparse.coo_matrix(data_np)
print(data_coo)
print(f'row: {data_coo.row}')
print(f"col: {data_coo.col}")
print(f"data: {data_coo.data}")

print("CSR形式(compressed Row Storage)")
data_csr = sparse.csr_matrix(data_np)
print("行ごとに処理が行われることが特徴")
print(f"index pointer: {data_csr.indptr}")
print(f"indices: {data_csr.indices}")
print(f"data: {data_csr.data}")