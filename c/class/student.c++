#include<iostream>

// class Student {
//     public:
//         std::string name;
//         int         grade;
//         int         score;

//     public:
//         Student(){
//             name = "test";
//             grade = 10;
//             score =100;
//         }
//         void SetData(std::string name, int grade, int score) {
//             this->name = name;
//             this->grade = grade;
//             this->score = score;
//         }
// };

class OuterClass
{
    public: OuterClass()
    {
        puts("OuterClass() が呼ばれました。");
    }

    public: ~OuterClass()
    {
        puts("~OuterClass() が呼ばれました。");
    }

    class InnerClass
    {
        public: InnerClass()
        {
            puts("InnerClass() が呼ばれました。");
        }

        public: ~InnerClass()
        {
            puts("~InnerClass() が呼ばれました。");
        }
    };
};


int main()
{
    OuterClass::InnerClass *tInnerClass=new OuterClass::InnerClass();
    delete tInnerClass;
    // Student student;
    // std::cout << student.name << " " << student.grade << " " << student.score << "\n";
    // student.SetData("Saitou Hiroyuki", 2, 80);
    // std::cout << student.name << " " << student.grade << " " << student.score << "\n";
    return 0;
}
