#include <stdio.h>
#include "swap.h"

int main()
{
    int a, b;
    printf("第一数と第二数を交換します。\n");
    printf("第一数:");scanf("%d",&a);
    printf("第二数:");scanf("%d",&b);
    swap(&a,&b);
    printf("**********\n第一数:%d\n第二数:%d\n",a,b);
    return(0);
}