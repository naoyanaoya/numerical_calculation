// 自身で定義したヘッダーをインクルード
#include "Mammal.h"
// コンストラクタ
Mammal::Mammal(std::string tmp_name, int tmp_age){
    name = tmp_name;
    age = tmp_age;
}
// デストラクタ
Mammal::~Mammal(){
    
}
// メソッド
void Mammal::eat(){
    std::cout << name << " is eating" << std::endl;
}