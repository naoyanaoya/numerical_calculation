#include "Mammal.h"

int main() {
    // 別ファイルで定義したクラスを使って動かしてみる。
    Mammal *mammal;
    mammal = new Mammal("hopsii",10);
    mammal->eat();

    return 0;
}