#include <iostream>
#include <string>

class Mammal {
    private:
        std::string name;
        int age;

    public:
        Mammal(std::string tmp_name, int tmp_age);
        ~Mammal();
        void eat();
};