#include <stdio.h>
#include "calc.h"

/*
gcc main.c calc.c -o main
or
gcc -c main.c →これでオブジェクトファイルが出来る
gcc -c calc.c →これでオブジェクトファイルが出来る
gcc main.o calc.o -o main
*/


void main(){
    double d1, d2, d3;
    double a = 1.2, b = 3.4, c = 2.7;
    d1 = avg(a, b);
    d2 = avg(4.1, 5.7);
    d3 = avg(c, 2.8);
    printf("d1 = %f ,d2 = %f, d3 = %f\n", d1, d2, d3);
}