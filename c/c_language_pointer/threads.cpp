#include <iostream>
#include <thread>
using namespace std;

void func_a()
{
	for(int i = 0; i < 10000; i++)
		cout << 'a';
}

void func_b()
{
	for (int i = 0; i < 10000; i++)
		cout << 'b';
}

int main()
{
	//スレッド作成
	thread threadA(func_a);
	thread threadB(func_b);

	//スレッド終了待ち
	threadA.join();
	threadB.join();

	return 0;
}