#include <stdio.h>

int main(void)
{
    // int型の変数hoge
    int     hoge = 5;
    // int型の変数piyo
    int     piyo = 10;
    // 「int型へのポインタ」型の変数hoge_p
    /*
        「*hoge_p」という変数を宣言しているわけではない
        宣言している変数はあくまでhoge_pであり、
        その型が「intへのポインタ」型である
    */
    int     *hoge_p;

    /* それぞれの変数のアドレスを表示する */
    /*
        アドレス演算子&を用いてそれぞれの変数のアドレスを表示する
    */
    printf("&hoge..%p\n", (void*)&hoge);
    printf("&piyo..%p\n", (void*)&piyo);
    printf("&hoge_p..%p\n", (void*)&hoge_p);

    /* ポインタ変数hoge_pにhogeのアドレスを代入する */
    hoge_p = &hoge;
    printf("hoge_p..%p\n", (void*)hoge_p);

    /* hoge_pを経由してhogeの値を表示する */
    /*
        間接演算子*を用いて、hoge_pからポインタを一つ「たぐり寄せて」
        その値wお表示する
    */
    printf("*hoge_p..%d\n", *hoge_p);

    /* hoge_pを経由してhogeの値を変更する */
    *hoge_p = 10;
    printf("hoge..%d\n", hoge);

    hoge = 15;
    printf("hoge..%d\n", hoge);
    printf("*hoge_p..%d\n", *hoge_p);

    return 0;
}
