#include <stdio.h>

// 配列を内包する構造体定義
typedef struct
{
    long num[10]; // 配列10個
} S_PACK;

void func(S_PACK pack)
{
    // 配列[0]を書き換え
    pack.num[0] = 0x89ABCDEF;

    return;
}

int main(void)
{
    S_PACK data;

    // 配列[0]に値を格納
    data.num[0] = 0x01234567;

    func(data);

    // 結果変わらずは0x01234567
    printf("0x%08", data.num[0]);

    return 0;
}