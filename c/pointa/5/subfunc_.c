#include <stdio.h>

void subfunc(long * pnum, int size);

int main(void)
{
    long num[5] = {10, 20, 30, 40, 50};

    subfunc(num, sizeof(num) / sizeof(num[0]));

    return 0;
}

void subfunc(long * pnum, int size)
{
    int i;

    // 1:配列的参照
    // ポインタを配列的に参照するパターン
    for(i = 0; i < size; i++){
        printf("%d\n", pnum[i]);
    }

    // 2:ポインタ位置からの相対参照
    // ポインタの番地にインデックスを加え、ポインタの参照位置をずらすパターン
    for(i = 0; i < size; i++){
        printf("%d\n", *(pnum + i));
    }

    // 3:ポインタ位置をずらして参照
    // ポインタの参照位置を直接ずらしながら順番に参照していく
    for(i = 0; i < size; i++){
        printf("%d\n", *pnum);
        pnum++;
    }

    return;
}