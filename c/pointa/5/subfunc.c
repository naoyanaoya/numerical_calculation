#include <stdio.h>

void subfunc(char *pMoji)
{
    *pMoji = 'C';

    return;
}

int main(void)
{
    char moji[2] = {'A', 'B'};

    char * pMoji = moji;

    subfunc(moji); // 配列渡し

    printf("%c\n", moji[0]);

    pMoji[0] = 'D';
    pMoji[1] = 'E';

    printf("%c\n", moji[0]);
    printf("%c\n", moji[1]);
    
    return 0;
}