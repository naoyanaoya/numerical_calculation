#include <stdio.h>

typedef struct 
{
    double lon; // 軽度
    double lat; // 緯度
} S_Coordinate;

int main(void)
{
    // ポインタへの照準設定 pPos --> pos
    S_Coordinate pos;
    S_Coordinate * pPos = &pos;

    // ポインタによるメンバへのアクセス
    pPos->lon = 139.7459914;
    pPos->lat = 35.6568407;
    
    printf("%f\n", pPos->lon);
    printf("%f\n", pPos->lat);
}
