#include <stdio.h>

typedef struct
{
    double latitude;
    double longitude;
} S_Coordinate;

int getTokyoTowerPosiiton(S_Coordinate *pPos)
{
    if(pPos == NULL){
        return -1;
    }

    // 東京タワーの緯度/経度
    pPos->latitude = 35.658581;
    pPos->longitude = 139.745433;

    return 0;
}

int main(void)
{
    S_Coordinate pos;
    int ret = -1;

    ret = getTokyoTowerPosiiton(&pos);

    if(ret < 0){
        printf("ERROR");
    }else{
        printf("緯度:%lf 経度:%lf\n", pos.latitude, pos.longitude);
    }

    return 0;
}