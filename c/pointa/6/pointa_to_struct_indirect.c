#include <stdio.h>

/*
    https://monozukuri-c.com/langc-pointer-struct/
*/

typedef struct
{
	double lon;  // 経度
	double lat;  // 緯度
} S_Coordinate;

int main(void)
{
    // ポインタの照準設定 pPos --> pos
    S_Coordinate pos;
    S_Coordinate *pPos = &pos;

    // ポインタの場合はアロー演算子を使う
    pPos->lon = 139.7459914;
    pPos->lat = 35.6568407;

    // 間接参照の場合がドット演算子を使う
    (*pPos).lon = 139.7459914;
    (*pPos).lat = 35.6568407;

    return 0;
}