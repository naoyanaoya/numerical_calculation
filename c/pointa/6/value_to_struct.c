#include <stdio.h>

typedef struct
{
    double lon; // 軽度
    double lat; // 緯度
} S_Coordinate;

int main(void)
{
    // 構造体の変数定義
    S_Coordinate pos;

    // 変数によるメンバへのアクセス
    pos.lon = 139.7459914;
    pos.lat = 35.6568407;
    
    printf("%f\n", pos.lon);
    printf("%f\n", pos.lat);

    return 0;
}