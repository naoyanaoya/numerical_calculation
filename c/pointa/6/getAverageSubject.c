#include <stdio.h>

typedef struct
{
    long japaneseLanguage;
    long arithmetic;
    long science;
    long socialStudies;
} S_Subject;

int getAverageSubject(S_Subject pSub[], int num, S_Subject *pAve)
{
    S_Subject ave = {0};
    int i;

    // 教科ごとの装填数を算出
    for(i = 0; i < num; i++){
        ave.japaneseLanguage += pSub[i].japaneseLanguage;
        ave.arithmetic       += pSub[i].arithmetic;
        ave.science          += pSub[i].science;
        ave.socialStudies    += pSub[i].socialStudies;
    }

    //  教科毎の平均点を算出
    ave.japaneseLanguage /= num;
    ave.arithmetic       /= num;
    ave.science          /= num;
    ave.socialStudies    /= num;

    //  引数へデータコピー
    *pAve = ave;

    return 0;
}

int main(void)
{
    //  学生の教科毎のテスト結果
    S_Subject point[] = {
        {72, 85, 54, 61}, //  学生A
        {50, 43, 38, 49}, //  学生B
        {89, 92, 87, 78}, //  学生C
        {72, 25, 36, 98}, //  学生D
    };
    S_Subject ave;
    int ret;

    //  getAverageSubject関数の呼び出し
    ret = getAverageSubject(point, sizeof(point) / sizeof(point[0]), &ave);

    // 関数呼び出しの結果
    // 正常なら平均点を表示
    // 異常なら"ERROR"を表示
    if (ret < 0)
    {
        printf("ERROR");
    }
    else
    {
        printf("国語：%d\n", ave.japaneseLanguage);
        printf("算数：%d\n", ave.arithmetic);
        printf("理科：%d\n", ave.science);
        printf("社会：%d\n", ave.socialStudies);
    }

    return 0;
}