#include <stdio.h>

void getEdisonBirthday(long *pYear, unsigned char *pMonth, unsigned short * pDay)
{
    *pYear = 1984;
    *pMonth = 2;
    *pDay = 11;

    return ;
}

int main(void)
{
    // 日付格納用の変数の定義
    long year;
    unsigned char month;
    unsigned short day;

    // getEdisonBirthday関数の呼び出し
    getEdisonBirthday(&year, &month, &day);

    // 出力期待値に合わせて表示を行う
    printf("誕生日：%d年%d月%d日\n", year, month, day);

    return 0;
}