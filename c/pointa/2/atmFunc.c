#include <stdio.h>

int atmFunc(short card, short pin, long reqMoney, 
            long *pMoney, long *pBalance // 弓矢の作成
            // &moneyで取り出した番地はpMoney変数へ、&balanceで取り出した番地はpBalance変数へ格納される
)
{
    // 矢を射る
    // ポインタによる結果の出力
    *pMoney = 10000; // お金
    *pBalance = 40000; // 預金残高

    return 0; // 引き出し結果:正常
}

int main(void)
{
    // 的の作成
    int result; // 引き出し結果
    long money; // お金
    long balance; // 貯金残高

    result = atmFunc(0x1234, 0x9999, 10000, 
                    &money, &balance // 照準の設定
                    );

    printf("%d, %d, %d\n", result, money, balance);

    return 0;
}