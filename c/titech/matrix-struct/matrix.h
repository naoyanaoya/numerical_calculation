#ifndef __MATRIX_H__
#define __MATRIX_H__

struct matrix {
    int row;
    int col;
    int data[3][3];
};

typedef struct matrix Matrix;

void init_matrix(Matrix* entry);
void print_matrix(Matrix* entry);
void add_matrix(Matrix* mat1, Matrix* mat2);
void sub_matrix(Matrix* mat1, Matrix* mat2);
void mul_matrix(Matrix* mat1, Matrix* mat2);

#endif

