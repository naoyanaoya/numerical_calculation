#include<stdio.h>

struct matrix {
    int row; // 行方向(row)の要素数、現在は3に固定
    int col; // 列方向(column)の要素数、現在は3に固定
    double data[3][3]; // 行列の各要素
};

typedef struct matrix Matrix;

// initialize matrix
void init_matrix(Matrix* entry) {
    entry->row = 3;
    entry->col = 3;

    for (int i = 0; i < entry->row; i++) {
        for (int j = 0; j < entry->col; j++) {
            entry->data[i][j] = 0;
        }
    }
}

// print matrix
void print_matrix(Matrix* entry) {
    for (int i = 0; i < entry->row; i++) {
        for (int j = 0; j < entry->col; j++) {
            printf("%f ", entry->data[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

// add matrix matrix
void add_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i++) {
        for (int j = 0; j < mat1->col; j++) {
            mat1->data[i][j] = mat1->data[i][j] + mat2->data[i][j];
        }
    }
}

// sub matrix matrix
void sub_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i++) {
        for (int j = 0; j < mat1->col; j++) {
            mat1->data[i][j] = mat1->data[i][j] - mat2->data[i][j];
        }
    }
}

// multiplication matrix
void multi_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i++) {
        for (int j = 0; j < mat1->col; j++) {
            double tmp = 0;
            for (int k = 0; k < mat1->col; k++) {
                tmp += mat1->data[i][k] * mat2->data[k][j];
            }
            mat1->data[i][j] = tmp;
        }
    }
}

int main() 
{
    // 下の2行で宣言する変数はどちらも同じデータ型
    Matrix mat1;
    Matrix mat2;
    
    init_matrix(&mat1);
    init_matrix(&mat2);
    print_matrix(&mat1);
    mat1.data[0][0] = 1.0;
    mat1.data[1][1] = 1.0;
    mat1.data[2][2] = 1.0;
    mat2.data[0][0] = 5.0;
    mat2.data[0][1] = 5.0;
    mat2.data[0][2] = 5.0;
    print_matrix(&mat1);
    printf(" + \n\n");
    print_matrix(&mat2);

    add_matrix(&mat1, &mat2);

    printf(" = \n\n");
    print_matrix(&mat1);

    multi_matrix(&mat1, &mat2);
    print_matrix(&mat1);

    return 0;
}