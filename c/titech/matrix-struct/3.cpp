#include<stdio.h>

struct matrix {
    int row; // 行方向(row)の要素数、現在は3に固定
    int col; // 列方向(column)の要素数、現在は3に固定
    int data[3][3]; // 行列の各要素
};

typedef struct matrix Matrix;

// initialize matrix
void init_matrix(Matrix* entry) {
    entry->row = 3;
    entry->col = 3;

    for (int i = 0; i < entry->row; i++) {
        for (int j = 0; j < entry->col; j++) {
            entry->data[i][j] = 0;
        }
    }
}

// print matrix
void print_matrix(Matrix* entry) {
    for (int i = 0; i < entry->row; i++) {
        for (int j = 0; j < entry->col; j++) {
            printf("%d ", entry->data[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() 
{
    // 下の2行で宣言する変数はどちらも同じデータ型
    Matrix mat;
    
    init_matrix(&mat);
    print_matrix(&mat);
    mat.data[0][0] = 1;
    mat.data[1][1] = 1;
    mat.data[2][2] = 1;
    print_matrix(&mat);

    return 0;
}