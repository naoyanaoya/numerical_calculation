#include<stdio.h>

struct matrix {
    int row; // 行方向(row)の要素数、現在は3に固定
    int col; // 列方向(column)の要素数、現在は3に固定
    int data[3][3]; // 行列の各要素
};

typedef struct matrix Matrix;

int main() 
{
    // 下の2行で宣言する変数はどちらも同じデータ型
    struct matrix mat1;
    matrix        mat2;

    printf("sizeof(struct matrix) = %d\n", sizeof(struct  matrix));
    printf("sizeof(Matrix)        = %d\n", sizeof(Matrix));
}