#include<stdio.h>
#include"matrix.h"

void init_matrix(Matrix* entry) {
    entry->row = 3;
    entry->col = 3;

    for (int i = 0; i < entry->row; i = i + 1) {
        for (int j = 0; j < entry->col; j = j + 1) {
            entry->data[i][j] = 0;
        }
    }
}

void print_matrix(Matrix* entry) {
    for (int i = 0; i < entry->row; i = i + 1) {
        for (int j = 0; j < entry->col; j = j + 1) {
            printf("%d ", entry->data[i][j]);
        }
        printf("\n");
    }
    /* 行列の終わりであることを示すため空行を入れる */
    printf("\n");
}

void add_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i = i + 1) {
        for (int j = 0; j < mat1->col; j = j + 1) {
            mat1->data[i][j] = mat1->data[i][j] + mat2->data[i][j];
        }
    }
}

void sub_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i++) {
        for (int j = 0; j < mat1->col; j++) {
            mat1->data[i][j] = mat1->data[i][j] - mat2->data[i][j];
        }
    }
}

void mul_matrix(Matrix* mat1, Matrix* mat2) {
    for (int i = 0; i < mat1->row; i++) {
        for (int j = 0; j < mat1->col; j++) {
            double tmp = 0;
            for (int k = 0; k < mat1->col; k++) {
                tmp += mat1->data[i][k] * mat2->data[k][j];
            }
            mat1->data[i][j] = tmp;
        }
    }
}