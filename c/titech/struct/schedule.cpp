#include<stdio.h>
#include<string.h>

struct schedule {
    int year;
    int month;
    int day;
    int hour;
    char title[100]; // 表題
};

int main() {
    struct schedule schedules[3];

    /* 「2008/09/02 13:00 前期筆記試験」という情報を作成する */
    schedules[0].year = 2008;
    schedules[0].month = 9;
    schedules[0].day = 2;
    schedules[0].hour = 13;
    strcpy(schedules[0].title, "guidance");

    schedules[1].year  = 2008;
    schedules[1].month = 7;
    schedules[1].day   = 14;
    schedules[1].hour  = 15;
    strcpy(schedules[1].title, "lecture");

    schedules[2].year  = 2008;
    schedules[2].month = 3;
    schedules[2].day   = 2;
    schedules[2].hour  = 12;
    strcpy(schedules[2].title, "x-day");

    for (int i = 0; i < 3; i = i + 1)
    {
        printf("%04d/%02d/%02d %02d:00 %s\n",
               schedules[i].year, schedules[i].month, schedules[i].day, schedules[i].hour,
               schedules[i].title);
    }
    return 0;
}