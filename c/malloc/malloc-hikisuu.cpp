
#include <iostream>

void data_set(int *data, int n);  // 配列に値をセットする関数のプロトタイプ
                                      // 配列の先頭のアドレス data と配列サイズ n を受け取る
int main(int argc, char* argv[])
{
	const int n = 5;  // 配列の要素数を定数で定める
	int* array = (int*)calloc(n, sizeof(int));   // int 型の配列

	for(int i=0 ; i<n ; i++){
		std::cout << array[i] << "\n";  // 利用法はこれまで通り
	}


	data_set(array, n); // 配列へデータを与える仕事を別関数に任せる
                               // 配列 array の先頭アドレスは array とも書けることを利用。さらに要素数 n も渡す

	for(int i=0 ; i<n ; i++){
		std::cout << array[i] << "\n";  // 利用法はこれまで通り
	}

	return 0;
}

void data_set(int *data, int n){
    std::cout << sizeof(data[0]) << "\n";
    std::cout << sizeof(data) << "\n";
    exit(1);
	for (int i = 0; i < n; i++){
		data[i] = 2 * i;   // ポインタ data に対して data[i] と書くことで配列へアクセスできる
	}
}
