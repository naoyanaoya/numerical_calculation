#include <stdio.h>

// malloc,free関数を呼ぶためにインクルード
#include <stdlib.h>
//
// malloc関数は、ヒープメモリを貸し付けるサービスを提供している
//

enum
{
    E_MUSIC_A, //  音楽A
    E_MUSIC_B, //  音楽B
};

void music(int musicKind)
{
    // 確保したメモリへのポインタ
    char *  pMusic = NULL;
    size_t  musicSize = 0;

    if (musicKind == E_MUSIC_A)
    {
        // 1MByte
        musicSize = 1024 * 1024;
    }
    else
    {
        // 5MByte
        musicSize = 5 * 1024 * 1024;
    }

    // ヒープメモリ
    // 指定サイズ分のメモリ確保
    pMusic = (char *)malloc(musicSize);

    // 音楽の読み込みと再生処理・・・
    // readAndPlay(pMusic);

    // 使用後は不要なメモリを解放
    free(pMusic);
    pMusic = NULL;

    return;
}

int main(void)
{
    // 音楽Aを再生
    music(E_MUSIC_A);

    return 0;
}