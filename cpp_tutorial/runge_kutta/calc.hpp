#ifndef LORENZ_ATTRACTOR_EULER_CALC_HPP_
#define LORENZ_ATTRACTOR_EULER_CALC_HPP_

#include <vector>

namespace my_lib {

class Calc {
  double p;
  double r;
  double b;
  bool lorenz(const double[], double(&)[3]);  // 計算（各ステップ）

public:
  Calc(double p, double r, double b) : p(p), r(r), b(b) {}     // コンストラクタ
  bool lorenz_runge_kutta(std::vector<std::vector<double> >&);  // 計算
};

}  // namespace my_lib

#endif