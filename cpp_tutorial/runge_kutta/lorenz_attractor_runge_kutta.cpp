/***********************************************************
  Lorenz attractor (Runge-Kutta method)

    DATE          AUTHOR          VERSION
    2020.10.14    mk-mode.com     1.00 新規作成

  Copyright(C) 2020 mk-mode.com All Rights Reserved.
***********************************************************/
#include "calc.hpp"

#include <cstdlib>   // for EXIT_XXXX
#include <iomanip>   // for setprecision
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char* argv[]) {
  double p;
  double r;
  double b;
  std::vector<std::vector<double> > res;  // データ配列（計算結果）
  std::size_t i;                         // loop インデックス

  try {
    // コマンドライン引数のチェック
    if (argc < 4) {
      std::cerr << "[ERROR] Number of arguments is wrong!\n"
                << "[USAGE] ./lorenz_attractor_runge_kutta p r b"
                << std::endl;
      return EXIT_FAILURE;
    }

    // p, r, b の取得
    p = std::stod(argv[1]);
    r = std::stod(argv[2]);
    b = std::stod(argv[3]);

    // 計算用オプジェクトのインスタンス化
    my_lib::Calc calc(p, r, b);

    // 計算
    if (!calc.lorenz_runge_kutta(res)) {
      std::cout << "[ERROR] Failed to calculare!" << std::endl;
      return EXIT_FAILURE;
    }

    // 結果出力
    std::cout << std::fixed << std::setprecision(8);
    for (i = 0; i < res.size(); ++i) {
      std::cout << std::setw(14) << std::right << res[i][0]
                << std::setw(14) << std::right << res[i][1]
                << std::setw(14) << std::right << res[i][2]
                << std::endl;
    }
  } catch (...) {
      std::cerr << "EXCEPTION!" << std::endl;
      return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}