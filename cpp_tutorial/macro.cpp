#include <iostream>

#define D_PI (3.141592)

double getCircleLength(double radius)
{
    return radius * D_PI * 2;
}

#define CIRCLE_LEN(radius) (2 * D_PI * (radius))

int main(){
    double len;
    len = getCircleLength(1.4 + 2.8);
    std::cout << len << std::endl;

    len = CIRCLE_LEN(2.8 + 1.4);
    std::cout << len << std::endl;

    return 0;
}