#ifndef SUB_HPP
#define SUB_HPP
//------------------------------------------------
#include <iostream>

//------------------------------------------------
//  マクロ定義(Macro definition)
//------------------------------------------------
#define D_SUB_NUM (10)
//------------------------------------------------
//  型定義(Type definition)
//------------------------------------------------
typedef struct
{
    long num1;
    char moji[D_SUB_NUM];
} S_SUBINFO;
//------------------------------------------------
//  プロトタイプ宣言(Prototype declaration)
//------------------------------------------------
int subPrint(S_SUBINFO info);

//------------------------------------------------
#endif