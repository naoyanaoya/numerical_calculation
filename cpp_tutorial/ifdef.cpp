#include <iostream>

#define D_DEBUG_FLAG (1)

#define D_DEBUG_ON

int main(){
// #if 1
//     std::cout << "hello" << std::endl
// #endif
//     std::cout << "world" << std::endl
// #if 0
//     for(int i=0;i<10;i++){
//         std::cout << "A";
//     }
// #endif
//     for(int i=0;i<10;i++){
//         std::cout << "B";
//     }

// #if D_DEBUG_FLAG
//     std::cout << "hello" << std::endl;
// #else
//     std::cout << "world" << std::endl;
// #endif

// #ifdef D_DEBUG_ON
//     std::cout << "hello" << std::endl;
// #else
//     std::cout << "world" << std::endl;
// #endif

#if 0
    std::cout << "hello" << std::endl;
#else
    std::cout << "world" << std::endl;
#endif


    return 0;
}