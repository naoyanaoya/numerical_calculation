#include <iostream>

// 引数としてオブジェクトを使う

// Carクラスの宣言
class Car{
    private:
        int num;
        double gas;
    public:
        int getNum() {return num;} // クラスの中での関数の定義なので、インライン関数となる
        double getGas() {return gas;} // クラスの中での関数の定義なので、インライン関数となる
        void show();
        void setNumGas(int n, double g);
};

void Car::show()
{
    std::cout << "車のナンバーは" << num << "です。" << std::endl;
    std::cout << "ガソリン量は" << gas << "です。" << std::endl;
}

void Car::setNumGas(int n, double g)
{
    if(0 < g && g < 1000){
        num = n;
        gas = g;
        std::cout << "ナンバーを" << num << "にガソリン量を" << gas << "にしました。" << std::endl;
    }else{
        std::cout << g << "は正しいガソリン量ではありません。" << std::endl;
        std::cout << "ガソリン量を変更できませんでした。" << std::endl;
    }
}

// buy関数の宣言
void buy(Car c);

int main()
{
    Car car1;
    
    car1.setNumGas(1324, 20.5);

    // 関数にオブジェクトの値を呼び出す。
    buy(car1);
    
    return 0;
}

// buy関数の定義
void buy(Car c)
{
    // 渡されたオブジェクトの値を利用する。
    int n = c.getNum();
    double g = c.getGas();
    std::cout << "ナンバー" << n << ":ガソリン量" << g << "の車を購入しました。" << std::endl;
}