#include <iostream>

// Carクラスの宣言
class Car{
    private:
        int num;
        double gas;
    public:
        void show();
        void setNumGas(int n, double g);
};

//  Carクラスメンバ関数の定義
inline void Car::show()
{
    std::cout << "車のナンバーは" << num << "です。" << std::endl;
    std::cout << "ガソリン量は" << gas << "です。" << std::endl;
}

inline void Car::setNumGas(int n, double g)
{
    if(0 < g && g < 1000){
        num = n;
        gas = g;
        std::cout << "ナンバーを" << num << "にガソリン量を" << gas << "にしました。" << std::endl;
    }else{
        std::cout << g << "は正しいガソリン量ではありません。" << std::endl;
        std::cout << "ガソリン量を変更できませんでした。" << std::endl;
    }
}


int main(){
    // クラス型の変数(オブジェクト)を定義した。
    Car car1;

    // このようなアクセスが出来ない。
    // car1.num = 1324;
    // car1.gas = 5.3;

    car1.setNumGas(1234, 5.3);
    car1.show();

    std::cout << "正しくないガソリン量(-10.0)を指定してみる" << std::endl;
    car1.setNumGas(1234, -10.0);
    car1.show();

    return 0;

    car1.show();

    return 0;
}