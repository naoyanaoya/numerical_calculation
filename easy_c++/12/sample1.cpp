#include <iostream>

// Carクラスの宣言
class Car{
    public:
        int num;
        double gas;
        void show();
};

//  Carクラスメンバ関数の定義
void Car::show()
{
    std::cout << "車のナンバーは" << num << "です。" << std::endl;
    std::cout << "ガソリン量は" << gas << "です。" << std::endl;
}

int main(){
    // クラス型の変数(オブジェクト)を定義した。
    Car car1;
    car1.num = 1234;
    car1.gas = 20.5;
    Car car2;
    car2.num = 4321;
    car2.gas = 50.5;

    car1.show();
    car2.show();

    return 0;
}