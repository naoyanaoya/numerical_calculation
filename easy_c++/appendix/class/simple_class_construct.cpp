#include <iostream>
#include <string>

class SimpleClass
{
    private:
        int number;
        std::string name;
    public:
        // コンストラクタ
        // 戻り値は持たない
        // クラス名と同じ名前にする
        SimpleClass()
        {
            number = 0;
            name = "none"; // メンバ変数nameはstringクラスなため、明示的に初期化しなくても空の文字で自動的に初期化されている
        }

        int getNumber()
        {
            return number;
        }
        void setNumber(int n)
        {
            number = n;
        }
        std::string getName()
        {
            return name;
        }
        void setName(char* s)
        {
            name = s;
        }
};

int main()
{
    SimpleClass sc;
    // この時点でsc.numberは0で初期化されている
    // この時点でsc.nameは0で初期化されている

    // 0を表示
    std::cout << sc.getNumber() << std::endl;
    // noneを表示
    std::cout << sc.getName() << std::endl;
}