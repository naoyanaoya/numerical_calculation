#include <iostream>
#include <string>

class SimpleClass
{
    private:
        int number;
        std::string name;
    public:
        int GetNumber()
        {
            return number;
        }
        void SetNumber(int n) // アクセッサ
        {
            number = n;
        }
        std::string GetName() // アクセッサ
        {
            return name;
        }
        void SetName(const char* s) // 引数にconstをつけないと警告が出る。
        {
            name = s;
        }
};

int main()
{
    SimpleClass sc;
    sc.SetNumber(0);
    sc.SetName("山田");

    // privateメンバなので下のようなアクセスがd境内
    // sc.number = 0;

    std::cout << "number: " << sc.GetNumber() << std::endl;
    std::cout << "name: " << sc.GetName() << std::endl;

    return 0;
}

