#include <iostream>
#include <string>

class SimpleClass
{
    public;
        int number;
        std::string name;
};

int main(){
    // 定義したクラスの変数(インスタンス)を宣言
    SimpleClass sc;

    // ここではまだ、sc.numberに実数が入っていないので変な値が出力される。
    std::cout << "number: " << sc.number << std::endl;

    sc.number = 0;
    sc.name = "山田";

    std::cout << "number: " << sc.number << std::endl;
    std::cout << "name: " << sc.name << std::endl;

    return 0;
}