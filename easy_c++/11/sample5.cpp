#include <iostream>

// 構造体型Carの宣言
struct Car{
    int num;
    double gas;
};

// show関数の宣言
void show(Car* pC);
// 構造体へのポインtなを引数にもつ関数

int main(){
    Car car1 = {0, 0.0};

    std::cout << "ナンバーを入力してくだいさい。" << std::endl;
    std::cin >> car1.num;

    std::cout << "ガソリン量を入力してくだいさい。" << std::endl;
    std::cin >> car1.gas;

    show(&car1); // 構造体car1のアドレスを渡します。

    car1.gas = 111.11;

    show(&car1); // 構造体car1のアドレスを渡します。

    return 0;
}

// show関数の定義
void show(
    Car* pC
)
{
    // 関数内ではポインタが渡されるので、ドット演算子ではなくアロー演算子を使う。
    std::cout << "車のナンバーは" << pC->num << ":ガソリン量は" << pC->gas << "です。" << std::endl;
}