#include <iostream>

// 列挙型(enumerated data type)
// enum 列挙型名 {識別子1, 識別子2, 識別子3, ...}
enum Week {SUN, MON, TUE, WED, THU, FRI, SAT};

int main(){
	Week w;
	w = SUN;

	switch(w){
		case SUN: std::cout << "日曜です。" << std::endl; break;
		case MON: std::cout << "月曜です。" << std::endl; break;
		case TUE: std::cout << "火曜です。" << std::endl; break;
		case WED: std::cout << "水曜です。" << std::endl; break;
		case THU: std::cout << "木曜です。" << std::endl; break;
		case FRI: std::cout << "金曜です。" << std::endl; break;
		case SAT: std::cout << "土曜です。" << std::endl; break;
		default: std::cout << "何曜日かわかりません。" << std::endl;; break;
	}

	return 0;
}
