#include <iostream>

struct Car{
	int num;
	double gas;
};

int main(){
	Car car1 = {1234, 25.5};
	Car car2 = {4567, 52.2};

	std::cout << "cat1の車のナンバーは" << car1.num << ":ガソリン量は" << car1.gas << "です。" << std::endl;
	std::cout << "cat2の車のナンバーは" << car2.num << ":ガソリン量は" << car2.gas << "です。" << std::endl;

	car2 = car1;

	std::cout << "cat2の車のナンバーは" << car2.num << ":ガソリン量は" << car2.gas << "です。" << std::endl;

	return 0;
}
