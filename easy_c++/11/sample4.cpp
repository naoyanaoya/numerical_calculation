#include <iostream>

// 構造体を関数の引数として使う


// 構造体型Carの宣言
struct Car{
	int num;
	double gas;
};

// show関数の宣言
void show(Car c);

int main(){
	Car car1 = {0, 0.0};

	std::cout << "ナンバーを入力してください。" << std::endl;
	std::cin >> car1.num;

	std::cout << "ガソリン量を入力してください。" << std::endl;
	std::cin >> car1.gas;

	show(car1); // 構造体car1の値を引数として渡す

	return 0;
}

void show(Car c)
{
	std::cout << "車のナンバーは" << c.num << "ガソリン量は" << c.gas << "です。" << std::endl;
}
