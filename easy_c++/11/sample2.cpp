#include <iostream>

// 構造体のメンバにアクセスする
// 構造体は異なる型の値をまとめるための型

struct Car{
	int num;
	double gas;
};

int main(){
	Car car1;

	std::cout << "ナンバーを入力してください。" << std::endl;
	std::cin >> car1.num;
	std::cout << "ガソリン量を入力してください。" << std::endl;
	std::cin >> car1.gas;

	std::cout << "車のナンバーは" << car1.num << ":ガソリン量は" << car1.gas << "です。" << std::endl;

	return 0;
}
