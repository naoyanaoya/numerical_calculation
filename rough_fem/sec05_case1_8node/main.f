      implicit none

      integer :: ndofn, nint, nnode, nelem, mx_ntnoel, nbcg, nbcnz,
     1           nbcl, mx_matp, mx_matel, ntotdf, nstiff, nsolvdf,
     1           iproc
      integer, allocatable :: lnods(:,:), ntnoel(:), imatel(:)
      real(8), allocatable :: coords(:,:),udispl(:),equivf(:),tdispl(:)
      real(8), allocatable :: amatel(:,:)
      integer, allocatable :: isr(:),ndofn2(:),ibcl(:),ibcnz(:),ibcg(:)
      real(8), allocatable :: vbcl(:), vbcnz(:)
      real(8), allocatable :: au(:), al(:)
      integer, allocatable :: isd(:)

      integer :: i, j

      call datain(0, ndofn, nint, nnode, coords, nbcg, ibcg,
     1            nelem, lnods, mx_ntnoel, ntnoel,
     1            nbcl, ibcl, vbcl, nbcnz, ibcnz, vbcnz, ndofn2,
     1            amatel, imatel, mx_matp, mx_matel)
      ntotdf = nnode * ndofn

      allocate(coords(ndofn,nnode), tdispl(ntotdf), udispl(ntotdf),
     1         equivf(ntotdf), isd(ntotdf+1), isr(ntotdf),ndofn2(nnode),
     1         ibcg(nbcg), lnods(mx_ntnoel,nelem), ntnoel(nelem),
     1         imatel(nelem), amatel(mx_matp,mx_matel),
     1         ibcl(nbcl), ibcnz(nbcnz), vbcl(nbcl), vbcnz(nbcnz))

      call datain(1, ndofn, nint, nnode, coords, nbcg, ibcg,
     1            nelem, lnods, mx_ntnoel, ntnoel,
     1            nbcl, ibcl, vbcl, nbcnz, ibcnz, vbcnz, ndofn2,
     1            amatel, imatel, mx_matp, mx_matel)

      tdispl(1:ntotdf) = 0.D0

      call sky3_init(ntotdf, isd, nelem, lnods, mx_ntnoel, ntnoel,
     1               ndofn, nnode, ndofn2, isr, nsolvdf,
     1               nbcg, ibcg, nbcnz, ibcnz, nstiff)
      allocate(au(nstiff),al(nstiff))

      call elastic(au,al,nstiff,nelem,amatel,imatel,mx_matp,
     1               coords,lnods,mx_ntnoel,nint,ntnoel,ndofn,
     1               isd,isr,nsolvdf,nbcg,nbcnz,
     1               tdispl,equivf,ntotdf)

      udispl(1:ntotdf) = 0.D0
      do i = 1,nbcl
          udispl(ibcl(i)) = vbcl(i)
      end do

      call ssky3(au,udispl,ntotdf,nbcg,ibcg,isd,
     1            nbcnz,ibcnz,vbcnz,nsolvdf,isr)

      do i = 1,ntotdf
          tdispl(i) = tdispl(i) + udispl(i)
      end do

      call elastic(au,al,nstiff,nelem,amatel,imatel,MX_MATP,
     1               coords,lnods,mx_ntnoel,nint,ntnoel,ndofn,
     1               isd,isr,nsolvdf,nbcg,nbcnz,
     1               tdispl,equivf,ntotdf)

      call output(nnode, ndofn, tdispl, equivf)

      call sky3_product(au,au,ntotdf,tdispl,equivf,isd,isr,nsolvdf,
     1                        nbcg,nbcnz)

      call output(nnode, ndofn, tdispl, equivf)

      stop
      end

c#######################################################################
      subroutine datain(iproc, ndofn, nint, nnode, coords, nbcg, ibcg,
     1           nelem,lnods,mx_ntnoel,ntnoel,
     1           nbcl,ibcl,vbcl,
     1           nbcnz,ibcnz,vbcnz,ndofn2,
     1           amatel,imatel, mx_matp, mx_matel)
c#######################################################################
      implicit none
      integer :: iproc, ndofn, mx_ntnoel, nint, nnode, nbcg, nelem, 
     1           nbcl, nbcnz, mx_matp, mx_matel
      real(8), dimension(ndofn,*) :: coords
      integer, dimension(mx_ntnoel,*) :: lnods
      integer, dimension(*) :: ibcg,ntnoel,ibcl,ibcnz,ndofn2,imatel
      real(8), dimension(*) :: vbcnz, vbcl
      real(8), dimension(mx_matp,*) :: amatel

      integer, dimension(3) :: ibctemp
      real(8), dimension(3) :: ctemp

      integer :: i, j, itemp, nmatel, inode, idof
      open(10,file='temp.dat')

      if (iproc .eq. 0) then
          read(10,*) nnode, ndofn
          nbcg = 0
          do i = 1, nnode
              read(10,*) itemp, (ctemp(j),j=1,ndofn),
     1                      (ibctemp(j),j=1,ndofn)
              do j = 1,ndofn
                  if (ibctemp(j) .eq. 1) nbcg = nbcg + 1
              end do
          end do
          read(10,*) nelem, mx_ntnoel, nint
          do i = 1,nelem
              read(10,*)
          end do
          read(10,*) mx_matel, mx_matp
          do i = 1,mx_matel
              read(10,*) 
          end do
          read(10,*) nbcl
          do i = 1,nbcl
              read(10,*) 
          end do
          read(10,*) nbcnz
          do i = 1,nbcnz
              read(10,*) 
          end do
      else if (iproc .eq. 1) then
          read(10,*) ! nnode, ndofn
          nbcg = 0
          do i = 1, nnode
              ndofn2(i) = ndofn
              read(10,*) itemp, (coords(j,i),j=1,ndofn),
     1                      (ibctemp(j),j=1,ndofn)
              do j = 1,ndofn
                  if (ibctemp(j) .eq. 1) then
                      nbcg = nbcg + 1
                      ibcg(nbcg) = (i-1)*ndofn+j
                  end if
              end do
          end do
          read(10,*) nelem, mx_ntnoel, nint
          do i = 1,nelem
              ntnoel(i) = mx_ntnoel
              read(10,*) itemp, imatel(i), (lnods(j,i),j=1,ntnoel(i))
          end do
          read(10,*) ! mx_matel, mx_matp
          do i = 1,mx_matel
              read(10,*) itemp, (amatel(j,i),j=1,mx_matp)
          end do
          read(10,*) ! nbcl
          do i = 1,nbcl
              read(10,*) inode,idof,vbcl(i)
              ibcl(i) = (inode-1)*ndofn+idof
          end do
          read(10,*) ! nbcnz
          do i = 1,nbcnz
              read(10,*) inode, idof, vbcnz(i)
              ibcnz(i) = (inode-1)*ndofn+idof
          end do
      end if
      close(10)
    !   write(*,*) "test"
c
      return
      end
c

c#######################################################################
      subroutine elastic(au, al, nstiff, nelem, amatel, imatel, mx_matp,
     1           coords, lnods, mx_ntnoel, nint, ntnoel, ndofn,
     1           isd, isr, nsolvdf, nbcg, nbcnz,
     1           tdisp, equivf, ntotdf)
c#######################################################################
      implicit none
      integer :: nstiff, nelem, mx_ntnoel, nint, ndofn, nbcg, nbcnz,
     1           ntotdf, nsolvdf, mx_matp
      real(8), dimension(*) :: au, al, tdisp, equivf
      real(8), dimension(ndofn,*) :: coords
      real(8), dimension(mx_matp,*) :: amatel
      integer, dimension(mx_ntnoel,*) :: lnods
      integer, dimension(*) :: isd, isr, ntnoel, imatel

      integer :: ielem
      real(8) :: yo, po
      real(8), allocatable :: astiff(:)

      allocate(astiff( (mx_ntnoel*ndofn)**2) )

      au(1:nstiff) = 0.D0
      al(1:nstiff) = 0.D0

      equivf(1:ntotdf) = 0.D0

      do ielem = 1,nelem
          yo = amatel(1,imatel(ielem))
          po = amatel(2,imatel(ielem))
          call element(coords,lnods(1,ielem),nint,ntnoel(ielem),
     1                 ndofn,astiff,yo,po,tdisp,equivf)

          call ssky3_merge(au,lnods(1,ielem),astiff,
     1                ntnoel(ielem),ndofn,isd,isr,
     1                nsolvdf,nbcg,nbcnz)
      end do

      return
      end

c#######################################################################
      subroutine output(nnode, ndofn, tdispl, equivf)
c#######################################################################
      implicit none
      integer :: nnode, ndofn
      real(8), dimension(ndofn,*) :: tdispl, equivf
      integer :: i, j

      write(*,*) 'DISPLACEMENT'
      do i = 1,nnode
          write(*,1000) i,(tdispl(j,i),j=1,ndofn)
      end do

      write(*,*) 'REACTION FORCE'
      do i = 1,nnode
          write(*,1000) i,(equivf(j,i),j=1,ndofn)
      end do
 1000 format(i6,2x,e13.6,2x,e13.6,2x,e13.6,2x,e13.6)

      return
      end

