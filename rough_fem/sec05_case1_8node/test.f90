program main
  implicit none

  integer :: ndofn, nint, nnode, nelem, mx_ntnoel, nbcg, nbcnz, nbcl, mx_matp, mx_matel, ntotdf, nstiff, nsolvdf, iproc
  integer, allocatable :: lnods(:,:), ntnoel(:), imatel(:)
  double precision, allocatable :: coords(:,:), udispl(:), equivf(:), tdispl(:)
  double precision, allocatable :: amatel(:,:)
  integer, allocatable :: isr(:), ndofn2(:), ibcl(:), ibcnz(:), ibcg(:)
  double precision, allocatable :: vbcl(:), vbcnz(:)
  double precision, allocatable :: au(:), al(:)
  integer, allocatable :: isd(:)

  integer i, j

  call datain(0, ndofn, nint, nnode, coords, nbcg, ibcg, nelem, lnods, mx_ntnoel, ntnoel, nbcl, ibcl, vbcl, nbcnz, ibcnz, vbcnz, &
              ndofn2, amatel, imatel, mx_matp, mx_matel)
  ntotdf = nnode * ndofn

  allocate(coords(ndofn,nnode), tdispl(ntotdf), udispl(ntotdf), equivf(ntotdf), isd(ntotdf+1), isr(ntotdf),ndofn2(nnode), & 
          ibcg(nbcg), lnods(mx_ntnoel,nelem), ntnoel(nelem), imatel(nelem), amatel(mx_matp,mx_matel), &
           ibcl(nbcl), ibcnz(nbcnz), vbcl(nbcl), vbcnz(nbcnz))

  call datain(1, ndofn, nint, nnode, coords, nbcg, ibcg, nelem, lnods, mx_ntnoel, ntnoel, nbcl, ibcl, vbcl, nbcnz, ibcnz, vbcnz, &
              ndofn2, amatel, imatel, mx_matp, mx_matel)

  tdispl(1:ntotdf) = 0.d0

  call sky3_init(ntotdf, isd, nelem, lnods, mx_ntnoel, ntnoel, ndofn, nnode, ndofn2, isr, nsolvdf, nbcg, ibcg, nbcnz, ibcnz, nstiff)

  allocate(au(nstiff),al(nstiff))

  call elastic(au,al,nstiff,nelem,amatel,imatel,mx_matp, coords,lnods,mx_ntnoel,nint,ntnoel,ndofn, isd,isr,nsolvdf,nbcg,nbcnz, & 
                tdispl,equivf,ntotdf)

  udispl(1:ntotdf) = 0.0d0

  do i = 1, nbcl
    udispl(ibcl(i)) = vbcl(i)
  end do

  call ssky3(au,udispl,ntotdf,nbcg,ibcg,isd,nbcnz,ibcnz,vbcnz,nsolvdf,isr)

  do i = 1, ntotdf
    tdispl(i) = tdispl(i) + udispl(i)
  end do

  call eleastic(au,al,nstiff,nelem,amatel,imatel,MX_MATP, coords,lnods,mx_ntnoel,nint,ntnoel,ndofn, isd,isr,nsolvdf,nbcg,nbcnz, &
                tdispl,equivf,ntotdf)


  call output(nnode, ndofn, tdispl, equivf)

  call sky3_product(au,au,ntotdf,tdispl,equivf,isd,isr,nsolvdf, nbcg,nbcnz)

  call output(nnode, ndofn, tdispl, equivf)

end program main


subroutine datain(iproc, ndofn, nint, nnode, coords, nbcg, ibcg, nelem,lnods,mx_ntnoel,ntnoel, nbcl,ibcl,vbcl, nbcnz,ibcnz,vbcnz,ndofn2, amatel,imatel, mx_matp, mx_matel)
  implicit none
  integer, intent(in) :: iproc, ndofn, mx_ntnoel, nint, nnode, nbcg, nelem, nbcl, nbcnz, mx_matp, mx_matel
  double precision, dimension(ndofn,*) :: coords
  integer, dimension(mx_ntnoel,*) ::lnods
  integer, dimension(*) :: ibcg,ntnoel,ibcl,ibcnz,ndofn2,imatel
  double precision, dimension(*) :: vbcnz, vbcl
  double precision, dimension(mx_matp,*) :: amatel

  integer, dimension(3) :: ibctemp
  double precision(3) :: ctemp

  integer i, j, itemp, nmatel, inode, idof

  open(10, file="temp.dat")

  if (iproc .eq. 0) then
    read(10,*) nnode, ndofn
    nbcg = 0
    do i = 1, nnode
        read(10,*) itemp, (ctemp(j),j=1,ndofn), (ibctemp(j),j=1,ndofn)
        do j = 1,ndofn
            if (ibctemp(j) .eq. 1) nbcg = nbcg + 1
        end do
    end do
    read(10,*) nelem, mx_ntnoel, nint
    do i = 1,nelem
        read(10,*)
    end do
    read(10,*) mx_matel, mx_matp
    do i = 1,mx_matel
        read(10,*) 
    end do
    read(10,*) nbcl
    do i = 1,nbcl
        read(10,*) 
    end do
    read(10,*) nbcnz
    do i = 1,nbcnz
        read(10,*) 
    end do
  else if (iproc .eq. 1) then
    read(10,*) ! nnode, ndofn
    nbcg = 0
    do i = 1, nnode
        ndofn2(i) = ndofn
        read(10,*) itemp, (coords(j,i),j=1,ndofn), (ibctemp(j),j=1,ndofn)
        do j = 1,ndofn
            if (ibctemp(j) .eq. 1) then
                nbcg = nbcg + 1
                ibcg(nbcg) = (i-1)*ndofn+j
            end if
        end do
    end do
    read(10,*) nelem, mx_ntnoel, nint
    do i = 1,nelem
        ntnoel(i) = mx_ntnoel
        read(10,*) itemp, imatel(i), (lnods(j,i),j=1,ntnoel(i))
    end do
    read(10,*) ! mx_matel, mx_matp
    do i = 1,mx_matel
        read(10,*) itemp, (amatel(j,i),j=1,mx_matp)
    end do
    read(10,*) ! nbcl
    do i = 1,nbcl
        read(10,*) inode,idof,vbcl(i)
        ibcl(i) = (inode-1)*ndofn+idof
    end do
    read(10,*) ! nbcnz
    do i = 1,nbcnz
        read(10,*) inode, idof, vbcnz(i)
        ibcnz(i) = (inode-1)*ndofn+idof
    end do
  end if
  close(10)
end subroutine datain

subroutine elastic(au, al, nstiff, nelem, amatel, imatel, mx_matp, coords, lnods, mx_ntnoel, nint, ntnoel, ndofn, isd, isr, nsolvdf, nbcg, nbcnz, tdisp, equivf, ntotdf)
  implicit none
  integer, intent(in) :: nstiff, nelem, mx_ntnoel, nint, ndofn, nbcg, nbcnz, ntotdf, nsolvdf, mx_matp
  double precision, dimension(*) :: au, al, tdisp, equivf
  double precision, dimension(ndofn,*) :: coords
  double precision, dimension(mx_matp,*) :: amatel
  integer, dimension(mx_ntnoel,*) :: lnods
  integer, dimension(*) :: isd, isr, ntnoel, imatel

  integer ielem
  double precision yo, po
  double precision, allocatable :: astiff(:)

  allocate(astiff((mx_ntnoel*ndofn) ** 2))

  au(1:nstiff) = 0.D0
  al(1:nstiff) = 0.D0

  equivf(1:ntotdf) = 0.D0

  do ielem = 1,nelem
      yo = amatel(1,imatel(ielem))
      po = amatel(2,imatel(ielem))
      call element(coords,lnods(1,ielem),nint,ntnoel(ielem), ndofn,astiff,yo,po,tdisp,equivf)

      call ssky3_merge(au,lnods(1,ielem),astiff, ntnoel(ielem),ndofn,isd,isr, nsolvdf,nbcg,nbcnz)
  end do
end subroutine elastic

subroutine output(nnode, ndofn, tdispl, equivf)
  implicit none
  integer, intent(in) :: nnode, ndofn
  double precision, dimension(ndofn,*) :: tdispl, equivf

  integer i, j

  write(*,*) 'DISPLACEMENT'
  do i = 1,nnode
      write(*,1000) i,(tdispl(j,i),j=1,ndofn)
  end do

  write(*,*) 'REACTION FORCE'
  do i = 1,nnode
      write(*,1000) i,(equivf(j,i),j=1,ndofn)
  end do
 1000 format(i6,2x,e13.6,2x,e13.6,2x,e13.6,2x,e13.6)
end subroutine output
