c#######################################################################
      subroutine asky3(au, al, udispl, ntotdf, nbcg, ibcg, isd,
     1            nbcnz, ibcnz, vbcnz, nsolvdf, isr)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, al, udispl, vbcnz
      integer, dimension(*) :: ibcg, isd, ibcnz, isr
      integer :: ntotdf, nbcg, nbcnz, nsolvdf

      real(8), allocatable :: sky_temp(:)
      integer :: i, n_active

      allocate(sky_temp(ntotdf))

      do i = 1,ntotdf
          if (isr(i) .ne. 0) sky_temp(isr(i)) = udispl(i)
      end do

      n_active = nsolvdf - nbcg

      call asky3_bound(au,al,sky_temp,n_active,isd,
     1            nbcnz,ibcnz,vbcnz,isr)

      call asky_ver1(al,au,sky_temp,n_active,isd)
c      call asky_ver11(al,au,sky_temp,n_active,isd)
c      call asky_ver12(al,au,sky_temp,n_active,isd)
c
      do i = 1,nbcg
          sky_temp(isr(ibcg(i))) = 0.D0
      end do

      do i = 1,nbcnz
          sky_temp(isr(ibcnz(i))) = vbcnz(i)
      end do

      do i = ntotdf,1,-1
          if (isr(i) .ne. 0 ) then
              udispl(i) = sky_temp(isr(i)) 
          else
              udispl(i) = 0.D0
          end if
      end do

      deallocate(sky_temp)

      return
      end
     
c#######################################################################
      subroutine ssky3(au, udispl, ntotdf, nbcg, ibcg, isd,
     1            nbcnz, ibcnz, vbcnz, nsolvdf, isr)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, udispl, vbcnz
      integer, dimension(*) :: ibcg, isd, ibcnz, isr
      integer :: ntotdf, nbcg, nbcnz, nsolvdf

      real(8), allocatable :: sky_temp(:)
      integer :: i, n_active

      allocate(sky_temp(ntotdf))

      do i = 1,ntotdf
          if (isr(i) .ne. 0) sky_temp(isr(i)) = udispl(i)
      end do

      n_active = nsolvdf - nbcg

      call ssky3_bound(au,sky_temp,n_active,isd,
     1            nbcnz,ibcnz,vbcnz,isr)

      call ssky_ver1(au,sky_temp,n_active,isd)
c      call ssky_ver11(au,sky_temp,n_active,isd)
c      call ssky_ver12(au,sky_temp,n_active,isd)
c
      do i = 1,nbcg
          sky_temp(isr(ibcg(i))) = 0.D0
      end do

      do i = 1,nbcnz
          sky_temp(isr(ibcnz(i))) = vbcnz(i)
      end do

      do i = ntotdf,1,-1
          if (isr(i) .ne. 0 ) then
              udispl(i) = sky_temp(isr(i)) 
          else
              udispl(i) = 0.D0
          end if
      end do

      deallocate(sky_temp)

      return
      end

c#######################################################################
      subroutine sky3_init(ntotdf, isd, nelem, lnods, mx_ntnoel, ntnoel,
     1               ndofn, nnode, ndofn2, isr, nsolvdf,
     1               nbcg, ibcg, nbcnz, ibcnz, nstiff)
c#######################################################################
      implicit none
      integer :: ntotdf, nelem, mx_ntnoel, ndofn, nnode, nsolvdf, nbcg,
     1           nbcnz, nstiff
      integer, dimension(*) :: isd, ntnoel, ndofn2, isr, ibcg, ibcnz
      integer, dimension(mx_ntnoel,*) :: lnods

      integer :: i, j, inode, idof, ielem, mj, mj_temp, n_height, 
     1           n_previous, n_current, n_active
c
      do i = 1,ntotdf
          isr(i) = 0
      end do
c
      do i = 1,nbcg
          isr(ibcg(i)) = -1
      end do
c
      nsolvdf = 0
      do inode = 1,nnode
          do idof = 1,ndofn2(inode)
              if (isr((inode-1)*ndofn+idof) .eq. 0)then
                  nsolvdf = nsolvdf + 1
                  isr((inode-1)*ndofn+idof) = nsolvdf
              end if
          end do
      end do
      do i = 1,nbcnz
          nsolvdf = nsolvdf + 1
          isr(ibcnz(i)) = nsolvdf
      end do
      do inode = 1,nnode
          do idof = 1,ndofn2(inode)
              if (isr((inode-1)*ndofn+idof) .eq. -1)then
                  nsolvdf = nsolvdf + 1
                 isr((inode-1)*ndofn+idof) = nsolvdf
              end if
          end do
      end do
c
      do i = 1,ntotdf+1
          isd(i) = 1
      end do
c
      do ielem = 1,nelem
          mj = ntotdf
          do inode = 1,ntnoel(ielem)
              do idof = 1,ndofn2(lnods(inode,ielem))
                  mj_temp = isr((lnods(inode,ielem)-1)*ndofn+idof)
                  if (mj_temp .lt. mj) mj = mj_temp
              end do
          end do
          do inode = 1,ntnoel(ielem)
              do idof = 1,ndofn
                  j = isr((lnods(inode,ielem)-1)*ndofn+idof)
                  n_height = j - mj + 1
                  if (isd(j) .lt. n_height) then
                      isd(j) = n_height
                  end if
              end do
          end do
      end do
c
      n_previous = isd(2)
      isd(1) = 1
      isd(2) = 2
      do i = 3,ntotdf+1
          n_current = isd(i)
          isd(i) = isd(i-1) + n_previous
          n_previous = n_current
      end do
c
      n_active = nsolvdf - nbcg + nbcnz
      nstiff = isd(n_active+1)-1
      write(*,*) 'stiffness matrix = ',nstiff

      return
      end
c
c#######################################################################
      subroutine asky3_merge(au, al, lnods, astiff, ntnoel, ndofn,
     1           isd, isr, nsolvdf, nbcg, nbcnz)
c#######################################################################
      implicit none
      integer :: ntnoel, ndofn, nsolvdf, nbcg, nbcnz
      real(8), dimension(*) :: au, al
      integer, dimension(*) :: lnods, isd, isr
      real(8), dimension(ntnoel*ndofn,*) :: astiff

      integer, allocatable :: ip(:)
      integer :: n_active, inode, idof, i, j, ij

      allocate(ip(ntnoel*ndofn))

      n_active = nsolvdf - nbcg + nbcnz
c
      do inode = 1,ntnoel
          do idof = 1, ndofn
              ip((inode-1)*ndofn+idof) =
     1            isr((lnods(inode)-1)*ndofn + idof)
          end do
      end do
c
      do i = 1,ntnoel*ndofn
          if (ip(i) .ne. 0 .and. ip(i) .le. n_active) then
              do j = 1,ntnoel*ndofn
                  if (ip(j) .ne. 0 .and. ip(j) .le. n_active) then
                      if (ip(i) .le. ip(j)) then
                          ij = isd(ip(j)) + ip(j) - ip(i)
                          au(ij) = au(ij) + astiff(i,j)
                      end if
                      if (ip(i) .ge. ip(j)) then
                          ij = isd(ip(i)) + ip(i) - ip(j)
                          al(ij) = al(ij) + astiff(i,j)
                      end if
                  end if
              end do
          end if
      end do

      deallocate(ip)

      return
      end
c
c#######################################################################
      subroutine ssky3_merge(au, lnods, astiff, ntnoel, ndofn,
     1           isd,isr, nsolvdf, nbcg, nbcnz)
c#######################################################################
      implicit none
      integer :: ntnoel, ndofn, nsolvdf, nbcg, nbcnz
      real(8), dimension(*) :: au
      integer, dimension(*) :: lnods, isd, isr
      real(8), dimension(ntnoel*ndofn,*) :: astiff

      integer, allocatable :: ip(:)
      integer :: n_active, inode, idof, i, j, ij

      allocate(ip(ntnoel*ndofn))

      n_active = nsolvdf - nbcg + nbcnz

      do inode = 1,ntnoel
          do idof = 1, ndofn
              ip((inode-1)*ndofn+idof) =
     1            isr((lnods(inode)-1)*ndofn + idof)
          end do
      end do

      do i = 1,ntnoel*ndofn
          if (ip(i) .ne. 0 .and. ip(i) .le. n_active) then
              do j = i,ntnoel*ndofn
                  if (ip(j) .ne. 0 .and. ip(j) .le. n_active) then
                      if (ip(i) .le. ip(j)) then
                          ij = isd(ip(j)) + ip(j) - ip(i)
                      else
                          ij = isd(ip(i)) + ip(i) - ip(j)
                      end if
                      au(ij) = au(ij) + astiff(i,j)
                  end if
              end do
          end if
      end do

      deallocate(ip)

      return
      end

c#######################################################################
      subroutine asky3_bound(au,al,c,n,isd,nbcnz,ibcnz,vbcnz,isr)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, al, c, vbcnz
      integer, dimension(*) :: isd, ibcnz, isr
      integer :: n, nbcnz

      integer :: i, j, k, mj, mk, kj, jk

      do i = 1,nbcnz
          j = isr(ibcnz(i))
          mj = j - isd(j+1) + isd(j) + 1
          do k = mj,j
              kj = isd(j) + j - k
              c(k) = c(k) - vbcnz(i) * au(kj)
          end do
          do k = j+1,n
              mk = k - isd(k+1) + isd(k) + 1
              if (mk .le. j) then
                  jk = isd(k) + k - j
                  c(k) = c(k) - vbcnz(i) * al(jk)
              end if
          end do
      end do

      return
      end

c#######################################################################
      subroutine ssky3_bound(au,c,n,isd,nbcnz,ibcnz,vbcnz,isr)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, c, vbcnz
      integer, dimension(*) :: isd, ibcnz, isr
      integer :: n, nbcnz

      integer :: i, j, k, mj, mk, kj, jk

      do i = 1,nbcnz
          j = isr(ibcnz(i))
          mj = j - isd(j+1) + isd(j) + 1
          do k = mj,j
              kj = isd(j) + j - k
              c(k) = c(k) - vbcnz(i) * au(kj)
          end do
          do k = j+1,n
              mk = k - isd(k+1) + isd(k) + 1
              if (mk .le. j) then
                  jk = isd(k) + k - j
                  c(k) = c(k) - vbcnz(i) * au(jk)
              end if
          end do
      end do

      return
      end

c#######################################################################
      subroutine asky_ver1(al, au, b, n, isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: al, au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp_l, temp

      al(3) = al(3) / au(1)
      au(3) = au(3) / au(1)

      au(2) = au(2) - al(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              temp_l = 0.d0
              do k = max(mi,mj), i-1
                  ki = isd(i) + i - k
                  kj = isd(j) + j - k
                  temp_u = temp_u + al(ki) * au(kj)
                  temp_l = temp_l + al(kj) * au(ki)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
              al(ij) = al(ij) - temp_l
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
              al(ij) = al(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + al(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i-j
              b(i) = b(i) - al(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i-j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end
c
c#######################################################################
      subroutine asky_ver11(al,au,b,n,isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: al, au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp_l, temp

      al(3) = al(3) / au(1)
      au(3) = au(3) / au(1)

      au(2) = au(2) - al(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              temp_l = 0.d0
              ki = isd(i) + i
              kj = isd(j) + j
              do k = max(mi,mj), i-1
                  temp_u = temp_u + al(ki-k) * au(kj-k)
                  temp_l = temp_l + al(kj-k) * au(ki-k)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
              al(ij) = al(ij) - temp_l
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
              al(ij) = al(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + al(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i-j
              b(i) = b(i) - al(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i-j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end
c
c#######################################################################
      subroutine asky_ver12(al,au,b,n,isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: al, au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp_l, temp

      al(3) = al(3) / au(1)
      au(3) = au(3) / au(1)

      au(2) = au(2) - al(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              temp_l = 0.d0
              ki = isd(i) + i - i
              kj = isd(j) + j - i
              do k = 1,i-max(mi,mj)
                  temp_u = temp_u + al(ki+k) * au(kj+k)
                  temp_l = temp_l + al(kj+k) * au(ki+k)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
              al(ij) = al(ij) - temp_l
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
              al(ij) = al(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + al(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i-j
              b(i) = b(i) - al(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i-j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end

c#######################################################################
      subroutine ssky_ver1(au,b,n,isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp

      au(3) = au(3) / au(1)

      au(2) = au(2) - au(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              do k = max(mi,mj), i-1
                  ki = isd(i) + i - k
                  kj = isd(j) + j - k
                  temp_u = temp_u + au(ki) * au(kj)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + au(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i-j
              b(i) = b(i) - au(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i-j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end

c#######################################################################
      subroutine ssky_ver11(au,b,n,isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp

      au(3) = au(3) / au(1)

      au(2) = au(2) - au(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              ki = isd(i) + i
              kj = isd(j) + j
              do k = max(mi,mj), i-1
                  temp_u = temp_u + au(ki-k) * au(kj-k)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + au(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i-j
              b(i) = b(i) - au(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i-j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end

c#######################################################################
      subroutine ssky_ver12(au,b,n,isd)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, b
      integer, dimension(*) :: isd
      integer :: n

      integer :: i, j, k, mi, mj, ij, ki, kj, ji
      real(8) :: temp_u, temp

      au(3) = au(3) / au(1)

      au(2) = au(2) - au(3) * au(1) * au(3)

      do j = 3,n
          mj = j - isd(j+1) + isd(j) + 1
          do i = mj+1, j-1
              mi = i - isd(i+1) + isd(i) + 1
              temp_u = 0.d0
              ki = isd(i) + i - i
              kj = isd(j) + j - i
              do k = 1, i-max(mi,mj)
                  temp_u = temp_u + au(ki+k) * au(kj+k)
              end do
              ij = isd(j) + j-i
              au(ij) = au(ij) - temp_u
          end do
          do i = mj,j-1
              ij = isd(j) + j-i
              au(ij) = au(ij) / au(isd(i))
          end do
          temp = 0.d0
          do k = mj,j-1
              kj = isd(j) + j-k
              temp = temp + au(kj) * au(isd(k)) * au(kj)
          end do
          au(isd(j)) = au(isd(j)) - temp
      end do

      do i = 2,n
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ij = isd(i) + i - j
              b(i) = b(i) - au(ij) * b(j)
          end do
      end do

      do i = 1,n
          b(i) = b(i) / au(isd(i))
      end do

      do i = n,2,-1
          mi = i - isd(i+1) + isd(i) + 1
          do j = mi,i-1
              ji = isd(i) + i - j
              b(j) = b(j) - au(ji) * b(i)
          end do
      end do

      return
      end

c#######################################################################
      subroutine sky3_product(au, al, ntotdf, b_in, b_out, isd, isr,
     1           nsolvdf, nbcg, nbcnz)
c#######################################################################
      implicit none
      real(8), dimension(*) :: au, al, b_in, b_out
      integer, dimension(*) :: isd, isr
      integer :: ntotdf, nsolvdf, nbcg, nbcnz

      integer :: i, j, n_active, ij
      real(8), allocatable :: sky_temp(:)

      allocate(sky_temp(ntotdf))

      do i = 1,ntotdf
          if (isr(i) .ne. 0 ) sky_temp(isr(i)) = b_in(i)
          b_out(i) = 0.D0
      end do

      n_active = nsolvdf - nbcg + nbcnz

      do i = 1,n_active
          b_out(i) = b_out(i) + al(isd(i))*sky_temp(i)
          do j = i-1,i-(isd(i+1)-isd(i))+1,-1
              ij = isd(i) + (i-j)
              b_out(j) = b_out(j)+au(ij)*sky_temp(i)
              b_out(i) = b_out(i)+al(ij)*sky_temp(j)
          end do
      end do

      do i = 1,ntotdf
          sky_temp(i) = b_out(i)
      end do

      do i = ntotdf,1,-1
         if (isr(i) .ne. 0 ) then
              b_out(i) = sky_temp(isr(i)) 
          else
              b_out(i) = 0.D0
          end if
      end do

      deallocate(sky_temp)

      return
      end
