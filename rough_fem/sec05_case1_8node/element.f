c#######################################################################
      subroutine element(coords,lnods,nint,ntnoel,ndofn,
     1                   astiff,yo,po,tdispl,equivf)
c#######################################################################
      implicit none
      integer :: ndofn, nint, ntnoel
      integer, dimension(*) :: lnods
      real(8) :: yo, po
      real(8), dimension(ndofn,*) :: coords 
      real(8), dimension(ntnoel*ndofn,*) :: astiff
      real(8), dimension(*) :: tdispl, equivf

      integer :: ir1, ir2, ir3, matsize, i, j
      real(8) :: r1, r2, r3, detwei, detjac, vol
      real(8), dimension(4,4) :: gsp, wgh
      real(8), dimension(6,6) :: dmat

      real(8), dimension(3,8) :: dndx
      real(8), dimension(6,24) :: bmat
c
      data gsp /  0.D0          , 0.D0          , 0.D0,          0.D0,
     1           -0.577350269189626D0, 0.577350269189626D0, 0.D0,0.D0,
     1           -0.774596669241483D0, 0.D0, 0.774596669241483D0,0.D0,
     1           -0.861136311594053D0,-0.339981043584856D0,
     1            0.339981043584856D0, 0.861136311594053D0 /
      data wgh /  2.D0          , 0.D0          , 0.D0,          0.D0,
     1            1.D0          , 1.D0          , 0.D0,          0.D0,
     1            0.555555555555556D0, 0.888888888888889D0,
     1            0.555555555555556D0, 0.D0,
     1            0.347854845137454D0, 0.652145154862546D0,
     1            0.652145154862546D0, 0.347854845137454D0 /
c
      matsize = ntnoel*ndofn 
      do j = 1,matsize
          do i = 1,matsize
              astiff(i,j) = 0.D0
          end do
      end do
c
      vol = 0.D0
c
      call cal_dmat(yo,po,dmat)
c
      do ir1 = 1,nint
      do ir2 = 1,nint
      do ir3 = 1,nint
c
          r1 = gsp(ir1,nint)
          r2 = gsp(ir2,nint)
          r3 = gsp(ir3,nint)
c
          call shap3d(r1,r2,r3,coords,ndofn,lnods,detjac,dndx,ntnoel)
c
          call blu3d(dndx, bmat, ntnoel, ndofn)
c
          detwei= detjac*wgh(ir1,nint)*wgh(ir2,nint)*wgh(ir3,nint)
          vol = vol + detwei
c
          call kuu3d(astiff, matsize, dmat, bmat, detwei)
          call equiv(equivf,dmat,bmat,detwei,tdispl,lnods, ntnoel,ndofn)
c
      end do
      end do
      end do
c
c      write(*,*) vol
c
      return
      end
c
C#######################################################################
      subroutine equiv(equivf, dmat, bmat, detwei, tdispl, lnods,
     1           ntnoel, ndofn)
C#######################################################################
      implicit none
      real(8), dimension(*) ::  equivf, tdispl
      integer, dimension(*) :: lnods
      real(8), dimension(6,*) :: dmat, bmat
      integer :: ntnoel, ndofn
      real(8) :: detwei

      real(8), dimension(6) :: stress, strain
      integer :: i, j, k, ii, jj
      real(8) :: temp
 
      do i = 1,6
          strain(i) = 0.D0
          do j = 1, ntnoel
              do k = 1, ndofn
                  ii = (j-1)*ndofn + k
                  jj = (lnods(j)-1)*ndofn + k
                  strain(i) = strain(i) + bmat(i,ii) * tdispl(jj)
              end do
          end do
      end do

      do i = 1,6
          stress(i) = 0.D0
          do j = 1,6
              stress(i) = stress(i) + dmat(i,j) * strain(j)
          end do
      end do

      do i = 1,ntnoel
          do j = 1,ndofn
              ii =        (i-1)*ndofn + j
              jj = (lnods(i)-1)*ndofn + j
              temp = 0.D0
              do k = 1,6
                  temp = temp + bmat(k,ii) * stress(k)
              end do
              equivf(jj) = equivf(jj) + detwei * temp
          end do
      end do

      return
      end

C#######################################################################
      subroutine shap3d(r1,r2,r3, coords, ndofn, lnods, detjac, dndx,
     1           ntnoel)
C#######################################################################
      implicit none
      integer :: ndofn, ntnoel
      integer, dimension(*) :: lnods
      real(8) :: r1, r2, r3, detjac
      real(8), dimension(ndofn,*) :: coords
      real(8), dimension(ndofn,*) :: dndx
      real(8), dimension(3,3) :: jacobi
      real(8), dimension(3,8) :: dndr
      real(8), dimension(3,3) :: jacinv
      integer :: i, j, k
      real(8) :: temp

      dndr(1,1) = -0.125D0 * (1.D0 - r2) * (1.D0 - r3)
      dndr(2,1) = -0.125D0 * (1.D0 - r1) * (1.D0 - r3)
      dndr(3,1) = -0.125D0 * (1.D0 - r1) * (1.D0 - r2)

      dndr(1,2) =  0.125D0 * (1.D0 - r2) * (1.D0 - r3)
      dndr(2,2) = -0.125D0 * (1.D0 + r1) * (1.D0 - r3)
      dndr(3,2) = -0.125D0 * (1.D0 + r1) * (1.D0 - r2)

      dndr(1,3) =  0.125D0 * (1.D0 + r2) * (1.D0 - r3)
      dndr(2,3) =  0.125D0 * (1.D0 + r1) * (1.D0 - r3)
      dndr(3,3) = -0.125D0 * (1.D0 + r1) * (1.D0 + r2)

      dndr(1,4) = -0.125D0 * (1.D0 + r2) * (1.D0 - r3)
      dndr(2,4) =  0.125D0 * (1.D0 - r1) * (1.D0 - r3)
      dndr(3,4) = -0.125D0 * (1.D0 - r1) * (1.D0 + r2)

      dndr(1,5) = -0.125D0 * (1.D0 - r2) * (1.D0 + r3)
      dndr(2,5) = -0.125D0 * (1.D0 - r1) * (1.D0 + r3)
      dndr(3,5) =  0.125D0 * (1.D0 - r1) * (1.D0 - r2)

      dndr(1,6) =  0.125D0 * (1.D0 - r2) * (1.D0 + r3)
      dndr(2,6) = -0.125D0 * (1.D0 + r1) * (1.D0 + r3)
      dndr(3,6) =  0.125D0 * (1.D0 + r1) * (1.D0 - r2)

      dndr(1,7) =  0.125D0 * (1.D0 + r2) * (1.D0 + r3)
      dndr(2,7) =  0.125D0 * (1.D0 + r1) * (1.D0 + r3)
      dndr(3,7) =  0.125D0 * (1.D0 + r1) * (1.D0 + r2)

      dndr(1,8) = -0.125D0 * (1.D0 + r2) * (1.D0 + r3)
      dndr(2,8) =  0.125D0 * (1.D0 - r1) * (1.D0 + r3)
      dndr(3,8) =  0.125D0 * (1.D0 - r1) * (1.D0 + r2)

      call jacb3d(dndr, coords, ndofn, lnods, jacobi, detjac, jacinv,
     1            ntnoel)

      do i = 1, ntnoel
          do j = 1, ndofn
              temp = 0.D0
              do k = 1, ndofn
                  temp = temp + jacinv(j,k) * dndr(k,i)
              end do
              dndx(j,i) = temp
          end do
      end do

      return
      end

C#######################################################################
      subroutine jacb3d(dndr, coords, ndofn, lnods, jacobi, detjac,
     1           jacinv, ntnoel)
C#######################################################################
      implicit none
      integer :: ndofn, ntnoel
      integer, dimension(*) :: lnods
      real(8) :: detjac
      real(8), dimension(3,*) :: jacobi, jacinv, dndr
      real(8), dimension(ndofn,*) :: coords
      integer :: i, j, k
      real(8) :: deta

      do i = 1,3
          do j = 1,3
              jacobi(i,j) = 0.D0
          end do
      end do

      do k = 1,8
        do i = 1,3
          do j = 1,3
            jacobi(j,i) = jacobi(j,i) + dndr(j,k) * coords(i,lnods(k))
          end do
        end do
      end do

      detjac = jacobi(1,1)*jacobi(2,2)*jacobi(3,3)
     1       + jacobi(2,1)*jacobi(3,2)*jacobi(1,3)
     1       + jacobi(3,1)*jacobi(1,2)*jacobi(2,3)
     1       - jacobi(1,1)*jacobi(3,2)*jacobi(2,3)
     1       - jacobi(3,1)*jacobi(2,2)*jacobi(1,3)
     1       - jacobi(2,1)*jacobi(1,2)*jacobi(3,3)

      deta = 1.D0 / detjac

      jacinv(1,1) =
     1      ( jacobi(2,2)*jacobi(3,3)-jacobi(2,3)*jacobi(3,2) ) *deta
      jacinv(2,1) =
     1    - ( jacobi(2,1)*jacobi(3,3)-jacobi(2,3)*jacobi(3,1) ) *deta
      jacinv(3,1) =
     1      ( jacobi(2,1)*jacobi(3,2)-jacobi(2,2)*jacobi(3,1) ) *deta

      jacinv(1,2) =
     1    - ( jacobi(1,2)*jacobi(3,3)-jacobi(1,3)*jacobi(3,2) ) *deta
      jacinv(2,2) =
     1      ( jacobi(1,1)*jacobi(3,3)-jacobi(1,3)*jacobi(3,1) ) *deta
      jacinv(3,2) =
     1    - ( jacobi(1,1)*jacobi(3,2)-jacobi(1,2)*jacobi(3,1) ) *deta

      jacinv(1,3) =
     1      ( jacobi(1,2)*jacobi(2,3)-jacobi(1,3)*jacobi(2,2) ) *deta
      jacinv(2,3) =
     1    - ( jacobi(1,1)*jacobi(2,3)-jacobi(1,3)*jacobi(2,1) ) *deta
      jacinv(3,3) =
     1      ( jacobi(1,1)*jacobi(2,2)-jacobi(1,2)*jacobi(2,1) ) *deta

      return
      end
c
C#######################################################################
      subroutine cal_dmat(yo, po, dmat)
C#######################################################################
      implicit none
      real(8) :: yo, po
      real(8), dimension(6,*) :: dmat
      integer :: i, j
      real(8) :: kappa, g

      do i = 1,6
          do j = 1,6
              dmat(i,j) = 0.D0
          end do
      end do

      kappa = yo/3.D0/(1.D0 - 2.D0*po)
      g     = yo/2.D0/(1.D0 + po)

      do i = 1,3
          do j = 1,3
              dmat(i,j) = kappa- 2.D0/3.D0 * g
          end do
          dmat(i,i) = kappa + 4.D0/3.D0 * g
          dmat(i+3,i+3) = g
      end do

      return
      end

C#######################################################################
      subroutine kuu3d(astiff, matsize, dmat, bmat, detwei)
C#######################################################################
      implicit none
      integer :: matsize
      real(8), dimension(matsize,*) :: astiff
      real(8), dimension(6,*) :: bmat, dmat
      real(8) :: detwei

      real(8), allocatable :: tmpwrk(:,:)
      integer :: i, j, k
      real(8) :: temp

      allocate(tmpwrk(6,matsize))

      do i = 1,6
          do k = 1,24
              temp = 0.D0
              do j = 1,6
                  temp = temp + dmat(i,j) * bmat(j,k)
              end do
              tmpwrk(i,k) = temp
          end do
      end do

      do i = 1,24
          do k = i,24
              temp = 0.D0
              do j = 1,6
                  temp = temp + bmat(j,i) * tmpwrk(j,k)
              end do
              astiff(i,k) = astiff(i,k) + detwei*temp
          end do
      end do

      deallocate(tmpwrk)

      return
      end

C#######################################################################
      subroutine blu3d(dndx, bmat, ntnoel, ndofn)
C#######################################################################
      implicit none
      real(8), dimension(3,*) :: dndx
      real(8), dimension(6,*) :: bmat
      integer :: ntnoel, ndofn

      integer :: i, j, i1, i2, i3

      do i = 1, ndofn * ntnoel
          do j = 1, 6
              bmat(j,i) = 0.D0
          end do
      end do

      do i = 1, ntnoel
          i1 = 1 + (i-1) * ndofn
          i2 = 2 + (i-1) * ndofn
          i3 = 3 + (i-1) * ndofn

          bmat(1,i1) = dndx(1,i)
          bmat(2,i2) = dndx(2,i)
          bmat(3,i3) = dndx(3,i)
          bmat(4,i1) = dndx(2,i)
          bmat(4,i2) = dndx(1,i)
          bmat(5,i2) = dndx(3,i)
          bmat(5,i3) = dndx(2,i)
          bmat(6,i1) = dndx(3,i)
          bmat(6,i3) = dndx(1,i)
      end do

      return
      end

