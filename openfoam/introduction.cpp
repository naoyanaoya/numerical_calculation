/*
    オブジェクト指向プログラミングでは、あるクラスの機能を引き継いで新しいクラスを作ることが出来る。
    この機能のことを「継承」という。継承を使うと、新しいクラスと継承元のクラスとで異なっている部分だけｗプログラミングすればいいので、これを「差分プログラミング」といったりする。

    ここでは例としてあるファンタジ系のゲームのキャラクタのクラスを実装することを考える。「人間」という種族がいて、その中には「戦士」あるいは「魔法使い」を職業とする人がいるものとする。
    それぞれの職業にはそれ特有の「技」や「魔法」などの特殊技能があるものとする。
*/

#include <iostream>
#include <string>

using namespace std;

class Human
{
    public:
        Human(const char *name) : _name(name) {};
        void sayName() { cout << _name << endl;};
        void special() { cout << "special" << endl;};
    private:
        string _name;
};

class Fighter : public Human
{
    public:
        Fighter(const char *name) : Human(name) {};
        void special() { skill(); };
    private:
        void skill() { cout << "skill" << endl; };
};

class Magician : public Human
{
    public:
        Magician(const char *name) : Human(name) {};
        void special() { magic(); };
    private:
        void magic() { cout << "magic" << endl; };
};

int main()
{
    Human h("human1");
    Fighter f("fighter1");
    Magician m("magician1");

    h.sayName();
    h.special();

    f.sayName();
    f.special();

    m.sayName();
    m.special();

    return 0;
}