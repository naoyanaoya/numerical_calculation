#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<vector>
using namespace std;

//TDMA
inline void tdma(double a[],double c[],double b[],double d[],double x[],int size){
    int i;
    double *P=new double[size];
    double *Q=new double[size];
    //first step
    P[0]=-b[0]/a[0];
    Q[0]=d[0]/a[0];
    //second step
    for(i=1;i<size;i++){
        P[i]=-b[i]/(a[i]+c[i]*P[i-1]);
        Q[i]=(d[i]-c[i]*Q[i-1])/(a[i]+c[i]*P[i-1]);
    }
    //third step
    x[size-1]=Q[size-1];
    //fourth step,backward
    for(i=size-1;i>-1;i=i-1){
        x[i]=P[i]*x[i+1]+Q[i];
    }
    delete [] P,Q;
}
int main()
{
	int i;
	int size=4;
	
	double *t=new double[size];
	double *l=new double[size];
	double *r=new double[size];
	double *b=new double[size];
	double *x=new double[size];
	
	t[0]=2.0;t[1]=2.0;t[2]=2.0;t[3]=2.0;
	l[0]=0.0;l[1]=1.0;l[2]=1.0;l[3]=1.0;
	r[0]=1.0;r[1]=1.0;r[2]=1.0;r[3]=0.0;
	b[0]=4.0;b[1]=8.0;b[2]=12.0;b[3]=11.0;

	tdma(t,l,r,b,x,size);
	
	for(i=0;i<size;i++) cout<<x[i]<<endl;
	
	delete [] t,l,r,b,x;

    return 0;
}
