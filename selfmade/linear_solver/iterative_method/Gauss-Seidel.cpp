#include <iostream>
#include <math.h>
using namespace std;

#define max 100                 //繰り返し最大回数
#define eps 1.0e-5              //最小誤差許容範囲
int main(){
    int i,j,k;
        double err;
    int allnode=3;
    int allnode1=9;
    double* A=(double*)calloc(allnode1,sizeof(double));
    double* b=(double*)calloc(allnode,sizeof(double));
    double* xold=(double*)calloc(allnode,sizeof(double));
    double* xnew=(double*)calloc(allnode,sizeof(double));
/*配列a,bには係数の値等を。配列xoldには適当に決めた解の値を入れておく*/
    for(i=0;i<allnode1;i++){
        cin >> A[i];  
    }
    for(i=0;i<allnode;i++){
        cin >> b[i];  
    }
    for(i=0;i<allnode;i++){
        xold[i]=1.0;  
    }
    for(k=0;k<max;k++){
        err=0.0;                        //誤差のリセット
        for(i=0;i<allnode;i++) {
            xnew[i]=b[i];
            for(j=0;j<allnode;j++) {
                if(i>j){
                        xnew[i]-=A[i*allnode+j]*xnew[j];
                }
                if(i<j){
                        xnew[i]-=A[i*allnode+j]*xold[j];
                }
            }
            xnew[i]=xnew[i]/A[i*allnode+i];
        }

/*各解の誤差を足し、古い解は捨て配列xoldに新しい解を入れる*/
        for(i=0;i<allnode;i++) {
            err+=fabs(xold[i]-xnew[i]);
            xold[i]=xnew[i];
        }

/*足しあわされた誤差が許容範囲内だったら
計算終了とし解が求まったとする*/
        if(err<eps) break;
    }

/*求まった解を出力する*/
    for(i=0;i<allnode;i++) {
        cout << xnew[i] << endl;    //printf("%8.4f\n",xnew[i]);
    }

/*繰り返し回数が最大回数の値だったら
正確な解が求まっていない可能性大なので注意*/
    cout << "繰り返し回数は" << k+1 << endl;
    //printf("繰り返し回数は %d\n",repeat+1);

    free(A);
    free(b);
    free(xold);
    free(xnew);
    return 0;
}

/*
5 -4 6
7 -6 10
4 9 7
8 14 74

3.0 -6.0 9.0
2.0 5.0 -8.0
1.0 -4.0 7.0
6.0 8.0 2.0
*/