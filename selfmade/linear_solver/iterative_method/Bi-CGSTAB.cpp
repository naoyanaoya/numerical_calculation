#include <iostream>
#include <math.h>
using namespace std;

#define max 100                 //繰り返し最大回数
#define eps 1.0e-5              //最小誤差許容範囲

int main(){
    int i,j,k;
    double err;
    int allnode;
    cin >> allnode;
    double* A=(double*)calloc(allnode*allnode,sizeof(double));
    double* b=(double*)calloc(allnode,sizeof(double));
    double* x=(double*)calloc(allnode,sizeof(double));
    double* r=(double*)calloc(allnode,sizeof(double));
    double* rr=(double*)calloc(allnode,sizeof(double));
    double* p=(double*)calloc(allnode,sizeof(double));
    double* y=(double*)calloc(allnode,sizeof(double));
    double* z=(double*)calloc(allnode,sizeof(double));
    double* s=(double*)calloc(allnode,sizeof(double));

    for(i=0;i<allnode;i++){
        for(j=0;j<allnode;j++){
            cin >> A[i*allnode+j];
        }
    }
    for(i=0;i<allnode;i++){
        cin >> b[i];  
    }

    //第0近似解に対する残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        r[i] = b[i]-Ax;
        p[i] = r[i];
    }
    //第0近似解に対するシャドウ残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        rr[i] = b[i]-Ax;
    }

    double rr0=0;
    for(i=0;i<allnode;i++){
        rr0 += rr[i]*r[i];
    }

    double alpha=0;
    double beta=0;
    double omega=0;
    double e=0.0;

    for(k=0;k<max;k++){
        //y=Apの計算
        for(i=0;i<allnode;i++){
            y[i]=0;
        }   
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                y[i] += A[i*allnode+j]*p[j];
            }
        }
        double ry=0;
        for(i=0;i<allnode;i++){
                ry += rr[i]*y[i];
        }

        //alphaの計算
        double rr0=0;
        for(i=0;i<allnode;i++){
            rr0 += rr[i]*r[i];
        }
        alpha=rr0/ry;

        //sの計算
        for(i=0;i<allnode;i++){
            s[i] = r[i]-alpha*y[i];
        }

        //omegaの計算
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                z[i] = 0;
            }
        }
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                z[i] += A[i*allnode+j]*s[j];
            }
        }
        double zs=0;
        for(i=0;i<allnode;i++){
            zs += z[i]*s[i];
        }
        double zz=0;
        for(i=0;i<allnode;i++){
            zz += z[i]*z[i];
        }
        omega=zs/zz;

        //update x and r 
        for(i=0;i<allnode;i++){
            x[i] += (alpha*p[i]+omega*s[i]);
            r[i] = s[i]-omega*z[i];
        }

        double rr1=0;
        for(i=0;i<allnode;i++){
                rr1 += r[i]*r[i];
        }

        // for(i=0;i<allnode;i++){
        //     cout << x[i] << endl;
        // }
         
        //convergence test
        e = sqrt(rr1);
        cout << "LOOP " << " iter " << k+1 << " err " << e << endl;
        if(e < eps){
            //k++;
            break;
        }

        double rr2=0;
        for(i=0;i<allnode;i++){
                rr2 += rr[i]*r[i];
        }
        beta = alpha/omega*rr2/rr0;

        for(i=0;i<allnode;i++){
            p[i] = r[i]+beta*(p[i]-omega*y[i]);
        }

       // rr0 = rr1;
    }

    for(i=0;i<allnode;i++){
        cout << x[i] << endl;
    }

    cout << "Bi-CGSTAB " << "iter " << k+1 << " err " << e << endl;

    return 1;
}

/*
10 3 1 2 4
3 18 2 -1 5
1 2 12 1 1 
2 -1 1 9 -4
4 5 1 -4 14
19 2 34 -49 83

5 -4 6
7 -6 10
4 9 7
8 14 74
*/