#include <iostream>
#include <math.h>
using namespace std;

#define max 100                 //繰り返し最大回数
#define eps 1.0e-5              //最小誤差許容範囲

int main(){
    int i,j,k;
    double err;
    int allnode;
    cin >> allnode;
    double* A=(double*)calloc(allnode*allnode,sizeof(double));
    double* AA=(double*)calloc(allnode*allnode,sizeof(double));
    double* b=(double*)calloc(allnode,sizeof(double));
    double* x=(double*)calloc(allnode,sizeof(double));
    double* r=(double*)calloc(allnode,sizeof(double));
    double* rr=(double*)calloc(allnode,sizeof(double));
    double* p=(double*)calloc(allnode,sizeof(double));
    double* pp=(double*)calloc(allnode,sizeof(double));
    double* y=(double*)calloc(allnode,sizeof(double));
    double* yy=(double*)calloc(allnode,sizeof(double));

    for(i=0;i<allnode;i++){
        for(j=0;j<allnode;j++){
            cin >> A[i*allnode+j];
        }
    }
    for(i=0;i<allnode;i++){
        for(j=0;j<allnode;j++){
            AA[j*allnode+i]=A[i*allnode+j];
        }
    }
    for(i=0;i<allnode;i++){
        cin >> b[i];  
    }

    //第0近似解に対する残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        r[i] = b[i]-Ax;
        p[i] = r[i];
    }
    //第0近似解に対するシャドウ残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        rr[i] = b[i]-Ax;
        pp[i] = rr[i];
    }

    double rr0=0;
    for(i=0;i<allnode;i++){
        rr0 += r[i]*r[i];
    }

    double alpha=0;
    double beta=0;
    double e=0.0;

    for(k=0;k<max;k++){
        //y=Apの計算
        for(i=0;i<allnode;i++){
            y[i]=0;
        }   
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                y[i] += A[i*allnode+j]*p[j];
            }
        }
        double py=0;
        for(i=0;i<allnode;i++){
                py += p[i]*y[i];
        }

        //alphaの計算
        double rr0=0;
        for(i=0;i<allnode;i++){
            rr0 += r[i]*r[i];
        }
        alpha=rr0/py;

        //update x and r 
        for(i=0;i<allnode;i++){
            x[i] += alpha*p[i];
            r[i] -= alpha*y[i];
        }
        //update x and r and rr[i]
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                yy[i] += AA[i*allnode+j]*p[j];
            }
        }
        for(i=0;i<allnode;i++){
            rr[i] -= alpha*yy[i];
        }

        double rr1=0;
        for(i=0;i<allnode;i++){
                rr1 += r[i]*r[i];
        }

        //convergence test
        e = sqrt(rr1);
        if(e < eps){
            k++;
            break;
        }

        beta = rr1/rr0;
        for(i=0;i<allnode;i++){
            p[i] = r[i]+beta*p[i];
        }
        for(i=0;i<allnode;i++){
            pp[i] = rr[i]+beta*pp[i];
        }

        rr0 = rr1;
    }

    for(i=0;i<allnode;i++){
        cout << x[i] << endl;
    }
    cout << k+1 << endl;
    cout << e << endl;

    return 1;

}

/*
10 3 1 2 4
3 18 2 -1 5
1 2 12 1 1 
2 -1 1 9 -4
4 5 1 -4 14
19 2 34 -49 83
*/