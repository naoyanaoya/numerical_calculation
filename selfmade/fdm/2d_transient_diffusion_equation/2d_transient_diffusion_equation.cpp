#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>

#define Nx 10
#define Ny 10
#define Nt 1000

int main(
    int argc,
    char* argv[]
)
{
    std::vector<double> x(Nx+1,0);
    std::vector<double> y(Ny+1,0);
    // std::vector<vector<double>> u(Ny+1,vector<double>(Nx+1,0));
    std::vector<std::vector<double>> u(Ny+1,std::vector<double>(Nx+1,0));
    std::vector<std::vector<double>> u_temp(Ny+1,std::vector<double>(Nx+1,0));
    // std::vector<double> u((Nx+1)*(Ny+1),0);

    double xmax = 1.0;
    double ymax = 1.0;
    double tmax = 10.0;

    double delta_x = xmax / Nx;
    double delta_y = ymax / Ny;
    double delta_t = tmax / Nt;
    double k = 0.01;
    double alpha = k * delta_t / (delta_x * delta_x);
    double beta = k * delta_t / (delta_y * delta_y);

    // coordinate setting
    for(int i=0;i<Nx+1;i++){
        x[i] = i * delta_x;
    }
    for(int j=0;j<Ny+1;j++){
        y[j] = j * delta_y;
    }

    // initial value setting 
    for(int i=0;i<Nx+1;i++){
        for(int j=0;j<Ny+1;j++){
            u[i][j] = 2 * x[i] * (1 - x[i]) * 2 * y[j] * (1 - y[j]);
        }
    }
    // for(int i=0;i<Nx+1;i++){
    //     for(int j=0;j<Ny+1;j++){
    //         std::cout << x[i] << " " << y[j] " " << u[i][j] << std::endl; 
    //     }
    // }
    // for(int j=0;j<Ny+1;j++){
    //     for(int i=0;i<Nx+1;i++){
    //         std::cout << u[i][j] << " ";
    //     }
    //     std::cout << std::endl;
    // }

    // std::ofstream ofile_u;
    // ofile_u.open("2d_transient_diffusion_equation.txt");

    std::ofstream ofile_u_initial;
    ofile_u_initial.open("./out/answer_0.vtk");

    ofile_u_initial << "# vtk DataFile Version 2.0"  << std::endl;
    ofile_u_initial << "vtk output" << std::endl;
    ofile_u_initial << "ASCII" << std::endl;
    ofile_u_initial << "DATASET UNSTRUCTURED_GRID" << std::endl;
    ofile_u_initial << "POINTS" << " " << (Nx+1) * (Ny+1) << " " << "float" << std::endl;
    for(int i=0;i<Nx+1;i++){
        for(int j=0;j<Ny+1;j++){
            ofile_u_initial << x[i] << " " << y[j] << " " << 0.0 << std::endl;
        }
    }
    ofile_u_initial << "CELLS" << " " << (Nx+1) * (Ny+1) << " " << (Nx+1) * (Ny+1) * 2 << std::endl;
    // for(int i=0;i<Nx+1;i++){
    //     for(int j=0;j<Ny+1;j++){
    //         ofile_u_initial << 1 << " " << i << std::endl;
    //     }
    // }
    for(int i=0;i<(Nx+1)*(Ny+1);i++){
        ofile_u_initial << 1 << " " << i << std::endl;
    }
    ofile_u_initial << "CELL_TYPES" << " " << (Nx+1) * (Ny+1) << std::endl;
    for(int i=0;i<Nx+1;i++){
        for(int j=0;j<Ny+1;j++){
            ofile_u_initial << 1 << std::endl;
        }
    }
    ofile_u_initial << "POINT_DATA" << " " << (Nx+1) * (Ny+1) << std::endl;
    ofile_u_initial << "SCALARS u float" << std::endl;
    ofile_u_initial << "LOOKUP_TABLE default" << std::endl;
    for(int i=0;i<Nx+1;i++){
        for(int j=0;j<Ny+1;j++){
            ofile_u_initial << u[i][j] << std::endl;
        }
    }
 
    // for(int i=0;i<Nx+1;i++){
    //     for(int j=0;j<Ny+1;j++){
    //         ofile_u_initial << x[i] << " " << y[j] << " " << u[i][j] << std::endl;
    //     }
    // }
    ofile_u_initial.close();


    for(int n=1;n<Nt;n++){

        for(int i=0;i<Nx+1;i++){
            for(int j=0;j<Ny+1;j++){
                u_temp[i][j] = u[i][j];
            }
        }

        // fdm
        for(int i=1;i<Nx;i++){
            for(int j=1;j<Ny;j++){
                u[i][j] = alpha * u_temp[i + 1][j] + alpha * u_temp[i - 1][j] + (1 - 2 * alpha - 2 * beta) * u_temp[i][j] + beta * u_temp[i][j + 1] + beta * u_temp[i][j - 1];
            }
        }

        // set boundary condition
        for(int i=0;i<Nx+1;i++){
            u[i][0] = 0.0;
            u[i][Ny] = 0.0;
        }
        for(int j=0;j<Nx+1;j++){
            u[0][j] = 0.0;
            u[Nx][j] = 0.0;
        }


        std::cout << "----------------STEP:" << n << "--------------" << std::endl;
        std::stringstream ss;
        std::string output_number;
        std::string file_name;
        std::ofstream ofile_u;
        ss << n;
        output_number = ss.str();
        file_name = "./out/answer_" + output_number + ".vtk"; 
        ofile_u.open(file_name.c_str());
 
        ofile_u << "# vtk DataFile Version 2.0"  << std::endl;
        ofile_u << "vtk output" << std::endl;
        ofile_u << "ASCII" << std::endl;
        ofile_u << "DATASET UNSTRUCTURED_GRID" << std::endl;
        ofile_u << "POINTS" << " " << (Nx+1) * (Ny+1) << " " << "float" << std::endl;
        for(int i=0;i<Nx+1;i++){
            for(int j=0;j<Ny+1;j++){
                ofile_u << x[i] << " " << y[j] << " " << 0.0 << std::endl;
            }
        }
        ofile_u << "CELLS" << " " << (Nx+1) * (Ny+1) << " " << (Nx+1) * (Ny+1) * 2 << std::endl;
        // for(int i=0;i<Nx+1;i++){
        //     for(int j=0;j<Ny+1;j++){
        //         ofile_u_initial << 1 << " " << i << std::endl;
        //     }
        // }
        for(int i=0;i<(Nx+1)*(Ny+1);i++){
            ofile_u << 1 << " " << i << std::endl;
        }
        ofile_u << "CELL_TYPES" << " " << (Nx+1) * (Ny+1) << std::endl;
        for(int i=0;i<Nx+1;i++){
            for(int j=0;j<Ny+1;j++){
                ofile_u << 1 << std::endl;
            }
        }
        ofile_u << "POINT_DATA" << " " << (Nx+1) * (Ny+1) << std::endl;
        ofile_u << "SCALARS u float" << std::endl;
        ofile_u << "LOOKUP_TABLE default" << std::endl;
        for(int i=0;i<Nx+1;i++){
            for(int j=0;j<Ny+1;j++){
                ofile_u << u[i][j] << std::endl;
            }
        }

        ofile_u.close();
    }

    return 0;
}