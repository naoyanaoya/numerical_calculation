import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, PillowWriter 

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

def update(frame):
    ax.plot(frame, 0, "o")

anim = FuncAnimation(fig, update, frames=range(8), interval=1000)

anim.save("c01.gif", writer="imagemagick") # 画面に表示させたい場合は plt.show() で良い
plt.close()