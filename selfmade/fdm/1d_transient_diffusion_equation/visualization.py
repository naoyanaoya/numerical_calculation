import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()
ax = fig.add_subplot(111)
#Line2D objectを入れるリスト
ims = []

for i in range(10):
    x = np.array(range(60))
    Y  = np.sin(x/5 - i * 5)
    im = ax.plot(Y, color='blue')
    ims.append(im) #各フレーム画像をimsに追加

#アニメの生成
ani = animation.ArtistAnimation(fig, ims, interval=100, blit=True, repeat_delay=1000)

#保存
ani.save("sample.gif", writer="pillow")

#表示
plt.show()