# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation

# # フィギュアオブジェクトの生成
# fig = plt.figure()

# # figure内にサブプロットを一つ配置
# ax = fig.add_subplot(1,1,1)

# # アニメーション更新用の関数
# def update_func(i):
#     # 前のフレームで描画されたグラフを消去
#     ax.clear()
    
#     x = np.arange(0, 1, 0.01)
#     y = 2 * np.sin(x-i)
#     ax.plot(x, y, "b")
#     # 軸ラベルの設定
#     ax.set_xlabel('x', fontsize=12)
#     ax.set_ylabel('y', fontsize=12)
#     # サブプロットタイトルの設定
#     ax.set_title('Frame: ' + str(i))
    
# ani = animation.FuncAnimation(fig, update_func, frames=10, interval=1000, repeat=True)

# # 表示
# plt.show()

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# フィギュアオブジェクトの生成
fig = plt.figure()

# figure内にサブプロットを一つ配置
ax = fig.add_subplot(1,1,1)

# アニメーション更新用の関数
def update(i,fig_title,A):
    # 前のフレームで描画されたグラフを消去
    ax.clear()

    x_list = []
    y_list = []
    open_file = "./out/answer_" + str(i * 10) + ".txt"
    with open(open_file) as f:
        for line in f:
            data = line[:-1].split(' ')
            x_list.append(float(data[0]))
            y_list.append(float(data[1]))
    ax.plot(x_list,y_list)        
    # x = np.arange(0, 1, 0.01)
    # y = 2 * np.sin(x-i)
    # ax.plot(x, y, "b")
    # 軸ラベルの設定
    ax.set_xlim(0,A)
    ax.set_ylim(0,0.5)
    ax.set_xlabel('x', fontsize=12)
    ax.set_ylabel('y', fontsize=12)
    ax.tick_params(direction = "in")
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    # サブプロットタイトルの設定
    ax.set_title(fig_title + "step:" + str(i * 10) + "\n" + str(np.average(y_list)))
    # ax.set_title(fig_title + str(i * 10))
    print(i * 10)
    
ani = animation.FuncAnimation(fig, update, fargs = ("diffusion equation",1), interval=1, frames=200)

# # 表示
# plt.show()

ani.save("1d_transient_diffusion_equation_1darray.gif", writer="imagemagick") # 画面に表示させたい場合は plt.show() で良い
plt.close()