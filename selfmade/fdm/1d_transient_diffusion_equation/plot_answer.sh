#!/bin/zsh

# set terminal font 'Arial.14';

# set terminal post eps color enhanced 'Arial' 25;

gnuplot -e "
    set title 'diffusionequation';
    set xlabel 'x';
    set ylabel 'u';
    set lmargin 10;
    set bmargin 4;
    set xrange[0:1];
    set yrange[0:0.6];
    do for [i=0:2000]{;
        plot './answer_'.i.'.txt';
    };
    pause -1;
"

# set output 'step1.eps';

# do for [i=0:2000]{;
#         plot 'answer_'.i.'.txt';
#     };

# set xlabel font 'Arial,30';
#     set ylabel font 'Arial,30';
#     set tics font 'Arial,10';
#     set title font 'Arial,30';
#     set key font 'Arial,15';