#include <iostream>
#include <math.h>
#include <fstream>
#include <sstream>
using namespace std;

// Number of divisions in space
#define Nx 100
// Number of divisions in time
#define Nt 2000

int main(
    int argc,
    char* argv[])
{
    double** u;
    // double* u;
    double* x;

    // u = (double*)calloc( ( (Nx+1) * (Nt+1) ),sizeof(double);
    u = (double**)calloc(Nt+1,sizeof(double*));
    for(int i=0;i<Nt+1;i++){
        u[i] = (double*)calloc(Nx+1,sizeof(double));
    }
    if(u == NULL){
        cout << "can not secure memory";
    }

    x = (double*)calloc(Nx+1,sizeof(double));
    if(x == NULL){
        cout << "can not secure memory";
    }

    // for(int n=0;n<Nt+1;n++){
    //     for(int i=0;i<Nx+1;i++){
    //         u[n][i]= i;
    //     }
    // }

    // for(int n=0;n<Nt+1;n++){
    //     for(int i=0;i<Nx+1;i++){
    //         cout << u[n][i] << " ";
    //     }
    //     cout << endl;
    // }

    double xmax;
    double tmax;

    double delta_x;
    double delta_t;
    double k;
    double alpha;

    // calculation area
    xmax = 1.0;
    // calcualtion time
    tmax = 1.0;

    delta_x = xmax/Nx;
    delta_t = tmax/Nt;
    k=0.1;
    alpha = k * delta_t / (delta_x * delta_x);

    for(int i=0;i<Nx+1;i++){
        x[i] = i * delta_x;
    }
    // for(int i=0;i<Nx+1;i++){
    //     cout << x[i] << endl;
    // }

    // initail value setting
    for(int i=0;i<Nx+1;i++){
        u[0][i] = 2 * x[i] * (1 - x[i]);
    }
    // for(int i=0;i<Nx+1;i++){
    //     cout << x[i] << " " << u[0][i] << endl;
    // }
    // for(int i=0;i<Nx+1;i++){
    //     cout << u[i][0] << endl;
    // }

    // for(int i=0;i<=Nx;i++){
    //     cout << u[i][0] << endl;
    // }

    for(int n=0;n<Nt;n++){
        for(int i=1;i<Nx;i++){
            u[n+1][i] = alpha * u[n][i+1] + (1 - 2 * alpha) * u[n][i] + alpha * u[n][i-1];
            // if(u[i][n+1] < 0){
            //     u[i][n+1] = 0;
            // }
        }
        // left boundary condition
        u[n+1][0] = 0;
        // right boundary condition
        u[n+1][Nx] = 0;

        // for(int i =0;i<=Nx;i++){
        //     cout << u[i][n] << endl;
        // }
    }

    // for(int i=0;i<Nx+1;i++){
    //     cout << x[i] << " " << u[0][i] << endl;
    // }

    ofstream fk;
    fk.open("./out/answer_0.txt");
    if(!fk){
        cout << "can not open the answer_0.txt" << endl;
        exit(1);
    }
    for(int i=0;i<=Nx;i++){
        fk << x[i] << " " << u[i][0] << endl;
    }
    fk.close();

    int n;

    for(int n=1;n<=Nt;n++){
        // if(n % 1 == 0){
        //     cout << n << endl;
        //     stringstream ss;
        //     string name;
        //     ofstream fo;
        //     ss << n;
        //     name = ss.str();
        //     name="answer_" + name + ".txt";
        //     fo.open(name.c_str());
        //     for(int i=0;i<=Nx;i++){
        //         fo << x[i] << " " << u[i][n] << endl;
        //     }
        //     fo.close();
        // }
        cout << n << endl;
        stringstream ss;
        string name;
        ofstream fo;
        ss << n;
        name = ss.str();
        name="./out/answer_" + name + ".txt";
        fo.open(name.c_str());
        for(int i=0;i<Nx+1;i++){
            fo << x[i] << " " << u[n][i] << endl;
        }
        fo.close();
    }


    for(int i=0;i<Nt+1;i++){
        free(u[i]);
    }
    free(u);
    free(x);

    return 0;
}