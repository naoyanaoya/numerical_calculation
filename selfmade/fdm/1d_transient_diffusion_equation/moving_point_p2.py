import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig = plt.figure()
ax = fig.add_subplot(111)

# def update(frame):
#     ax.cla() # ax をクリア
#     ax.set_xlim(0,10)
#     ax.set_ylim(0,10)
#     ax.plot(frame, frame, "o")

# anim = FuncAnimation(fig, update, frames=range(9), interval=1000)


def update(frame):
    ax.cla() # ax をクリア
    ax.set_xlim(0,10)
    ax.set_ylim(0,10)
    ax.plot(frame, frame, "o")

anim = FuncAnimation(fig, update, frames=range(9), interval=1000)

anim.save("c02.gif", writer="imagemagick")
plt.close()