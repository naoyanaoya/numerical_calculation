// #include <iostream>
// #include <math.h>
// #include <fstream>
// #include <sstream>
// using namespace std;

// #define Nx 100
// #define Nt 2000

// int main(
//     int argc,
//     char* argv[])
// {
//     double *x;
//     double *u;
//     double *u_temp;

//     x = (double*)calloc(Nx+1,sizeof(double));
//     if(x == NULL){
//         cout << "can not secure memory";
//     }
//     u = (double*)calloc(Nx+1,sizeof(double));
//     if(u == NULL){
//         cout << "can not secure memory";
//     }
//     u_temp = (double*)calloc(Nx+1,sizeof(double));
//     if(u_temp == NULL){
//         cout << "can not secure memory";
//     }

//     double xmax = 1.0;
//     double tmax = 1.0;

//     double delta_x = xmax/Nx;
//     double delta_t = tmax/Nt;
//     double k = 0.1;
//     double alpha = k * delta_t / (delta_x * delta_x);

//     // coordinate setting
//     for(int i=0;i<Nx+1;i++){
//         x[i] = i * delta_x;
//     }

//     // initial value setting
//     for(int i=0;i<Nx+1;i++){
//         u[i] = 2 * x[i] * (1 - x[i]);
//     }
//     for(int i=0;i<Nx+1;i++){
//         cout << u[i] << " ";
//     }

//     // insulation conditions
//     u[0] = u[1];
//     u[Nx] = u[Nx - 1];

//     ofstream fk; 
//     fk.open("./out/answer_0.txt");
//     if(!fk){
//         cout << endl;
//         cout << "==================can not open the answer_0.txt======================" << endl;
//         exit(1);
//     }
//     for(int i=0;i<Nx+1;i++){
//         fk << x[i] << " " << u[i] << endl;
//     }

//     for(int k=1;k<Nt+1;k++){
//         cout << k << endl;
//         for(int i=0;i<Nx+1;i++){
//             u_temp[i] = u[i];
//         }
//         for(int i=1;i<Nx;i++){
//             u[i] = alpha * u_temp[i + 1] + (1 - 2 * alpha) * u_temp[i] + alpha * u_temp[i - 1];
//         }
//         u[0] = u[1];
//         u[Nx] = u[Nx - 1];
//         stringstream ss;
//         string name;
//         ofstream fo;
//         ss << k;
//         name = ss.str();
//         name = "./out/answer_" + name + ".txt";
//         fo.open(name.c_str());
//         for(int i=0;i<Nx+1;i++){
//             fo << x[i] << " " << u[i] << endl;
//         }
//     }

//     free(x);
//     free(u);
//     return 0;
// }

#include <iostream>
#include <math.h>
#include <fstream>
#include <sstream>
using namespace std;

#define Nx 100
#define Nt 2000

int main(
    int argc,
    char* argv[])
{
    double *x;
    double *u;
    double *u_temp;

    x = (double*)calloc(Nx+1,sizeof(double));
    if(x == NULL){
        cout << "can not secure memory";
    }
    u = (double*)calloc(Nx+1,sizeof(double));
    if(u == NULL){
        cout << "can not secure memory";
    }
    u_temp = (double*)calloc(Nx+1,sizeof(double));
    if(u_temp == NULL){
        cout << "can not secure memory";
    }

    double xmax = 1.0;
    double tmax = 1.0;

    double delta_x = xmax/Nx;
    double delta_t = tmax/Nt;
    double k = 0.1;
    double alpha = k * delta_t / (delta_x * delta_x);

    // coordinate setting
    for(int i=0;i<Nx+1;i++){
        x[i] = i * delta_x;
    }

    // initial value setting
    for(int i=0;i<Nx+1;i++){
        u[i] = 2 * x[i] * (1 - x[i]);
    }
    for(int i=0;i<Nx+1;i++){
        cout << u[i] << " ";
    }

    // insulation conditions
    u[0] = u[1];
    u[Nx] = u[Nx - 1];

    ofstream fk; 
    fk.open("./out/answer_0.txt");
    if(!fk){
        cout << endl;
        cout << "==================can not open the answer_0.txt======================" << endl;
        exit(1);
    }
    for(int i=0;i<Nx+1;i++){
        fk << x[i] << " " << u[i] << endl;
    }

    for(int k=1;k<Nt+1;k++){
        cout << k << endl;
        for(int i=0;i<Nx+1;i++){
            u_temp[i] = u[i];
        }
        for(int i=1;i<Nx;i++){
            u[i] = alpha * u_temp[i + 1] + (1 - 2 * alpha) * u_temp[i] + alpha * u_temp[i - 1];
        }
        u[0] = u[1];
        u[Nx] = u[Nx - 1];
        stringstream ss;
        string name;
        ofstream fo;
        ss << k;
        name = ss.str();
        name = "./out/answer_" + name + ".txt";
        fo.open(name.c_str());
        for(int i=0;i<Nx+1;i++){
            fo << x[i] << " " << u[i] << endl;
        }
    }

    free(x);
    free(u);
    return 0;
}