#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
using namespace std;

typedef struct
{
    double* f;
    double* ff;
    double* x;
    double* y;
    double* u;

    int e_x;
    int e_y;
    int n_x;
    int n_y;
    int allnode;
    int nnnn;
    int allelement;
    double d[2];
    double v[2];
    double dt;
    int n_step;
    double aa;
    double s;
    double q;
    double theta;
    double l_x;
    double l_y;
    double dx;
    double dy;
}CALC_POINTS;

typedef struct
{
    double* mat1;
    double* mat2;
    double* ele_diff_mat1;
    double* ele_diff_mat2;
    double* ele_mass_mat;
    double* ele_adve_mat1;
    double* ele_adve_mat2;
    double* mass_mat;
    double* diff_mat;
    double* adve_mat;
    double* tmp;
    double* rhs;
}LIN_SYS;

void display_mat1(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                cout << ls->mat1[allnode*j+i] << " ";
            }
            cout << endl;
        }
}

void display_mat2(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                cout << ls->mat2[allnode*j+i] << " ";
            }
            cout << endl;
        }
}

void display_rhs(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << ls->rhs[i] << endl;
    }
}

void display_u(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << cp->u[i] << endl;
    }
}

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS*     ls,
    int     allnode,
    int        nnnn)
{
    cp->f=(double*)calloc(allnode,sizeof(double));
    cp->ff=(double*)calloc(allnode,sizeof(double));
    cp->x=(double*)calloc(allnode,sizeof(double));
    cp->y=(double*)calloc(allnode,sizeof(double));
    cp->u=(double*)calloc(allnode,sizeof(double));
    ls->mat1=(double*)calloc(nnnn,sizeof(double));
    ls->mat2=(double*)calloc(nnnn,sizeof(double));
    ls->tmp=(double*)calloc(nnnn,sizeof(double));
    ls->ele_diff_mat1=(double*)calloc(9,sizeof(double));
    ls->ele_diff_mat2=(double*)calloc(9,sizeof(double));
    ls->ele_mass_mat=(double*)calloc(9,sizeof(double));
    ls->ele_adve_mat1=(double*)calloc(9,sizeof(double));
    ls->ele_adve_mat2=(double*)calloc(9,sizeof(double));
    ls->mass_mat=(double*)calloc(nnnn,sizeof(double));
    ls->diff_mat=(double*)calloc(nnnn,sizeof(double));
    ls->adve_mat=(double*)calloc(nnnn,sizeof(double));
    ls->rhs=(double*)calloc(allnode,sizeof(double));
}

void memory_free(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    free(cp->f);
    free(cp->ff);
    free(cp->x);
    free(cp->y);
    free(cp->u);
    free(ls->mat1);
    free(ls->mat2);
    free(ls->tmp);
    free(ls->ele_diff_mat1);
    free(ls->ele_diff_mat2);
    free(ls->ele_mass_mat);
    free(ls->ele_adve_mat1);
    free(ls->ele_adve_mat2);
    free(ls->mass_mat);
    free(ls->diff_mat);
    free(ls->adve_mat);
    free(ls->rhs);
}

void set_calc_points(
    CALC_POINTS*    cp)
{
    int allnode=cp->allnode;
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    double dx=l_x/(n_x-1.0);
    double dy=l_y/(n_y-1.0);

    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_x;j++){
            if(i%n_x==j){
                cp->x[i]=dx*j;
            } 
        }
    }
    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_y;j++){
            if(0+n_y*j<=i && i<=e_y+n_y*j){
                cp->y[i]=dy*j;
            }
            else{
                continue;
            }
        }
    }
}

void set_first_condition(
    CALC_POINTS*    cp)
{
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cp->ff[i]=abs(cp->x[i]*(cp->x[i]-l_x)*cp->y[i]*(cp->y[i]-l_y));
    }
    for(int i=0;i<allnode;i++){
        cp->u[i]=cp->ff[i];
    }

    // for(int i=0;i<allnode;i++){
    //     if(0.25 < cp->x[i] && cp->x[i] < .75 && 0.25 < cp->y[i] && cp->y[i] < 0.75){
    //         cp->ff[i]=1.0;
    //     }
    // }
    // for(int i=0;i<allnode;i++){
    //     cp->u[i]=cp->ff[i];
    // }
    
}

void set_element_diffusion_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    double d_x=cp->d[0];
    double d_y=cp->d[1];

    ls->ele_diff_mat1[0]=0.5*d_x+0*d_y;
    ls->ele_diff_mat1[1]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat1[2]=0*d_x+0*d_y;
    ls->ele_diff_mat1[3]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat1[4]=0.5*d_x+0.5*d_y;
    ls->ele_diff_mat1[5]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat1[6]=0*d_x+0*d_y;
    ls->ele_diff_mat1[7]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat1[8]=0*d_x+0.5*d_y;

    ls->ele_diff_mat2[0]=0*d_x+0.5*d_y;
    ls->ele_diff_mat2[1]=0*d_x+0*d_y;
    ls->ele_diff_mat2[2]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat2[3]=0*d_x+0*d_y;
    ls->ele_diff_mat2[4]=0.5*d_x+0*d_y;
    ls->ele_diff_mat2[5]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat2[6]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat2[7]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat2[8]=0.5*d_x+0.5*d_y;
}

void set_element_mass_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    double s=cp->s;
    ls->ele_mass_mat[0]=s/12*2;
    ls->ele_mass_mat[1]=s/12*1;
    ls->ele_mass_mat[2]=s/12*1;
    ls->ele_mass_mat[3]=s/12*1;
    ls->ele_mass_mat[4]=s/12*2;
    ls->ele_mass_mat[5]=s/12*1;
    ls->ele_mass_mat[6]=s/12*1;
    ls->ele_mass_mat[7]=s/12*1;
    ls->ele_mass_mat[8]=s/12*2;
}

void set_element_advection_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    double v_x=cp->v[0];
    double v_y=cp->v[1];
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    double dx=l_x/(n_x-1.0);
    double dy=l_y/(n_y-1.0);

    ls->ele_adve_mat1[0]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[1]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[2]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[3]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[4]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[5]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[6]=(-1)*(v_x*0+v_y*(dy))/6;
    ls->ele_adve_mat1[7]=(-1)*(v_x*0+v_y*(dy))/6;
    ls->ele_adve_mat1[8]=(-1)*(v_x*0+v_y*(dy))/6;

    ls->ele_adve_mat2[0]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[1]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[2]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[3]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[4]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[5]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[6]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;
    ls->ele_adve_mat2[7]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;
    ls->ele_adve_mat2[8]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;

    // ls->ele_adve_mat1[0]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[1]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[2]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[3]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[4]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[5]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[6]=(v_x*0+v_y*(dy))/6;
    // ls->ele_adve_mat1[7]=(v_x*0+v_y*(dy))/6;
    // ls->ele_adve_mat1[8]=(v_x*0+v_y*(dy))/6;

    // ls->ele_adve_mat2[0]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[1]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[2]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[3]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[4]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[5]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[6]=(v_x*((-1)*dx)+v_y*(dy))/6;
    // ls->ele_adve_mat2[7]=(v_x*((-1)*dx)+v_y*(dy))/6;
    // ls->ele_adve_mat2[8]=(v_x*((-1)*dx)+v_y*(dy))/6;
}

void assemble_mass_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_diff_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mass_mat[(allnode+1)*i+0]+=ls->ele_mass_mat[0];
            ls->mass_mat[(allnode+1)*i+1]+=ls->ele_mass_mat[1];
            ls->mass_mat[(allnode+1)*i+n_x+1]+=ls->ele_mass_mat[2];
            ls->mass_mat[(allnode+1)*i+0+allnode]+=ls->ele_mass_mat[3];
            ls->mass_mat[(allnode+1)*i+1+allnode]+=ls->ele_mass_mat[4];
            ls->mass_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_mass_mat[5];
            ls->mass_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_mass_mat[6];
            ls->mass_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_mass_mat[7];
            ls->mass_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_mass_mat[8];
        }
    }
    //assemble ele_diff_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mass_mat[(allnode+1)*i+0]+=ls->ele_mass_mat[0];
            ls->mass_mat[(allnode+1)*i+n_x+1]+=ls->ele_mass_mat[1];
            ls->mass_mat[(allnode+1)*i+n_x]+=ls->ele_mass_mat[2];
            ls->mass_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_mass_mat[3];
            ls->mass_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_mass_mat[4];
            ls->mass_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_mass_mat[5];
            ls->mass_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_mass_mat[6];
            ls->mass_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_mass_mat[7];
            ls->mass_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_mass_mat[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mass_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void assemble_diffusion_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_diff_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_diff_mat1[0];
            ls->diff_mat[(allnode+1)*i+1]+=ls->ele_diff_mat1[1];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_diff_mat1[2];
            ls->diff_mat[(allnode+1)*i+0+allnode]+=ls->ele_diff_mat1[3];
            ls->diff_mat[(allnode+1)*i+1+allnode]+=ls->ele_diff_mat1[4];
            ls->diff_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_diff_mat1[5];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_diff_mat1[6];
            ls->diff_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_diff_mat1[7];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_diff_mat1[8];
        }
    }
    //assemble ele_diff_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_diff_mat2[0];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_diff_mat2[1];
            ls->diff_mat[(allnode+1)*i+n_x]+=ls->ele_diff_mat2[2];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_diff_mat2[3];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_diff_mat2[4];
            ls->diff_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_diff_mat2[5];
            ls->diff_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_diff_mat2[6];
            ls->diff_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_diff_mat2[7];
            ls->diff_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_diff_mat2[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->diff_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void assemble_advection_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_adve_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_adve_mat1[0];
            ls->diff_mat[(allnode+1)*i+1]+=ls->ele_adve_mat1[1];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_adve_mat1[2];
            ls->diff_mat[(allnode+1)*i+0+allnode]+=ls->ele_adve_mat1[3];
            ls->diff_mat[(allnode+1)*i+1+allnode]+=ls->ele_adve_mat1[4];
            ls->diff_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_adve_mat1[5];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_adve_mat1[6];
            ls->diff_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_adve_mat1[7];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_adve_mat1[8];
        }
    }
    //assemble ele_adve_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_adve_mat2[0];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_adve_mat2[1];
            ls->diff_mat[(allnode+1)*i+n_x]+=ls->ele_adve_mat2[2];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_adve_mat2[3];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_adve_mat2[4];
            ls->diff_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_adve_mat2[5];
            ls->diff_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_adve_mat2[6];
            ls->diff_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_adve_mat2[7];
            ls->diff_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_adve_mat2[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->diff_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void make_mat1(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    int nnnn=cp->nnnn;
    double dt=cp->dt;
    double theta=cp->theta;
    for(int i=0;i<nnnn;i++){
        ls->mat1[i]=ls->mass_mat[i]/dt + (ls->diff_mat[i]+ls->adve_mat[i])*theta;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat1[allnode*j+i] << " ";
    //     }
    //     cout << ls->rhs[j] << endl;
    // }
}

void make_mat2(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    int nnnn=cp->nnnn;
    double dt=cp->dt;
    double theta=cp->theta;
    for(int i=0;i<nnnn;i++){
        ls->mat2[i]=ls->mass_mat[i]/dt - (ls->diff_mat[i]+ls->adve_mat[i])*(1-theta);
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat2[allnode*j+i] << " ";
    //     }
    //     cout << ls->rhs[j] << endl;
    // }
}

void make_rhs(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    int nnnn=cp->nnnn;
    for(int j=0;j<allnode;j++){
        ls->rhs[j]=0;
    }
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                ls->rhs[j] += ls->mat2[j * allnode + i] * cp->u[i];
        }
    }
    display_mat2;

    // display_u;

    // for(int i=0;i<allnode;i++){
    //         ls->rhs[4]+=ls->mat2[36+i]*cp->u[i];
    // }
}


void set_bc(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double aa=cp->aa;
    double tmp=0;
    
    for(int j=0;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[j*allnode+i]=0;
        }
        ls->mat1[j*allnode+j]=1;
    }
    for(int j=1;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[n_x*j*allnode+i]=0;
        }
        ls->mat1[n_x*j*allnode+n_x*j]=1;
    }
    for(int j=0;j<e_x-1;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[(e_x+n_x+n_x*j)*allnode+i]=0;
        }
        ls->mat1[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[j*allnode+i]=0;
        }
        ls->mat1[j*allnode+j]=1;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat1[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }

    for(int j=0;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[j*allnode+i]=0;
        }
        ls->mat2[j*allnode+j]=1;
    }
    for(int j=1;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[n_x*j*allnode+i]=0;
        }
        ls->mat2[n_x*j*allnode+n_x*j]=1;
    }
    for(int j=0;j<e_x-1;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[(e_x+n_x+n_x*j)*allnode+i]=0;
        }
        ls->mat2[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[j*allnode+i]=0;
        }
        ls->mat2[j*allnode+j]=1;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat2[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void set_every_bc(
    CALC_POINTS*    cp,
    LIN_SYS*        ls
)
{
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double aa=cp->aa;
    for(int j=0;j<n_x;j++){
        ls->rhs[j]=aa;
    }
    for(int j=1;j<n_x;j++){
        ls->rhs[n_x*j]=aa;
    }
    for(int j=0;j<e_x-1;j++){
        ls->rhs[e_x+n_x+n_x*j]=aa;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        ls->rhs[j]=aa;
    }
}

void solve_mat_GJ(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int allnode=cp->allnode;
    int nnnn=cp->nnnn;
    for(int i=0;i<nnnn;i++){
        ls->tmp[i]=ls->mat1[i];
    }
    double r=0;
    for(int k=0;k<allnode-1;k++){
        for(int i=(k+1);i<allnode;i++){
            r=ls->mat1[allnode*i+k]/ls->mat1[allnode*k+k];
            ls->mat1[allnode*i+k]=0;
            for(int j=(k+1);j<allnode;j++){
                ls->mat1[allnode*i+j] = ls->mat1[allnode*i+j] - ls->mat1[allnode*k+j]*r;
            }
            ls->rhs[i] = ls->rhs[i] - ls->rhs[k]*r;
        }
    }
    for(int i=allnode-1;i>=0;i--){
        for(int j=i+1;j<allnode;j++){
            ls->rhs[i] = ls->rhs[i] - ls->mat1[allnode*i+j]*ls->rhs[j];
            ls->mat1[allnode*i+j]=0;
        }
        ls->rhs[i] = ls->rhs[i]/ls->mat1[allnode*i+i];
        ls->mat1[allnode*i+i]=1.0;
    }
    for(int i=0;i<nnnn;i++){
        ls->mat1[i]=ls->tmp[i];
    }

    for(int i=0;i<allnode;i++){
        cp->u[i] = ls->rhs[i];
    }
}

static const int MAX_ITERATIONS=10000;
static const double EPSIRON=0.0001;
void solve_mat_GS(
    double* sol,
    double* mat1,
    double* rhs,
    int allnode,
    int n_x)
{
    for(int l=0;l<MAX_ITERATIONS;l++){
        for(int i=0;i<allnode;i++){
            sol[i]=rhs[i];
            for(int j=0;j<allnode;j++){
                if(i != j){
                    sol[i] -= mat1[i*allnode+j]*sol[j];
                }
            }      
            sol[i] /= mat1[i*allnode+i];      
        }

        double residual=0.0;
        for(int i=0;i<allnode;i++){
            double r_i = -rhs[i];
            for(int j=0;j<allnode;j++){
                r_i += mat1[i*allnode+j]*sol[j];
            }
            residual += r_i*r_i;
        }
        residual=sqrt(residual);
        printf("GS_loop %d: %e\n", l, residual);
        if(residual < EPSIRON){
            return;
        }
    }
}

void write_first_gnuplot(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    ofstream fout("answer_0.txt"); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->u[i] << endl;
    }
    fout.close();

}

void write_gnuplot(
    CALC_POINTS* cp,
    int           k)
{
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << k;
    name=ss.str();
    name="out/answer_" + name + ".txt";
    ofstream fout(name.c_str()); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->u[i] << endl;
    }
    fout.close();
}

void write_first_vtk(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    ofstream fout("out/answer_0.vtk");
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << allnode << " " << "float" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << endl;
    }
    fout << "CELLS" << " " << allnode << " " << allnode*2 << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << allnode << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << allnode << endl;
    fout << "SCALARS u float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->u[i] << endl;
    }
    fout.close(); 
}

void write_vtk(
    CALC_POINTS* cp,
    int           k)
{
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << k;
    name=ss.str();
    name="out/answer_" + name + ".vtk";
    ofstream fout(name.c_str());
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << allnode << " " << "float" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << endl;
    }
    fout << "CELLS" << " " << allnode << " " << allnode*2 << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << allnode << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << allnode << endl;
    fout << "SCALARS u float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->u[i] << endl;
    }
    fout.close();
}

int main(){
    clock_t start=clock();

    CALC_POINTS cp;
    LIN_SYS     ls;

    int E_X=0;
    int E_Y=0;
    cout << "enter the E_X and E_Y" << endl;
    cin >> E_X >> E_Y;
    int N_X=E_X+1;
    int N_Y=E_Y+1;
    int ALLNODE=N_X*N_Y;
    int NNNN=ALLNODE*ALLNODE;
    int ALLELEMENT=E_X*E_Y*2;
    //double D[2]={0,0};
    double D[2]={0.01,0.01};
    double V[2]={0.1,0.1};
    double DT=0.001;
    int N_STEP=10000;
    double AA=0;
    double S=1.0/ALLELEMENT;
    double THETA=0.5;
    double L_X=1.0;
    double L_Y=1.0;

    cp.e_x=E_X;
    cp.e_y=E_Y;
    cp.n_x=N_X;
    cp.n_y=N_Y;
    cp.allnode=ALLNODE;
    cp.nnnn=NNNN;
    cp.allelement=ALLELEMENT;
    cp.d[0]=D[0];
    cp.d[1]=D[1];
    cp.v[0]=V[0];
    cp.v[1]=V[1];
    cp.n_step=N_STEP;
    cp.aa=AA;
    cp.s=S;
    cp.dt=DT;
    cp.theta=THETA;
    cp.l_x=L_X;
    cp.l_y=L_Y;

    memory_allocation(&cp,&ls,ALLNODE,NNNN);

    set_calc_points(&cp);

    set_first_condition(&cp);

    // write_first_gnuplot(&cp);

    write_first_vtk(&cp);

    set_element_mass_matrix(&cp,&ls);

    set_element_diffusion_matrix(&cp,&ls);

    set_element_advection_matrix(&cp,&ls);

    assemble_mass_matrix(&cp,&ls);

    assemble_diffusion_matrix(&cp,&ls);

    assemble_advection_matrix(&cp,&ls);

    make_mat1(&cp,&ls);

    make_mat2(&cp,&ls);

    set_bc(&cp,&ls);

    for(int k=1;k<=N_STEP;k++){
        cout << "--------STEP" << k << "------" <<endl;
    
        set_every_bc(&cp,&ls);

        make_rhs(&cp,&ls);
        
        solve_mat_GJ(&cp,&ls);
		//cout<<k<<endl;
        // write_gnuplot(&cp,k);
        if(k % 100 == 0){
            write_vtk(&cp,k);
        }
    }

    memory_free(&cp,&ls);

    clock_t end=clock();

    const double time=static_cast<double>(end-start)/CLOCKS_PER_SEC;
    cout << time << endl;

    return 0;
}

/*
do for [i=0:10000]{
    splot "answer_".i.".txt"
}
do for [i=0:5000]{
    splot "answer_".i.".txt"
}
splot "answer_0.txt","answer_10.txt","answer_20.txt","answer_30.txt","answer_40.txt","answer_50.txt","answer_60.txt","answer_70.txt","answer_80.txt","answer_90.txt","answer_100.txt"
*/