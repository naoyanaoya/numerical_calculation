#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<string>
#include<sstream>
using namespace std;
int main(){
	int i,j,k;
	int Ele=100;
	int allnode=Ele+1;
	double LL=1.0;
	double dx=LL/Ele;
	double dt=0.001;
	double NT=10000;
	double Diff=0.01;
	double theta;
	cout << "θを入力、θ=0の場合、Eulerの前進差分法（陽解法）、θ=0.5の場合、Crank-Nicolson法（半員解法）、θ=1の場合、後退差分法（完全陰解法）" << endl;
	cin >> theta;
	double D0=0.0;
	double D1=0.0;

	double *b=new double[allnode];
	double *u=new double[allnode];
	double *f=new double[allnode];
	double *AD=new double[allnode];
	double *AL=new double[allnode];
	double *AR=new double[allnode];
	double *BD=new double[allnode];
	double *BL=new double[allnode];
	double *BR=new double[allnode];
    double *keijyoukansuu=new double[allnode];

	ofstream fk;
	fk.open("Answer_0.txt");

	//initial condition
	for(i=0;i<allnode;i++){
		u[i]=min(dx*float(i),1.0-dx*float(i));
		fk << dx*float(i) << " " << u[i] << endl;
	}

	//make matrix
	//initialization
	for(i=0;i<allnode;i++){
		AD[i]=0.0;
		AL[i]=0.0;
		AR[i]=0.0;
		BD[i]=0.0;
		BL[i]=0.0;
		BR[i]=0.0;
	}
	for(i=0;i<allnode;i++){
		//A//
		//teporal//
		AD[i]+=dx*2.0/6.0/dt;
		AR[i]+=dx*1.0/6.0/dt;
		AL[i+1]+=dx*1.0/6.0/dt;
		AD[i+1]+=dx*2.0/6.0/dt;
		
		//diffusion//
		AD[i]+=theta*Diff/dx;
		AR[i]+=theta*-Diff/dx;
		AL[i+1]+=theta*-Diff/dx;
		AD[i+1]+=theta*Diff/dx;
		
		//B//
		//temporal//
		BD[i]+=dx*2.0/6.0/dt;
		BR[i]+=dx*1.0/6.0/dt;
		BL[i+1]+=dx*1.0/6.0/dt;
		BD[i+1]+=dx*2.0/6.0/dt;
		
		//diffusion//
		BD[i]+=-(1.0-theta)*Diff/dx;
		BR[i]+=-(1.0-theta)*-Diff/dx;
		BL[i+1]+=-(1.0-theta)*-Diff/dx;
		BD[i+1]+=-(1.0-theta)*Diff/dx;
	}

	//initroduce boundary
		AD[0]=1.0;
		AR[0]=0.0;
		BD[0]=1.0;
		BR[0]=0.0;
		AL[allnode-1]=0.0;
		AD[allnode-1]=1.0;
		BL[allnode-1]=0.0;
		BD[allnode-1]=1.0;

	for(i=1;i<=NT;i++){
		//forb
		for(j=0;j<allnode;j++){
			b[j]=BL[j]*u[j-1]+BD[j]*u[j]+BR[j]*u[j+1];
		}
		b[0]=D0;
		b[allnode-1]=D1;

		//TDMA
		double *P=new double[allnode];
		double *Q=new double[allnode];
		//first step
		P[0]=-AR[0]/AD[0];
		Q[0]=b[0]/AD[0];
		//second ste
		for(k=1;k<allnode;k++){
			P[k]=-AR[k]/(AD[k]+AL[k]*P[k-1]);
			Q[k]=(b[k]-AL[k]*Q[k-1])/(AD[k]+AL[k]*P[k-1]);
		}
		//third step
		u[allnode-1]=Q[allnode-1];
		//backward
		for(k=allnode-2;k>-1;k=k-1)
		{
			u[k]=P[k]*u[k+1]+Q[k];
		}
		
		delete [] P,Q;

		if(i%1000==0){
			cout << i << endl;
			stringstream ss;
			string name;
			ofstream fo;
			ss << i;
			name=ss.str();
			name="Answer_" + name + ".txt";
			fo.open(name.c_str ());
			for(j=0;j<allnode;j++){
				fo << dx*float(j) << " " << u[j] << endl;
			}
		}
	}
	delete[] b,u,f,AD,AL,AR,BD,BL,BR;
	return 0;
}

