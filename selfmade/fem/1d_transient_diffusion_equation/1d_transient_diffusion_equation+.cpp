#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
using namespace std;

typedef struct
{
    double* f;
    double* ff;
    double* x;
    double* u;
    double* h_x;

    int e_x;
    int n_x;
    int allnode;
    int allelement;
    double d;
    double dt;
    int n_step;
    double theta;
}CALC_POINTS;

typedef struct
{
    double* mat;
    double* ele_diff_mat;
    double* diff_mat;
    double* tmp;
    double* rhs;
}LIN_SYS;

void display_mat(
    CALC_POINTS* cp,
    LIN_SYS* ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
        for(int i=0;i<allnode;i++){
            cout << ls->mat[allnode*j+i] << " "; 
        }
        cout << endl;
    }
}

void display_rhs(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << ls->rhs[i] << endl;
    }
}

void display_u(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << cp->u[i] << endl;
    }
}

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS* ls)
{
    int allnode=cp->allnode;
    int allelement=cp->allelement;
    cp->f=(double*)calloc(allnode,sizeof(double));
    cp->ff=(double*)calloc(allnode,sizeof(double));
    cp->x=(double*)calloc(allnode,sizeof(double));
    cp->u=(double*)calloc(allnode,sizeof(double));
    cp->h_x=(double*)calloc(allelement,sizeof(double));
    ls->mat=(double*)calloc(allnode*allnode,sizeof(double));
    ls->tmp=(double*)calloc(allnode*allnode,sizeof(double));
    ls->diff_mat=(double*)calloc(allnode*allnode,sizeof(double));
    ls->rhs=(double*)calloc(allnode,sizeof(double));
}

void memory_free(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    free(cp->f);
    free(cp->ff);
    free(cp->x);
    free(cp->u);
    free(cp->h_x);
    free(ls->tmp);
    free(ls->diff_mat);
    free(ls->rhs);
    cp->f=nullptr;
    cp->ff=nullptr;
    cp->x=nullptr;
    cp->u=nullptr;
    cp->h_x=nullptr;
    ls->tmp=nullptr;
    ls->diff_mat=nullptr;
    ls->rhs=nullptr;
}

void set_allelement_number(
    CALC_POINTS* cp)
{
    int number_of_line=0;
    ifstream fin2("element.txt");
    if(fin2.fail()){
        cout << "can not open the file" << endl;
    }
    string line;
    while(getline(fin2,line)){
        ++number_of_line;
    }
    cp->allelement=number_of_line;
}

void set_calc_points(
    CALC_POINTS* cp)
{
    // int allnode=cp->allnode;
    // int allelement=cp->allelement;
    // int e_x=cp->e_x;
    // int n_x=cp->n_x;
    int number_of_line=0;

    ifstream fin1("element.txt");
    if(fin1.fail()){
        cout << "can not open the file" << endl;
    }
    string line;
    while(getline(fin1,line)){
        ++number_of_line;
    }
    cout << "number_of_line = " << number_of_line << endl;
    fin1.close();

    ifstream fin2("element.txt");
    if(fin2.fail()){
        cout << "can not open the file" << endl;
    }
    for(int i=0;i<number_of_line;i++){
        fin2 >> cp->x[0+2*i] >> cp->x[1+2*i];
    }
    fin2.close();

    for(int i=0;i<number_of_line;i++){
        cout << cp->x[i] << endl;
    }
}

void set_h_x(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    int allelement=cp->allelement;
    for(int i=0;i<allelement;i++){
        cp->h_x[i]=cp->x[1+2*i]-cp->x[0+2*i];
    }
}

void set_first_condition(
    CALC_POINTS* cp)
{
}

int main(){
    clock_t start=clock();

    CALC_POINTS cp;
    LIN_SYS     ls;

    //set_allelement_number(&cp);

    int E_X;
    int N_X=E_X+1;
    int ALLNODE=N_X;
    int ALLELEMENT=E_X;
    double D=0.01;
    double DT=0.001;
    int N_STEP=10000;
    double THETA=0.5;

    memory_allocation(&cp, &ls);
    set_calc_points(&cp);

    return 0;
}