#include<iostream>
#include<fstream>
#include<math.h>
using namespace std;

int main(){
    int E;
    cout << "enter the E" << endl;
    cin >> E;
    double aa;
    cout << "enter the dirichlet boundary condition" << endl;
    cin >> aa;
    int Node=E+1;
    int n=Node*Node;
    int element=E*E*2;
    double S=1.0/element;
    double q=1*S/3;
    double A[n][n]={0};
    double u[n]={0};
    double b[n]={0};
    double F[n]={0};
    double FF[n]={0};
    const double LLx=1.0;
    const double LLy=1.0;
    double x[n]={0};
    double y[n]={0};
    double hx=LLx/E;
    double hy=LLy/E;
    int h=0;
    cout << "which?" << endl;
    cin >> h;

    if(h==1){
        //introducing coordinate
        for(int i=0;i<n;i++){
            for(int j=0;j<Node;j++){
                if(i%Node==j){
                    x[i]=hx*j;
                } 
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<Node;j++){
                if(0+Node*j<=i && i<=E+Node*j){
                    y[i]=hy*j;
                }
                else{
                    continue;
                }
            }
        }
        for(int i=0;i<n;i++){
            FF[i]=abs(x[i]*(x[i]-1)*y[i]*(y[i]-1));
        }
        for(int i=0;i<n;i++){
            F[i]=abs(2*x[i]*(x[i]-1)+2*y[i]*(y[i]-1));
        }

        //make element matrix
        double Ae1[3][3]={
            {1,-0.5,-0.5},
            {-0.5,0.5,0},
            {-0.5,0,0.5}
        };
        double Ae2[3][3]={
            {0.5,-0.5,0},
            {-0.5,1,-0.5},
            {0,-0.5,0.5}
        };

        //make A matrix
        for(int l=0;l<E;l++){
            for(int k=0;k<E;k++){
                A[0+Node*l+k][0+Node*l+k]+=Ae1[0][0];
                A[0+Node*l+k][1+Node*l+k]+=Ae1[0][1];
                A[0+Node*l+k][(Node)+Node*l+k]+=Ae1[0][2];
                A[1+Node*l+k][0+Node*l+k]+=Ae1[1][0];
                A[1+Node*l+k][1+Node*l+k]+=Ae1[1][1];
                A[1+Node*l+k][(Node)+Node*l+k]+=Ae1[1][2];
                A[(Node)+Node*l+k][0+Node*l+k]+=Ae1[2][0];
                A[(Node)+Node*l+k][1+Node*l+k]+=Ae1[2][1];
                A[(Node)+Node*l+k][(Node)+Node*l+k]+=Ae1[2][2];
            }
        }
        for(int l=0;l<E;l++){
            for(int k=0;k<E;k++){
                A[1+Node*l+k][1+Node*l+k]+=Ae2[0][0];
                A[1+Node*l+k][(Node+1)+Node*l+k]+=Ae2[0][1];
                A[1+Node*l+k][(Node+1-1)+Node*l+k]+=Ae2[0][2];
                A[(Node+1)+Node*l+k][1+Node*l+k]+=Ae2[1][0];
                A[(Node+1)+Node*l+k][(Node+1)+Node*l+k]+=Ae2[1][1];
                A[(Node+1)+Node*l+k][(Node+1-1)+Node*l+k]+=Ae2[1][2];
                A[(Node+1-1)+Node*l+k][1+Node*l+k]+=Ae2[2][0];
                A[(Node+1-1)+Node*l+k][(Node+1)+Node*l+k]+=Ae2[2][1];
                A[(Node+1-1)+Node*l+k][(Node+1-1)+Node*l+k]+=Ae2[2][2];
            }
        }

        //make b vector
        // for(int i=0;i<3;i++){
        //     f[i]=q;
        // }
        // for(int k=0;k<E;k++){
        //     for(int i=0;i<E;i++){
        //     b[0+Node*k+i]+=f[0];
        //     b[1+Node*k+i]+=f[1];
        //     b[Node+Node*k+i]+=f[2];
        //     }
        // }
        // for(int k=0;k<E;k++){
        //     for(int i=0;i<E;i++){
        //     b[1+Node*k+i]+=f[0];
        //     b[1+Node+Node*k+i]+=f[1];
        //     b[Node+Node*k+i]+=f[2];
        //     }
        // }
        for(int l=0;l<E;l++){
            for(int k=0;k<E;k++){
                b[0+Node*l+k]+=S/12*(2*F[0+Node*l+k]+1*F[1+Node*l+k]+1*F[Node+Node*l+k]);
                b[1+Node*l+k]+=S/12*(1*F[0+Node*l+k]+2*F[1+Node*l+k]+1*F[Node+Node*l+k]);
                b[Node+Node*l+k]+=S/12*(1*F[0+Node*l+k]+1*F[1+Node*l+k]+2*F[Node+Node*l+k]);
            }
        }
        for(int l=0;l<E;l++){
            for(int k=0;k<E;k++){
                b[1+Node*l+k]+=S/12*(2*F[1+Node*l+k]+1*F[(Node+1)+Node*l+k]+1*F[(Node)+Node*l+k]);
                b[(Node+1)+Node*l+k]+=S/12*(1*F[1+Node*l+k]+2*F[(Node+1)+Node*l+k]+1*F[(Node)+Node*l+k]);
                b[(Node)+Node*l+k]+=S/12*(1*F[1+Node*l+k]+1*F[(Node+1)+Node*l+k]+2*F[(Node)+Node*l+k]);
            }
        }

        //dixplay A|b
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j==n-1){
                    cout << A[i][j] << " " << b[i] << endl;
                }
                else{
                    cout << A[i][j] << " ";
                }
            }
        }

        //introducing boundary condition
        for(int j=0;j<Node;j++){
            for(int i=0;i<n;i++){
                A[j][i]=0;
                b[j]=aa;
            }
            A[j][j]=1;
        }
        for(int j=1;j<Node;j++){
            for(int i=0;i<n;i++){
                A[Node*j][i]=0;
                b[Node*j]=aa;
            }
            A[Node*j][Node*j]=1;
        }
        for(int j=0;j<E-1;j++){
            for(int i=0;i<n;i++){
                A[E+Node+Node*j][i]=0;
                b[E+Node+Node*j]=aa;
            }
            A[E+Node+Node*j][E+Node+Node*j]=1;
        }
        for(int j=E*Node+1;j<Node*Node;j++){
            for(int i=0;i<n;i++){
                A[j][i]=0;
                b[j]=aa;
            }
            A[j][j]=1;
        }
        // for(int j=0;j<Node;j++){
        //     for(int i=0;i<n;i++){
        //         A[j][i]=0;
        //         b[j]=0;
        //     }
        //     A[j][j]=1;
        // }
        // for(int j=1;j<Node;j++){
        //     for(int i=0;i<n;i++){
        //         A[Node*j][i]=0;
        //         b[Node*j]=0;
        //     }
        //     A[Node*j][Node*j]=1;
        // }
        // for(int j=0;j<E-1;j++){
        //     for(int i=0;i<n;i++){
        //         A[E+Node+Node*j][i]=0;
        //         b[E+Node+Node*j]=1;
        //     }
        // A[E+Node+Node*j][E+Node+Node*j]=1;
        // }
        // for(int j=E*Node+1;j<Node*Node;j++){
        //     for(int i=0;i<n;i++){
        //         A[j][i]=0;
        //         b[j]=1;
        //     }
        //     A[j][j]=1;
        // }
        //dixplay A|b
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j==n-1){
                    cout << A[i][j] << " " << b[i] << endl;
                }
                else{
                    cout << A[i][j] << " ";
                }
            }
        }

        
        //liner solver(gaussian)
        double r=0;
        for(int k=0;k<n-1;k++){
            for(int i=(k+1);i<n;i++){
                r = A[i][k]/A[k][k];
                A[i][k]=0.0;
                for(int j=k+1;j<n;j++){
                    A[i][j] = A[i][j] - A[k][j]*r;
                }
                b[i]=b[i]-b[k]*r;
            }
        }
        for(int i=n-1;i>=0;i--){
            for(int j=i+1;j<n;j++){
                b[i]=b[i]-A[i][j]*b[j];
                A[i][j]=0.0;
            }
            b[i]=b[i]/A[i][i];
            A[i][i]=1.0;
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j==n-1){
                    cout << A[i][j] << " " << b[i] << endl;
                }
                else{
                    cout << A[i][j] << " ";
                }
            }
        }
        for(int i=0;i<n;i++){
            u[i] = b[i];
        }
        for(int i=0;i<n;i++){
            cout << x[i] << " " << y[i] << " " << u[i] << endl;
        }

        ofstream fout("answer.txt"); 
        if(fout.fail()){
            cout << "出力ファイルをオープンできません" << endl;
            return 1;
        }
        for(int i=0;i<n;i++){
            fout << x[i] << " " << y[i] << " " << u[i] << endl;
        }
        fout.close();

        ofstream fout2("answer_theo.txt"); 
        if(fout2.fail()){
            cout << "出力ファイルをオープンできません" << endl;
            return 1;
        }
        for(int i=0;i<n;i++){
            fout2 << x[i] << " " << y[i] << " " << FF[i] << endl;
        }
        fout2.close();

        return 0;
    }
    else if(h==2){
        //introducing coordinate
    for(int i=0;i<n;i++){
        for(int j=0;j<Node;j++){
            if(i%Node==j){
                x[i]=hx*j;
            } 
            }
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<Node;j++){
            if(0+Node*j<=i && i<=E+Node*j){
                y[i]=hy*j;
            }
            else{
                continue;
            }
        }
    }
    for(int i=0;i<n;i++){
        FF[i]=abs(x[i]*(x[i]-1)*y[i]*(y[i]-1));
    }
    for(int i=0;i<n;i++){
        F[i]=abs(2*x[i]*(x[i]-1)+2*y[i]*(y[i]-1));
    }


    //make element matrix
    double Ae1[3][3]={
        {0.5,-0.5,0},
        {-0.5,1,-0.5},
        {0,-0.5,0.5}
    };
    double Ae2[3][3]={
        {0.5,0,-0.5},
        {0,0.5,-0.5},
        {-0.5,-0.5,1}
    };

    //make A matrix
    for(int l=0;l<E;l++){
        for(int k=0;k<E;k++){
            A[0+Node*l+k][0+Node*l+k]+=Ae1[0][0];
            A[0+Node*l+k][1+Node*l+k]+=Ae1[0][1];
            A[0+Node*l+k][(Node+1)+Node*l+k]=Ae1[0][2];
            A[1+Node*l+k][0+Node*l+k]+=Ae1[1][0];
            A[1+Node*l+k][1+Node*l+k]+=Ae1[1][1];
            A[1+Node*l+k][(Node+1)+Node*l+k]+=Ae1[1][2];
            A[(Node+1)+Node*l+k][0+Node*l+k]+=Ae1[2][0];
            A[(Node+1)+Node*l+k][1+Node*l+k]+=Ae1[2][1];
            A[(Node+1)+Node*l+k][(Node+1)+Node*l+k]+=Ae1[2][2];
        }
    }
    for(int l=0;l<E;l++){
        for(int k=0;k<E;k++){
            A[0+Node*l+k][0+Node*l+k]+=Ae2[0][0];
            A[0+Node*l+k][(Node+1)+Node*l+k]+=Ae2[0][1];
            A[0+Node*l+k][(Node)+Node*l+k]+=Ae2[0][2];
            A[(Node+1)+Node*l+k][0+Node*l+k]+=Ae2[1][0];
            A[(Node+1)+Node*l+k][(Node+1)+Node*l+k]+=Ae2[1][1];
            A[(Node+1)+Node*l+k][(Node)+Node*l+k]+=Ae2[1][2];
            A[(Node)+Node*l+k][0+Node*l+k]+=Ae2[2][0];
            A[(Node)+Node*l+k][(Node+1)+Node*l+k]+=Ae2[2][1];
            A[(Node)+Node*l+k][(Node)+Node*l+k]+=Ae2[2][2];
        }
    }

    //make b vector
    // for(int i=0;i<3;i++){
    //     f[i]=q;
    // }
    // for(int k=0;k<E;k++){
    //     for(int i=0;i<E;i++){
    //     b[0+Node*k+i]+=f[0];
    //     b[1+Node*k+i]+=f[1];
    //     b[Node+Node*k+i]+=f[2];
    //     }
    // }
    // for(int k=0;k<E;k++){
    //     for(int i=0;i<E;i++){
    //     b[1+Node*k+i]+=f[0];
    //     b[1+Node+Node*k+i]+=f[1];
    //     b[Node+Node*k+i]+=f[2];
    //     }
    // }
    for(int l=0;l<E;l++){
        for(int k=0;k<E;k++){
            b[0+Node*l+k]+=S/12*(2*F[0+Node*l+k]+1*F[1+Node*l+k]+1*F[(Node+1)+Node*l+k]);
            b[1+Node*l+k]+=S/12*(1*F[0+Node*l+k]+2*F[1+Node*l+k]+1*F[(Node+1)+Node*l+k]);
            b[(Node+1)+Node*l+k]+=S/12*(1*F[0+Node*l+k]+1*F[1+Node*l+k]+2*F[(Node+1)+Node*l+k]);
        }
    }
    for(int l=0;l<E;l++){
        for(int k=0;k<E;k++){
            b[0+Node*l+k]+=S/12*(2*F[0+Node*l+k]+1*F[(Node+1)+Node*l+k]+1*F[(Node)+Node*l+k]);
            b[(Node+1)+Node*l+k]+=S/12*(1*F[0+Node*l+k]+2*F[(Node+1)+Node*l+k]+1*F[(Node)+Node*l+k]);
            b[(Node)+Node*l+k]+=S/12*(1*F[0+Node*l+k]+1*F[(Node+1)+Node*l+k]+2*F[(Node)+Node*l+k]);
        }
    }

    
    //dixplay A|b
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(j==n-1){
                cout << A[i][j] << " " << b[i] << endl;
            }
            else{
                cout << A[i][j] << " ";
            }
        }
    }

    //introducing boundary condition
    for(int j=0;j<Node;j++){
        for(int i=0;i<n;i++){
            A[j][i]=0;
            b[j]=aa;
        }
        A[j][j]=1;
    }
    for(int j=1;j<Node;j++){
        for(int i=0;i<n;i++){
            A[Node*j][i]=0;
            b[Node*j]=aa;
        }
        A[Node*j][Node*j]=1;
    }
    for(int j=0;j<E-1;j++){
        for(int i=0;i<n;i++){
            A[E+Node+Node*j][i]=0;
            b[E+Node+Node*j]=aa;
        }
        A[E+Node+Node*j][E+Node+Node*j]=1;
    }
    for(int j=E*Node+1;j<Node*Node;j++){
        for(int i=0;i<n;i++){
            A[j][i]=0;
            b[j]=aa;
        }
        A[j][j]=1;
    }
    // for(int j=0;j<Node;j++){
    //     for(int i=0;i<n;i++){
    //         A[j][i]=0;
    //         b[j]=0;
    //     }
    //     A[j][j]=1;
    // }
    // for(int j=1;j<Node;j++){
    //     for(int i=0;i<n;i++){
    //         A[Node*j][i]=0;
    //         b[Node*j]=0;
    //     }
    //     A[Node*j][Node*j]=1;
    // }
    // for(int j=0;j<E-1;j++){
    //     for(int i=0;i<n;i++){
    //         A[E+Node+Node*j][i]=0;
    //         b[E+Node+Node*j]=1;
    //     }
    // A[E+Node+Node*j][E+Node+Node*j]=1;
    // }
    // for(int j=E*Node+1;j<Node*Node;j++){
    //     for(int i=0;i<n;i++){
    //         A[j][i]=0;
    //         b[j]=1;
    //     }
    //     A[j][j]=1;
    // }
    
    //liner solver(gaussian)
    double r=0;
    for(int k=0;k<n-1;k++){
        for(int i=(k+1);i<n;i++){
            r = A[i][k]/A[k][k];
            A[i][k]=0.0;
            for(int j=k+1;j<n;j++){
                A[i][j] = A[i][j] - A[k][j]*r;
            }
            b[i]=b[i]-b[k]*r;
        }
    }
    for(int i=n-1;i>=0;i--){
        for(int j=i+1;j<n;j++){
            b[i]=b[i]-A[i][j]*b[j];
            A[i][j]=0.0;
        }
        b[i]=b[i]/A[i][i];
        A[i][i]=1.0;
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(j==n-1){
                cout << A[i][j] << " " << b[i] << endl;
            }
            else{
                cout << A[i][j] << " ";
            }
        }
    }
    for(int i=0;i<n;i++){
        u[i] = b[i];
    }
    for(int i=0;i<n;i++){
        cout << x[i] << " " << y[i] << " " << u[i] << endl;
    }
    ofstream fout("answer+.txt"); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    for(int i=0;i<n;i++){
        fout << x[i] << " " << y[i] << " " << u[i] << endl;
    }
    fout.close();

    ofstream fout2("answer_theo+.txt"); 
    if(fout2.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    for(int i=0;i<n;i++){
        fout2 << x[i] << " " << y[i] << " " << FF[i] << endl;
    }
    fout2.close();

    return 0;
    }
    else{
        cout << "enter 1 or 2" << endl;
    }
}
