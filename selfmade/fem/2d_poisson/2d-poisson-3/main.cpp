#include<iostream>
#include<fstream>
#include<sstream>
#include<math.h>
#include<iomanip>

typedef struct
{
    double* x;
    double* y;
    double* u;
    double* v;
    double* w;
    double dx;
    double dy;
    double l_x;
    double l_y;
    double b;
    int n_x;
    int n_y;
    int e_x;
    int e_y;
    int number_of_nodes;
    int number_of_elements;
    int number_of_nodes_per_elements;
}CALC_POINTS;

typedef struct
{
    double* A;
    double* b;
    int** npe;
}LIN_SYS;

void solve_mat_GJ(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int NN = cp->number_of_nodes;
    double r = 0;
    for (int k = 0; k < NN - 1; k++){
        for (int i = (k + 1); i < NN; i++){
            r = ls->A[NN * i + k] / ls->A[NN * k + k];
            ls->A[NN * i + k] = 0;
            for (int j = (k + 1); j < NN; j++){
                ls->A[NN * i + j] = ls->A[NN * i + j] - ls->A[NN * k + j] * r;
            }
            ls->b[i] = ls->b[i] - ls->b[k] * r;
        }
    }
    for (int i = NN - 1; i >= 0; i--){
        for (int j = i + 1; j < NN; j++){
            ls->b[i] = ls->b[i] - ls->A[NN * i + j] * ls->b[j];
            ls->A[NN * i + j] = 0;
        }
        ls->b[i] = ls->b[i] / ls->A[NN * i + i];
        ls->A[NN * i + i] = 1.0;
    }

    for (int i = 0; i < NN; i++){
        cp->u[i] = ls->b[i];
    }
}

void solve_mat_GS(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    for (int n = 0; n <= 100; n++)
    {
        double s = 0;
        double e = 0;
        for (int i = 0; i < cp->number_of_nodes; i++)
        {
            s = ls->b[i];
            for (int j = 0; j <= (i - 1); j++) {
                s -= ls->A[i * cp->number_of_nodes + j] * cp->u[j];
            }
            for (int j = i + 1; j < cp->number_of_nodes; j++) {
                s -= ls->A[i * cp->number_of_nodes + j] * cp->u[j];
            }
            s /= ls->A[i * cp->number_of_nodes + i];
            e += fabs(s - cp->u[i]);
            cp->u[i] = s;
            std::cout << std::fixed << std::setprecision(10) << cp->u[i] << "\t";
        }
        std::cout << "\n";

        if (e <= 0.0000000001) {
            return;
        }
    }
    std::cout << "収束しない" << "\n";
}

void write_vtk(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    int e_x = cp->e_x;
    int NN = cp->number_of_nodes;
    int NE = cp->number_of_elements;

    std::stringstream ss;
    std::string name;
    ss << e_x;
    name = ss.str();
    name = "answer_numerical_" + name + ".vtk";
    std::ofstream fout(name.c_str());
    if (fout.fail()){
        std::cout << "出力ファイルをオープンできません" << "\n";
        return;
    }
    fout << "# vtk DataFile Version 2.0" << "\n";
    fout << "vtk output" << "\n";
    fout << "ASCII" << "\n";
    fout << "DATASET UNSTRUCTURED_GRID" << "\n";
    fout << "POINTS" << " " << NN << " " << "float" << "\n";
    for (int i = 0; i < NN; i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << "\n";
    }
    fout << "CELLS" << " " << NE << " " << NE * (cp->number_of_nodes_per_elements + 1) << "\n";
    for (int i = 0; i < NE; i++){
        fout << cp->number_of_nodes_per_elements << " " << ls->npe[i][0] << " " << ls->npe[i][1] << " " << ls->npe[i][2] << "\n";
    }
    fout << "CELL_TYPES" << " " << NE << "\n";
    for (int i = 0; i < NE; i++){
        fout << 5 << "\n";
    }
    fout << "POINT_DATA" << " " << NN << "\n";
    fout << "SCALARS u float" << "\n";
    fout << "LOOKUP_TABLE default" << "\n";
    for (int i = 0; i < NN; i++){
        fout << cp->u[i] << "\n";
    }
    fout << "VECTORS velocity float" << "\n";
    for (int i = 0; i < NN; i++) {
        fout << cp->w[i] << " " << cp->v[i] << " " << cp->u[i] << "\n";
    }
    fout.close();
}

int main() {
    CALC_POINTS cp;
    LIN_SYS ls;

    cp.b = 1.0;
    cp.e_x = 2;
    cp.e_y = 2;
    cp.n_x = cp.e_x + 1;
    cp.n_y = cp.e_y + 1;
    cp.number_of_nodes = cp.n_x * cp.n_y;
    cp.number_of_elements = cp.e_x * cp.e_y * 2;
    cp.number_of_nodes_per_elements = 3;
    cp.l_x = (double)1 / cp.e_x;
    cp.l_y = (double)1 / cp.e_y;

    std::cout << cp.number_of_elements << "\n";
    std::cout << cp.number_of_nodes << "\n";
    std::cout << cp.l_x << "\n";
    std::cout << cp.l_y << "\n";

    cp.x = (double*)calloc(cp.number_of_nodes, sizeof(double));
    cp.y = (double*)calloc(cp.number_of_nodes, sizeof(double));
    cp.u = (double*)calloc(cp.number_of_nodes, sizeof(double));
    cp.v = (double*)calloc(cp.number_of_nodes, sizeof(double));
    cp.w = (double*)calloc(cp.number_of_nodes, sizeof(double));

    ls.A = (double*)calloc(cp.number_of_nodes * cp.number_of_nodes, sizeof(double));
    ls.b = (double*)calloc(cp.number_of_nodes, sizeof(double));


    ls.npe = (int**)calloc(cp.number_of_elements, sizeof(int*));
    for (int i = 0; i < cp.number_of_elements; i++) {
        ls.npe[i] = (int*)calloc(3, sizeof(int));
    }

    ls.npe[0][0] = 0;
    ls.npe[0][1] = 3;
    ls.npe[0][2] = 4;
    ls.npe[1][0] = 0;
    ls.npe[1][1] = 4;
    ls.npe[1][2] = 1;
    ls.npe[2][0] = 1;
    ls.npe[2][1] = 4;
    ls.npe[2][2] = 5;
    ls.npe[3][0] = 1;
    ls.npe[3][1] = 5;
    ls.npe[3][2] = 2;
    ls.npe[4][0] = 3;
    ls.npe[4][1] = 6;
    ls.npe[4][2] = 7;
    ls.npe[5][0] = 3;
    ls.npe[5][1] = 7;
    ls.npe[5][2] = 4;
    ls.npe[6][0] = 4;
    ls.npe[6][1] = 7;
    ls.npe[6][2] = 8;
    ls.npe[7][0] = 4;
    ls.npe[7][1] = 8;
    ls.npe[7][2] = 5;

    for (int i = 0; i < cp.number_of_nodes; i++) {
        cp.x[i] = cp.l_x * (i / cp.n_x);
        cp.y[i] = cp.l_y * (i % cp.n_y);
    }

    // std::cout << "-----------------" << "\n";
    // for (int i = 0; i < cp.number_of_nodes; i++) {
    //     std::cout << cp.x[i] << " " << cp.y[i] << "\n";
    // }

    std::cout << "-----------------" << "\n";

    for (int ne = 0; ne < cp.number_of_elements; ne++) {
        double* a = (double*)calloc(cp.number_of_nodes_per_elements * cp.number_of_nodes_per_elements, sizeof(double));
        double* b = (double*)calloc(3, sizeof(double));
        double* c = (double*)calloc(3, sizeof(double));
        double* d = (double*)calloc(3, sizeof(double));
        if (ne % 2 == 0) {
            b[0] = (cp.y[ls.npe[ne][1]] - cp.y[ls.npe[ne][2]]) / ((0.5 * 0.5 / 2) * 2);
            b[1] = (cp.y[ls.npe[ne][2]] - cp.y[ls.npe[ne][0]]) / ((0.5 * 0.5 / 2) * 2);
            b[2] = (cp.y[ls.npe[ne][0]] - cp.y[ls.npe[ne][1]]) / ((0.5 * 0.5 / 2) * 2);
            c[0] = (cp.x[ls.npe[ne][2]] - cp.x[ls.npe[ne][1]]) / ((0.5 * 0.5 / 2) * 2);
            c[1] = (cp.x[ls.npe[ne][0]] - cp.x[ls.npe[ne][2]]) / ((0.5 * 0.5 / 2) * 2);
            c[2] = (cp.x[ls.npe[ne][1]] - cp.x[ls.npe[ne][0]]) / ((0.5 * 0.5 / 2) * 2);
        } else {
            b[0] = (cp.y[ls.npe[ne][1]] - cp.y[ls.npe[ne][2]]) / ((0.5 * 0.5 / 2) * 2);
            b[1] = (cp.y[ls.npe[ne][2]] - cp.y[ls.npe[ne][0]]) / ((0.5 * 0.5 / 2) * 2);
            b[2] = (cp.y[ls.npe[ne][0]] - cp.y[ls.npe[ne][1]]) / ((0.5 * 0.5 / 2) * 2);
            c[0] = (cp.x[ls.npe[ne][2]] - cp.x[ls.npe[ne][1]]) / ((0.5 * 0.5 / 2) * 2);
            c[1] = (cp.x[ls.npe[ne][0]] - cp.x[ls.npe[ne][2]]) / ((0.5 * 0.5 / 2) * 2);
            c[2] = (cp.x[ls.npe[ne][1]] - cp.x[ls.npe[ne][0]]) / ((0.5 * 0.5 / 2) * 2);
        }
        for (int i = 0; i < 3; i++) {
            d[i] = cp.b * ((0.5 * 0.5 / 2) * 2) / 2 / 3 * 1;
        }

        // std::cout << "------" << "\n";
        // std::cout << b[0] << " " << b[1] << " " << b[2] << "\n";
        // std::cout << c[0] << " " << c[1] << " " << c[2] << "\n";

        for (int ne = 0; ne < cp.number_of_nodes_per_elements; ne++) {
            for (int j = 0; j < cp.number_of_nodes_per_elements; j++) {
                a[ne * cp.number_of_nodes_per_elements + j] = ((0.5 * 0.5) / 2 * 2) / 2 * (b[ne] * b[j] + c[ne] * c[j]);
            }
        }

        // std::cout << "--------" << "\n";
        // for (int i = 0; i < cp.number_of_nodes_per_elements; i++) {
        //     for (int j = 0; j < cp.number_of_nodes_per_elements; j++) {
        //         std::cout << a[i * cp.number_of_nodes_per_elements + j] << " ";
        //     }
        //     std::cout << "\n";
        // }

        std::cout << std::fixed << std::setprecision(8);
        for (int i = 0; i < cp.number_of_nodes_per_elements; i++) {
            for (int j = 0; j < cp.number_of_nodes_per_elements; j++) {
                // std::cout << ls.npe[i][j] << " " << ls.npe[i][j] << " " << ls.npe[i][j] * cp.number_of_nodes + ls.npe[i][j] << " " << a[i * cp.number_of_nodes_per_elements + j] << "\n";
                ls.A[ls.npe[ne][i] * cp.number_of_nodes + ls.npe[ne][j]] += a[i * cp.number_of_nodes_per_elements + j];
            }
        }
        for (int i = 0; i < 3; i++) {
            ls.b[ls.npe[ne][i]] += d[i];
        }

        // for (int i = 0; i < cp.number_of_nodes; i++) {
        //     for (int j = 0; j < cp.number_of_nodes; j++) {
        //         printf("%+06.3f ", ls.A[i * cp.number_of_nodes + j]);
        //         // std::cout << ls.A[i * cp.number_of_nodes + j] << " ";
        //     }
        //     std::cout << "\n";
        // }
    }

    // for (int i = 0; i < cp.number_of_nodes; i++) {
    //     printf("%+06.3f \n", ls.b[i]);
    // }
    // exit(1);

    // boundary condition
    // 0, 1, 2, 3, 6
    int* bc = (int*)calloc(5, sizeof(int));
    bc[0] = 0;
    bc[1] = 1;
    bc[2] = 2;
    bc[3] = 3;
    bc[4] = 6;

    // for (int i = 0; i < cp.number_of_nodes; i++) {
    //     for (int j = 0; j < cp.number_of_nodes; j++) {
    //         printf("%+06.3f ", ls.A[i * cp.number_of_nodes + j]);
    //         // std::cout << ls.A[i * cp.number_of_nodes + j] << " ";
    //     }
    //     std::cout << "\n";
    // }

    for (int i = 0; i < cp.number_of_nodes; i++) {
        for (int j = 0; j < cp.number_of_nodes; j++) {
            for (int k = 0; k < 5; k++) {
                if (i == bc[k] && j == bc[k]) {
                    ls.A[i * cp.number_of_nodes + j] = 1.0;
                } else if (i == bc[k] || j == bc[k]) {
                    ls.A[i * cp.number_of_nodes + j] = 0.0;
                }
            }
        }
    }

    for (int i = 0; i < 5; i++) {
        ls.b[bc[i]] = 0.0;
        cp.u[bc[i]] = 0.0;
    }

    std::cout << "--------------------" << "\n";

    for (int i = 0; i < cp.number_of_nodes; i++) {
        for (int j = 0; j < cp.number_of_nodes; j++) {
            printf("%+06.3f ", ls.A[i * cp.number_of_nodes + j]);
            // std::cout << ls.A[i * cp.number_of_nodes + j] << " ";
        }
        std::cout << "\n";
    }
    for (int i = 0; i < cp.number_of_nodes; i++) {
        printf("%+06.3f \n", ls.b[i]);
    }

    // for (int i = 0; i < cp.number_of_nodes; i++) {
    //     for (int j = 0; j < cp.number_of_nodes; j++) {
    //         printf("%+06.3f ", ls.A[i * cp.number_of_nodes + j]);
    //         // std::cout << ls.A[i * cp.number_of_nodes + j] << " ";
    //     }
    //     std::cout << "\n";
    // }
    // for (int i = 0; i < cp.number_of_nodes; i++) {
    //     std::cout << ls.b[i] << "\n";
    // }

    for (int i = 0; i < cp.number_of_nodes; i++) {
        std::cout << cp.u[i] << "\n";
    }

    // solve_mat_GJ(&cp, &ls);
    solve_mat_GS(&cp, &ls);

    write_vtk(&cp, &ls);

    free(cp.x);
    free(cp.y);
    free(cp.u);
    for (int i = 0; i < cp.number_of_elements; i++) {
        free(ls.npe[i]);
    }
    free(ls.npe);

    return 0;
}
