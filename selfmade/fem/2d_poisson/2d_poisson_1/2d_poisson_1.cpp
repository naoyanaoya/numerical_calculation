#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
using namespace std;

typedef struct
{
    double* f;
    double* ff;
    double* x;
    double* y;
    double* u;

    int e_x;
    int e_y;
    int n_x;
    int n_y;
    int allnode;
    int nnnn;
    int allelement;
    double aa;
    double s;
    double q;
    double l_x;
    double l_y;
    double dx;
    double dy;
    // int max_iterations;
    // double epsiron;
}CALC_POINTS;

typedef struct
{
    double* mat;
    double* elemat1;
    double* elemat2;
    double* rhs;
}LIN_SYS;

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS*     ls,
    int     allnode,
    int        nnnn)
{
    cp->f=(double*)calloc(allnode,sizeof(double));
    cp->ff=(double*)calloc(allnode,sizeof(double));
    cp->x=(double*)calloc(allnode,sizeof(double));
    cp->y=(double*)calloc(allnode,sizeof(double));
    cp->u=(double*)calloc(allnode,sizeof(double));
    ls->mat=(double*)calloc(nnnn,sizeof(double));
    ls->elemat1=(double*)calloc(9,sizeof(double));
    ls->elemat2=(double*)calloc(9,sizeof(double));
    ls->rhs=(double*)calloc(allnode,sizeof(double));
}

void memory_free(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    free(cp->x);
    free(cp->y);
    free(cp->u);
    free(ls->mat);
    free(ls->elemat1);
    free(ls->elemat2);
    free(ls->rhs);
}

void set_calc_points(
    CALC_POINTS*    cp)
{
    int allnode=cp->allnode;
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    double dx=l_x/(n_x-1.0);
    double dy=l_y/(n_y-1.0);

    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_x;j++){
            if(i%n_x==j){
                cp->x[i]=dx*j;
            } 
        }
    }
    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_y;j++){
            if(0+n_y*j<=i && i<=e_y+n_y*j){
                cp->y[i]=dy*j;
            }
            else{
                continue;
            }
        }
    }
}

void set_theo(
    CALC_POINTS*    cp
)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cp->ff[i]=abs(cp->x[i]*(cp->x[i]-1)*cp->y[i]*(cp->y[i]-1));
    }
    for(int i=0;i<allnode;i++){
        cp->f[i]=(-1)*(2*cp->x[i]*(cp->x[i]-1)+2*cp->y[i]*(cp->y[i]-1));
    }
}

void set_element_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    // ls->elemat1[0]=1.0;
    // ls->elemat1[1]=-0.5;
    // ls->elemat1[2]=-0.5;
    // ls->elemat1[3]=-0.5;
    // ls->elemat1[4]=0.5;
    // ls->elemat1[5]=0;
    // ls->elemat1[6]=-0.5;
    // ls->elemat1[7]=0;
    // ls->elemat1[8]=0.5;

    // ls->elemat2[0]=0.5;
    // ls->elemat2[1]=-0.5;
    // ls->elemat2[2]=0;
    // ls->elemat2[3]=-0.5;
    // ls->elemat2[4]=1;
    // ls->elemat2[5]=-0.5;
    // ls->elemat2[6]=0;
    // ls->elemat2[7]=-0.5;
    // ls->elemat2[8]=0.5;

    ls->elemat1[0]=0.5;
    ls->elemat1[1]=-0.5;
    ls->elemat1[2]=0;
    ls->elemat1[3]=-0.5;
    ls->elemat1[4]=1;
    ls->elemat1[5]=-0.5;
    ls->elemat1[6]=0;
    ls->elemat1[7]=-0.5;
    ls->elemat1[8]=0.5;

    ls->elemat2[0]=0.5;
    ls->elemat2[1]=0;
    ls->elemat2[2]=-0.5;
    ls->elemat2[3]=0;
    ls->elemat2[4]=0.5;
    ls->elemat2[5]=-0.5;
    ls->elemat2[6]=-0.5;
    ls->elemat2[7]=-0.5;
    ls->elemat2[8]=1;
}

void set_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)\

{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double s=cp->s;
    double dx=cp->dx;
    double dy=cp->dy;

    //assemble elemat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mat[(allnode+1)*i+0]+=ls->elemat1[0];
            ls->mat[(allnode+1)*i+1]+=ls->elemat1[1];
            ls->mat[(allnode+1)*i+n_x+1]+=ls->elemat1[2];
            ls->mat[(allnode+1)*i+0+allnode]+=ls->elemat1[3];
            ls->mat[(allnode+1)*i+1+allnode]+=ls->elemat1[4];
            ls->mat[(allnode+1)*i+n_x+1+allnode]+=ls->elemat1[5];
            ls->mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->elemat1[6];
            ls->mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->elemat1[7];
            ls->mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->elemat1[8];
        }
    }
    //assemble Ae2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mat[(allnode+1)*i+0]+=ls->elemat2[0];
            ls->mat[(allnode+1)*i+n_x+1]+=ls->elemat2[1];
            ls->mat[(allnode+1)*i+n_x]+=ls->elemat2[2];
            ls->mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->elemat2[3];
            ls->mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->elemat2[4];
            ls->mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->elemat2[5];
            ls->mat[(allnode+1)*i+0+n_x*allnode]+=ls->elemat2[6];
            ls->mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->elemat2[7];
            ls->mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->elemat2[8];
        }
    }
    for(int l=0;l<e_y;l++){
        for(int k=0;k<e_x;k++){
            ls->rhs[0+n_x*l+k]+=s/12*(2*cp->f[0+n_x*l+k]+1*cp->f[1+n_x*l+k]+1*cp->f[n_x+n_x*l+k]);
            ls->rhs[1+n_x*l+k]+=s/12*(1*cp->f[0+n_x*l+k]+2*cp->f[1+n_x*l+k]+1*cp->f[n_x+n_x*l+k]);
            ls->rhs[n_x+n_x*l+k]+=s/12*(1*cp->f[0+n_x*l+k]+1*cp->f[1+n_x*l+k]+2*cp->f[n_x+n_x*l+k]);
        }
    }
    for(int l=0;l<e_y;l++){
        for(int k=0;k<e_x;k++){
            ls->rhs[1+n_x*l+k]+=s/12*(2*cp->f[1+n_x*l+k]+1*cp->f[(n_x+1)+n_x*l+k]+1*cp->f[n_x+n_x*l+k]);
            ls->rhs[(n_x+1)+n_x*l+k]+=s/12*(1*cp->f[1+n_x*l+k]+2*cp->f[(n_x+1)+n_x*l+k]+1*cp->f[n_x+n_x*l+k]);
            ls->rhs[n_x+n_x*l+k]+=s/12*(1*cp->f[1+n_x*l+k]+1*cp->f[(n_x+1)+n_x*l+k]+2*cp->f[n_x+n_x*l+k]);
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat[allnode*j+i] << " ";
    //     }
    //     cout << ls->rhs[j] << endl;
    // }
}

void set_bc(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double aa=cp->aa;
    double tmp=0;
    
    // for(int j=0;j<n_x;j++){
    //     for(int i=0;i<allnode;i++){
    //         ls->mat[j*allnode+i]=0;
    //     }
    //     ls->rhs[j]=aa;
    //     ls->mat[j*allnode+j]=1;
    //     for(int k=0;k<allnode;k++){
    //         if(k!=j){
    //             tmp=ls->rhs[j];
    //             ls->rhs[k]=ls->rhs[k]-tmp*ls->mat[j+k*allnode];
    //         }
    //     }
    // }
    // for(int j=1;j<n_x;j++){
    //     for(int i=0;i<allnode;i++){
    //         ls->mat[n_x*j*allnode+i]=0;
    //     }
    //     ls->rhs[n_x*j]=aa;
    //     ls->mat[n_x*j*allnode+n_x*j]=1;
    //     for(int k=0;k<allnode;k++){
    //         if(k!=j){
    //             tmp=ls->rhs[j];
    //             ls->rhs[k]=ls->rhs[k]-tmp*ls->mat[j+k*allnode];
    //         }
    //     }
    // }
    // for(int j=0;j<e_x-1;j++){
    //     for(int i=0;i<allnode;i++){
    //         ls->mat[(e_x+n_x+n_x*j)*allnode+i]=0;
    //     }
    //     ls->rhs[e_x+n_x+n_x*j]=aa;
    //     ls->mat[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    //     for(int k=0;k<allnode;k++){
    //         if(k!=j){
    //             tmp=ls->rhs[j];
    //             ls->rhs[k]=ls->rhs[k]-tmp*ls->mat[j+k*allnode];
    //         }
    //     }
    // }
    // for(int j=e_x*n_x+1;j<n_x*n_x;j++){
    //     for(int i=0;i<allnode;i++){
    //         ls->mat[j*allnode+i]=0;
    //     }
    //     ls->rhs[j]=aa;
    //     ls->mat[j*allnode+j]=1;
    //     for(int k=0;k<allnode;k++){
    //         if(k!=j){
    //             tmp=ls->rhs[j];
    //             ls->rhs[k]=ls->rhs[k]-tmp*ls->mat[j+k*allnode];
    //         }
    //     }
    // }

    for(int j=0;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat[j*allnode+i]=0;
        }
        ls->rhs[j]=aa;
        ls->mat[j*allnode+j]=1;
    }
    for(int j=1;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat[n_x*j*allnode+i]=0;
        }
        ls->rhs[n_x*j]=aa;
        ls->mat[n_x*j*allnode+n_x*j]=1;
    }
    for(int j=0;j<e_x-1;j++){
        for(int i=0;i<allnode;i++){
            ls->mat[(e_x+n_x+n_x*j)*allnode+i]=0;
        }
        ls->rhs[e_x+n_x+n_x*j]=aa;
        ls->mat[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat[j*allnode+i]=0;
        }
        ls->rhs[j]=aa;
        ls->mat[j*allnode+j]=1;
    }

    for(int j=0;j<allnode;j++){
        for(int i=0;i<allnode;i++){
            cout << ls->mat[allnode*j+i] << " ";
        }
        cout << ls->rhs[j] << endl;
    }
}

void solve_mat_GJ(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int allnode=cp->allnode;
    double r=0;
    for(int k=0;k<allnode-1;k++){
        for(int i=(k+1);i<allnode;i++){
            r=ls->mat[allnode*i+k]/ls->mat[allnode*k+k];
            ls->mat[allnode*i+k]=0;
            for(int j=(k+1);j<allnode;j++){
                ls->mat[allnode*i+j] = ls->mat[allnode*i+j] - ls->mat[allnode*k+j]*r;
            }
            ls->rhs[i] = ls->rhs[i] - ls->rhs[k]*r;
        }
    }
    for(int i=allnode-1;i>=0;i--){
        for(int j=i+1;j<allnode;j++){
            ls->rhs[i] = ls->rhs[i] - ls->mat[allnode*i+j]*ls->rhs[j];
            ls->mat[allnode*i+j]=0;
        }
        ls->rhs[i] = ls->rhs[i]/ls->mat[allnode*i+i];
        ls->mat[allnode*i+i]=1.0;
    }

    for(int i=0;i<allnode;i++){
        cp->u[i] = ls->rhs[i];
    }
}

static const int MAX_ITERATIONS=10000;
static const double EPSIRON=0.0001;
void solve_mat_GS(
    double* sol,
    double* mat,
    double* rhs,
    int allnode,
    int n_x)
{
    for(int l=0;l<MAX_ITERATIONS;l++){
        for(int i=0;i<allnode;i++){
            sol[i]=rhs[i];
            for(int j=0;j<allnode;j++){
                if(i != j){
                    sol[i] -= mat[i*allnode+j]*sol[j];
                }
            }      
            sol[i] /= mat[i*allnode+i];      
        }

        double residual=0.0;
        for(int i=0;i<allnode;i++){
            double r_i = -rhs[i];
            for(int j=0;j<allnode;j++){
                r_i += mat[i*allnode+j]*sol[j];
            }
            residual += r_i*r_i;
        }
        residual=sqrt(residual);
        printf("GS_loop %d: %e\n", l, residual);
        if(residual < EPSIRON){
            return;
        }
    }
}

void write_gnuplot(
    CALC_POINTS* cp)
{
    int e_x=cp->e_x;
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << e_x;
    name=ss.str();
    name="answer_" + name + ".txt";
    ofstream fout(name.c_str()); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->u[i] << endl;
    }
    fout.close();
}

void write_theo_gnuplot(
    CALC_POINTS* cp)
{
    int e_x=cp->e_x;
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << e_x;
    name=ss.str();
    name="answer_theo_" + name + ".txt";
    ofstream fout(name.c_str()); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->ff[i] << endl;
    }
    fout.close();
}

void write_vtk(
    CALC_POINTS* cp)
{
    int e_x=cp->e_x;
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << e_x;
    name=ss.str();
    name="answer_theo_" + name + ".vtk";
    ofstream fout(name.c_str());
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << allnode << " " << "float" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << endl;
    }
    fout << "CELLS" << " " << allnode << " " << allnode*2 << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << allnode << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << allnode << endl;
    fout << "SCALARS u float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->u[i] << endl;
    }
    fout.close(); 
}

void absolute_L2_error_norm(
    CALC_POINTS* cp,
    int allnode)
{
    double aerror1=0;
    double aerror2=0;
    double aerror3=0;
    for(int i=0;i<allnode;i++){
        aerror3 += (cp->u[i]-cp->ff[i])*(cp->u[i]-cp->ff[i]);
    }
    aerror2=aerror3/allnode;
    aerror1=sqrt(aerror2);
    cout << "absolute L2 error norm is " << aerror1 << endl;
}

void relative_L2_error_norm(
    CALC_POINTS* cp,
    int allnode)
{
    double aerror1=0;
    double aerror2=0;
    double aerror3=0;
    for(int i=0;i<allnode;i++){
        aerror3 += (cp->u[i]-cp->ff[i])*(cp->u[i]-cp->ff[i]);
    }
    aerror2=aerror3/allnode;
    aerror1=sqrt(aerror2);

    double rerror1=0;
    double theo1=0;
    double theo2=0;
    double theo3=0;
    for(int i=0;i<allnode;i++){
        theo3 += cp->ff[i]*cp->ff[i];
    }
    theo2=theo3/allnode;
    theo1=sqrt(theo2);
    rerror1=aerror1/theo1;
    cout << "relative L2 error norm is " << rerror1 << endl;
}

int main(int argc,//コマンドライン引数
    char* argv[])
{
    CALC_POINTS cp;
    LIN_SYS     ls;

    int E_X=0;
    int E_Y=0;
    cout << "enter the E_X and E_Y" << endl;
    cin >> E_X >> E_Y;
    int N_X=E_X+1;
    int N_Y=E_Y+1;
    int ALLNODE=N_X*N_Y;
    int NNNN=ALLNODE*ALLNODE;
    int ALLELEMENT=E_X*E_Y*2;
    double AA=0;
    double S=1.0/ALLELEMENT;
    double Q=1*S/3;
    double L_X=1.0;
    double L_Y=1.0;
    const int MAX_ITERATIONS=10000;
    const double EPSIRON=0.0000001;

    cp.e_x=E_X;
    cp.e_y=E_Y;
    cp.n_x=N_X;
    cp.n_y=N_Y;
    cp.allnode=ALLNODE;
    cp.nnnn=NNNN;
    cp.allelement=ALLELEMENT;
    cp.aa=AA;
    cp.s=S;
    cp.q=Q;
    cp.l_x=L_X;
    cp.l_y=L_Y;
    // cp.max_iterations=MAX_ITERATIONS;
    // cp.epsiron=EPSIRON;

    memory_allocation(&cp,&ls,ALLNODE,NNNN);

    set_calc_points(&cp);

    set_theo(&cp);

    set_element_matrix(&cp,&ls);

    set_matrix(&cp,&ls);

    set_bc(&cp,&ls);

    solve_mat_GJ(&cp,&ls);

    //solve_mat_GS(cp.u,ls.mat,ls.rhs,ALLNODE,N_X);

    write_gnuplot(&cp);

    write_theo_gnuplot(&cp);

    write_vtk(&cp);

    absolute_L2_error_norm(&cp,ALLNODE);

    relative_L2_error_norm(&cp,ALLNODE);

    memory_free(&cp,&ls);

    return 0;
}
