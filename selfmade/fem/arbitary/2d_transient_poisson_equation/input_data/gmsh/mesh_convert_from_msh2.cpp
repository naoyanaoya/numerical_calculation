#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
#include<iomanip>
#include<cstdlib>

int main(
    int argc,
    char * argv[]
)
{
    int nodes_per_elements = 3;
    int number_of_nodes = 0;
    int number_of_elements = 0;
    int int_temp = 0;
    double double_temp  = 0;

    int number_of_line = 0;
    std::ifstream file_in1("square.msh2");
    if(file_in1.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    std::string line1;
    while(getline(file_in1, line1)){
        ++number_of_line;
    }
    file_in1.close();

    // std::cout << number_of_line << std::endl;

    double *x;
    double *y;
    double *z;
    int *kind_of_elements; // kind of elements
    int *physical_of_elements;
    int **nodesPerElements; // nodes per elements

    x = (double*)calloc(number_of_line / 2, sizeof(double));
    y = (double*)calloc(number_of_line / 2, sizeof(double));
    z = (double*)calloc(number_of_line / 2, sizeof(double));
    kind_of_elements = (int*)calloc(number_of_line, sizeof(int));
    physical_of_elements = (int*)calloc(number_of_line, sizeof(int));
    nodesPerElements = (int**)calloc(number_of_line, sizeof(int*));
    for(int i = 0; i < number_of_line; i++){
        nodesPerElements[i] = (int*)calloc(3, sizeof(int));
    }

    std::ifstream file_in2("square.msh2");
    if(file_in2.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    std::string line2;
    for(int row = 0; getline(file_in2, line2); row++){
        std::istringstream stream(line2);

        // number of nodesの抽出
        if(line2.find("$Nodes") != std::string::npos){
            file_in2 >> number_of_nodes;
            // std::cout << number_of_nodes << std::endl;
            for(int i = 0; i < number_of_nodes; i++){
                file_in2 >> std::setprecision(8) >> std::scientific >> int_temp >>  x[i] >> y[i] >> z[i];
            }
        }

        // nodes per elementsの抽出
        if(line2.find("$Elements") != std::string::npos){
            file_in2 >> number_of_elements;
            // std::cout << number_of_elements << std::endl;
            for(int i = 0; i < number_of_elements; i++){
                file_in2 >> int_temp >> kind_of_elements[i] >> int_temp >> physical_of_elements[i];
                if(kind_of_elements[i] == 1){
                    file_in2 >> int_temp >> nodesPerElements[i][0] >> nodesPerElements[i][1];
                }else if(kind_of_elements[i] == 2){
                    file_in2 >> int_temp >> nodesPerElements[i][0] >> nodesPerElements[i][1] >> nodesPerElements[i][2];
                }
            }
        }
    }

    // for(int i = 0; i < number_of_nodes; i++){
    //     std::cout << std::setprecision(8) << std::scientific << x[i] << " " << y[i] << " " << z[i] << std::endl;
    // }

    // for(int i = 0; i < number_of_elements; i++){
    //     if(kind_of_elements[i] == 1){
    //         std::cout << kind_of_elements[i] << " " << physical_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
    //     }else if(kind_of_elements[i] == 2){
    //         std::cout << kind_of_elements[i] << " " << physical_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
    //     }
    // }

    std::ofstream file_out1("gmsh_nodes.txt");
    if(file_out1.fail()){
        std::cout << "can not open the nodes.txt." << std::endl;
    }
    for(int i = 0; i < number_of_nodes; i++){
        file_out1 << std::setprecision(8) << std::scientific << x[i] << " " << y[i] << " " << z[i] << std::endl;
    }
    file_out1.close();

    std::ofstream file_out2("gmsh_elements.txt");
    if(file_out2.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < number_of_elements; i++){
        if(kind_of_elements[i] == 1){
            // file_out2 << physical_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
        }else if(kind_of_elements[i] == 2){
            // file_out2 << kind_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
            file_out2 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        }
    }
    file_out2.close();

    std::ofstream file_out3("gmsh_boundary_condition.txt");
    if(file_out3.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < number_of_elements; i++){
        if(physical_of_elements[i] == 1){
            // file_out3 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
            file_out3 << nodesPerElements[i][0] << " " << "0" << std::endl;
        }else if(kind_of_elements[i] == 5){
            // file_out2 << kind_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        }
    }
    file_out3.close();

    std::ofstream file_out4("gmsh_initial_condition.txt");
    if(file_out4.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 1; i < number_of_nodes + 1; i++){
        if(i < 41 + 1){
            file_out4 << i << " " << "0" << std::endl;
        }else{
            // file_out4 << i << " " << std::min((std::min(std::abs(41 - i), std::abs(49 - i))), std::min(std::abs(113 - i), std::abs(121 - i))) * 10 << std::endl;
            // file_out4 << i << " " << abs(4 - (int)(abs(i - 81) / 10)) * 10 << std:: endl;
            file_out4 << i << " " << i * 10 << std::endl;
        }
        // if(physical_of_elements[i] == 1){
        //     // file_out4 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
        //     file_out4 << nodesPerElements[i][0] << " " << "0" << std::endl;
        // }else if(kind_of_elements[i] == 5){
        //     // file_out4 << kind_of_elements[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        // }
    }
    file_out4.close();

    return 0;
}
