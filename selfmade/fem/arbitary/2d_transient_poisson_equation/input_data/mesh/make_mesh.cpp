#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>

int main(
    int argc,
    char* argv[]
)
{
    int NX = 11;
    int NY = 11;
    int LX = 10;
    int LY = 10;
    int NN = NX * NY;

    double* x = (double*)calloc(NN, sizeof(double));
    double* y = (double*)calloc(NN, sizeof(double));

    for(int i=0;i<NY;i++){
        for(int j=0;j<NX;j++){
            x[i * NX + j] = i;
        }
    }
    for(int i=0;i<NY;i++){
        for(int j=0;j<NX;j++){
            y[i * NX + j] = j;
        }
    }

    // for(int i=0;i<NY;i++){
    //     for(int j=0;j<NX;j++){
    //         std::cout << x[i * NX + j] << " " << y[i * NX + j] << std::endl;
    //     }
    // }
    
    std::ofstream file_out1("mesh_nodes.txt");
    if(file_out1.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    for(int i=0;i<NY;i++){
        for(int j=0;j<NX;j++){
            file_out1 << x[i * NX + j] << " " << y[i * NX + j] << std::endl;
        }
    }
    file_out1.close();

    std::ofstream file_out2("mesh_boundary_condition.txt");
    if(file_out2.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    for(int i=0;i<NY;i++){
        for(int j=1;j<NX+1;j++){
            if(i == 0){
                file_out2 << i * NX + j << " " << 0 << std::endl;
            }else if(i == 10){
                file_out2 << i * NX + j << " " << 0 << std::endl;
            }else{
                if(j == 1 || j == 11){
                    file_out2 << i * NX + j << " " << 0 << std::endl;
                }
            }
        }
    }
    file_out2.close();

    std::ofstream file_out3("mesh_initial_condition.txt");
    if(file_out3.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    for(int i=0;i<NY;i++){
        for(int j=1;j<NX + 1;j++){
            if(i == 0){
                // file_out3 << i << " " << j << " " << i * NX + j << " " << 0 << std::endl;
                file_out3 << i * NX + j << " " << 0 << std::endl;
            }else if(i == 10){
                // file_out3 << i << " " << j << " " <<  i * NX + j << " " << 0 << std::endl;
                file_out3 << i * NX + j << " " << 0 << std::endl;
            }else if(j == 1 || j == 11){
                    // file_out3 << i << " " << j << " " << i * NX + j << " " << 0 << std::endl;
                    file_out3 << i * NX + j << " " << 0 << std::endl;
            }else{
                // file_out3 << i << " " << j << " " <<  i * NX + j << " " << abs(x[i * NX + j - 1] * (x[i * NX + j - 1] - LX) * y[i * NX + j - 1] * (y[i * NX + j - 1] - LY)) << std::endl;
                file_out3 << i * NX + j << " " << abs(x[i * NX + j - 1] * (x[i * NX + j - 1] - LX) * y[i * NX + j - 1] * (y[i * NX + j - 1] - LY)) << std::endl;
            }
        }
    }
    file_out3.close();


    std::ofstream file_out4("mesh_elements.txt");
    if(file_out4.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    int k = 1;
    int a = 1;
    for(int i=0;i<NY / 2;i++){
        for(int j=1;j<NX / 2 + 1;j++){
            // file_out4 << "flag" << std::endl;
            file_out4 << i * NY + j << " " << i * NY  + j + (LY + 1) << " " << i * NY  + j + (LY + 1) + 1<< std::endl;
            file_out4 << i * NY + j << " " << i * NY  + j + (LY + 1) + 1 << " " << i * NY  + j+ 1 << std::endl;
        }
        // file_out4 << std::endl;
        for(int j=NX / 2 + 1;j<NX;j++){
            // file_out4 << "flag_flag" << std::endl;
            file_out4 << i * NY + j << " " << i * NY  + j + (LY + 1) << " " << i * NY  + j + 1<< std::endl;
            file_out4 << i * NY + j + 1 << " " << i * NY  + j + (LY + 1) << " " << i * NY  + j + (LY + 1) + 1 << std::endl;
        }
        // file_out4 << std::endl;
        // std::exit(1);
    }
    for(int i=NY / 2;i<NY - 1;i++){
        for(int j=1;j<NX / 2 + 1;j++){
            // file_out4 << "flag_flag_flag" << std::endl;
            file_out4 << i * NY + j << " " << i * NY + j + (LY + 1) << " " << i * NY + j + 1<< std::endl;
            file_out4 << i * NY + j + 1 << " " << i * NY + j + (LY + 1) << " " << i * NY + j + (LY + 1) + 1 << std::endl;
        }
        // file_out4 << std::endl;
        for(int j=NX / 2 + 1;j<NX;j++){
            // file_out4 << "flag_flag_flag_flag" << std::endl;
            file_out4 << i * NY + j << " " << i * NY + j + (LY + 1) << " " << i * NY + j + (LY + 1) + 1 << std::endl;
            file_out4 << i * NY + j << " " << i * NY + j + (LY + 1) + 1 << " " << i * NY + j + 1 << std::endl;
        }
        // file_out4 << std::endl;
    }
    file_out4.close();

    return 0;
}