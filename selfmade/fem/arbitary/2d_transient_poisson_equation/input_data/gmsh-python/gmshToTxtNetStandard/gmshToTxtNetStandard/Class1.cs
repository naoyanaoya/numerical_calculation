﻿using System;
using System.IO;
using System.Collections.Generic;

namespace gmshToTxtNetStandard
{
    public class HelloWorld
    {
        static public void Main()
        {
            Console.WriteLine("Hello World");
            List<string> lines = new List<string>();
            using (var reader = File.OpenText("./square.msh"))
            {
                while (!reader.EndOfStream)
                {
                    lines.Add(reader.ReadLine());
                }
            }
            foreach (var l in lines)
            {
                Console.WriteLine(l);
            }
            if (lines == null)
            {
                return;
            }
        }
        public class Mesh
        {
            public Dictionary<int, string> NameDictionary { get; set; }
            public float[] Nodes { get; set; }
            public Cell[] Cells { get; set; }
            public List<((int, int), float[])> XYZ { get; set; }

            public Mesh(List<string> lines)
            {
                (this.Nodes, var cellsJugArray, this.NameDictionary) = InterpretLinesOnMesh(lines);
                // Cell配列を新たに定義
                this.Cells = new Cell[cellsJugArray.Length];
                for (int i = 0; i < cellsJugArray.Length; i++)
                {
                    var line = cellsJugArray[i];
                }

                // foreach (var l in lines)
                // {
                //     Console.WriteLine(l);
                // }
                // for (int i = 0; i < 10; i++) {
                //     Console.WriteLine(lines[i]);
                // }
            }
            private (float[], int[][], Dictionary<int, string>) InterpretLinesOnMesh(List<string> lines)
            {
                float[] nodes = null;
                int[][] elements = null;
                Dictionary<int, string> PhysicalNamesCorrespondence = null;
                for (int currentLine = 0; currentLine < lines.Count; currentLine++)
                {
                    if (lines[currentLine] == "$MeshFormat")
                    {
                        currentLine += 2;
                    }
                    else if (lines[currentLine] == "$PhysicalNames")
                    {
                        currentLine += 1;
                        var physicalNameNumber = int.Parse(lines[currentLine]);
                        Console.WriteLine(physicalNameNumber);
                    }

                }
                return (nodes, elements, PhysicalNamesCorrespondence);
            }
        }

        public class Cell
        {
            public int[] NodesIndex { get; set; }
            public CellType CellType { get; set; }
            public int PhysicalNameID { get; set; }
            public int EntityID { get; set; }
        }
        public enum CellType
        {
            Triangle = 2,
            Quadrilateral = 3,
            Tetrahedron = 4,
            Prism = 6
        }
    }
}

