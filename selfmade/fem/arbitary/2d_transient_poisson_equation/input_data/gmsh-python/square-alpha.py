import gmsh
import sys

gmsh.initialize()

# 2次元メッシュの可視化オプションをONにするコマンド
gmsh.option.setNumber("Mesh.SurfaceFaces", 1)
# マウスのホイールをズームイン・ズームアウトを自然な向きに変えるコマンド
gmsh.option.setNumber("General.MouseInvertZoom", 1)
# メッシュの線を見やすくするために、線の太さを変えるコマンド
gmsh.option.setNumber("Mesh.LineWidth", 4)
# 目盛りのついたboxを表示
gmsh.option.setNumber("General.Axes", 3)

gmsh.model.add("square-alpha")

p = 1
lc = 0.02
lcDouble = 2 * lc
lcFifth = 5 * lc
gmsh.model.geo.addPoint(0, 0, 0, lc, 1)
gmsh.model.geo.addPoint(p, 0, 0, lc, 2)
gmsh.model.geo.addPoint(p, p, 0, lc, 3)
gmsh.model.geo.addPoint(0, p, 0, lc, 4)
gmsh.model.geo.addPoint(p / 2, p / 2, 0, lcFifth, 5)

gmsh.model.geo.addLine(1, 2, 1)
gmsh.model.geo.addLine(2, 5, 2)
gmsh.model.geo.addLine(5, 1, 3)

gmsh.model.geo.addLine(2, 3, 4)
gmsh.model.geo.addLine(3, 5, 5)
gmsh.model.geo.addLine(5, 2, 6)

gmsh.model.geo.addLine(3, 4, 7)
gmsh.model.geo.addLine(4, 5, 8)
gmsh.model.geo.addLine(5, 3, 9)

gmsh.model.geo.addLine(4, 1, 10)
gmsh.model.geo.addLine(1, 5, 11)
gmsh.model.geo.addLine(5, 4, 12)

gmsh.model.geo.addCurveLoop([1, 2, 3], 1)
gmsh.model.geo.addCurveLoop([4, 5, 6], 2)
gmsh.model.geo.addCurveLoop([7, 8, 9], 3)
gmsh.model.geo.addCurveLoop([10, 11, 12], 4)

gmsh.model.geo.addPlaneSurface([1], 1)
gmsh.model.geo.addPlaneSurface([2], 2)
gmsh.model.geo.addPlaneSurface([3], 3)
gmsh.model.geo.addPlaneSurface([4], 4)

# ここでやっとモデルが可視化出来る
gmsh.model.geo.synchronize()

print(f"{gmsh.model.getEntities(0)}")
print(f"{gmsh.model.getEntities(1)}")
print(f"{gmsh.model.getEntities(2)}")
print(f"{gmsh.model.getEntities(3)}")

# gmsh.model.addPhysicalGroup(次元数, [タグのlist], PhisicalGroupのタグ)
# gmsh.model.setPhysicalName(次元数, PhisicalGroupのタグ, "名前")
gmsh.model.addPhysicalGroup(1, [1, 4, 7, 10] ,9)
gmsh.model.setPhysicalName(1, 9, "boundary")
gmsh.model.addPhysicalGroup(2, [1, 2, 3 ,4], 99)
gmsh.model.setPhysicalName(2, 99, "internal")

# gmsh.model.addPhysicalGroup(1, [])

# メッシュを生成するコマンド
gmsh.model.mesh.generate(2)

# change some option
gmsh.option.setColor("Geometry.Color.Points", 255, 165, 0)
# gmsh.model.mesh.setVisibility(3, 1)

gmsh.option.setNumber("Mesh.MshFileVersion", 2.2)
gmsh.write("square-alpha.msh")
gmsh.write("square-alpha.vtk")

if '-nopopup' not in sys.argv:
    gmsh.fltk.run()

gmsh.finalize()
