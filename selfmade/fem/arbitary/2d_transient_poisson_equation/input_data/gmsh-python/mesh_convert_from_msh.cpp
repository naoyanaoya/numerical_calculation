#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
#include<iomanip>
#include<cstdlib>

int main(
    int argc,
    char * argv[]
)
{
    printf("%s\n", argv[1]);
    int nodes_per_elements = 3;
    int number_of_nodes = 0;
    int number_of_elements = 0;
    int int_temp = 0;
    double double_temp  = 0;

    int number_of_line = 0;
    std::ifstream file_in1(argv[1]);
    if(file_in1.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    std::string line1;
    while(getline(file_in1, line1)){
        ++number_of_line;
    }
    file_in1.close();

    // std::cout << number_of_line << std::endl;

    double *x;
    double *y;
    double *z;
    int *element_type; // kind of elements
    int *physical_tag;
    int **nodesPerElements; // nodes per elements

    // 本当はnumber_of_nodesにしたいが、number_of_nodesはこの時点でわからないので
    // 余分を持ってnumber_of_line / 2にしておけばあふれることはない
    x = (double*)calloc(number_of_line / 2, sizeof(double));
    y = (double*)calloc(number_of_line / 2, sizeof(double));
    z = (double*)calloc(number_of_line / 2, sizeof(double));
    element_type = (int*)calloc(number_of_line, sizeof(int));
    physical_tag = (int*)calloc(number_of_line, sizeof(int));
    nodesPerElements = (int**)calloc(number_of_line, sizeof(int*));
    for(int i = 0; i < number_of_line; i++){
        nodesPerElements[i] = (int*)calloc(3, sizeof(int));
    }

    std::ifstream file_in2(argv[1]);
    if(file_in2.fail()){
        std::cout << "can not open the file." << std::endl;
    }
    std::string line2;
    for(int row = 0; getline(file_in2, line2); row++){
        std::istringstream stream(line2);

        // number of nodesの抽出
        if(line2.find("$Nodes") != std::string::npos){
            file_in2 >> number_of_nodes;
            // std::cout << number_of_nodes << std::endl;
            for(int i = 0; i < number_of_nodes; i++){
                file_in2 >> std::setprecision(8) >> std::scientific >> int_temp >>  x[i] >> y[i] >> z[i];
            }
        }

        // nodes per elementsの抽出
        if(line2.find("$Elements") != std::string::npos){
            file_in2 >> number_of_elements;
            // std::cout << number_of_elements << std::endl;
            for(int i = 0; i < number_of_elements; i++){
                file_in2 >> int_temp >> element_type[i] >> int_temp >> physical_tag[i] >> int_temp;
                if(element_type[i] == 1){
                    file_in2 >> nodesPerElements[i][0] >> nodesPerElements[i][1];
                }else if(element_type[i] == 2){
                    file_in2 >> nodesPerElements[i][0] >> nodesPerElements[i][1] >> nodesPerElements[i][2];
                }
            }
        }
    }
    printf("number_of_nodes %d\n", number_of_nodes);
    file_in2.close();

    std::ofstream file_out1("gmsh_nodes.txt");
    if(file_out1.fail()){
        std::cout << "can not open the nodes.txt." << std::endl;
    }
    for(int i = 0; i < number_of_nodes; i++){
        file_out1 << std::setprecision(8) << std::scientific << x[i] << " " << y[i] << " " << z[i] << std::endl;
    }
    file_out1.close();

    std::ofstream file_out2("gmsh_elements.txt");
    if(file_out2.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < number_of_elements; i++){
        if(element_type[i] == 1){
            // file_out2 << physical_tag[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
        }else if(element_type[i] == 2){
            // file_out2 << element_type[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
            file_out2 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        }
    }
    file_out2.close();

    int number_of_boundary_nodes = 0;
    std::ofstream file_out3("gmsh_boundary_condition.txt");
    if(file_out3.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < number_of_elements; i++){
        if(physical_tag[i] == 9){
            // file_out3 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
            file_out3 << nodesPerElements[i][0] << " " << "0" << std::endl;
            ++number_of_boundary_nodes;
        }else if(physical_tag[i] == 99){
            // file_out2 << element_type[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        }
    }
    file_out3.close();
    printf("number_of_boundary_nodes is %d\n", number_of_boundary_nodes);

    std::ofstream file_out4("gmsh_initial_condition.txt");
    if(file_out4.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < number_of_nodes; i++){
        if(i < number_of_boundary_nodes){
            file_out4 << (i + 1) << " " << "0.0" << std::endl;
        }else{
            file_out4 << (i + 1) << " " << std::min(abs(1.0 - x[i]), abs(0.0 - x[i])) * std::min(abs(1.0 - y[i]), abs(0.0 - y[i])) * 4 * 300 << std::endl;
        }
    }
    file_out4.close();

    printf("output gmsh~~ completed\n");

    return 0;
}
