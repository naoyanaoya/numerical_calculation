#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<vector>
#include<fstream>
#include<iomanip>

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> allString;
    std::stringstream ss(s);
    std::string item;
    while (getline(ss, item, delim)) {
    if (!item.empty()) {
            allString.push_back(item);
        }
    }
    return allString;
}

class Mesh {
    public:
        int NumberOfNodes;
        std::vector<double> Nodes;
        int NumberOfElements;
        int PhysicalNamesLength;
        std::vector<std::string> PhysicalNames;
        Mesh()
        {
            NumberOfNodes = 0;
            NumberOfElements = 0;
            PhysicalNamesLength = 0;
        }
};

class Cell {
    public:
        int ID;
        int ElementType;
        int tmp;
        int PhysicalTag;
        int EntityID;
        std::vector<int> NodesIndex;
        Cell () {
            ID = 0;
            ElementType = 0;
            tmp = 0;
            PhysicalTag = 0;
            EntityID = 0;
        }

};

int main (
    int argc,
    char *argv[]
)
{
    printf("%s\n", argv[1]);
    std::string fileName(argv[1]);
    std::vector<std::string> lines;
    std::string line;
    std::vector<std::vector<std::string>> allString;

    Mesh mesh = Mesh();
    std::cout << mesh.NumberOfNodes << " " << mesh.NumberOfElements << " " << mesh.PhysicalNamesLength << "\n";


    std::ifstream input_file(fileName);
    if (!input_file.is_open()) {
        printf("can not open the file %s\n", fileName);
    }

    int lineCount = 0;
    int N = 100000;
    allString.resize(N);
    while (getline(input_file, line)){
        std::vector<std::string> test = split(line, ' ');
        for (int i = 0; i < test.size(); i++) {
            allString[lineCount].push_back(test[i]);
        }
        lineCount++;
    }

    Cell cells[lineCount];

    for (int i = 0; i < lineCount; i++) {
        if (allString[i][0] == "$MeshFormat") {
            std::cout << i << "\n";
            i += 2;
        } else if (allString[i][0] == "$PhysicalNames") {
            // std::cout << i << "\n";
            mesh.PhysicalNamesLength = atoi(allString[i + 1][0].c_str());
            std::cout << "mesh.PhysicalNamesLength is " << mesh.PhysicalNamesLength << "\n";
            for (int j = 1; j < mesh.PhysicalNamesLength + 1; j++) {
                mesh.PhysicalNames.push_back(allString[i + 1 + j][2]);
            }
            for (int j = 0; j < mesh.PhysicalNamesLength; j++) {
                std::cout << mesh.PhysicalNames[j] << "\n";
            }
        } else if (allString[i][0] == "$Nodes") {
            mesh.NumberOfNodes = atoi(allString[i + 1][0].c_str());
            std::cout << "mesh.NumberOfNodes is " << mesh.NumberOfNodes << "\n";
            for (int j = 1; j < mesh.NumberOfNodes + 1; j++) {
                mesh.Nodes.push_back(std::stod(allString[i + 1 + j][1]));
                mesh.Nodes.push_back(std::stod(allString[i + 1 + j][2]));
                mesh.Nodes.push_back(std::stod(allString[i + 1 + j][3]));
            }
        } else if (allString[i][0] == "$Elements") {
            mesh.NumberOfElements = atoi(allString[i + 1][0].c_str());
            std::cout << "mesh.NumberOfElements is " << mesh.NumberOfElements << "\n";
            for (int j = 0; j < mesh.NumberOfElements; j++) {
                cells[j].ID = std::atoi(allString[i + 1 + j + 1][0].c_str());
                cells[j].ElementType = std::atoi(allString[i + 1 + j + 1][1].c_str());
                cells[j].tmp = std::atoi(allString[i + 1 + j + 1][2].c_str());
                cells[j].PhysicalTag = std::atoi(allString[i + 1 + j + 1][3].c_str());
                cells[j].EntityID = std::atoi(allString[i + 1 + j + 1][4].c_str());
                if (allString[i + 1 + j + 1][1] == "1") {
                    cells[j].NodesIndex.push_back(std::atoi(allString[i + 1 + j + 1][5].c_str()));
                    cells[j].NodesIndex.push_back(std::atoi(allString[i + 1 + j + 1][6].c_str()));
                }
                if (allString[i + 1 + j + 1][1] == "2") {
                    cells[j].NodesIndex.push_back(std::atoi(allString[i + 1 + j + 1][5].c_str()));
                    cells[j].NodesIndex.push_back(std::atoi(allString[i + 1 + j + 1][6].c_str()));
                    cells[j].NodesIndex.push_back(std::atoi(allString[i + 1 + j + 1][7].c_str()));
                }
            }
        }
    }

    for (int i = 0; i < mesh.NumberOfElements; i++) {
        // std::cout << i + 1 << " ";
        std::cout << cells[i].ID << " " << cells[i].ElementType << " " << cells[i].PhysicalTag << " " << cells[i].EntityID << " ";
        for (int j = 0; j < cells[i].NodesIndex.size(); j++) {
            std::cout << cells[i].NodesIndex[j] << " ";
        }
        std::cout << "\n";
    }

    input_file.close();

std::ofstream file_out1("gmsh_nodes.txt");
    if(file_out1.fail()){
        std::cout << "can not open the nodes.txt." << std::endl;
    }
    for(int i = 0; i < mesh.NumberOfNodes; i++){
        file_out1 << std::setprecision(8) << std::scientific << mesh.Nodes[i * 3 + 0] << " " << mesh.Nodes[i * 3 + 1] << " " << mesh.Nodes[i * 3 + 0] << std::endl;
    }
    file_out1.close();

    std::ofstream file_out2("gmsh_elements.txt");
    if(file_out2.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < mesh.NumberOfElements; i++){
        if (cells[i].ElementType == 1) {
            // file_out2 << cell.NodesIndex[i][0] << " " << cell.NodesIndex[i][1] << std::endl;
        } else if (cells[i].ElementType == 2) {
            file_out2 << cells[i].NodesIndex[0] << " " << cells[i].NodesIndex[1] << " " << cells[i].NodesIndex[2] << std::endl;
        }
    }
    file_out2.close();

    int number_of_boundary_nodes = 0;
    std::ofstream file_out3("gmsh_boundary_condition.txt");
    if(file_out3.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < mesh.NumberOfElements; i++){
        if (cells[i].PhysicalTag == 9) {
            // file_out3 << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << std::endl;
            file_out3 << cells[i].NodesIndex[0] << " " << "0" << std::endl;
            ++number_of_boundary_nodes;
        } else if(cells[i].PhysicalTag == 99) {
            // file_out2 << element_type[i] << " " << nodesPerElements[i][0] << " " << nodesPerElements[i][1] << " " << nodesPerElements[i][2] << std::endl;
        }
    }
    file_out3.close();
    printf("number_of_boundary_nodes is %d\n", number_of_boundary_nodes);

    std::ofstream file_out4("gmsh_initial_condition.txt");
    if(file_out4.fail()){
        std::cout << "can not open the elements.txt" << std::endl;
    }
    for(int i = 0; i < mesh.NumberOfNodes; i++){
        file_out4 << (i + 1) << " " << std::min(std::abs(1.0 - mesh.Nodes[i * 3 + 0]), std::abs(0.0 - mesh.Nodes[i * 3 + 0])) * std::min(std::abs(1.0 - mesh.Nodes[i * 3 + 1]), std::abs(0.0 - mesh.Nodes[i * 3 + 1])) * 4 * 300 << std::endl;
    }
    file_out4.close();

    printf("output gmsh~~ completed\n");

	return 0;
}
