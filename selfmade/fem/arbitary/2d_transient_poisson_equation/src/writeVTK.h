#ifndef __INCLUDE_WRITEVTK_H__
#define __INCLUDE_WRITEVTK_H__
#include"struct.h"

void write_vtk(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    int           k);

#endif