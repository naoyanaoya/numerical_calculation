#include<iostream>
#include"makeLeftAndRightMatrix.h"

void make_left_and_right_matrix(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    int NN = cp->number_of_nodes;
    for (int j = 0; j < NN; j++) {
        for (int i = 0; i < NN; i++) {
            ls->left_mat[j * NN + i] = ls->mat1[j * NN + i] / cc->DT + cc->theta * ls->mat2[j * NN + i];
        }
    }
    for (int j = 0; j < NN ;j++){
        for (int i = 0;i < NN;i++){
            ls->right_mat[j * NN + i] = ls->mat1[j * NN + i] / cc->DT - (1 - cc->theta) * ls->mat2[j * NN + i];
        }
    }
}