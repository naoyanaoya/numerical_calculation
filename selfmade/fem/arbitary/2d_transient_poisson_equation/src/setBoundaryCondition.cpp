#include<iostream>
#include<fstream>
#include"setBoundaryCondition.h"

void set_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++) {
        cp->u[cp->nodes_boundary[i]] = cp->u_boundary[i];
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++) {
        for (int j = 0; j < cp->number_of_nodes; j++) {
            ls->left_mat[cp->nodes_boundary[i] * cp->number_of_nodes + j] = 0;
        }
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        ls->left_mat[cp->nodes_boundary[i] * cp->number_of_nodes + cp->nodes_boundary[i]] = 1;
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        ls->rhs[cp->nodes_boundary[i]] = 0;
    }
}