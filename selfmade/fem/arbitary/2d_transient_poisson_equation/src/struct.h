#ifndef __INCLUDE_STRUCT_H__
#define __INCLUDE_STRUCT_H__

typedef struct {
    double* x; // coordinate x
    double* y; // coordinate y
    double* z; // coordinate z
    double* u; // scalar
    double* u_boundary; // boundary scalar
    int* nodes_boundary; // 境界上のnode番号
    int number_of_boundary_condition_nodes; // 境界上にあるノードの個数
	int number_of_nodes; // ノードの個数
    int number_of_elements; // エレメントの個数
    int number_of_initial_conditions; // 初期条件の個数(ノードの個数と多分一緒)
    int number_of_nodes_per_element;
}CALC_POINTS;

typedef struct {
    double* mat1; // 質量マトリックス
    double* mat2; // 剛性マトリックス
    double* left_mat; // 左マトリックス
    double* right_mat; // 右マトリックス
    double* mat; //
    double* rhs; //
    int** npe; //
    double* ax; //
    double* ap; //
    double* b; //
    double* p; // 探索方向ベクトル
    double* r; // 残差ベクトル
    // bicgstab
    double* r0; // 初期残差ベクトル
    double* e; //
    double* ae; //
}LIN_SYS;

typedef struct {
    const int iteration_max = 1000;
    const double EPS = 1.0e-6;
    const int TIMESTEP = 250;
    const double DT = 0.01;
    const double diffusion_coef = 0.001;
    const double theta = 1.0;
}CAL_COND;

#endif