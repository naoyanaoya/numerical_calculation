#ifndef __INCLUDE_MAKEM1ANDM2_H__
#define __INCLUDE_MAKEM1ANDM2_H__
#include"struct.h"

void make_M1_and_M2(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif