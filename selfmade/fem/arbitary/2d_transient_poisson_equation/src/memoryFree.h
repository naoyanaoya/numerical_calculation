#ifndef __INCLUDE_MEMORYFREE_H__
#define __INCLUDE_MEMORYFREE_H__
#include"struct.h"

void memory_free(
    CALC_POINTS* cp,
    LIN_SYS* ls
);

#endif