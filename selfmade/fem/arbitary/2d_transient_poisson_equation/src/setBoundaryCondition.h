#ifndef __INCLUDE_SETBOUNDARYCONDITION_H__
#define __INCLUDE_SETBOUNDARYCONDITION_H__
#include"struct.h"

void set_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
);

#endif