// ***********************************************************************************
// ポインタ渡しについて
// https://qiita.com/agate-pris/items/05948b7d33f3e88b8967
// 構造体へアドレス渡しとポインタ
// https://9cguide.appspot.com/16-02.html
// ***********************************************************************************

#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
#include<iomanip>
#include<cstdlib>
#include"struct.h"
#include"makeOutputDir.h"
#include"fileRead.h"
#include"setInitialCondition.h"
#include"readBoundaryCondition.h"
#include"setBoundaryCondition.h"
#include"meshConnectivity.h"
#include"setCalcPoints.h"
#include"memoryAllocation.h"
#include"memoryFree.h"
#include"makeM1AndM2.h"
#include"makeLeftAndRightMatrix.h"
#include"makeRhs.h"
#include"linearSolver.h"
#include"writeVTK.h"


// if output_matrix = 1, out "mmatrix.txt" and quit
const int output_matrix_flag = 0;

int main(
    int argc,
    char* argv[]
)
{
    clock_t start = clock();

    CALC_POINTS cp;
    LIN_SYS ls;
    CAL_COND cc;

    makeOutputDir();

    file_read(&cp, &ls);
    memory_allocation(&cp, &ls);
    set_calc_points(&cp);
    set_initial_condition(&cp);
    read_boundary_condition(&cp, &ls);
    mesh_connectivity(&cp, &ls);
    make_M1_and_M2(&cp, &ls, &cc);
    make_left_and_right_matrix(&cp, &ls, &cc);
    set_boundary_condition(&cp, &ls);
    write_vtk(&cp, &ls, 0);
    for (int k = 1; k <= cc.TIMESTEP; k++){
        std::cout << "----------" << k << " " << "step" << "--------" << std::endl;
        set_boundary_condition(&cp, &ls);
        make_rhs(&cp,&ls);
        cg(&cp, &ls, &cc);
        // bicgstab(&cp, &ls);
        if(k % 1 == 0){
            write_vtk(&cp, &ls, k);
        }
    }
    memory_free(&cp, &ls);

    clock_t end = clock();
    const double time = static_cast<double>(end - start) / CLOCKS_PER_SEC;
    std::cout << time << std::endl;

    return 0;
}
