#include<iostream>
#include"memoryAllocation.h"

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    int NN = cp->number_of_nodes;
    cp->x = (double*)calloc(NN, sizeof(double));
    cp->y = (double*)calloc(NN, sizeof(double));
    cp->z = (double*)calloc(NN, sizeof(double));
    cp->u = (double*)calloc(NN, sizeof(double));
    cp->u_boundary = (double*)calloc(cp->number_of_boundary_condition_nodes, sizeof(double));
    cp->nodes_boundary = (int*)calloc(cp->number_of_boundary_condition_nodes, sizeof(int));

    ls->ax = (double*)calloc(NN, sizeof(double));
    ls->ap = (double*)calloc(NN, sizeof(double));
    ls->b = (double*)calloc(NN, sizeof(double));
    ls->p = (double*)calloc(NN, sizeof(double));
    ls->r = (double*)calloc(NN, sizeof(double));
    ls->r0 = (double*)calloc(NN, sizeof(double));
    ls->e = (double*)calloc(NN, sizeof(double));
    ls->ae = (double*)calloc(NN, sizeof(double));
    ls->mat1 = (double*)calloc(NN * NN, sizeof(double));
    ls->mat2 = (double*)calloc(NN * NN, sizeof(double));
    ls->left_mat = (double*)calloc(NN * NN, sizeof(double));
    ls->right_mat = (double*)calloc(NN * NN, sizeof(double));
    ls->mat = (double*)calloc(NN * NN, sizeof(double*));
    ls->rhs = (double*)calloc(NN, sizeof(double));
    ls->npe = (int**)calloc(cp->number_of_elements, sizeof(int*));
    for (int i = 0; i < cp->number_of_elements; i++){
        ls->npe[i] = (int*)calloc(3, sizeof(int));
    }
}
