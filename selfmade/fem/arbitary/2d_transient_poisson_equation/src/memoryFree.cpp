#include<iostream>
#include"memoryFree.h"

void memory_free(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    free(cp->x);
    free(cp->y);
    free(cp->z);
    free(cp->u);
    free(cp->u_boundary);
    free(cp->nodes_boundary);
    free(ls->ax);
    free(ls->ap);
    free(ls->b);
    free(ls->p);
    free(ls->r);
    free(ls->r0);
    free(ls->e);
    free(ls->ae);

    free(ls->mat1);
    free(ls->mat2);
    free(ls->left_mat);
    free(ls->right_mat);
    free(ls->mat);
    free(ls->rhs);
    for (int i = 0; i < cp->number_of_elements; i++){
        free(ls->npe[i]);
    }
    free(ls->npe);
}