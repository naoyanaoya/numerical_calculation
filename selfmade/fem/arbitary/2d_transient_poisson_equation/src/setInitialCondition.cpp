#include<iostream>
#include<fstream>
#include"setInitialCondition.h"

void set_initial_condition(
    CALC_POINTS* cp
)
{
    std::ifstream file_in2("../input_data/gmsh-python/gmsh_initial_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the ../input_data//gmsh/gmsh_initial_condition.txt. setInitialCondition.cpp" << std::endl;
    }
    int tmp = 0;
    for (int i = 0; i < cp->number_of_initial_conditions; i++){
        file_in2 >> tmp >> cp->u[i];
    }
    file_in2.close();
}
