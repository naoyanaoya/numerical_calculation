#ifndef __INCLUDE_READBOUNDARYCONDITION_H__
#define __INCLUDE_READBOUNDARYCONDITION_H__
#include"struct.h"

void read_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
);

#endif