#include<iostream>
#include<fstream>
#include<sstream>
#include"writeVTK.h"

void write_vtk(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    int           k)
{
    std::stringstream ss;
    std::string name;
    ss << k;
    name=ss.str();
    name="../output/answer_" + name + ".vtk";
    std::ofstream fout(name.c_str());
    if(fout.fail()){
        std::cout << "出力ファイルをオープンできません" << std::endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << std::endl;
    fout << "vtk output" << std::endl;
    fout << "ASCII" << std::endl;
    fout << "DATASET UNSTRUCTURED_GRID" << std::endl;
    fout << "POINTS" << " " << cp->number_of_nodes << " " << "float" << std::endl;
    for(int i=0;i<cp->number_of_nodes;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << std::endl;
    }
    fout << "CELLS" << " " << cp->number_of_elements << " " << cp->number_of_elements * (cp->number_of_nodes_per_element + 1) << std::endl;
    for(int i=0;i<cp->number_of_elements;i++){
        fout << cp->number_of_nodes_per_element << " " << ls->npe[i][0] << " "  << ls->npe[i][1] << " " <<  ls->npe[i][2] << std::endl;
    }
    fout << "CELL_TYPES" << " " << cp->number_of_elements << std::endl;
    for(int i=0;i<cp->number_of_elements;i++){
        fout << 5 << std::endl;
    }
    fout << "POINT_DATA" << " " << cp->number_of_nodes << std::endl;
    fout << "SCALARS u float" << std::endl;
    fout << "LOOKUP_TABLE default" << std::endl;
    for (int i = 0; i < cp->number_of_nodes; i++){
        fout << cp->u[i] << std::endl;
    }
    fout.close();
}
