#include<iostream>
#include<fstream>
#include"readBoundaryCondition.h"

void read_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    std::ifstream file_in2("../input_data/gmsh-python/gmsh_boundary_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the ../input_data/gmsh-python/gmsh_boundary_condition.txt readBoundaryCondition.cpp" << std::endl;
    }
    for(int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        file_in2 >> cp->nodes_boundary[i] >> cp->u_boundary[i];
        // 0indexに合わせる
        --cp->nodes_boundary[i];
    }
    file_in2.close();
}
