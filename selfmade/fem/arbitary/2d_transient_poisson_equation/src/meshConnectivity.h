#ifndef __INCLUDE_MESHCONNECTIVITY_H__
#define __INCLUDE_MESHCONNECTIVITY_H__
#include"struct.h"

void mesh_connectivity(
    CALC_POINTS* cp,
    LIN_SYS* ls
);

#endif