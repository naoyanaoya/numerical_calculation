#ifndef __INCLUDE_MAKERHS_H__
#define __INCLUDE_MAKERHS_H__
#include"struct.h"

void make_rhs(
    CALC_POINTS*    cp,
    LIN_SYS*        ls
);

#endif