#ifndef __INCLUDE_MAKELEFTANDRIGHTMATRIX_H__
#define __INCLUDE_MAKELEFTANDRIGHTMATRIX_H__
#include"struct.h"

void make_left_and_right_matrix(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
);

#endif