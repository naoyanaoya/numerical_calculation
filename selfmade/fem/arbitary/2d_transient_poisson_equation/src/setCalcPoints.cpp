#include<iostream>
#include<fstream>
#include"setCalcPoints.h"

void set_calc_points(
    CALC_POINTS* cp
)
{
    std::ifstream file_in2("../input_data/gmsh-python/gmsh_nodes.txt");
    if(file_in2.fail()){
        std::cout << "can not open the ../input_data/gmsh/gmsh_nodes.txt. setCalcPoints.cpp" << std::endl;
    }
    for (int i = 0; i < cp->number_of_nodes; i++){
        file_in2 >> cp->x[i] >> cp->y[i] >> cp->z[i];
    }
    file_in2.close();
}
