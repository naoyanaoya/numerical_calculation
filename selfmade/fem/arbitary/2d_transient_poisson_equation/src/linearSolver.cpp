#include<iostream>
#include<math.h>
#include"linearSolver.h"

void cg(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    // Axを計算
    int NN = cp->number_of_nodes;
    double mxv = 0;
    for (int j = 0; j < cp->number_of_nodes; j++) {
        mxv = 0;
        for (int i = 0; i < cp->number_of_nodes; i++){
            mxv += ls->left_mat[j * NN + i] * cp->u[i];
        }
        ls->ax[j] = mxv;
    }

    // pとrを計算
    for (int i = 0; i <cp->number_of_nodes; i++) {
        ls->p[i] = ls->rhs[i] - ls->ax[i];
        ls->r[i] = ls->p[i];
    }

    for (int iter = 1; iter < cc->iteration_max; iter++) {
        double alpha = 0;
        double alpha1 =0;
        double alpha2 = 0;
        double beta = 0;
        double beta1 = 0;
        double beta2 = 0;
        double error = 0;

        // alphaを計算
        for (int j = 0; j < cp->number_of_nodes; j++) {
            mxv = 0;
            for (int i = 0; i < cp->number_of_nodes; i++) {
                mxv += ls->left_mat[j * NN + i] * ls->p[i];
            }
            ls->ap[j] = mxv;
        }
        for (int i = 0; i < cp->number_of_nodes; i++) {
            alpha1 += ls->p[i] * ls->r[i];
            alpha2 += ls->p[i] * ls->ap[i];
        }
        alpha = alpha1 / alpha2;

        for (int i = 0; i < cp->number_of_nodes; i++) {
            cp->u[i] += alpha * ls->p[i];
            ls->r[i] += -alpha * ls->ap[i];
        }

        for (int i = 0; i < cp->number_of_nodes; i++) {
            error += fabs(ls->r[i]);
        }

        std::cout << "LOOP : " << iter << " ERROR : " << error << std::endl;

        if(error < cc->EPS){
            break;
        }

        for (int i = 0; i < cp->number_of_nodes; i++) {
            beta1 += ls->r[i] * ls->ap[i];
            beta2 += ls->p[i] * ls->ap[i];
        }
        beta = -1.0 * beta1 / beta2;

        for (int i = 0; i < cp->number_of_nodes; i++) {
            ls->p[i] = ls->r[i] + beta * ls->p[i];
        }
    }
}

// void bicgstab(
//     CALC_POINTS* cp,
//     LIN_SYS* ls
// )
// {
//     // calculate y=Ax
//     for (int j = 0; j < cp->number_of_nodes; j++) {
//         double mxv = 0;
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             mxv += ls->left_mat[j][i] * cp->u[i];
//         }
//         ls->ax[j] = mxv;
//     }

//     // calculate p and r
//     for (int i = 0; i < cp->number_of_nodes; i++) {
//         ls->r0[i] = ls->rhs[i] - ls->ax[i];
//         ls->r[i] = ls->r0[i];
//         ls->p[i] = ls->r0[i];
//     }

//     double c1 = 0;

//     // calculate c1
//     for (int i = 0; i < cp->number_of_nodes; i++) {
//         c1 += ls->r0[i] * ls->r0[i];
//     }

//     if (c1 == 0.0) {
//         fprintf(stderr, "(r0, r0*) == 0!!\n");
//         exit(1);
//     }

//     for (int iter = 1; iter <= iteration_max; iter) {
//         for (int j = 0; j < cp->number_of_nodes; j++) {
//             double mxv = 0;
//             for (int i = 0; i < cp->number_of_nodes; i++) {
//                 mxv += ls->left_mat[j][i] * ls->p[i];
//             }
//             ls->ap[j] = mxv;
//         }
//         double c2 = 0;
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             c2 += ls->r0[i] * ls->ap[i];
//         }
//         double alpha = c1 / c2;

//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             ls->e[i] = ls->r[i] - alpha * ls->ap[i];
//         }
//         for (int j = 0; j < cp->number_of_nodes; j++) {
//             double mxv = 0;
//             for (int i = 0; i < cp->number_of_nodes; i++) {
//                 mxv += ls->left_mat[j][i] * ls->e[i];
//             }
//             ls->ae[j] = mxv;
//         }

//         double e_dot_ae = 0;
//         double ae_dot_ae = 0;
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             e_dot_ae += ls->e[i] * ls->ae[i];
//             ae_dot_ae += ls->ae[i] * ls->ae[i];
//         }
//         double c3 = e_dot_ae / ae_dot_ae;

//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             cp->u[i] += alpha * ls->p[i] + c3 * ls->e[i];
//             ls->r[i] = ls->e[i] - c3 * ls->ae[i];
//         }

//         double a = 0;
//         double b = 0;
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             // std::cout << ls->rhs[i] << " " << std::endl;
//             a += ls->r[i] * ls->r[i];
//             b += ls->rhs[i] * ls->rhs[i];
//         }
//         double error = a / b;
//         std::cout << "LOOP : " << iter << " ERROR : " << error << std::endl;
//         if (error < EPS) {
//             break;
//         }

//         double t1 = 0;
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             t1 += ls->r[i] * ls->r0[i];
//         }
//         double beta = t1 / (c2 * c3);
//         for (int i = 0; i < cp->number_of_nodes; i++) {
//             ls->p[i] = ls->r[i] + beta * (ls->p[i] - c3 * ls->ap[i]);
//         }
//     }
// }