#include<iostream>
#include"makeRhs.h"

void make_rhs(
    CALC_POINTS*    cp,
    LIN_SYS*        ls
)
{
    int NN = cp->number_of_nodes;
    for (int i = 0; i < NN; i++) {
        ls->rhs[i]=0;
    }
    for (int j = 0; j < NN; j++) {
        for (int i = 0; i < NN; i++) {
            ls->rhs[i] += ls->right_mat[j * NN + i] * cp->u[i];
        }
    }
}
