#include<iostream>
#include<fstream>
#include"meshConnectivity.h"

void mesh_connectivity(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    cp->number_of_nodes_per_element = 3;
    std::ifstream file_in2("../input_data/gmsh-python/gmsh_elements.txt");
    if (file_in2.fail()) {
        std::cout << "can not open the ../input_data/gmsh/gmsh_elements.txt. meshConnectivity.cpp" << std::endl;
    }
    for (int i = 0; i < cp->number_of_elements; i++){
        file_in2 >> ls->npe[i][0] >> ls->npe[i][1] >> ls->npe[i][2];
    }
    file_in2.close();
    // change to zero index
    for (int i = 0; i < cp->number_of_elements; i++) {
        ls->npe[i][0] = ls->npe[i][0] - 1;
        ls->npe[i][1] = ls->npe[i][1] - 1;
        ls->npe[i][2] = ls->npe[i][2] - 1;;
    }
}
