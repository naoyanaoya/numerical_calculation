#include<iostream>
#include"makeM1AndM2.h"

void make_M1_and_M2(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    CAL_COND* cc
)
{
    int NN = cp->number_of_nodes;
    int I1 = 0;
    int I2 = 0;
    int I3 = 0;
    double X1 = 0;
    double X2 = 0;
    double X3 = 0;
    double Y1 = 0;
    double Y2 = 0;
    double Y3 = 0;
    double B1 = 0;
    double B2 = 0;
    double B3 = 0;
    double C1 = 0;
    double C2 = 0;
    double C3 = 0;
    double det = 0;
    double area = 0;
    for (int i = 0; i < cp->number_of_elements; i++) {
        // printf("-----------------start--------------\n");
        I1 = ls->npe[i][0];
        I2 = ls->npe[i][1];
        I3 = ls->npe[i][2];
        // printf("%d, %d, %d\n", I1, I2, I3);
        X1 = cp->x[I1];
        X2 = cp->x[I2];
        X3 = cp->x[I3];
        Y1 = cp->y[I1];
        Y2 = cp->y[I2];
        Y3 = cp->y[I3];
        // printf("%02.3f, %02.3f, %02.3f, %02.3f, %02.3f, %02.3f\n", X1, X2, X3, Y1, Y2, Y3);
        det = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2);
        area = 0.5 * det;
        if (area <= 0.0) {
            printf("area failure\n");
            exit(1);
        }
        // printf("%d's determinant is %02.3f\n", i, det);
        ls->mat1[I1 * NN + I1] += area / 2.0 * 12.0;
        ls->mat1[I1 * NN + I2] += area / 12.0;
        ls->mat1[I1 * NN + I3] += area / 12.0;
        ls->mat1[I2 * NN + I1] += area / 12.0;
        ls->mat1[I2 * NN + I2] += area / 2.0 * 12.0;
        ls->mat1[I2 * NN + I3] += area / 12.0;
        ls->mat1[I3 * NN + I1] += area / 12.0;
        ls->mat1[I3 * NN + I2] += area / 12.0;
        ls->mat1[I3 * NN + I3] += area / 2.0 * 12.0;
        double area_inverse = 1.0 / area;
        B1 = (Y2 - Y3) * 0.5 * area_inverse;
        B2 = (Y3 - Y1) * 0.5 * area_inverse;
        B3 = (Y1 - Y2) * 0.5 * area_inverse;
        C1 = (X3 - X2) * 0.5 * area_inverse;
        C2 = (X1 - X3) * 0.5 * area_inverse;
        C3 = (X2 - X1) * 0.5 * area_inverse;
        ls->mat2[I1 * NN + I1] += cc->diffusion_coef * (B1 * B1) + cc->diffusion_coef * (C1 * C1);
        ls->mat2[I1 * NN + I2] += cc->diffusion_coef * (B1 * B2) + cc->diffusion_coef * (C1 * C2);
        ls->mat2[I1 * NN + I3] += cc->diffusion_coef * (B1 * B3) + cc->diffusion_coef * (C1 * C3);
        ls->mat2[I2 * NN + I1] += cc->diffusion_coef * (B2 * B1) + cc->diffusion_coef * (C2 * C1);
        ls->mat2[I2 * NN + I2] += cc->diffusion_coef * (B2 * B2) + cc->diffusion_coef * (C2 * C2);
        ls->mat2[I2 * NN + I3] += cc->diffusion_coef * (B2 * B3) + cc->diffusion_coef * (C2 * C3);
        ls->mat2[I3 * NN + I1] += cc->diffusion_coef * (B3 * B1) + cc->diffusion_coef * (C3 * C1);
        ls->mat2[I3 * NN + I2] += cc->diffusion_coef * (B3 * B2) + cc->diffusion_coef * (C3 * C2);
        ls->mat2[I3 * NN + I3] += cc->diffusion_coef * (B3 * B3) + cc->diffusion_coef * (C3 * C3);
    }
}