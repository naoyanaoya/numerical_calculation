#include<iostream>
#include<fstream>
#include"fileRead.h"

void file_read(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    // ノードの個数を読み取る
    int number_of_line = 0;
    std::ifstream file_in1("../input_data/gmsh-python/gmsh_nodes.txt");
    if(file_in1.fail()){
        std::cout << "can not open the ../input_data/gmsh/gmsh_nodes.txt.  fileRead.cpp" << std::endl;
    }
    std::string line;
    while(getline(file_in1, line)){
        ++number_of_line;
    }
    file_in1.close();
    cp->number_of_nodes = number_of_line;
    printf("number_of_nodes is %d\n", cp->number_of_nodes);

    // エレメントの個数を読み取る
    number_of_line = 0;
    std::ifstream file_in4("../input_data/gmsh-python/gmsh_elements.txt");
    if(file_in4.fail()){
        std::cout << "can not open the ../input_data/gmsh-python/gmsh_elements.txt.  fileRead.cpp" << std::endl;
    }
    while(getline(file_in4, line)){
        ++number_of_line;
    }
    file_in4.close();
    cp->number_of_elements = number_of_line;
    printf("number_of_elements is %d\n", cp->number_of_elements);

    // 初期条件の個数
    number_of_line = 0;
    std::ifstream file_in2("../input_data/gmsh-python/gmsh_initial_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the ../input_data/gmsh-python/gmsh_initial_condition.txt. fileRead.cpp" << std::endl;
    }
    while(getline(file_in2, line)){
        ++number_of_line;
    }
    cp->number_of_initial_conditions = number_of_line;
    file_in2.close();
    printf("number_of_initial_conditions is %d\n", cp->number_of_initial_conditions);

    // 境界条件の個数
    number_of_line = 0;
    std::ifstream file_in3("../input_data/gmsh-python/gmsh_boundary_condition.txt");
    if(file_in3.fail()){
        std::cout << "can not open the ../input_data/gmsh-python/gmsh_boundary_condition.txt. fileRead.cpp" << std::endl;
    }
    while(getline(file_in3, line)){
        ++number_of_line;
    }
    cp->number_of_boundary_condition_nodes = number_of_line;
    file_in3.close();
    printf("number_of_boundary_condition is %d\n", cp->number_of_boundary_condition_nodes);
    // exit(1);
}
