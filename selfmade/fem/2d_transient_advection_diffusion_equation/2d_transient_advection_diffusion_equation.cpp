#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
using namespace std;

typedef struct
{
    double* f;
    double* ff;
    double* x;
    double* y;
    double* u;

    int e_x;
    int e_y;
    int n_x;
    int n_y;
    int allnode;
    int allelement;
    double d[2];
    double v[2];
    double dt;
    int n_step;
    double aa;
    double s;
    double q;
    double theta;
    double l_x;
    double l_y;
    double dx;
    double dy;
}CALC_POINTS;

typedef struct
{
    double* mat1;
    double* mat2;
    double* ele_diff_mat1;
    double* ele_diff_mat2;
    double* ele_mass_mat;
    double* ele_adve_mat1;
    double* ele_adve_mat2;
    double* mass_mat;
    double* diff_mat;
    double* adve_mat;
    double* tmp;
    double* rhs;
}LIN_SYS;

void display_mat1(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                cout << ls->mat1[allnode*j+i] << " ";
            }
            cout << endl;
        }
}

void display_mat2(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                cout << ls->mat2[allnode*j+i] << " ";
            }
            cout << endl;
        }
}

void display_rhs(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << ls->rhs[i] << endl;
    }
}

void display_u(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cout << cp->u[i] << endl;
    }
}

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS*     ls,
    int     allnode)
{
    cp->f=(double*)calloc(allnode,sizeof(double));
    cp->ff=(double*)calloc(allnode,sizeof(double));
    cp->x=(double*)calloc(allnode,sizeof(double));
    cp->y=(double*)calloc(allnode,sizeof(double));
    cp->u=(double*)calloc(allnode,sizeof(double));
    ls->mat1=(double*)calloc(allnode*allnode,sizeof(double));
    ls->mat2=(double*)calloc(allnode*allnode,sizeof(double));
    ls->tmp=(double*)calloc(allnode*allnode,sizeof(double));
    ls->ele_diff_mat1=(double*)calloc(9,sizeof(double));
    ls->ele_diff_mat2=(double*)calloc(9,sizeof(double));
    ls->ele_mass_mat=(double*)calloc(9,sizeof(double));
    ls->ele_adve_mat1=(double*)calloc(9,sizeof(double));
    ls->ele_adve_mat2=(double*)calloc(9,sizeof(double));
    ls->mass_mat=(double*)calloc(allnode*allnode,sizeof(double));
    ls->diff_mat=(double*)calloc(allnode*allnode,sizeof(double));
    ls->adve_mat=(double*)calloc(allnode*allnode,sizeof(double));
    ls->rhs=(double*)calloc(allnode,sizeof(double));
}

void memory_free(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    free(cp->f);
    free(cp->ff);
    free(cp->x);
    free(cp->y);
    free(cp->u);
    free(ls->mat1);
    free(ls->mat2);
    free(ls->tmp);
    free(ls->ele_diff_mat1);
    free(ls->ele_diff_mat2);
    free(ls->ele_mass_mat);
    free(ls->ele_adve_mat1);
    free(ls->ele_adve_mat2);
    free(ls->mass_mat);
    free(ls->diff_mat);
    free(ls->adve_mat);
    free(ls->rhs);
    cp->f=NULL;
    cp->ff=NULL;
    cp->x=NULL;
    cp->y=NULL;
    cp->u=NULL;
    ls->mat1=NULL;
    ls->mat2=NULL;
    ls->tmp=NULL;
    ls->ele_diff_mat1=NULL;
    ls->ele_diff_mat2=NULL;
    ls->ele_mass_mat=NULL;
    ls->ele_adve_mat1=NULL;
    ls->ele_adve_mat2=NULL;
    ls->mass_mat=NULL;
    ls->diff_mat=NULL;
    ls->adve_mat=NULL;
    ls->rhs=NULL;
}

void set_calc_points(
    CALC_POINTS*    cp)
{
    int allnode=cp->allnode;
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    double dx=l_x/(n_x-1.0);
    double dy=l_y/(n_y-1.0);

    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_x;j++){
            if(i%n_x==j){
                cp->x[i]=dx*j;
            } 
        }
    }
    for(int i=0;i<allnode;i++){
        for(int j=0;j<n_y;j++){
            if(0+n_y*j<=i && i<=e_y+n_y*j){
                cp->y[i]=dy*j;
            }
            else{
                continue;
            }
        }
    }
}

void set_first_condition(
    CALC_POINTS*    cp)
{
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    int allnode=cp->allnode;
    for(int i=0;i<allnode;i++){
        cp->ff[i]=abs(cp->x[i]*(cp->x[i]-l_x)*cp->y[i]*(cp->y[i]-l_y));
    }
    for(int i=0;i<allnode;i++){
        cp->u[i]=cp->ff[i];
    }

    // for(int i=0;i<allnode;i++){
    //     if(0.25 < cp->x[i] && cp->x[i] < .75 && 0.25 < cp->y[i] && cp->y[i] < 0.75){
    //         cp->ff[i]=1.0;
    //     }
    // }
    // for(int i=0;i<allnode;i++){
    //     cp->u[i]=cp->ff[i];
    // }
    
}

void set_element_diffusion_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    double d_x=cp->d[0];
    double d_y=cp->d[1];

    ls->ele_diff_mat1[0]=0.5*d_x+0*d_y;
    ls->ele_diff_mat1[1]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat1[2]=0*d_x+0*d_y;
    ls->ele_diff_mat1[3]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat1[4]=0.5*d_x+0.5*d_y;
    ls->ele_diff_mat1[5]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat1[6]=0*d_x+0*d_y;
    ls->ele_diff_mat1[7]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat1[8]=0*d_x+0.5*d_y;

    ls->ele_diff_mat2[0]=0*d_x+0.5*d_y;
    ls->ele_diff_mat2[1]=0*d_x+0*d_y;
    ls->ele_diff_mat2[2]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat2[3]=0*d_x+0*d_y;
    ls->ele_diff_mat2[4]=0.5*d_x+0*d_y;
    ls->ele_diff_mat2[5]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat2[6]=0*d_x+(-0.5)*d_y;
    ls->ele_diff_mat2[7]=(-0.5)*d_x+0*d_y;
    ls->ele_diff_mat2[8]=0.5*d_x+0.5*d_y;
}

void set_element_mass_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    double s=cp->s;
    ls->ele_mass_mat[0]=s/12*2;
    ls->ele_mass_mat[1]=s/12*1;
    ls->ele_mass_mat[2]=s/12*1;
    ls->ele_mass_mat[3]=s/12*1;
    ls->ele_mass_mat[4]=s/12*2;
    ls->ele_mass_mat[5]=s/12*1;
    ls->ele_mass_mat[6]=s/12*1;
    ls->ele_mass_mat[7]=s/12*1;
    ls->ele_mass_mat[8]=s/12*2;
}

void set_element_advection_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    double v_x=cp->v[0];
    double v_y=cp->v[1];
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double l_x=cp->l_x;
    double l_y=cp->l_y;
    double dx=l_x/(n_x-1.0);
    double dy=l_y/(n_y-1.0);

    ls->ele_adve_mat1[0]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[1]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[2]=(-1)*(v_x*((-1)*dx)+v_y*0)/6;
    ls->ele_adve_mat1[3]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[4]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[5]=(-1)*(v_x*(dx)+v_y*((-1)*dy))/6;
    ls->ele_adve_mat1[6]=(-1)*(v_x*0+v_y*(dy))/6;
    ls->ele_adve_mat1[7]=(-1)*(v_x*0+v_y*(dy))/6;
    ls->ele_adve_mat1[8]=(-1)*(v_x*0+v_y*(dy))/6;

    ls->ele_adve_mat2[0]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[1]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[2]=(-1)*(v_x*0+v_y*((-1)*dy))/6;
    ls->ele_adve_mat2[3]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[4]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[5]=(-1)*(v_x*(dx)+v_y*0)/6;
    ls->ele_adve_mat2[6]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;
    ls->ele_adve_mat2[7]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;
    ls->ele_adve_mat2[8]=(-1)*(v_x*((-1)*dx)+v_y*(dy))/6;

    // ls->ele_adve_mat1[0]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[1]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[2]=(v_x*((-1)*dx)+v_y*0)/6;
    // ls->ele_adve_mat1[3]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[4]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[5]=(v_x*(dx)+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat1[6]=(v_x*0+v_y*(dy))/6;
    // ls->ele_adve_mat1[7]=(v_x*0+v_y*(dy))/6;
    // ls->ele_adve_mat1[8]=(v_x*0+v_y*(dy))/6;

    // ls->ele_adve_mat2[0]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[1]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[2]=(v_x*0+v_y*((-1)*dy))/6;
    // ls->ele_adve_mat2[3]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[4]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[5]=(v_x*(dx)+v_y*0)/6;
    // ls->ele_adve_mat2[6]=(v_x*((-1)*dx)+v_y*(dy))/6;
    // ls->ele_adve_mat2[7]=(v_x*((-1)*dx)+v_y*(dy))/6;
    // ls->ele_adve_mat2[8]=(v_x*((-1)*dx)+v_y*(dy))/6;
}

void assemble_mass_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_diff_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mass_mat[(allnode+1)*i+0]+=ls->ele_mass_mat[0];
            ls->mass_mat[(allnode+1)*i+1]+=ls->ele_mass_mat[1];
            ls->mass_mat[(allnode+1)*i+n_x+1]+=ls->ele_mass_mat[2];
            ls->mass_mat[(allnode+1)*i+0+allnode]+=ls->ele_mass_mat[3];
            ls->mass_mat[(allnode+1)*i+1+allnode]+=ls->ele_mass_mat[4];
            ls->mass_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_mass_mat[5];
            ls->mass_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_mass_mat[6];
            ls->mass_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_mass_mat[7];
            ls->mass_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_mass_mat[8];
        }
    }
    //assemble ele_diff_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->mass_mat[(allnode+1)*i+0]+=ls->ele_mass_mat[0];
            ls->mass_mat[(allnode+1)*i+n_x+1]+=ls->ele_mass_mat[1];
            ls->mass_mat[(allnode+1)*i+n_x]+=ls->ele_mass_mat[2];
            ls->mass_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_mass_mat[3];
            ls->mass_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_mass_mat[4];
            ls->mass_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_mass_mat[5];
            ls->mass_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_mass_mat[6];
            ls->mass_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_mass_mat[7];
            ls->mass_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_mass_mat[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mass_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void assemble_diffusion_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_diff_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_diff_mat1[0];
            ls->diff_mat[(allnode+1)*i+1]+=ls->ele_diff_mat1[1];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_diff_mat1[2];
            ls->diff_mat[(allnode+1)*i+0+allnode]+=ls->ele_diff_mat1[3];
            ls->diff_mat[(allnode+1)*i+1+allnode]+=ls->ele_diff_mat1[4];
            ls->diff_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_diff_mat1[5];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_diff_mat1[6];
            ls->diff_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_diff_mat1[7];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_diff_mat1[8];
        }
    }
    //assemble ele_diff_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_diff_mat2[0];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_diff_mat2[1];
            ls->diff_mat[(allnode+1)*i+n_x]+=ls->ele_diff_mat2[2];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_diff_mat2[3];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_diff_mat2[4];
            ls->diff_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_diff_mat2[5];
            ls->diff_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_diff_mat2[6];
            ls->diff_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_diff_mat2[7];
            ls->diff_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_diff_mat2[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->diff_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void assemble_advection_matrix(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{   
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;

    //assemble ele_adve_mat1
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_adve_mat1[0];
            ls->diff_mat[(allnode+1)*i+1]+=ls->ele_adve_mat1[1];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_adve_mat1[2];
            ls->diff_mat[(allnode+1)*i+0+allnode]+=ls->ele_adve_mat1[3];
            ls->diff_mat[(allnode+1)*i+1+allnode]+=ls->ele_adve_mat1[4];
            ls->diff_mat[(allnode+1)*i+n_x+1+allnode]+=ls->ele_adve_mat1[5];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_adve_mat1[6];
            ls->diff_mat[(allnode+1)*i+1+(n_x+1)*allnode]+=ls->ele_adve_mat1[7];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_adve_mat1[8];
        }
    }
    //assemble ele_adve_mat2
    for(int j=0;j<e_y;j++){
        for(int i=n_x*j;i<n_x*j+e_x;i++){
            ls->diff_mat[(allnode+1)*i+0]+=ls->ele_adve_mat2[0];
            ls->diff_mat[(allnode+1)*i+n_x+1]+=ls->ele_adve_mat2[1];
            ls->diff_mat[(allnode+1)*i+n_x]+=ls->ele_adve_mat2[2];
            ls->diff_mat[(allnode+1)*i+0+(n_x+1)*allnode]+=ls->ele_adve_mat2[3];
            ls->diff_mat[(allnode+1)*i+n_x+1+(n_x+1)*allnode]+=ls->ele_adve_mat2[4];
            ls->diff_mat[(allnode+1)*i+n_x+(n_x+1)*allnode]+=ls->ele_adve_mat2[5];
            ls->diff_mat[(allnode+1)*i+0+n_x*allnode]+=ls->ele_adve_mat2[6];
            ls->diff_mat[(allnode+1)*i+n_x+1+n_x*allnode]+=ls->ele_adve_mat2[7];
            ls->diff_mat[(allnode+1)*i+n_x+n_x*allnode]+=ls->ele_adve_mat2[8];
        }
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->diff_mat[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void make_mat1(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    double dt=cp->dt;
    double theta=cp->theta;
    for(int i=0;i<allnode*allnode;i++){
        ls->mat1[i]=ls->mass_mat[i]/dt + (ls->diff_mat[i]+ls->adve_mat[i])*theta;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat1[allnode*j+i] << " ";
    //     }
    //     cout << ls->rhs[j] << endl;
    // }
}

void make_mat2(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    double dt=cp->dt;
    double theta=cp->theta;
    for(int i=0;i<allnode*allnode;i++){
        ls->mat2[i]=ls->mass_mat[i]/dt - (ls->diff_mat[i]+ls->adve_mat[i])*(1-theta);
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat2[allnode*j+i] << " ";
    //     }
    //     cout << ls->rhs[j] << endl;
    // }
}

void make_rhs(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int allnode=cp->allnode;
    for(int j=0;j<allnode;j++){
        ls->rhs[j]=0;
    }
    for(int j=0;j<allnode;j++){
            for(int i=0;i<allnode;i++){
                ls->rhs[j]+=ls->mat2[j*allnode+i]*cp->u[i];
        }
    }
    display_mat2;

    // display_u;

    // for(int i=0;i<allnode;i++){
    //         ls->rhs[4]+=ls->mat2[36+i]*cp->u[i];
    // }
}


void set_bc(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double aa=cp->aa;
    double tmp=0;
    
    for(int j=0;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[j*allnode+i]=0;
        }
        ls->mat1[j*allnode+j]=1;
    }
    for(int j=1;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[n_x*j*allnode+i]=0;
        }
        ls->mat1[n_x*j*allnode+n_x*j]=1;
    }
    for(int j=0;j<e_x-1;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[(e_x+n_x+n_x*j)*allnode+i]=0;
        }
        ls->mat1[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat1[j*allnode+i]=0;
        }
        ls->mat1[j*allnode+j]=1;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat1[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }

    for(int j=0;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[j*allnode+i]=0;
        }
        ls->mat2[j*allnode+j]=1;
    }
    for(int j=1;j<n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[n_x*j*allnode+i]=0;
        }
        ls->mat2[n_x*j*allnode+n_x*j]=1;
    }
    for(int j=0;j<e_x-1;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[(e_x+n_x+n_x*j)*allnode+i]=0;
        }
        ls->mat2[(e_x+n_x+n_x*j)*allnode+e_x+n_x+n_x*j]=1;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        for(int i=0;i<allnode;i++){
            ls->mat2[j*allnode+i]=0;
        }
        ls->mat2[j*allnode+j]=1;
    }
    // for(int j=0;j<allnode;j++){
    //     for(int i=0;i<allnode;i++){
    //         cout << ls->mat2[allnode*j+i] << " ";
    //     }
    //     cout << endl;
    // }
}

void set_every_bc(
    CALC_POINTS*    cp,
    LIN_SYS*        ls
)
{
    int e_x=cp->e_x;
    int e_y=cp->e_y;
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    int allnode=cp->allnode;
    double aa=cp->aa;
    for(int j=0;j<n_x;j++){
        ls->rhs[j]=aa;
    }
    for(int j=1;j<n_x;j++){
        ls->rhs[n_x*j]=aa;
    }
    for(int j=0;j<e_x-1;j++){
        ls->rhs[e_x+n_x+n_x*j]=aa;
    }
    for(int j=e_x*n_x+1;j<n_x*n_x;j++){
        ls->rhs[j]=aa;
    }
}

void solve_mat_GaussJordan(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    int allnode=cp->allnode;
    for(int i=0;i<allnode*allnode;i++){
        ls->tmp[i]=ls->mat1[i];
    }
    double r=0;
    for(int k=0;k<allnode-1;k++){
        for(int i=(k+1);i<allnode;i++){
            r=ls->mat1[allnode*i+k]/ls->mat1[allnode*k+k];
            ls->mat1[allnode*i+k]=0;
            for(int j=(k+1);j<allnode;j++){
                ls->mat1[allnode*i+j] = ls->mat1[allnode*i+j] - ls->mat1[allnode*k+j]*r;
            }
            ls->rhs[i] = ls->rhs[i] - ls->rhs[k]*r;
        }
    }
    for(int i=allnode-1;i>=0;i--){
        for(int j=i+1;j<allnode;j++){
            ls->rhs[i] = ls->rhs[i] - ls->mat1[allnode*i+j]*ls->rhs[j];
            ls->mat1[allnode*i+j]=0;
        }
        ls->rhs[i] = ls->rhs[i]/ls->mat1[allnode*i+i];
        ls->mat1[allnode*i+i]=1.0;
    }
    for(int i=0;i<allnode*allnode;i++){
        ls->mat1[i]=ls->tmp[i];
    }

    for(int i=0;i<allnode;i++){
        cp->u[i] = ls->rhs[i];
    }
}

// void solve_mat_GS(
//     double* sol,
//     double* mat1,
//     double* rhs,
//     int allnode,
//     int n_x)
// {
//     for(int l=0;l<MAX_ITERATIONS;l++){
//         for(int i=0;i<allnode;i++){
//             sol[i]=rhs[i];
//             for(int j=0;j<allnode;j++){
//                 if(i != j){
//                     sol[i] -= mat1[i*allnode+j]*sol[j];
//                 }
//             }      
//             sol[i] /= mat1[i*allnode+i];      
//         }

//         double residual=0.0;
//         for(int i=0;i<allnode;i++){
//             double r_i = -rhs[i];
//             for(int j=0;j<allnode;j++){
//                 r_i += mat1[i*allnode+j]*sol[j];
//             }
//             residual += r_i*r_i;
//         }
//         residual=sqrt(residual);
//         printf("GS_loop %d: %e\n", l, residual);
//         if(residual < EPSIRON){
//             return;
//         }
//     }
// }

void solve_mat_BiCGSTAB(
    CALC_POINTS*   cp,
    LIN_SYS*       ls)
{
    const int max=100;
    const double eps=1.0e-5;
    int allnode=cp->allnode;
    int i,j,k;
    double err;
    double* A=(double*)calloc(allnode*allnode,sizeof(double));
    double* b=(double*)calloc(allnode,sizeof(double));
    double* x=(double*)calloc(allnode,sizeof(double));
    double* r=(double*)calloc(allnode,sizeof(double));
    double* rr=(double*)calloc(allnode,sizeof(double));
    double* p=(double*)calloc(allnode,sizeof(double));
    double* y=(double*)calloc(allnode,sizeof(double));
    double* z=(double*)calloc(allnode,sizeof(double));
    double* s=(double*)calloc(allnode,sizeof(double));

    for(i=0;i<allnode;i++){
        for(j=0;j<allnode;j++){
            A[i*allnode+j]=ls->mat1[i*allnode+j];
        }
    }
    for(i=0;i<allnode;i++){
        b[i]=ls->rhs[i];  
    }

    //第0近似解に対する残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        r[i] = b[i]-Ax;
        p[i] = r[i];
    }
    //第0近似解に対するシャドウ残差の計算
    for(i=0;i<allnode;i++){
        double Ax=0.0;
        for(j=0;j<allnode;j++){
            Ax += A[i*allnode+j]*x[j];
        }
        rr[i] = b[i]-Ax;
    }

    double rr0=0;
    for(i=0;i<allnode;i++){
        rr0 += rr[i]*r[i];
    }

    double alpha=0;
    double beta=0;
    double omega=0;
    double e=0.0;

    for(k=0;k<max;k++){
        //y=Apの計算
        for(i=0;i<allnode;i++){
            y[i]=0;
        }   
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                y[i] += A[i*allnode+j]*p[j];
            }
        }
        double ry=0;
        for(i=0;i<allnode;i++){
                ry += rr[i]*y[i];
        }

        //alphaの計算
        double rr0=0;
        for(i=0;i<allnode;i++){
            rr0 += rr[i]*r[i];
        }
        alpha=rr0/ry;

        //sの計算
        for(i=0;i<allnode;i++){
            s[i] = r[i]-alpha*y[i];
        }

        //omegaの計算
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                z[i] = 0;
            }
        }
        for(i=0;i<allnode;i++){
            for(j=0;j<allnode;j++){
                z[i] += A[i*allnode+j]*s[j];
            }
        }
        double zs=0;
        for(i=0;i<allnode;i++){
            zs += z[i]*s[i];
        }
        double zz=0;
        for(i=0;i<allnode;i++){
            zz += z[i]*z[i];
        }
        omega=zs/zz;

        //update x and r 
        for(i=0;i<allnode;i++){
            x[i] += (alpha*p[i]+omega*s[i]);
            r[i] = s[i]-omega*z[i];
        }

        double rr1=0;
        for(i=0;i<allnode;i++){
                rr1 += r[i]*r[i];
        }

        // for(i=0;i<allnode;i++){
        //     cout << x[i] << endl;
        // }
         
        //convergence test
        e = sqrt(rr1);
        cout << "LOOP " << " iter " << k+1 << " err " << e << endl;
        if(e < eps){
            //k++;
            break;
        }

        double rr2=0;
        for(i=0;i<allnode;i++){
                rr2 += rr[i]*r[i];
        }
        beta = alpha/omega*rr2/rr0;

        for(i=0;i<allnode;i++){
            p[i] = r[i]+beta*(p[i]-omega*y[i]);
        }

       // rr0 = rr1;
    }

    for(i=0;i<allnode;i++){
        cp->u[i]=x[i];
    }

    cout << "Bi-CGSTAB " << "iter " << k+1 << " err " << e << endl;

    free(A);
    free(b);
    free(x);
    free(r);
    free(rr);
    free(p);
    free(y);
    free(z);
    free(s);
    A=NULL;
    b=NULL;
    x=NULL;
    r=NULL;
    rr=NULL;
    p=NULL;
    y=NULL;
    z=NULL;
    s=NULL;
}



/* y = Ax */
void vector_x_matrix(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    double *y,
    double *a,
    double *x)
{
    int allnode=cp->allnode;
    double vxm;
    for(int i=0; i<allnode; i++) {
            vxm = 0.0;
            for(int j=0; j<allnode; j++) {
                    vxm += a[i*allnode+j]*x[j];
            }
            y[i] = vxm;
    }
}
/* inner product */
double dot_product(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    double *p,
    double *q)
{
    int allnode=cp->allnode;
    double dot_p = 0.0;
    for(int i=0; i<allnode; i++) {
            dot_p += p[i]*q[i];
    }
    return dot_p;
}
/* L1norm*/
double vector_L1norm(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    double *p)
{
    int allnode=cp->allnode;
    double norm = 0.0;
    for(int i=0; i<allnode; i++) {
            norm += fabs(p[i]); //fabsは絶対値を返す
    }
    return norm;
}
/*L2norm*/
double vector_L2norm(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    double *p)
{
    int allnode=cp->allnode;
    double norm = 0.0;
    for(int i=0; i<allnode; i++) {
            norm += pow(p[i],2.0);
    }
    norm = sqrt(norm);
    return norm;
}

    static const int MAX_ITERATIONS=100;
    static const double EPSIRON=1.0e-7;
/*
void solve_mat_BICGSTAB(
    CALC_POINTS*   cp,
    LIN_SYS*       ls,
    int out,
    int out_last)
{   
    int allnode=cp->allnode;
    int nnnn=cp->nnnn;
    double* x_1 = (double*)calloc(allnode,sizeof(double));
    double* y_1 = (double*)calloc(allnode,sizeof(double));
    double* p  = (double*)calloc(allnode,sizeof(double)); //探索方向ベクトル
    double* r0 = (double*)calloc(allnode,sizeof(double)); //初期残差ベクトル
    double* r  = (double*)calloc(allnode,sizeof(double)); //残差ベクトル
    double* A = (double*)calloc(nnnn,sizeof(double));
    double* Ax = (double*)calloc(allnode,sizeof(double)); //AX
    double* Ap = (double*)calloc(allnode,sizeof(double));
    double* s  = (double*)calloc(allnode,sizeof(double));
    double* As = (double*)calloc(allnode,sizeof(double));

    for(int i=0; i<allnode; i++){
        x_1[i]=cp->u[i];
    }
    for(int i=0; i<allnode; i++){
        y_1[i]=ls->rhs[i];
    }
    for(int i=0; i<nnnn; i++){
        A[i] = ls->mat1[i];
    }

    vector_x_matrix(cp,ls,Ax,A,x_1);
    // r0,r,pを計算 r0 := b - Ax
    for(int i = 0; i < allnode; i++) {
            r0[i]   = y_1[i] - Ax[i];
            r[i]    = r0[i];
            //p0 = r0
            p[i]    = r0[i];
    }
    // (r0, r0*) != 0
    double c1 = dot_product(cp,ls,r0,r0);
    if(c1 == 0.0) {
            fprintf(stderr, "(r0, r0*) == 0!!\n");
            exit(1);  //異常終了
    }


    int count = 0;
    double err;
    for(int iter = 1;iter<MAX_ITERATIONS;iter++) {
        count++;

        vector_x_matrix(cp,ls,Ap,A,p);
        double c2    = dot_product(cp,ls,Ap,r0);
        double alpha = c1 / c2;
        for(int i=0;i<allnode;i++) {
                s[i] = r[i] - alpha*Ap[i];  //e[i]は次のステップのｒ
        }

        vector_x_matrix(cp,ls,As,A,s);
        double s_dot_as  = dot_product(cp,ls,Ap,r0);
        double as_dot_as = dot_product(cp,ls,As,As);
        double c3        = s_dot_as / as_dot_as;
        for(int i = 0; i < allnode; i++) {
                x_1[i] += alpha*p[i] + c3*s[i];
                r[i]  = s[i] - c3*As[i];
        }

        err = vector_L2norm(cp,ls,r)/vector_L2norm(cp,ls,r0);
        if(out==1) {
                printf("LOOP : %d\t Error : %g\n", iter, err);
        }
        if(err < EPSIRON) break;

        double c1_old = c1;
        c1           = dot_product(cp,ls,r,r0);
        double beta  = (alpha*c1) / (c3*c1_old);
        for(int i = 0; i < allnode; i++) {
                p[i] = r[i] + beta*(p[i] - c3*Ap[i]);
        }
    }

    if(out_last == 1) {
            printf(" BiCGSTAB  iter:%d  err:%g\n",count,err);
    }

    free(x_1);
    free(y_1);
    free(p);
    free(r0);
    free(r);
    free(A);
    free(Ax);
    free(Ap);
    free(s);
    free(As);
    x_1=NULL;
    y_1=NULL;
    p = NULL;
    r0 = NULL;
    r = NULL;
    A=NULL;
    Ax = NULL;
    Ap = NULL;
    s = NULL;
    As = NULL;
}
*/

void write_first_gnuplot(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    ofstream fout("answer_0.txt"); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->u[i] << endl;
    }
    fout.close();

}

void write_gnuplot(
    CALC_POINTS* cp,
    int           k)
{
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << k;
    name=ss.str();
    name="answer_" + name + ".txt";
    ofstream fout(name.c_str()); 
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << cp->u[i] << endl;
    }
    fout.close();
}

void write_first_vtk(
    CALC_POINTS* cp)
{
    int allnode=cp->allnode;
    ofstream fout("answer_0.vtk");
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << allnode << " " << "float" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << endl;
    }
    fout << "CELLS" << " " << allnode << " " << allnode*2 << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << allnode << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << allnode << endl;
    fout << "SCALARS u float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->u[i] << endl;
    }
    fout.close(); 
}

void write_vtk(
    CALC_POINTS* cp,
    int           k)
{
    int allnode=cp->allnode;
    stringstream ss;
    string name;
    ss << k;
    name=ss.str();
    name="answer_" + name + ".vtk";
    ofstream fout(name.c_str());
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << allnode << " " << "float" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << endl;
    }
    fout << "CELLS" << " " << allnode << " " << allnode*2 << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << allnode << endl;
    for(int i=0;i<allnode;i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << allnode << endl;
    fout << "SCALARS u float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=0;i<allnode;i++){
        fout << cp->u[i] << endl;
    }
    fout.close();
}

int main(){
    clock_t start=clock();

    CALC_POINTS cp;
    LIN_SYS     ls;

    int E_X=0;
    int E_Y=0;
    cout << "enter the E_X and E_Y" << endl;
    cin >> E_X >> E_Y;
    int N_X=E_X+1;
    int N_Y=E_Y+1;
    int ALLNODE=N_X*N_Y;
    int ALLELEMENT=E_X*E_Y*2;
    //double D[2]={0,0};
    double D[2]={0.01,0.01};
    double V[2]={0.1,0.1};
    double DT=0.001;
    int N_STEP=10000;
    double AA=0;
    double S=1.0/ALLELEMENT;
    double THETA=0.5;
    double L_X=1.0;
    double L_Y=1.0;

    cp.e_x=E_X;
    cp.e_y=E_Y;
    cp.n_x=N_X;
    cp.n_y=N_Y;
    cp.allnode=ALLNODE;
    cp.allelement=ALLELEMENT;
    cp.d[0]=D[0];
    cp.d[1]=D[1];
    cp.v[0]=V[0];
    cp.v[1]=V[1];
    cp.n_step=N_STEP;
    cp.aa=AA;
    cp.s=S;
    cp.dt=DT;
    cp.theta=THETA;
    cp.l_x=L_X;
    cp.l_y=L_Y;

    memory_allocation(&cp,&ls,ALLNODE);

    set_calc_points(&cp);

    set_first_condition(&cp);

    write_first_gnuplot(&cp);

    //write_first_vtk(&cp);

    set_element_mass_matrix(&cp,&ls);

    set_element_diffusion_matrix(&cp,&ls);

    set_element_advection_matrix(&cp,&ls);

    assemble_mass_matrix(&cp,&ls);

    assemble_diffusion_matrix(&cp,&ls);

    assemble_advection_matrix(&cp,&ls);

    make_mat1(&cp,&ls);

    make_mat2(&cp,&ls);

    set_bc(&cp,&ls);

    for(int k=1;k<=N_STEP;k++){
        cout << "--------STEP" << k << "------" <<endl;
    
        set_every_bc(&cp,&ls);

        make_rhs(&cp,&ls);
        
        //solve_mat_GaussJordan(&cp,&ls);
        solve_mat_BiCGSTAB(&cp,&ls);
		//cout<<k<<endl;
        if(k%10==0){
            write_gnuplot(&cp,k);   
        }
        //write_gnuplot(&cp,k);
        //write_vtk(&cp,k);
    }

    memory_free(&cp,&ls);

    clock_t end=clock();

    const double time=static_cast<double>(end-start)/CLOCKS_PER_SEC;
    cout << time << endl;

    return 0;
}

/*
set xrange[0:1]
set yrange[0:1]
set zrange[-0.01:0.07]
do for [i=0:10000]{
    splot "answer_".i.".txt"
}
do for [i=0:1000]{
    splot "answer_".(10*i).".txt"
}
do for [i=0:5000]{
    splot "answer_".i.".txt"
}
splot "answer_0.txt","answer_10.txt","answer_20.txt","answer_30.txt","answer_40.txt","answer_50.txt","answer_60.txt","answer_70.txt","answer_80.txt","answer_90.txt","answer_100.txt"
*/