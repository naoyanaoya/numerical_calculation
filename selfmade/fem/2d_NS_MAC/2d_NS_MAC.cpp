#include<iostream>
#include<cmath>
#include<time.h>
#include<fstream>
#include<iomanip>
#include<string>
#include<sstream>
using namespace std;

typedef struct
{
    int n_x;
    int n_y;
    double divv;
    double err;
    double uwall;
    double dx;
    double dy;
    double dt;
    double re;
    int lm;
    int km;
}CALC_POINTS;

typedef struct
{
    double* u;
    double* v;
    double* p;
    double* rhs;
}LIN_SYS;

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    ls->u=(double*)calloc((n_x+2)*(n_y+1),sizeof(double));
    ls->v=(double*)calloc((n_x+1)*(n_y+2),sizeof(double));
    ls->p=(double*)calloc((n_x+1)*(n_y+1),sizeof(double));
    ls->rhs=(double*)calloc((n_x+1)*(n_y+1),sizeof(double));
}

void memory_free(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    free(ls->u);
    free(ls->v);
    free(ls->p);
    free(ls->rhs);
}

void rhspoisson(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{	
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double dx=cp->dx;
    double dy=cp->dy;
    double uwall=cp->uwall;
    double divv=cp->divv;
    double ua;
    double ub;
    double va;
    double vb;
    double udiv;
    double vdiv;
    double dt=cp->dt;

	//BC for left and right//
	for(int j=0;j<n_y+1;j++)
	{
		ls->u[1*(n_y+1)+j]=0.;
		ls->u[0*(n_y+1)+j]=ls->u[2*(n_y+1)+j];
		ls->v[0*(n_y+2)+j]=-ls->v[1*(n_y+2)+j];
		
		ls->u[n_x*(n_y+1)+j]=0.;
		ls->u[(n_y+1)*(n_y+1)+j]=ls->u[(n_x-1)*(n_y+1)+j];
		ls->v[n_x*(n_y+2)+j]=-ls->v[(n_x-1)*(n_y+2)+j];
	}
	ls->v[0*(n_y+2)+n_y+1]=-ls->v[1*(n_y+2)+n_y+1];
	ls->v[n_x*(n_y+2)+n_y+1]=-ls->v[(n_x-1)*(n_y+2)+n_y+1];
	
	//BC for bottom and top//
	for(int i=0;i<n_x+1;i++)
	{
		ls->v[i*(n_y+2)+1]=0.;
		ls->v[i*(n_y+2)+0]=ls->v[i*(n_y+2)+2];
		ls->u[i*(n_y+1)+0]=-ls->u[i*(n_y+1)+1];
		
		ls->v[i*(n_y+2)+n_y]=0.;
		ls->v[i*(n_y+2)+n_y+1]=ls->v[i*(n_y+2)+n_y-1];
		ls->u[i*(n_y+1)+n_y]=2.0*uwall-ls->u[i*(n_y+1)+n_y-1];//for wall
	}
	ls->u[(n_x+1)*(n_y+1)+0]=-ls->u[n_x*(n_y+1)+0];
	ls->u[(n_x+1)*(n_y+1)+n_y]=-ls->u[n_x*(n_y+1)+n_y];
	
	divv=0.;
	//r.h.s. of Poisson equation
	for(int i=1;i<n_x;i++)
	{
		for(int j=1;j<n_y;j++)
		{
			udiv=(ls->u[(i+1)*(n_y+1)+j]-ls->u[i*(n_y+1)+j])/dx;
			vdiv=(ls->v[i*(n_y+2)+j+1]-ls->v[i*(n_y+2)+j])/dy;
			divv+=fabs(udiv+vdiv);
			ua=(ls->u[i*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j+1]+ls->u[i*(n_y+1)+j+1])/4.0;
			ub=(ls->u[i*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j-1]+ls->u[i*(n_y+1)+j-1])/4.0;
			va=(ls->v[i*(n_y+2)+j]+ls->v[i*(n_y+2)+j+1]+ls->v[(i+1)*(n_y+2)+j+1]+ls->v[(i+1)*(n_y+2)+j])/4.0;
			vb=(ls->v[i*(n_y+2)+j]+ls->v[i*(n_y+2)+j+1]+ls->v[(i-1)*(n_y+2)+j+1]+ls->v[(i-1)*(n_y+2)+j])/4.0;
			ls->rhs[i*(n_y+1)+j]=-udiv*udiv-2.0*(ua-ub)*(va-vb)/dx/dy-vdiv*vdiv+1.0/dt*(udiv+vdiv);
		}
	}
}

void poisson(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
	int n_x=cp->n_x;
    int n_y=cp->n_y;
    double dx=cp->dx;
    double dy=cp->dy;
	double C1=0.5*dy*dy/(dx*dx+dy*dy);
	double C2=0.5*dx*dx/(dx*dx+dy*dy);
	double C3=0.5*dy*dy/(1.+dy*dy/(dx*dx));
	double pres;
    double err=cp->err;
    double km=cp->km;
	
	//Poisson equation//
	for(int k=1;k<=km;k++)
	{
		err=0.;
		//Neumann BC//
		for(int j=0;j<n_y+1;j++)
		{
			ls->p[0*(n_y+1)+j]=ls->p[1*(n_y+1)+j]-1.0/cp->re*2.0*ls->u[2*(n_y+1)+j];
			ls->p[n_x*(n_y+1)+j]=ls->p[(n_x-1)*(n_y+1)+j]+1.0/cp->re*2.0*ls->u[(n_x-1)*(n_y+1)+j];
		}
		
		for(int i=0;i<n_x+1;i++)
		{
			ls->p[i*(n_y+1)+0]=ls->p[i*(n_y+1)+1]-1.0/cp->re*2.0*ls->v[i*(n_y+2)+2];
			ls->p[i*(n_y+1)+n_y]=ls->p[i*(n_y+1)+n_y-1]+1.0/cp->re*2.0*ls->v[i*(n_y+2)+n_y-1];
		}
		
		//iteration//
		for(int i=1;i<n_x;i++)
		{
			for(int j=1;j<n_y;j++)
			{
				pres=C1*(ls->p[(i+1)*(n_y+1)+j]+ls->p[(i-1)*(n_y+1)+j])+C2*(ls->p[i*(n_y+1)+j+1]+ls->p[i*(n_y+1)+j-1])-C3*ls->rhs[i*(n_y+1)+j]-ls->p[i*(n_y+1)+j];
				err+=pres*pres;
				ls->p[i*(n_y+1)+j]=pres+ls->p[i*(n_y+1)+j];
			}
		}
		
		if(err<=0.000005) break;
	}
}

void velocity(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;
    double dx=cp->dx;
    double dy=cp->dy;
    double dt=cp->dt;
	double uad;
    double vad;
	double udif;
    double vdif;
	double umid;
    double vmid;
	
	//u//
	for(int i=2;i<n_x;i++)
	{
		for(int j=1;j<n_y;j++)
		{
			vmid=(ls->v[i*(n_y+2)+j]+ls->v[i*(n_y+2)+j+1]+ls->v[(i-1)*(n_y+2)+j+1]+ls->v[(i-1)*(n_y+2)+j])/4.0;
			uad=ls->u[i*(n_y+1)+j]*(ls->u[(i+1)*(n_y+1)+j]-ls->u[(i-1)*(n_y+1)+j])/2.0/dx+vmid*(ls->u[i*(n_y+1)+j+1]-ls->u[i*(n_y+1)+j-1])/2.0/dy;
			udif=(ls->u[(i+1)*(n_y+1)+j]-2.0*ls->u[i*(n_y+1)+j]+ls->u[(i-1)*(n_y+1)+j])/dx/dx+(ls->u[i*(n_y+1)+j+1]-2.0*ls->u[i*(n_y+1)+j]+ls->u[i*(n_y+1)+j-1])/dy/dy;
			ls->u[i*(n_y+1)+j]=ls->u[i*(n_y+1)+j]+dt*(-uad-(ls->p[i*(n_y+1)+j]-ls->p[(i-1)*(n_y+1)+j])/dx+1.0/cp->re*udif);
		}
	}
	
	//v//
	for(int i=1;i<n_x;i++)
	{
		for(int j=2;j<n_y;j++)
		{
			umid=(ls->u[i*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j]+ls->u[(i+1)*(n_y+1)+j-1]+ls->u[i*(n_y+1)+j-1])/4.0;
			vad=umid*(ls->v[(i+1)*(n_y+2)+j]-ls->v[(i-1)*(n_y+2)+j])/2.0/dx+ls->v[i*(n_y+2)+j]*(ls->v[i*(n_y+2)+j+1]-ls->v[i*(n_y+2)+j-1])/2.0/dy;
			vdif=(ls->v[(i+1)*(n_y+2)+j]-2.0*ls->v[i*(n_y+2)+j]+ls->v[(i-1)*(n_y+2)+j])/dx/dx+(ls->v[i*(n_y+2)+j+1]-2.0*ls->v[i*(n_y+2)+j]+ls->v[i*(n_y+2)+j-1])/dy/dy;
			ls->v[i*(n_y+2)+j]=ls->v[i*(n_y+2)+j]+dt*(-vad-(ls->p[i*(n_y+1)+j]-ls->p[i*(n_y+1)+j-1])/dy+1.0/cp->re*vdif);
		}
	}
}

//いらない
void initialization(
    CALC_POINTS* cp,
    LIN_SYS*     ls)
{
    int n_x=cp->n_x;
    int n_y=cp->n_y;

    for(int i=0;i<n_x+1;i++)
	{
		for(int j=0;j<n_y+1;j++)
		{
			ls->p[i*(n_y+1)+j]=0.;
		}
	}

    for(int i=0;i<n_x+2;i++)
	{
		for(int j=0;j<n_y+1;j++)
		{
			ls->u[i*(n_y+1)+j]=0.;
		}
	}

    for(int i=0;i<n_x+1;i++)
	{
		for(int j=0;j<n_x+2;j++)
		{
			ls->v[i*(n_y+2)+j]=0.;
		}
	}
}

int main(){
    clock_t start=clock();

    CALC_POINTS cp;
    LIN_SYS     ls;

    int N_X=6;
    int N_Y=6;
    double DX=1.0/(N_X-1);
    double DY=1.0/(N_Y-1);
    double DIVV;
    double ERR;
    double UWALL=1.0;
    double DT=0.0001;
    double RE=1;
    int LM=20000;
    int KM=100;

    cp.n_x=N_X;
    cp.n_y=N_Y;
    cp.divv=DIVV;
    cp.err=ERR;
    cp.uwall=UWALL;
    cp.dx=DX;
    cp.dy=DY;
    cp.dt=DT;
    cp.re=RE;
    cp.lm=LM;
    cp.km=KM;

    memory_allocation(&cp,&ls);

    //time step
    for(int l=1;l<=cp.lm;l++)
	{
		rhspoisson(&cp,&ls);
		poisson(&cp,&ls);
	
		if(l%1000==0){
            cout<<l<<" "<<cp.err<<" "<<cp.divv<<endl;
        }

		velocity(&cp,&ls);
	}

    //output
	ofstream fout("pressure.vtk");
    if(fout.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    fout << "# vtk DataFile Version 2.0" << endl;
    fout << "vtk output" << endl;
    fout << "ASCII" << endl;
    fout << "DATASET UNSTRUCTURED_GRID" << endl;
    fout << "POINTS" << " " << (cp.n_x-1)*(cp.n_y-1) << " " << "float" << endl;
    for(int i=1;i<cp.n_x;i++){
        for(int j=1;j<cp.n_y;j++){
            fout << double((i-0.5)*cp.dx) << " "<< double((j-0.5)*cp.dy) << " " << double(0) << endl;
        }
    }
    fout << "CELLS" << " " << (cp.n_x-1)*(cp.n_y-1) << " " << (cp.n_x-1)*(cp.n_y-1)*2 << endl;
    for(int i=0;i<(cp.n_x-1)*(cp.n_y-1);i++){
        fout << 1 << " " << i << endl;
    }
    fout << "CELL_TYPES" << " " << (cp.n_x-1)*(cp.n_y-1) << endl;
    for(int i=0;i<(cp.n_x-1)*(cp.n_y-1);i++){
        fout << 1 << endl;
    }
    fout << "POINT_DATA" << " " << (cp.n_x-1)*(cp.n_y-1) << endl;
    fout << "SCALARS p float" << endl;
    fout << "LOOKUP_TABLE default" << endl;
    for(int i=1;i<cp.n_x;i++)
	{
		for(int j=1;j<cp.n_y;j++)
		{
            fout << ls.p[i*(cp.n_y+1)+j] << endl;
		}
	}
    fout.close();

    ofstream fout2("vector.vtk");
    if(fout2.fail()){
        cout << "出力ファイルをオープンできません" << endl;
        return 1;
    }
    fout2 << "# vtk DataFile Version 2.0" << endl;
    fout2 << "vtk output" << endl;
    fout2 << "ASCII" << endl;
    fout2 << "DATASET UNSTRUCTURED_GRID" << endl;
    fout2 << "POINTS" << " " << (cp.n_x-1)*(cp.n_y-1) << " " << "float" << endl;
    for(int i=1;i<cp.n_x;i++){
        for(int j=1;j<cp.n_y;j++){
            fout2 << double((i-0.5)*cp.dx) << " "<< double((j-0.5)*cp.dy) << " " << double(0) << endl;
        }
    }
    fout2 << "POINT_DATA" << " " << (cp.n_x-1)*(cp.n_y-1) << endl;
    fout2 << "VECTORS velocity float" << endl;
    for(int i=1;i<cp.n_x;i++)
	{
		for(int j=1;j<cp.n_y;j++)
		{
            fout2 << (ls.u[i*(cp.n_y+1)+j]+ls.u[(i+1)*(cp.n_y+1)+j])/2.0 << " " << (ls.v[i*(cp.n_y+2)+j]+ls.v[i*(cp.n_y+2)+j+1])/2.0 << " " << double(0) << endl;
		}
	}
    fout.close(); 

    memory_free(&cp,&ls);

    clock_t end=clock();

    const double time=static_cast<double>(end-start)/CLOCKS_PER_SEC;
    cout << time << endl;

    return 0;
}