#include<iostream>
#include<math.h>
#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
#include<iomanip>
#include<cstdlib>

const int nodes_per_element = 3;
const double theta = 1.0;
const double diffusion_coef = 0.01;
const double EPS = 1.0e-6;
const int iteration_max = 1000;
const double DT = 0.01;
const int TIMESTEP = 250;

// if output_matrix = 1, out "mmatrix.txt" and quit
const int output_matrix_flag = 0;

typedef struct {
    double* x; // coordinate x
    double* y; // coordinate y
    double* z; // coordinate z
    double* u; // scalar
    double* u_boundary; // boundary scalar
    int* nodes_boundary; // 境界上のnode番号
    int number_of_boundary_condition_nodes; // 境界上にあるノードの個数
	int number_of_nodes; // ノードの個数
    int number_of_elements; // エレメントの個数
    int number_of_initial_conditions; // 初期条件の個数(ノードの個数と多分一緒)
}CALC_POINTS;

typedef struct {
    double** mat1; // 質量マトリックス
    double** mat2; // 剛性マトリックス
    double** left_mat; // 左マトリックス
    double** right_mat; // 右マトリックス
    double* mat;
    double** tmp;
    double* rhs;
    int** npe;
    double* ax;
    double* ap;
    double* b;
    double* p; // 探索ベクトル
    double* r; // 残渣ベクトル
}LIN_SYS;

void initial_file_read(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    // ノードの個数を読み取る
    int number_of_line = 0;
    std::ifstream file_in1("input_data/gmsh-python/gmsh_nodes.txt");
    if(file_in1.fail()){
        std::cout << "can not open the input_data/gmsh/gmsh_nodes.txt." << std::endl;
    }
    std::string line;
    while(getline(file_in1, line)){
        ++number_of_line;
    }
    file_in1.close();
    cp->number_of_nodes = number_of_line;
    printf("number_of_nodes is %d\n", cp->number_of_nodes);

    // エレメントの個数を読み取る
    number_of_line = 0;
    std::ifstream file_in4("input_data/gmsh-python/gmsh_elements.txt");
    if(file_in4.fail()){
        std::cout << "can not open the input_data/gmsh-python/gmsh_elements.txt." << std::endl;
    }
    while(getline(file_in4, line)){
        ++number_of_line;
    }
    file_in4.close();
    cp->number_of_elements = number_of_line;
    printf("number_of_elements is %d\n", cp->number_of_elements);

    // 初期条件の個数
    number_of_line = 0;
    std::ifstream file_in2("input_data/gmsh-python/gmsh_initial_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the input_data/gmsh-python/gmsh_initial_condition.txt." << std::endl;
    }
    while(getline(file_in2, line)){
        ++number_of_line;
    }
    cp->number_of_initial_conditions = number_of_line;
    file_in2.close();
    printf("number_of_initial_conditions is %d\n", cp->number_of_initial_conditions);

    // 境界条件の個数
    number_of_line = 0;
    std::ifstream file_in3("input_data/gmsh-python/gmsh_boundary_condition.txt");
    if(file_in3.fail()){
        std::cout << "can not open the input_data/gmsh-python/gmsh_boundary_condition.txt." << std::endl;
    }
    while(getline(file_in3, line)){
        ++number_of_line;
    }
    cp->number_of_boundary_condition_nodes = number_of_line;
    file_in3.close();
    printf("number_of_boundary_condition is %d\n", cp->number_of_boundary_condition_nodes);
}

void memory_allocation(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    int NN = cp->number_of_nodes;
    cp->x = (double*)calloc(NN, sizeof(double));
    cp->y = (double*)calloc(NN, sizeof(double));
    cp->z = (double*)calloc(NN, sizeof(double));
    cp->u = (double*)calloc(NN, sizeof(double));
    cp->u_boundary = (double*)calloc(cp->number_of_boundary_condition_nodes, sizeof(double));
    cp->nodes_boundary = (int*)calloc(cp->number_of_boundary_condition_nodes, sizeof(int));

    ls->ax = (double*)calloc(NN, sizeof(double));
    ls->ap = (double*)calloc(NN, sizeof(double));
    ls->b = (double*)calloc(NN, sizeof(double));
    ls->p = (double*)calloc(NN, sizeof(double));
    ls->r = (double*)calloc(NN, sizeof(double));
    ls->mat1 = (double**)calloc(NN, sizeof(double*));
    for (int i = 0; i < NN; i++) {
        ls->mat1[i] = (double*)calloc(NN, sizeof(double));
    }
    ls->mat2 = (double**)calloc(NN, sizeof(double*));
    for (int i = 0; i < NN; i++) {
        ls->mat2[i] = (double*)calloc(NN, sizeof(double));
    }
    ls->left_mat = (double**)calloc(NN, sizeof(double*));
    for (int i = 0 ; i < NN; i++) {
        ls->left_mat[i] = (double*)calloc(NN, sizeof(double));
    }
    ls->right_mat = (double**)calloc(NN, sizeof(double*));
    for (int i = 0 ; i < NN; i++) {
        ls->right_mat[i] = (double*)calloc(NN, sizeof(double));
    }
    ls->mat = (double*)calloc(NN * NN, sizeof(double*));
    ls->tmp = (double**)calloc(NN, sizeof(double*));
    for (int i = 0; i < NN; i++) {
        ls->tmp[i] = (double*)calloc(NN, sizeof(double));
    }
    ls->rhs = (double*)calloc(NN, sizeof(double));
    ls->npe = (int**)calloc(cp->number_of_elements, sizeof(int*));
    for (int i = 0; i < cp->number_of_elements; i++){
        ls->npe[i] = (int*)calloc(3, sizeof(double));
    }
}

void memory_free(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    free(cp->x);
    free(cp->y);
    free(cp->z);
    free(cp->u);
    free(cp->u_boundary);
    free(cp->nodes_boundary);
    free(ls->ax);
    free(ls->ap);
    free(ls->b);
    free(ls->p);
    free(ls->r);

    for (int i = 0; i < cp->number_of_nodes; i++){
        free(ls->mat1[i]);
    }
    free(ls->mat1);
    for (int i = 0; i < cp->number_of_nodes; i++){
        free(ls->mat2[i]);
    }
    free(ls->mat2);
    for (int i = 0; i < cp->number_of_nodes; i++){
        free(ls->left_mat[i]);
    }
    free(ls->left_mat);
    for (int i = 0; i < cp->number_of_nodes; i++){
        free(ls->right_mat[i]);
    }
    free(ls->right_mat);
    free(ls->mat);
    for (int i = 0; i < cp->number_of_nodes; i++){
        free(ls->tmp[i]);
    }
    free(ls->tmp);
    free(ls->rhs);
    for (int i = 0; i < cp->number_of_elements; i++){
        free(ls->npe[i]);
    }
    free(ls->npe);
}

void set_calc_points(
    CALC_POINTS* cp
)
{
    std::ifstream file_in2("input_data/gmsh-python/gmsh_nodes.txt");
    if(file_in2.fail()){
        std::cout << "can not open the input_data/gmsh/gmsh_nodes.txt." << std::endl;
    }
    for (int i = 0; i < cp->number_of_nodes; i++){
        file_in2 >> cp->x[i] >> cp->y[i] >> cp->z[i];
    }
    file_in2.close();
}

void set_initial_condition(
    CALC_POINTS* cp
)
{
    std::ifstream file_in2("input_data/gmsh-python/gmsh_initial_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the input_data//gmsh/gmsh_initial_condition.txt." << std::endl;
    }
    int tmp = 0;
    for (int i = 0; i < cp->number_of_initial_conditions; i++){
        file_in2 >> tmp >> cp->u[i];
    }
    file_in2.close();
}

void read_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    std::ifstream file_in2("input_data/gmsh-python/gmsh_boundary_condition.txt");
    if(file_in2.fail()){
        std::cout << "can not open the input_data/gmsh-python/gmsh_boundary_condition.txt" << std::endl;
    }
    for(int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        file_in2 >> cp->nodes_boundary[i] >> cp->u_boundary[i];
        // 0indexに合わせる
        --cp->nodes_boundary[i];
    }
    file_in2.close();
}

void set_boundary_condition(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++) {
        cp->u[cp->nodes_boundary[i]] = cp->u_boundary[i];
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++) {
        for (int j = 0; j < cp->number_of_nodes; j++) {
            ls->left_mat[cp->nodes_boundary[i]][j] = 0;
        }
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        ls->left_mat[cp->nodes_boundary[i]][cp->nodes_boundary[i]] = 1;
    }
    for (int i = 0; i < cp->number_of_boundary_condition_nodes; i++){
        ls->rhs[cp->nodes_boundary[i]] = 0;
    }
}

void mesh_connectivity(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    std::ifstream file_in2("input_data/gmsh-python/gmsh_elements.txt");
    if (file_in2.fail()) {
        std::cout << "can not open the input_data/gmsh/gmsh_elements.txt." << std::endl;
    }
    for (int i = 0; i < cp->number_of_elements; i++){
        file_in2 >> ls->npe[i][0] >> ls->npe[i][1] >> ls->npe[i][2];
    }
    file_in2.close();
    for (int i = 0; i < cp->number_of_elements; i++) {
        ls->npe[i][0] = ls->npe[i][0] - 1;
        ls->npe[i][1] = ls->npe[i][1] - 1;
        ls->npe[i][2] = ls->npe[i][2] - 1;;
    }
}

void make_M1_and_M2(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    int I1 = 0;
    int I2 = 0;
    int I3 = 0;
    double X1 = 0;
    double X2 = 0;
    double X3 = 0;
    double Y1 = 0;
    double Y2 = 0;
    double Y3 = 0;
    double B1 = 0;
    double B2 = 0;
    double B3 = 0;
    double C1 = 0;
    double C2 = 0;
    double C3 = 0;
    double det = 0;
    for (int i = 0; i < cp->number_of_elements; i++) {
        // printf("-----------------start--------------\n");
        I1 = ls->npe[i][0];
        I2 = ls->npe[i][1];
        I3 = ls->npe[i][2];
        // printf("%d, %d, %d\n", I1, I2, I3);
        X1 = cp->x[I1];
        X2 = cp->x[I2];
        X3 = cp->x[I3];
        Y1 = cp->y[I1];
        Y2 = cp->y[I2];
        Y3 = cp->y[I3];
        // printf("%02.3f, %02.3f, %02.3f, %02.3f, %02.3f, %02.3f\n", X1, X2, X3, Y1, Y2, Y3);
        det = 0.5 * (X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2));
        // printf("%d's determinant is %02.3f\n", i, det);
        ls->mat1[I1][I1] += det / 2.0 * 12.0;
        ls->mat1[I1][I2] += det / 12.0;
        ls->mat1[I1][I3] += det / 12.0;
        ls->mat1[I2][I1] += det / 12.0;
        ls->mat1[I2][I2] += det / 2.0 * 12.0;
        ls->mat1[I2][I3] += det / 12.0;
        ls->mat1[I3][I1] += det / 12.0;
        ls->mat1[I3][I2] += det / 12.0;
        ls->mat1[I3][I3] += det / 2.0 * 12.0;
        B1 = (Y2 - Y3) * 0.5 / det;
        B2 = (Y3 - Y1) * 0.5 / det;
        B3 = (Y1 - Y2) * 0.5 / det;
        C1 = (X3 - X2) * 0.5 / det;
        C2 = (X1 - X3) * 0.5 / det;
        C3 = (X2 - X1) * 0.5 / det;
        ls->mat2[I1][I1] += diffusion_coef * (B1 * B1) + diffusion_coef * (C1 * C1);
        ls->mat2[I1][I2] += diffusion_coef * (B1 * B2) + diffusion_coef * (C1 * C2);
        ls->mat2[I1][I3] += diffusion_coef * (B1 * B3) + diffusion_coef * (C1 * C3);
        ls->mat2[I2][I1] += diffusion_coef * (B2 * B1) + diffusion_coef * (C2 * C1);
        ls->mat2[I2][I2] += diffusion_coef * (B2 * B2) + diffusion_coef * (C2 * C2);
        ls->mat2[I2][I3] += diffusion_coef * (B2 * B3) + diffusion_coef * (C2 * C3);
        ls->mat2[I3][I1] += diffusion_coef * (B3 * B1) + diffusion_coef * (C3 * C1);
        ls->mat2[I3][I2] += diffusion_coef * (B3 * B2) + diffusion_coef * (C3 * C2);
        ls->mat2[I3][I3] += diffusion_coef * (B3 * B3) + diffusion_coef * (C3 * C3);
    }
    // int NN = cp->number_of_nodes;
    // for (int i = 0; i < NN; i++) {
    //     for (int j = 0; j < NN; j++) {
    //         printf("%1.3f ", ls->mat1[i][j]);
    //     }
    //     printf("\n");
    // }
    // exit(1);
}

void make_left_and_right_matrix(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    int NN = cp->number_of_nodes;
    for (int i = 0; i < NN; i++) {
        for (int j = 0; j < NN; j++) {
            ls->left_mat[i][j] = ls->mat1[i][j] / DT + theta * ls->mat2[i][j];
            // ls->left_mat[i][j] = ls->mat1[i][j] / DT + theta * ls->mat2[i][j];
        }
    }
    for (int i = 0; i < NN ;i++){
        for (int j = 0;j < NN;j++){
            ls->right_mat[i][j] = ls->mat1[i][j] / DT - (1 - theta) * ls->mat2[i][j];
            // ls->right_mat[i][j] = ls->mat1[i][j] / DT - (1 - theta) * ls->mat2[i][j];
        }
    }
}

void make_rhs(
    CALC_POINTS*    cp,
    LIN_SYS*        ls)
{
    int NN = cp->number_of_nodes;
    for (int i = 0; i < NN; i++) {
        ls->rhs[i]=0;
    }
    for (int i = 0; i < NN; i++) {
        for (int j = 0; j < NN; j++) {
            ls->rhs[j] += ls->right_mat[i][j] * cp->u[j];
        }
    }
}

void cg_method(
    CALC_POINTS* cp,
    LIN_SYS* ls
)
{
    // Axを計算
    double vxm = 0;
    for (int i = 0; i < cp->number_of_nodes; i++) {
        vxm = 0;
        for (int j = 0; j < cp->number_of_nodes; j++){
            vxm += ls->left_mat[i][j] * cp->u[j];
        }
        ls->ax[i] = vxm;
    }

    // pとrを計算
    for (int i = 0; i <cp->number_of_nodes; i++) {
        ls->p[i] = ls->rhs[i] - ls->ax[i];
        ls->r[i] = ls->p[i];
    }

    for (int iter = 1; iter<iteration_max; iter++) {
        double alpha = 0;
        double alpha1 =0;
        double alpha2 = 0;
        double beta = 0;
        double beta1 = 0;
        double beta2 = 0;
        double error = 0;

        // alphaを計算
        for(int i=0;i<cp->number_of_nodes;i++){
            vxm = 0;
            for(int j=0;j<cp->number_of_nodes;j++){
                vxm += ls->left_mat[i][j] * ls->p[j];
            }
            ls->ap[i] = vxm;
        }
        for (int i = 0; i < cp->number_of_nodes; i++) {
            alpha1 += ls->p[i] * ls->r[i];
            alpha2 += ls->p[i] * ls->ap[i];
        }
        alpha = alpha1 / alpha2;

        for (int i = 0; i < cp->number_of_nodes; i++) {
            cp->u[i] += alpha * ls->p[i];
            ls->r[i] += -alpha * ls->ap[i];
        }

        for (int i = 0; i < cp->number_of_nodes; i++) {
            error += fabs(ls->r[i]);
        }

        std::cout << "LOOP : " << iter << " ERROR : " << error << std::endl;

        if(error < EPS){
            break;
        }

        for (int i = 0; i < cp->number_of_nodes; i++) {
            beta1 += ls->r[i] * ls->ap[i];
            beta2 += ls->p[i] * ls->ap[i];
        }
        beta = -1.0 * beta1 / beta2;

        for (int i = 0; i < cp->number_of_nodes; i++) {
            ls->p[i] = ls->r[i] + beta * ls->p[i];
        }
    }
}

void write_vtk(
    CALC_POINTS* cp,
    LIN_SYS* ls,
    int           k)
{
    std::stringstream ss;
    std::string name;
    ss << k;
    name=ss.str();
    name="output/answer_" + name + ".vtk";
    std::ofstream fout(name.c_str());
    if(fout.fail()){
        std::cout << "出力ファイルをオープンできません" << std::endl;
        return;
    }
    fout << "# vtk DataFile Version 2.0" << std::endl;
    fout << "vtk output" << std::endl;
    fout << "ASCII" << std::endl;
    fout << "DATASET UNSTRUCTURED_GRID" << std::endl;
    fout << "POINTS" << " " << cp->number_of_nodes << " " << "float" << std::endl;
    for(int i=0;i<cp->number_of_nodes;i++){
        fout << cp->x[i] << " " << cp->y[i] << " " << 0.0 << std::endl;
    }
    fout << "CELLS" << " " << cp->number_of_elements << " " << cp->number_of_elements * (nodes_per_element + 1) << std::endl;
    for(int i=0;i<cp->number_of_elements;i++){
        fout << nodes_per_element << " " << ls->npe[i][0] << " "  << ls->npe[i][1] << " " <<  ls->npe[i][2] << std::endl;
    }
    fout << "CELL_TYPES" << " " << cp->number_of_elements << std::endl;
    for(int i=0;i<cp->number_of_elements;i++){
        fout << 5 << std::endl;
    }
    fout << "POINT_DATA" << " " << cp->number_of_nodes << std::endl;
    fout << "SCALARS u float" << std::endl;
    fout << "LOOKUP_TABLE default" << std::endl;
    for (int i = 0; i < cp->number_of_nodes; i++){
        fout << cp->u[i] << std::endl;
    }
    fout.close();
}

// void output_matrix(
//     CALC_POINTS* cp,
//     LIN_SYS* ls
// )
// {
//     if(output_matrix_flag == 1){
//         std::ofstream file_out3("out/left_matrix.txt");
//         for(int i = 0; i < cp->number_of_nodes; i++){
//             for(int j = 0; j < cp->number_of_nodes; j++){
//                 file_out3 << ls->left_mat[i][j] << " ";
//             }
//             file_out3 << std::endl;
//         }
//         file_out3.close();
//         // std::exit(1);
//         std::ofstream file_out4("out/right_matrix.txt");
//         for(int i = 0; i < cp->number_of_nodes; i++){
//             for(int j = 0; j < cp->number_of_nodes; j++){
//                 file_out4 << ls->right_mat[i][j] << " ";
//             }
//             file_out4 << std::endl;
//         }
//         file_out4.close();
//         std::exit(1);
//     }
// }

int main(
    int argc,
    char* argv[]
)
{
    clock_t start = clock();

    CALC_POINTS cp;
    LIN_SYS ls;

    initial_file_read(&cp, &ls);
    memory_allocation(&cp, &ls);
    set_calc_points(&cp);
    set_initial_condition(&cp);
    mesh_connectivity(&cp, &ls);
    make_M1_and_M2(&cp, &ls);
    make_left_and_right_matrix(&cp, &ls);
    // output_matrix(&cp, &ls);
    read_boundary_condition(&cp, &ls);
    set_boundary_condition(&cp, &ls);
    write_vtk(&cp, &ls, 0);
    for (int k = 1; k <= TIMESTEP; k++){
        std::cout << "----------" << k << " " << "step" << "--------" << std::endl;
        set_boundary_condition(&cp, &ls);
        make_rhs(&cp,&ls);
        cg_method(&cp, &ls);
        if(k % 1 == 0){
            write_vtk(&cp, &ls, k);
        }
    }
    memory_free(&cp, &ls);

    clock_t end = clock();
    const double time = static_cast<double>(end - start) / CLOCKS_PER_SEC;
    std::cout << time << std::endl;

    return 0;
}
