#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<string>
#include<sstream>
using namespace std;

int main(){
    int i,j,k;
    int Ele=600;
    int Node=Ele+1;
    double LL=1.0;
    double dx=LL/Ele;
    double dt=0.001;
    double NT=9000;
    double Diff=0.01;
    double V=0.1;
    int bc=3;
    double D0=0.0;
    double D1=0.0;
    double N0=0.0;
    double N1=0.0;
    int div=5;

    double *b=new double[Node];
	double *x=new double[Node];
	double *f=new double[Node];
	double *AD=new double[Node];
	double *AL=new double[Node];
	double *AR=new double[Node];
	double *BD=new double[Node];
	double *BL=new double[Node];
	double *BR=new double[Node];

    //initial condition
    for(i=0;i<Node;i++){
        x[i]=0.0;
    }
    for(i=20;i<100;i++){
        x[i]=1.0;
    }
    
    stringstream ss;
    string name;
    ofstream fo;
    static int count=0;
    ss << count;
    name=ss.str();
    name="answer_" + name + ".txt";
    fo.open(name.c_str());
    for(int i=0;i<Node;i++){
        fo << dx* double(i) << " " << x[i] << endl;
    }
    fo << endl;
    count+=1;

    //make matrix
    //initialization
    for(j=0;j<Node;j++){
        AD[i]=0.0;
        AL[i]=0.0;
        AR[i]=0.0;
        BD[i]=0.0;
        BL[i]=0.0;
        BR[i]=0.0;
    }
    for(j=0;j<Ele;j++){
        double time=(2.0/dt);
        double advec=(2.0*abs(V)/dx);
        double tau=1.0/sqrt(time*time+advec*advec);
        double the=0.0;
        AD[j]+=dx*2.0/6.0/dt;
		AR[j]+=dx*1.0/6.0/dt;
		AL[j+1]+=dx*1.0/6.0/dt;
		AD[j+1]+=dx*2.0/6.0/dt;

		//SUPG for temporal//
		AD[j]+=-tau*V/2.0/dt;
		AR[j]+=tau*V/2.0/dt;
		AL[j+1]+=-tau*V/2.0/dt;
		AD[j+1]+=tau*V/2.0/dt;

		//advection//
        AD[j]+=the*-V/2.0;
		AR[j]+=the*V/2.0;
		AL[j+1]+=the*-V/2.0;
		AD[j+1]+=the*V/2.0;

		//SUPG for advection//
		AD[j]+=the*tau*V*V/dx;
		AR[j]+=-the*tau*V*V/dx;
		AL[j+1]+=the*-tau*V*V/dx;
		AD[j+1]+=the*tau*V*V/dx;
		
		//B//
		//temporal//
		BD[j]+=dx*2.0/6.0/dt;
		BR[j]+=dx*1.0/6.0/dt;
		BL[j+1]+=dx*1.0/6.0/dt;
		BD[j+1]+=dx*2.0/6.0/dt;

		//SUPG for temporal//
		BD[j]+=-tau*V/2.0/dt;
		BR[j]+=tau*V/2.0/dt;
		BL[j+1]+=-tau*V/2.0/dt;
		BD[j+1]+=tau*V/2.0/dt;
		
		//advection//
		BD[j]-=(1.0-the)*-V/2.0;
		BR[j]-=(1.0-the)*V/2.0;
		BL[j+1]-=(1.0-the)*-V/2.0;
		BD[j+1]-=(1.0-the)*V/2.0;

		//SUPG for advection//
		BD[j]-=(1.0-the)*tau*V*V/dx;
		BR[j]-=(1.0-the)*-tau*V*V/dx;
		BL[j+1]-=(1.0-the)*-tau*V*V/dx;
		BD[j+1]-=(1.0-the)*tau*V*V/dx;
    }

    //introduce boundary
    if(bc==1)
	{
		AD[0]=1.0;AR[0]=0.0;BD[0]=1.0;BR[0]=0.0;
		AL[Node-1]=0.0;AD[Node-1]=1.0;BL[Node-1]=0.0;BD[Node-1]=1.0;
	}
	if(bc==2)
	{
		AL[Node-1]=0.0;AD[Node-1]=1.0;BL[Node-1]=0.0;BD[Node-1]=1.0;
	}
	if(bc==3)
	{
		AD[0]=1.0;AR[0]=0.0;BD[0]=1.0;BR[0]=0.0;
	}

    for(i=1;i<=NT;i++){
        //for b
        if(bc==1)
		{
			b[0]=D0;
			b[Node-1]=D1;
		}
		if(bc==2)
		{
			b[Node-1]=D1;
			b[0]=BD[0]*x[0]+BR[0]*x[1]-N0*Diff;
		}
		if(bc==3)
		{
			b[0]=D0;
			b[Node-1]=BL[Node-1]*x[Node-2]+BD[Node-1]*x[Node-1]+N1*Diff;
		}
        for(j=1;j<Node-1;j++){
            b[j]=BL[j]*x[j-1]+BD[j]*x[j]+BR[j]*x[j+1];
        }

        //TDMA
        double *P=new double[Node];
	    double *Q=new double[Node];
        //first step
		P[0]=-AR[0]/AD[0];
		Q[0]=b[0]/AD[0];
		//second ste
		for(k=1;k<Node;k++){
			P[k]=-AR[k]/(AD[k]+AL[k]*P[k-1]);
			Q[k]=(b[k]-AL[k]*Q[k-1])/(AD[k]+AL[k]*P[k-1]);
		}
		//third step
		x[Node-1]=Q[Node-1];
		//backward
		for(k=Node-2;k>-1;k=k-1){
			x[k]=P[k]*x[k+1]+Q[k];
		}	
		delete [] P,Q;

        if(i%div==0){
            cout << i << endl;
            stringstream ss;
            string name;
            ofstream fo;
            static int count=0;
            ss << count;
            name=ss.str();
            name="answer_" + name + ".txt";
            fo.open(name.c_str());
            for(int j=0;j<Node;j++){
                fo << dx* double(j) << " " << x[j] << endl;
            }
            fo << endl;
            count+=1;
        }
    }
}

/*
do for [i=0:1799]{
    plot "answer_".i.".txt"
}
*/