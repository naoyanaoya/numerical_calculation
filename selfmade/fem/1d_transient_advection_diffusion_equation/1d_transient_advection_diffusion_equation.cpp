#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<string>
#include<sstream>
using namespace std;
int main(){
	int i,j,k;
	int Ele=100;
	int Node=Ele+1;
	double LL=1.0;
	double dx=LL/Ele;
	double dt=0.001;
	double NT=10000;
	double Diff=0.01;
    double V=0.1;
	double theta;
	cout << "θを入力、θ=0の場合、Eulerの前進差分法（陽解法）、θ=0.5の場合、Crank-Nicolson法（半員解法）、θ=1の場合、後退差分法（完全陰解法）" << endl;
	cin >> theta;
	double D0=0.0;
	double D1=0.0;

	double *b=new double[Node];
	double *x=new double[Node];
	double *xx=new double[Node];
	double *f=new double[Node];
	double *AD=new double[Node];
	double *AL=new double[Node];
	double *AR=new double[Node];
	double *BD=new double[Node];
	double *BL=new double[Node];
	double *BR=new double[Node];

	ofstream fk;
	fk.open("answer_0.txt");

	//initial condition
	for(i=0;i<Node;i++){
		x[i]=min(dx*float(i),1.0-dx*float(i));
		fk << dx*float(i) << " " << x[i] << endl;
	}

	//make matrix
	//initialization
	for(i=0;i<Node;i++){
		AD[i]=0.0;
		AL[i]=0.0;
		AR[i]=0.0;
		BD[i]=0.0;
		BL[i]=0.0;
		BR[i]=0.0;
	}
	for(i=0;i<Node;i++){
		//A//
		//teporal//
		AD[i]+=dx*2.0/6.0/dt;
		AR[i]+=dx*1.0/6.0/dt;
		AL[i+1]+=dx*1.0/6.0/dt;
		AD[i+1]+=dx*2.0/6.0/dt;
		
		//diffusion//
		AD[i]+=theta*Diff/dx;
		AR[i]+=theta*-Diff/dx;
		AL[i+1]+=theta*-Diff/dx;
		AD[i+1]+=theta*Diff/dx;

        //advection
        AD[i]+=theta*-V/2.0;
		AR[i]+=theta*V/2.0;
		AL[i+1]+=theta*-V/2.0;
		AD[i+1]+=theta*V/2.0;
		
		//B//
		//temporal//
		BD[i]+=dx*2.0/6.0/dt;
		BR[i]+=dx*1.0/6.0/dt;
		BL[i+1]+=dx*1.0/6.0/dt;
		BD[i+1]+=dx*2.0/6.0/dt;
		
		//diffusion//
		BD[i]+=-(1.0-theta)*Diff/dx;
		BR[i]+=-(1.0-theta)*-Diff/dx;
		BL[i+1]+=-(1.0-theta)*-Diff/dx;
		BD[i+1]+=-(1.0-theta)*Diff/dx;

        //advection//
		BD[i]+=-(1.0-theta)*-V/2.0;
		BR[i]+=-(1.0-theta)*V/2.0;
		BL[i+1]+=-(1.0-theta)*-V/2.0;
		BD[i+1]+=-(1.0-theta)*V/2.0;
	}

	//initroduce boundary
		AD[0]=1.0;
		AR[0]=0.0;
		BD[0]=1.0;
		BR[0]=0.0;
		AL[Node-1]=0.0;
		AD[Node-1]=1.0;
		BL[Node-1]=0.0;
		BD[Node-1]=1.0;

	for(i=1;i<=NT;i++){
		//forb
		for(j=0;j<=Node;j++){
			b[j]=BL[j]*x[j-1]+BD[j]*x[j]+BR[j]*x[j+1];
		}
		b[0]=D0;
		b[Node-1]=D1;

		//TDMA
		double *P=new double[Node];
		double *Q=new double[Node];
		//first step
		P[0]=-AR[0]/AD[0];
		Q[0]=b[0]/AD[0];
		//second ste
		for(k=1;k<Node;k++){
			P[k]=-AR[k]/(AD[k]+AL[k]*P[k-1]);
			Q[k]=(b[k]-AL[k]*Q[k-1])/(AD[k]+AL[k]*P[k-1]);
		}
		//third step
		xx[Node-1]=Q[Node-1];
		//backward
		for(k=Node-2;k>-1;k=k-1)
		{
			x[k]=P[k]*x[k+1]+Q[k];
		}
		
		delete [] P,Q;

		if(i%1==0){
			cout << i << endl;
			stringstream ss;
			string name;
			ofstream fo;
			ss << i;
			name=ss.str();
			name="answer_" + name + ".txt";
			fo.open(name.c_str ());
			for(j=0;j<Node;j++){
				fo << dx*float(j) << " " << x[j] << endl;
			}
		}
	}
	delete[] b,x,xx,f,AD,AL,AR,BD,BL,BR;
	return 0;
}

/*
do for [i=0:10000]{
    plot "answer_".i.".txt"
}
*/
