参考書とプログラムの対応

PythonCFD-main pythonで学ぶ流体力学の数値計算法

fem_3 第3版有限要素法による流れのシミュレーション

cfd_suutiryuutai 数値流体解析の基礎

cfd_ryuutaikaisekinokiso 流体解析の基礎プログラム 

fem_parallel 有限要素法で学ぶ並列プログラミングの基礎

argorithm アルゴリズムとデータ構造

fortran90 数値計算のためのfortran90
