! ==================================================
! phi = (sin(pi * x1) * sinh(pi * (1 - x2))) / sinh(pi)
! ===================================================

! =====================================================
! gnuplotでの表示方法
! gnuplot> set pm3d
! gnuplot> set palette rgbformulae 33, 13, 10
! gnuplot> splot "lattice_phi.d" with pm3d
! =====================================================

program lattice_phi
  implicit none
  real(8), allocatable ::  phi(:,:)
  real(8), allocatable :: xy(:, : ,:)
  real(8), parameter :: pi = 2.0d0 * acos(0.0d0)
  integer :: i, j
  integer :: n(2)=(/ 50, 50 /) ! x方向とy方向の分割数
  open(11, file='lattice_phi.d')
  ! allocate(phi(n(1) + 1, n(2) + 1))
  allocate(xy(2, n(1) + 1, n(2) + 1))

  do j=1, n(2) + 1
    do i=1, n(1) + 1
      xy(1,i,j)= (1.0d0 - 0.0d0) * (i - 1) / real(n(1), kind = 8)
       ! 整数倍して最後に割って、浮動小数点数の誤差が溜まるのを避ける
      xy(2,i,j)= (1.0d0 - 0.0d0) * (j - 1) / real(n(2), kind = 8)
    enddo
  enddo


  ! x-direction
  do j = 1, n(2) + 1
    do i = 1, n(1) + 1
      write(11, *) xy(1, i, j), xy(2, i, j), (sin(pi * xy(1, i, j)) * sinh(pi * (1 - xy(2, i, j)))) / sinh(pi)
    end do
    write(11, *)
  end do

  ! y-direction
  do i = 1, n(1) + 1
    do j = 1, n(2) + 1
      write(11, *) xy(1, i, j), xy(2, i, j), (sin(pi * xy(1, i, j)) * sinh(pi * (1 - xy(2, i, j)))) / sinh(pi)
    end do
    write(11, *)
  end do

  close(11)
end program lattice_phi