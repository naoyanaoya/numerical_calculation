! ===============================================
! 標準入力から割付け配列の寸法を定める内積計算プログラム
! ===============================================
program dotp4
  implicit none
  real(8), allocatable :: u(:), v(:)
  integer :: n
  write(*,*) "input n : "
  read(*,*) n
  allocate(u(n), v(n))
  write(*,*) "input u(1 : n) : "
  read(*,*) u(1 : n)
  write(*,*) "input v(1 : n) : "
  read(*,*) v(1 : n)
  write(*,*) "dp = ", dot_product(u,v)
  deallocate(u,v)
end program dotp4
