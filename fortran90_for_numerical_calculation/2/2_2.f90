program test
  implicit none
  integer i
  integer, parameter :: DEBUG = 1
  integer :: ia(1:4) = (/1, 2, 3, 4 /)
  if(DEBUG == 1) then
    ia(2:4) = ia(1:3)
    do i = 1, 4
      write(*,*) ia(i)
    end do
  else
    do i = 2, 4
      ia(i) = ia(i - 1)
    end do
    do i = 1, 4
      write(*,*) ia(i)
    end do
  end if
end program test