! ==========================================
! 定数により配列の寸法を定めるプログラム例
! ==========================================
program dotp2
  implicit none
  integer i
  integer, parameter :: n = 2
  real(8) u(n), v(n), dotp
  ! 配列の初期値そ設定(プログラムは省略)
  dotp = 0.0d0
  do i = 1, n
    dotp = dotp + u(i) * v(i)
  end do
  write(*,*) "dot product = ", dotp
end program dotp2
