! =================================================
! ベクトルの内積を計算する最初のプログラム
! =================================================
program dotp1
  implicit none
  integer i
  real(8) u(2), v(2), dotp
  u(1) = 1.2d0
  u(2) = 3.4d0
  v(1) = 4.1d0
  v(2) = 2.6d0
  write(*,"(2f10.7)") (u(i), i = 1, 2)
  write(*,"(2f10.7)") (v(i), i = 1, 2)
  dotp = 0.0d0
  do i = 1, 2
    dotp = dotp + u(i) * v(i)
  end do
  write(*,*) "dot product = ", dotp
end program dotp1