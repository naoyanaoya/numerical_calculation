! ===========================================
! 3次元配列要素に0を代入する時間を計測するプログラム
! ===========================================
! m1 macbook airの16GBだと、n=900が限界?
program mat3d
  implicit none
  integer i, j, k, is, n
  real(8) t1, t2
  integer, allocatable :: a(:, :, :)
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  allocate(a(n,n,n), stat = is) ! 状態変数値の取得
  if(is /= 0) stop "cannot allocate (n is too large)"
  ! ================ メモリ上で近接する要素に順に0を代入 =================
  call cpu_time(t1)
  do k = 1, n
    do j = 1, n
      do i = 1, n
        a(i, j, k) = 0
      end do
    end do
  end do
  call cpu_time(t2)
  write(*,*) "cpu time = ", t2 - t1
  call cpu_time(t1)
  do i = 1, n
    do j = 1, n
      do k = 1, n
        a(i, j, k) = 0
      end do
    end do
  end do
  call cpu_time(t2)
  write(*,*) "cpu time = ", t2 - t1
  call cpu_time(t1)
  a(1:n, 1:n, 1:n) = 0
  call cpu_time(t2)
  write(*,*) "cpu time = ", t2 - t1
  deallocate(a)
end program mat3d
