! ===========================================
! 一様乱数を要素とする上三角行列を設定するプログラム
! ===========================================
program random_umat
  implicit none
  real(8), allocatable :: a(:,:)
  integer n, i, j
  write(*,*) "input n (1 <= n <= 100) : "
  read(*,*) n
  if(n < 1 .or. 100 < n) stop "stop, n is valed!"
  allocate(a(n,n))
  call random_seed
  do j = 1, n
    call random_number(a(1:j, j)) ! j列の1行からj行までの要素に乱数を設定
    a(j + 1:n, j) = 0.0d0 ! j列の対角要素より下の行の要素に0を設定
  end do
  do i = 1, n
    write(*, "(100e12.4)") a(i, 1:n)
  end do
end program random_umat