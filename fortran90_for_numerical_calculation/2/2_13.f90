! ===========================================
! 一様乱数を要素とする対象行列を設定するプログラム
! ===========================================
program random_umat
  implicit none
  real(8), allocatable :: a(:,:)
  integer n, i, j, k, l
  write(*,*) "input n (1 <= n <= 100) : "
  read(*,*) n
  if(n < 1 .or. 100 < n) stop "stop, n is valed!"
  allocate(a(n,n))
  call random_seed
  a(:,:) = 0
  do j = 1, n
    call random_number(a(j + 1:n, j)) ! j列の対角要素より下の行の要素に乱数を設定
    do k = 1, n
      do l = 1, n
        a(k, l) = a(l, k)
      end do
    end do
    call random_number(a(j:j, j:j))
! 上か下かのどっちか
    ! call random_number(a(1:j, j + 1))
    ! do k = 1, n
    !   do l = 1, n
    !     a(l, k) = a(k, l)
    !   end do
    ! end do
    ! call random_number(a(j:j, j:j))
  end do
  do i = 1, n
    write(*, "(100e12.4)") a(i, 1:n)
  end do
end program random_umat