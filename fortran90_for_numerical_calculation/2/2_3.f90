program test
  implicit none
  integer i
  real(8) u(3), v(3), w(3)
  open(10, file="u.d")
  open(20, file="v.d")
  do i = 1, 3
    read(10,*) u(i)
  end do
  do i = 1, 3
    read(20,*) v(i)
  end do
  write(*,"(3f10.7)") (u(i), i = 1, 3)
  write(*,"(3f10.7)") (v(i), i = 1, 3)
  w(:) = (u(:) - v(:)) ** 2
  write(*,"(3f10.7)") (w(i), i = 1, 3)
end program test