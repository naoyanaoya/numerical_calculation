! ============================================
! 一様乱数を1次元配列に格納し、変換して出力するプログラム
! ============================================
program rnum
  implicit none
  real(8), allocatable :: r(:)
  integer, allocatable :: seed(:)
  integer, parameter :: SEED_SELF = 123456789
  integer seedsize
  integer n
  write(*,*) "input n (>= 1) : "
  read(*,*) n
  if(n < 1) stop "stop n < 1"
  allocate (r(n))
  call random_seed(size=seedsize)
  allocate(seed(seedsize))
  seed = SEED_SELF
  call random_seed(put=seed)
  call random_number(r)
  ! r(1:n) = 2.0d0 * r(1:n) - 1.0d0
  write(*,*) r(1:n)
end program rnum

! program random_seed_example
!   implicit none
!   integer,parameter :: n = 5
!   real :: x
!   integer i, seedsize
!   integer,allocatable:: seed(:)

!   call random_seed(size=seedsize)  ! シードの格納に必要なサイズを取得する
!   allocate(seed(seedsize))         ! シード格納領域を確保
!   call random_seed(get=seed)       ! 次回同じ乱数を発生できるように

!   print *, "Size of seed array is", seedsize

!   print *, "1st try..."
!   do i = 1, n
!     call random_number(x)
!     print *, i, x
!   end do

!   call random_seed(put=seed)       ! 前回と同じシードを設定

!   print *, "2nd try..."
!   do i = 1, n
!     call random_number(x)
!     print *, i, x
!   end do

! end program random_seed_example