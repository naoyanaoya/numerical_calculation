! gfortran -fbounds-check 2_1.f90 -o 2_1
! -fbounds-check 配列の上下限をチェックするオプション
program dotp2
  implicit none
  integer i
  real(8) u(3), v(3), dotp
  open(10, file="u.d")
  open(20, file="v.d")
  do i = 1, 3
    read(10,*) u(i)
  end do
  do i = 1, 3
    read(20,*) v(i)
  end do
  write(*,"(3f10.7)") (u(i), i = 1, 3)
  write(*,"(3f10.7)") (v(i), i = 1, 3)
  dotp = 0.0d0
  do i = 1, 3
    dotp = dotp + u(i) * v(i)
  end do
  write(*,*) "dot product = ", dotp
  write(*,*) "dot product = ", dot_product(u(:),v(:))
end program dotp2