! ==========================================
! 割付け配列を利用するプログラム例
! ==========================================
program dotp2
  implicit none
  integer :: i, n = 2 ! このnは定数ではなく、一般の整数型変数
  real(8), allocatable :: u(:), v(:) ! allocatable属性をつけて割付け配列を宣言
  real(8) dotp
  allocate(u(n),v(n))
  ! 配列の初期値そ設定(プログラムは省略)
  dotp = 0.0d0
  do i = 1, n
    dotp = dotp + u(i) * v(i)
  end do
  write(*,*) "dot product = ", dotp
  deallocate(u,v) ! 割付け解除
end program dotp2