! ==================================================
! 直交座標系の格子点を出力するプログラム
! https://teratail.com/questions/69805
! ===================================================

program lattice
  implicit none
  double precision, allocatable ::  xy(:,:,:)
  integer :: i,j, m = 2
  integer :: n(2)=(/ 5, 5 /) ! x方向とy方向の分割数
  open(11, file='lattice.d')
  allocate(xy(m,n(1) + 1,n(2) + 1))

  do j=1, n(2) + 1
    do i=1, n(1) + 1
      xy(1,i,j)= (1.0d0 - 0.0d0) * (i - 1) / real(n(1), kind = 8)
       ! 整数倍して最後に割って、浮動小数点数の誤差が溜まるのを避ける
      xy(2,i,j)= (1.0d0 - 0.0d0) * (j - 1) / real(n(2), kind = 8)
    enddo
  enddo

  ! x-direction
  do j = 1, n(2) + 1
    do i = 1, n(1) + 1
      write(11, *) xy(1, i, j), xy(2, i, j), 1
    end do
    write(11, *)
  end do

  ! y-direction
  do i = 1, n(1) + 1
    do j = 1, n(2) + 1
      write(11, *) xy(1, i, j), xy(2, i, j), 1
    end do
    write(11, *)
  end do

  close(11)
end program lattice

! === 良くないコード ===
! program lattice
!   implicit none
!   real(8), allocatable ::  x(:,:,:)
!   real(8) :: x1 = 0.0d0, x2 = 0.0d0, x3 = 0.0d0, x4 = 0.0d0
!   integer :: i,j,l, m = 2, n(2)=(/ 10, 10 /)
!   real(8) dx1, dx2
!   open(11, file='lattice.d')
!   allocate(x(m,n(1),n(2)))
!   dx1=1.0d0/dble(n(1)-1)
!   dx2=1.0d0/dble(n(2)-1)
!   do j=1, n(2)
!     do i=1, n(1)
!       x(1,i,j)=x1
!       x(2,i,j)=x2
!       write(11,*) x(1,j,i), x(2,i,j)
!     enddo
!     write(11,*) ''
!     x1=x1+dx1 ! 浮動小数点数では避けたほうがいい演算
!     x2=x2+dx2 ! 0.333を3解足しても1.0にならない
!   enddo

!   do i=1, n(1)
!     do j=1, n(2)
!       x(1,i,j)=x3
!       x(2,i,j)=x4
!       write(11,*) x(1,i,j), x(2,j, i)
!     enddo
!     write(11,*) ''
!     x3=x3+dx1
!     x4=x4+dx2
!   enddo
!   close(11)
! end program lattice