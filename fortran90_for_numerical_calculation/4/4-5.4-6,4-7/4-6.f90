! =======================================================
! 4-5の外部関数のインターフェイス・モジュール
! ======================================================
module interface_mod
  interface
    function normal_vec(v) result(nv)
      real(8), intent(in) :: v(:) ! 関数の仮引数の宣言文と一致させる
      real(8) nv(size(v))
    end function normal_vec
  end interface
end module interface_mod