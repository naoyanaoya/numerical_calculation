! ============================================
! 配列を返すがん部関数を使う主プログラムの例
! ============================================
program main_nvec
  use interface_mod ! インターフェース・モジュールの使用宣言
  implicit none
  real(8) v(10), w(10)
  call random_seed
  call random_number(v)
  w(:) = normal_vec(v)
  write(*,*) w, dot_product(w, w)
end program main_nvec