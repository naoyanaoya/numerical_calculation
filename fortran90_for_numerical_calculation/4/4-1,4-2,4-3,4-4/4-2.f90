! =========================================
! 割付け配列の設定と出力を行う外部サブルーチン(exsub.f90)
! =========================================
subroutine allocate_rmat(a)
  implicit none ! 書く副プログラムごとにこの宣言が必要
  real(8), allocatable, intent(out) :: a(:, :) ! 仮配列は未割付けの割付け配列
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  if (n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 100"
  end if
  allocate(a(n, n)) ! 割付け配列の割付け
  call random_number(a)
end subroutine allocate_rmat

subroutine print_mat(a)
  implicit none ! 書く副プログラムごとにこの宣言が必要
  real(8), intent(in) :: a(:, :) ! 形状引継ぎ配列
  integer i, m, n
  m = size(a, 1)
  n = size(a, 2)
  do i = 1, m
    write(*,"(100e12.4)") a(i, 1:n)
  end do
end subroutine print_mat