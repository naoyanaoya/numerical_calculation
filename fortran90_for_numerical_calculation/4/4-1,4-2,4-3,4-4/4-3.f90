! ===========================================
! リスト4-2に対するインターフェース・モジュール(ifmod.f90)
! ===========================================
module interface_mod
  
  interface
    subroutine allocate_rmat(a)
      real(8), allocatable, intent(out) :: a(:, :)
    end subroutine allocate_rmat
  
    subroutine print_mat(a)
      real(8), intent(in) :: a(:, :)
    end subroutine print_mat

  end interface
end module interface_mod