! =============================================
! 外部サブルーチンを使う主プログラム(main.f90)
! =============================================
program random_mat
  use interface_mod ! インターフェースモジュールの使用宣言
  implicit none
  real(8), allocatable :: a(:, :)
  call allocate_rmat(a) ! 配列の割付けと乱数の設定
  call print_mat(a) ! 要素の値を出力(形状引継ぎ配列を利用)
end program random_mat

