! ===================================================
! インターフェース・モジュールの使用宣言にonly句を用いる例
! ===================================================
subroutine allocate_rmat(a)
  use interface_mod, only : print_mat ! only句を伴うモジュールの使用宣言
  implicit none ! 書く副プログラムごとにこの宣言が必要
  real(8), allocatable, intent(out) :: a(:, :) ! 仮配列は未割付けの割付け配列
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*)
  if (n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 100"
  end if
  allocate(a(n, n)) ! 割付け配列の割付け
  call random_number(a)
  call print_mat(a) ! 追加された外部サブルーチンの呼び出し
end subroutine allocate_rmat 