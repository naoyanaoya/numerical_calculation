! ==================================
! グロ＝バル変数モジュールを使用する外部サブルーチン(4-9)
! ==================================
subroutine allocata_rmat
  use globals
  implicit none
  write(*,"(a)",advance="no") "input m, n : "
  read(*,*) m, n
  allocate(a(m, n))
  call random_number(a)
end subroutine allocata_rmat