! ==================================
! グロ＝バル変数モジュールを使用する外部サブルーチン(4-9)
! ==================================
subroutine print_mat
  use globals
  implicit none
  integer i
  do i = 1, m
    write(*,"(100e12.4)") a(i, 1:n)
  end do
end subroutine print_mat

