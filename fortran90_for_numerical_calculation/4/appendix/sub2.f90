! ==================================
! 正規化したベクトルを返す外部関数
! ==================================
function normal_vec(v) result(nv)
  implicit none
  real(8), intent(in) :: v(:) ! 形状引継ぎ配列として受け取る
  real(8) nv(size(v, 1)), v1
  v1 = sqrt(dot_product(v, v))
  if (v1 == 0.0d0) then
    nv(:) = 0.0d0
  else
    nv(:) = v(:) / v1
  end if
end function normal_vec