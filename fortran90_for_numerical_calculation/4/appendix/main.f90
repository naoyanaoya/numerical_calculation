! ====================================
! 主プログラム
! ====================================
program random_mat
  use interface_mod
  implicit none
  real(8) v(10), w(10)
  call random_seed
  call random_number(v)
  w(:) = normal_vec(v)
  write(*,*) w, dot_product(w, w)
  call allocata_rmat
  call print_mat
end program random_mat