! ============================================
! 文字列の並び順を逆にした文字列を返す主プログラム
! ===========================================
program main
  use interface_mod
  implicit none
  character(11) :: c = "I prefer Pi"
  write(*,"(a)") c
  write(*,"(a)") revchar(c)
end program main