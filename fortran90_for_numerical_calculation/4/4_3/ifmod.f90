! ======================================
! インターフェース・モジュール
! ======================================
module interface_mod
  interface

    function revchar(c) result(rc)
      character(*), intent(in) :: c ! 仮引数となる文字型変数は長さ*で指定
      character(len(c)) rc
    end function revchar

  end interface
end module interface_mod