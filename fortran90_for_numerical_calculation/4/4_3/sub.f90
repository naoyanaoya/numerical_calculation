! =======================================
! 文字列を逆順に返す外部関数
! =======================================
function revchar(c) result(rc)
  implicit none
  character(*), intent(in) :: c ! 仮引数となる文字型変数は長さ*で指定
  character(len(c)) rc
  integer i
  do i = 1, len(c)
    rc(i: i) = c(len(c) - i + 1: len(c) - i + 1)
  end do
end function revchar
