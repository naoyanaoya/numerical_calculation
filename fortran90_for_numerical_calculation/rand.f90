! ======================================
! 毎回同じ乱数を発生させる方法
! https://leetmikeal.hatenablog.com/entry/20130417/1366182172
! ======================================
program rand
  implicit none

  ! integer(kind=4), allocatable, dimension(:) :: seed
  ! integer(kind=4) :: nrand
  ! integer(kind=4), parameter :: SEED_SELF = 123456789
  ! integer(kind=4) :: clock
  ! integer(kind=4) :: i
  ! integer(kind=4), parameter :: NCHECK = 10
  ! real(kind=4) :: x

  integer, allocatable, dimension(:) :: seed
  integer :: nrand
  integer, parameter :: SEED_SELF = 123456789
  integer :: clock
  integer :: i
  integer, parameter :: NCHECK = 10
  real :: x

  ! initialize
  call random_seed(size=nrand)
  allocate(seed(nrand))
  call system_clock(count=clock)

  ! show parameter
  write(*,*) "## parameters"
  write(*,'(a,I20)') "    clock       = ", clock
  write(*,'(a,I20)') "    SEED_SELF   = ", SEED_SELF
  write(*,'(a,I20)') "    nrand       = ", nrand
  do i = 1, size(seed)
    write(*,'(a,I5,a,I20)') "    seed(", i, ") = ", seed(i)
  enddo
  write(*,'(a,I20)') "    NCHECK      = ", NCHECK

  ! default random seed
  write(*,*) "## default random seed"
  call random_seed(put=seed)

  do i = 1, NCHECK
    call random_number(x)
    write(*, '(I5,a,E15.5)') i, " : ", x
    !print *, i, " : ", x
  enddo

  ! system clock random seed
  write(*,*) "## system clock random seed"
  seed = clock
  call random_seed(put=seed)

  do i = 1, NCHECK
    call random_number(x)
    write(*, '(I5,a,E15.5)') i, " : ", x
    !print *, i, " : ", x
  enddo

  ! personalized random seed
  write(*,*) "## personalized random seed"
  seed = SEED_SELF
  call random_seed(put=seed)

  do i = 1, NCHECK
    call random_number(x)
    write(*, '(I5,a,E15.5)') i, " : ", x
    !print *, i, " : ", x
  enddo

end program rand