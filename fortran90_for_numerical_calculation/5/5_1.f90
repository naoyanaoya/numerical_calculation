! =======================================
! 指定した大きさの外積を返すモジュール関数
! =======================================
module subprogs
  implicit none
contains
  function cross_product(vec1, vec2, norm) result(nvec)
    real(8), intent(in) :: vec1(:)
    real(8), intent(in) :: vec2(:)
    real(8), intent(in), optional :: norm
    real(8) :: nvec(size(vec1))
    real(8) vec1_norm, vec2_norm, nvec_norm, factor
    vec1_norm = dot_product(vec1, vec1)
    vec2_norm = dot_product(vec2, vec2)
    if (present(norm)) then
      factor = norm
    else
      factor = 1.0d0
    end if
    if (vec1_norm == 0.0d0 .or. vec2_norm == 0.0d0) then
      nvec(:) = 0.0d0
    else
      nvec(1) = vec1(2) * vec2(3) - vec1(3) * vec2(2)
      nvec(2) = vec1(3) * vec2(1) - vec1(1) * vec2(3)
      nvec(3) = vec1(1) * vec2(2) - vec1(2) * vec2(1)
    end if
    if(vec1_norm == 0.0d0) then
      nvec(:) = 0.0d0
    else
      nvec_norm = dot_product(nvec, nvec)
      nvec_norm = factor / sqrt(nvec_norm)
      nvec(:) = nvec_norm * nvec(:)
    end if
  end function cross_product
end module subprogs

program main
  use subprogs
  real(8) u(3), v(3), w(3)
  call random_number(u)
  write(*,"(a)",advance="no") "u = "
  write(*,"(3e12.4)") u(:)
  call random_number(v)
  write(*,"(a)",advance="no") "v = "
  write(*,"(3e12.4)") v(:)
  ! w(:) = cross_product(u, v, norm = 5.0d0)
  w(:) = cross_product(u, v)
  write(*,"(a)",advance="no") "w = "
  write(*,"(3e12.4)") w(:)
end program main
