! ==========================================
! n×nの実行列Aの最大列和と最大行和を求める
! optional属性を用いる外部サブルーチンの例
! ==========================================
module interface_mod
  interface

    subroutine matnorm(smat, s1, s2)
      real(8), intent(in) :: smat(:, :)
      real(8), intent(out), optional :: s1, s2
    end subroutine matnorm

  end interface
end module interface_mod

subroutine matnorm(smat, s1, s2)
  implicit none
  real(8), intent(in) :: smat(:, :) ! 形状引継ぎ配列
  real(8), intent(out), optional :: s1, s2
  real(8) as1(size(smat, 2)), as2(size(smat, 1)) ! 作業用配列1次元配列
  integer i
  if (present(s1)) then
    do i = 1, size(smat, 2)
      as1(i) = sum(abs(smat(:, i))) ! 各列の行方向の和
    end do
    s1 = maxval(as1) ! 最大値を求める
  end if
  if (present(s2)) then
    do i = 1, size(smat, 1)
      as2(i) = sum(abs(smat(i, :))) ! 各行の列方向の和
    end do
    s2 = maxval(as2) ! 最大値を求める
  end if
end subroutine matnorm

program main
  use interface_mod
  implicit none
  integer, parameter :: m = 4
  integer, parameter :: n = 4
  real(8) w1, w2, a(m, n)
  integer i
  call random_seed
  call random_number(a)
  call matnorm(a, w1, w2)
  call matnorm(a, w1)
  call matnorm(s2 = w2, smat = a)
  do i = 1, m
    write(*,"(100e12.4)") a(i, 1:n)
  end do
  write(*,*) w1
  write(*,*) w2
end program main