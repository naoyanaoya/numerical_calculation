! =============================================
! 再帰呼び出し関数により行列式の値を求めるプログラム
! =============================================
module mat_subprogs
  implicit none
contains
  recursive function det_mat(a, n) result(det)
    integer, intent(in) :: n ! nは配列の寸法
    real(8), intent(in) :: a(n, n) ! 形状明示仮配列を利用
    real(8) det, b(n - 1, n - 1) ! detは関数が返す変数、bは小行列
    integer i
    if(1 < n) then
      det = 0.0d0
      ! === 展開の公式により、第一列で展開して行列式を計算 ===
      do i = 1, n
        ! === aのi行と1列を除く小行列bを作る
        b(1:i - 1, 1:n - 1) = a(1:i - 1, 2:n)
        b(i:n - 1, 1:n - 1) = a(i + 1:n, 2:n)
        det = det + (-1.0d0) ** (i + 1) * a(i, 1) * det_mat(b, n - 1)
      end do
    else
      det = a(1, 1)
    end if
  end function det_mat
end module mat_subprogs

program cal_det
  use mat_subprogs
  implicit none
  integer, parameter :: n = 5
  real(8) a(n, n)
  call random_seed
  call random_number(a)
  ! === 配列aの要素の値を設定 ===
  write(*,*) "det = ", det_mat(a, n)
end program cal_det