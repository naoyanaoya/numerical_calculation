! ==========================================================
! 指定した大きさのベクトルを返すモジュール関数(optional属性を指定)
! ==========================================================
module subprogs
  implicit none
contains
 ! モジュール関数vnormは、仮引数normの全原文でoptional属性をしていている
  function vnorm(vec, norm) result(nvec)
    real(8), intent(in) :: vec(:)
    real(8), intent(in), optional :: norm ! optional属性を指定 実引数normは省略可能
    real(8) :: nvec(size(vec))
    real(8) vec1, factor
    vec1 = dot_product(vec, vec)
    if (present(norm)) then ! 組み込み関数presentにより実引数の有無を確認
      factor = norm ! 実引数normがあれば、factorをnormにする
    else
      factor = 1.0d0 ! 実引数normがなければ、factorを1とする
    end if
    if(vec1 == 0.0d0) then
      nvec(:) = 0.0d0 ! ゼロベクトルの場合
    else
      vec1 = factor / sqrt(vec1) ! ベクトルの大きさをfactorとする
      nvec(:) = vec1 * vec(:)
    end if
  end function vnorm
end module subprogs

program main
  use subprogs
  implicit none
  real(8) u(5), w(5)
  call random_number(u)
  write(*,"(100e12.4)") u(:)
  w(:) = vnorm(u, 3.0d0) ! 通常の利用方法、引数の並び順所が整合する
  write(*,"(100e12.4)") w(:)
  w(:) = vnorm(vec = u, norm = 3.0d0) ! 引数キーワードそ使用
  w(:) = vnorm(norm = 3.0d0, vec = u) ! 引数キーワードを使用
  w(:) = vnorm(u, norm = 3.0d0) ! 引数キーワードを使用
  ! w(:) = vnorm(vec = u, 3.0d0) ! 誤った使用方法、いったん引数キーワードを指定したら、後続する実引数にはすべて引数キーワードを付けなければならない。
  ! w(:) = vnorm(u) ! 実引数normを省略
end program main