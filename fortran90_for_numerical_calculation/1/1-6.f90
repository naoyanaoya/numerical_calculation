! ========================================================
! 1から入力値までの和を計算するプログラム(if文,goto文を使用)
! ========================================================
program loop_goto
  implicit none
  integer wa, n, i
  do
    write(*,"(a)",advance="no") "input n (input 0 to stop) : "
    read(*,*) n
    if(n == 0) then
      goto 1
    else if(n < 0) then
      write(*,*) "sorry, input positive n ... "
    else
      wa = 0
      do i = 1, n
        wa = wa + i
      end do
      write(*,"(a,i3)") "wa = ", wa
    end if
  end do
1 continue
  write(*,"(a)") "exit from do-loop"
end program loop_goto