program fibonacchi
  implicit none
  integer i, a_1, a_2, a_3
  a_1 = 1
  a_2 = 2
  a_3 = 0
  write(*,*) a_1
  write(*,*) a_2
  do i = 3, 10
    a_3 = a_2 + a_1
    a_1 = a_2
    a_2 = a_3
    write(*,*) a_3
  end do
end program fibonacchi