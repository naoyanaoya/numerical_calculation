! =========================================
! 入力値から入力値までの和を計算するプログラム(無限ループを使用)
! =========================================
program loop_inf
  implicit none
  integer wa, m, n, i
  do
    write(*,"(a)", advance = "no") "input m : "
    read(*,*) m
    write(*,"(a)", advance = "no") "input n : "
    read(*,*) n
    if(m <= n) then
      wa = 0
      do i = m, n
        wa = wa + i
      end do
      write(*,*)  "wa = ", wa
    else
      wa = 0
      do i = n, m
        wa = wa + i
      end do
      write(*,*)  "wa = ", wa
    end if
  end do
end program loop_inf