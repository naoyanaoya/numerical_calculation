program main
  implicit none
  real(8) d, x, sinh, cosh, tanh
  integer :: n, i, is
  integer, parameter :: fi = 10, fo = 11
  open(fi,file="1_19_input.d",action="read",iostat=is)
  if(is /= 0) stop "cannot open file"
  open(fo,file="1_19_output.d",action="write")
  read(fi,*) n
  close(fi)
  if(n < 3) stop "stop, n < 3"
  d = 2.0d0 / dble(n - 1)
  do i = 1, n
    x = -1.0d0 + dble(i - 1) * d
    sinh = (exp(x) - exp(-x)) / 2 
    cosh = (exp(x) + exp(-x)) / 2
    tanh = sinh / cosh
    write(fo,"(4e12.4)") x, sinh, cosh, tanh
  end do
  write(fo,*)
  close(fo)
end program main

! sinhをplotする
! plot "1_19_output.d" using 1:2
! coshをplotする
! plot "1_19_output.d" using 1:3
! tanhをplotする
! plot "1_19_output.d" using 1:4
