! ./1-4 < 1-4_input.d > 1-4_output.d

! =========================================
! 1から入力値までの和を計算するプログラム(無限ループを使用)
! =========================================
program loop_inf
  implicit none
  integer wa, n, i
  do
    write(*,"(a)", advance = "no") "input n(if n <= stop) : " 
    read(*,*) n
    if(n <= 0) stop "good bye ..."
    wa = 0
    do i = 1, n
      wa = wa + i
    end do
    write(*,*)  "wa = ", wa
  end do
end program loop_inf