! =========================================
! 入力値から入力値までの和を計算するプログラム(無限ループを使用)
! =========================================
program loop_inf
  implicit none
  integer wa, m, n, i
  do
    write(*,"(a)", advance = "no") "input m(if n <= stop) : " 
    read(*,*) m
    write(*,"(a)", advance = "no") "input n(if n <= stop) : " 
    read(*,*) n
    if(n <= 0) stop "good bye ..."
    wa = 0
    do i = m, n
      wa = wa + i
    end do
    write(*,*)  "wa = ", wa
  end do
end program loop_inf