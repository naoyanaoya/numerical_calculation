! ==================================================
! ガウスージョルダン法により解を求めるモジュールサブルーチン
! ==================================================
module subprogs
  implicit none
contains
  subroutine gauss_jordan(a0, x, b0, n)
    ! === ガウスージョルダン法(部分pibot選択なし) ===
    integer, intent(in) :: n
    real(8), intent(in) :: a0(n, n), b0(n) ! 形状明示仮配列
    real(8), intent(out) :: x(n) ! 形状明示仮配列
    integer i, k
    real(8) ar, a(n, n) ! aは作業用の自動割付け配列
    a(:, :) = a0(:, :) ! 係数行列a0をaにコピー
    x(:) = b0(:) ! 右辺ベクトルb0はxにコピーされて用いられるので、b0に対する演算は行われない
    do k = 1, n
      if (a(k, k) == 0.0d0) then
        stop "pivot = 0" ! pivotが0なら停止する
      end if
      ar = 1.0d0 / a(k, k)
      a(k, k) = 1.0d0 ! 対角成分に1を設定
      a(k, k + 1:n) = ar * a(k, k + 1:n)
      x(k) = ar * x(k)
      do i =1 , n
        if (i /= k) then
          a(i, k + 1:n) = a(i, k + 1:n) - a(i, k) * a(k, k + 1:n)
          x(i) = x(i) - a(i, k) * x(k)
          a(i, k) = 0.0d0
        end if
      end do
    end do
  end subroutine gauss_jordan

  subroutine set_random_ab(a, b, x, n)
    ! === nを取得しa,b,xを割り付け、aとbに乱数を設定
    integer, intent(in) :: n
    real(8), allocatable  , intent(out) :: a(:, :)
    real(8), allocatable  , intent(out) :: b(:)
    real(8), allocatable  , intent(out) :: x(:)
    allocate(a(n, n))
    allocate(b(n))
    allocate(x(n))
    call random_number(a)
    call random_number(b)
  end subroutine set_random_ab

end module subprogs

program main
  use subprogs
  implicit none
  real(8), allocatable :: a(:, :), b(:), x(:), r(:)
  real(8) t1, t2
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  call cpu_time(t1)
  call set_random_ab(a, b, x, n) ! nを取得しa,b,xを割り付け、aとbに乱数を設定
  call gauss_jordan(a, x, b, n) ! ガウスージョルダン法
  call cpu_time(t2)
  write(*,"(a,f6.4)") "cpu time = ", t2 - t1
  allocate(r(n)) ! 残渣ベクトルの内積を出力
  r(:) = b(:) - matmul(a, x)
  write(*,*) "Gauss_Jordan error = ", dot_product(r, r)
  deallocate(a, b, x, r)
end program main