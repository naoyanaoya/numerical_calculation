! ==================================================
! ガウスージョルダン法(ピボット選択あり)により解を求めるモジュールサブルーチン
! ==================================================
module subprogs
  implicit none
contains

  subroutine gauss_jordan_pv(a, x, b, n)
    ! === ガウスージョルダン法(部分pibot選択あり) ===
    integer, intent(in) :: n
    real(8), intent(in) :: a(n, n), b(n) ! 形状明示仮配列
    real(8), intent(out) :: x(n) ! 形状明示仮配列
    integer i, k, m
    real(8) ar, am, t, a0(n, n), w(n) ! a0,wは作業用の自動割り付け配列
    a0(:, :) = a(:, :)
    x(:) = b(:)
    do k = 1, n
      ! === 部分pibot選択 ===
      m = k
      am = abs(a0(k, k))
      do i = k + 1, n ! a(i, k)の絶対値が最大となるm行を探す
        if(am < abs(a0(i, k))) then
          am = abs(a0(i, k))
          m = i
        end if
      end do
      if (am == 0.0d0) then
        stop "A is singular" ! Aが得意なら停止
      end if
      if (k /= m) then ! k行とm行の入れ替え
        w(k:n) = a0(k, k:n)
        a0(k, k:n) = a0(m, k:n)
        a0(m, k:n) = w(k:n)
        t = x(k)
        x(k) = x(m)
        x(m) = t
      end if
      ! === 以下は通常のガウスージョルダン法の演算 ===
      ar = 1.0d0 / a0(k, k)
      a0(k, k) = 1.0d0
      a0(k, k + 1:n) = ar * a0(k, k + 1:n)
      x(k) = ar * x(k)
      do i = 1, n
        if (i /= k) then
          a0(i, k + 1:n) = a0(i, k + 1:n) - a0(i, k) * a0(k, k + 1:n)
          x(i) = x(i) - a0(i, k) * x(k)
          a0(i, k) = 0.0d0
        end if
      end do
    end do
  end subroutine gauss_jordan_pv

  subroutine set_random_ab(a, b, x, n)
    ! === nを取得しa,b,xを割り付け、aとbに乱数を設定
    integer, intent(in) :: n
    real(8), allocatable  , intent(out) :: a(:, :)
    real(8), allocatable  , intent(out) :: b(:)
    real(8), allocatable  , intent(out) :: x(:)
    allocate(a(n, n))
    allocate(b(n))
    allocate(x(n))
    call random_number(a)
    call random_number(b)
  end subroutine set_random_ab

end module subprogs

program main
  use subprogs
  implicit none
  real(8), allocatable :: a(:, :), b(:), x(:), r(:)
  real(8) t1, t2
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  call cpu_time(t1)
  call set_random_ab(a, b, x, n) ! nを取得しa,b,xを割り付け、aとbに乱数を設定
  call gauss_jordan_pv(a, x, b, n) ! ガウスージョルダン法
  call cpu_time(t2)
  write(*,"(a,f6.4)") "cpu time = ", t2 - t1
  allocate(r(n)) ! 残渣ベクトルの内積を出力
  r(:) = b(:) - matmul(a, x)
  write(*,*) "Gauss_Jordan error = ", dot_product(r, r)
  deallocate(a, b, x, r)
end program main
