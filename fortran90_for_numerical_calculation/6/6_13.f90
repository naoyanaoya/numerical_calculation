! ==============================================
! SOR法のプログラム例
! ==============================================
module subprogs
  implicit none
contains

  subroutine sor(a, b, x, n, ITERATE_MAX, EPS)
    integer, intent(in) :: n, ITERATE_MAX
    real(8), intent(in) :: a(n, n), b(n), EPS
    real(8), intent(out) :: x(n)
    real(8) s, er, rd(n), r(n)
    real(8), parameter :: omega = 1.3
     ! ========================================
     ! 行列のスペクトル半径から最適なomegaを求めたい
     ! ========================================
    integer i, itr
    do i = 1, n
      if (a(i, i) == 0.0d0) then
        stop "a(i, i) = 0.0d0"
      end if
      rd(i) = 1.0d0 / a(i, i)
    end do
    x(1:n) = 0.0d0
    do itr = 1, ITERATE_MAX
      do i = 1, n
        s = dot_product(a(i, 1:i - 1), x(1:i - 1))
        s = s + dot_product(a(i, i + 1:n), x(i + 1:n))
        x(i) = x(i) + omega * (rd(i) * (b(i) - s) - x(i))
      end do
      r(1:n) = b(1:n) - matmul(a, x)
      er = dot_product(r, r)
      write(*,*) "itr = ", itr, " err = ", er
      if (er <= EPS) then
        write(*,*) "# converged #"
        exit
      end if
    end do
  end subroutine SOR

  subroutine alloc_dd_mat(a, b, x, n)
    ! nを取得し、a,b,xを割り付け、aに狭義の対角優位行列を設定
    integer, intent(in) :: n
    real(8), allocatable  , intent(out) :: a(:, :)
    real(8), allocatable  , intent(out) :: b(:)
    real(8), allocatable  , intent(out) :: x(:)
    real(8), allocatable :: aa(:, :)
    integer, allocatable :: seed(:)
    integer i, j
    integer, parameter :: SEED_SELF = 123456789
    integer seedsize
    allocate(a(n, n))
    allocate(b(n))
    allocate(x(n))
    allocate(aa(n, n))
    call random_seed(size = seedsize)
    allocate(seed(seedsize))
    seed = SEED_SELF
    call random_seed(put = seed)
    call random_number(aa)
    a(:, :) = aa(:, :) / 100
    do j = 1, n
      do i = 1, n
        if(i == j) then
          a(i, j) = 1.0d0
        end if
      end do
    end do
    ! do i = 1, n
    !   write(*,"(100e12.4)") a(i, 1:n)
    ! end do
    call random_number(b)
  end subroutine alloc_dd_mat

end module subprogs

program main
  use subprogs
  implicit none
  real(8), allocatable :: a(:, :), b(:), x(:)
  integer, parameter :: ITERATE_MAX = 1000
  real(8), parameter :: EPS = 1.0d-6
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  call alloc_dd_mat(a, b, x, n)
  call SOR(a, b, x, n , ITERATE_MAX, EPS)
  ! write(*,*) x(:)
end program main