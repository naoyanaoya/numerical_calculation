! ====================================================
! ガウスの消去法により解を求めるモジュールサブルーチン
! ====================================================
module subprogs
  implicit none
contains
  subroutine gauss_eliminaiton(a, x, b, n)
    ! === ガウスの消去法 ===
    integer, intent(in) :: n
    real(8), intent(in) :: a(n, n), b(n) ! 形状明示仮配列
    real(8), intent(out) :: x(n) ! 形状明示仮配列
    integer i, j, k
    real(8) a0(n, n)
    real(8) alpha, temp
    a0(:, :) = a(:, :)
    x(:) = b(:)
    do k = 1, n - 1
      do i = k + 1, n
        alpha = a0(i, k) / a0(k, k)
        do j = k + 1, n
          a0(i, j) = a0(i, j) - alpha * a0(k, j)
        end do
        x(i) = x(i) - alpha * x(k)
      end do
    end do
    x(n) = x(n) / a0(n, n)
    do k = n - 1, 1, -1
      temp = 0.0d0
      do j = k + 1, n
        temp = temp + a0(k, j) * x(j)
      end do
      x(k) = (x(k) - temp) / a0(k, k)
    end do
  end subroutine gauss_eliminaiton

  subroutine set_random_ab(a, b, x, n)
    ! === nを取得しa,b,xを割り付け、aとbに乱数を設定
    integer, intent(in) :: n
    real(8), allocatable  , intent(out) :: a(:, :)
    real(8), allocatable  , intent(out) :: b(:)
    real(8), allocatable  , intent(out) :: x(:)
    allocate(a(n, n))
    allocate(b(n))
    allocate(x(n))
    call random_number(a)
    call random_number(b)
  end subroutine set_random_ab

end module subprogs

program main
  use subprogs
  implicit none
  real(8), allocatable :: a(:, :), b(:), x(:), r(:)
  real(8) t1, t2
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  call cpu_time(t1)
  call set_random_ab(a, b, x, n) ! nを取得しa,b,xを割り付け、aとbに乱数を設定
  call gauss_eliminaiton(a, x, b, n) ! ガウスの消去法
  call cpu_time(t2)
  write(*,"(a,f6.4)") "cpu time = ", t2 - t1
  allocate(r(n)) ! 残渣ベクトルの内積を出力
  r(:) = b(:) - matmul(a, x)
  write(*,*) "Gauss_Jordan error = ", dot_product(r, r)
  deallocate(a, b, x, r)
end program main


