! ===========================================
! bicgstab
! ===========================================

module subprogs
  implicit none
contains

  subroutine bicgstab1d(a, b, x, n, ITERATE_MAX, EPS)
    integer, intent(in) :: n, ITERATE_MAX
    double precision, intent(in) :: a(n, n), b(n), EPS
    double precision, intent(inout) :: x(n)
    integer itr
    double precision alp, bet, c1, c2, c3, ev, vv, rr
    double precision r(n), r0(n), p(n), y(n), e(n), v(n)
    ! === 初期値の設定 ===
    x(:) = 0.0d0 ! 初期の解ベクトルをゼロベクトルとする
    r(:) = b - matmul(a, x) ! 初期残渣ベクトル 
    c1 = dot_product(r, r) ! 初期残渣ベクトルの内積
    if (c1 < EPS) then ! 初期残渣 < EPS ならreturn
      return
    end if
    p(:) = r(:) ! 修正方向ベクトル
    r0(:) = r(:) ! 初期残渣ベクトルをr0として保存
    do itr = 1, ITERATE_MAX ! 最大反復回数まで反復計算
      y(:) = matmul(a, p) ! 行列ベクトル積
      c2 = dot_product(r0, y) ! ベクトルの内積
      alp = c1 / c2 ! 
      e(:) = r(:) - alp * y(:)
      v(:) = matmul(a, e) ! 行列ベクトル積
      ev = dot_product(e, v) ! ベクトルの内積
      vv = dot_product(v, v) ! ベクトルの内積
      c3 = ev / vv
      x(:) = x(:) + alp * p(:) + c3 * e(:) ! 解ベクトルの更新
      r(:) = e(:) - c3 * v(:) ! 残渣ベクトルの更新
      rr = dot_product(r, r) ! 残渣ベクトルの内積
      write(*,*) "itr = ", itr, " err = ", rr
      if (rr < EPS) then
        exit
      end if
      c1 = dot_product(r0, r)
      bet = c1 / (c2 * c3)
      p(:) = r(:) + bet * (p(:) - c3 * y(:)) ! 修正方向ベクトルの更新
    end do
  end subroutine bicgstab1d

  subroutine alloc_dd_mat(a, b, x, n)
    ! nを取得し、a,b,xを割り付け、aに狭義の対角優位行列を設定
    integer, intent(in) :: n
    double precision, allocatable  , intent(out) :: a(:, :)
    double precision, allocatable  , intent(out) :: b(:)
    double precision,  allocatable  , intent(out) :: x(:)
    double precision, allocatable :: aa(:, :)
    integer, allocatable :: seed(:)
    integer i, j
    integer, parameter :: SEED_SELF = 123456789
    integer seedsize
    allocate(a(n, n))
    allocate(b(n))
    allocate(x(n))
    allocate(aa(n, n))
    call random_seed(size = seedsize)
    allocate(seed(seedsize))
    seed = SEED_SELF
    call random_seed(put = seed)
    call random_number(aa)
    a(:, :) = aa(:, :) / 100
    do j = 1, n
      do i = 1, n
        if(i == j) then
          a(i, j) = 1.0d0
        end if
      end do
    end do
    ! do i = 1, n
    !   write(*,"(100e12.4)") a(i, 1:n)
    ! end do
    call random_number(b)
  end subroutine alloc_dd_mat

end module subprogs

program main
  use subprogs
  implicit none
  double precision, allocatable :: a(:, :), b(:), x(:)
  integer, parameter :: ITERATE_MAX = 10000
  double precision, parameter :: EPS = 1.0d-6
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  call alloc_dd_mat(a, b, x, n)
  call bicgstab1d(a, b, x, n , ITERATE_MAX, EPS)
  ! write(*,*) x(:)
end program main