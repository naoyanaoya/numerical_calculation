! =============================================
! 円錐の体積を求めるモジュールサブルーチン
! =============================================
module subprogs
  implicit none
contains
  subroutine cone_vol(r, h, v)
    real(8), intent(in) :: r, h ! intent属性付きの仮引数の宣言
    real(8), intent(out) :: v ! intent属性付きの仮引数の宣言
    ! intent属性は仮引数に対してのみ使用できる
    real(8) s, pi ! 局所変数の宣言
    pi = 2.0d0 * acos(0.0d0)
    s = pi * r ** 2
    v = s * h / 3.0d0
  end subroutine cone_vol
end module subprogs

program main
  use subprogs
  implicit none
  real(8) :: a = 1.5d0, l = 3.0d0, vol
  call cone_vol(a, l, vol)
  write(*,*) vol
end program main


