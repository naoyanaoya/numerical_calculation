! =============================================
! 依存関係を確認するためのグローバル変数モジュール
! =============================================
module globals
  integer, parameter :: n = 2
  real(8), save :: a, b, c(n)
end module globals