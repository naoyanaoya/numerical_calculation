! =======================================
! 2×2の実行列の固有値を求めるモジュール関数
! =======================================

module sample
  implicit none
contains
  function eval2by2mat(a) result(eval)
    real(8), intent(in) :: a(:, :) ! aは形状引継ぎ配列
    complex(8) eval(2) ! 固有値を要素数2の(倍精度)複素数型配列に格納して返す
    real(8) b, c, d, e ! 関数内の演算で用いる局所変数
    if (size(a, 1) /= size(a, 2)) then ! 正方行列であることを確認
      stop "not square"
    end if
    if (size(a, 1) /= 2) then ! aの寸法を確認
      stop "not 2×2 matrix"
    end if
    b = -0.5d0 * (a(1, 1) + a(2, 2))
    c = a(1, 1) * a(2, 2) - a(1, 2) * a(2, 1)
    d = b ** 2 - c ! dに判別式の値を設定
    if (d < 0.0d0) then
      eval(1) = cmplx(-b, sqrt(-d), kind = 8)
      eval(2) = conjg(eval(1)) ! 組み込み関数conjgにより共役複素数を求める
    else if (0.0d0 < d) then ! 固有値が異なる2実根となる場合
      e = -b + sign(sqrt(d), -b) ! 桁落ちを防ぐ計算方法
      eval(1) = cmplx(e, 0.0d0, kind=8)
      eval(2) = cmplx(c / e, 0.0d0, kind=8)
    else ! 固有値が重根となる場合
      eval(1) = cmplx(-b, 0.0d0, kind=8)
      eval(2) = eval(1)
    end if
  end function eval2by2mat
end module sample

program main
  use sample
  implicit none
  real(8) a(2,2)
  a(1, 1:2) = (/-1.0d0, 1.0d0/)
  a(2, 1:2) = (/-1.0d0, -1.0d0/)
  write(*,*) eval2by2mat(a)
end program main

