! ===========================================
! 2つの整数型変数の値を交換するモジュールサブルーチン
! ===========================================
module subprog
  implicit none
contains
  subroutine add(i, j, k)
    integer i, j, k
    k = i + j
  end subroutine add
end module subprog

program exchange
  use subprog
  implicit none
  integer :: i = 77, j = 99, k = 0
  write(*,*) "i, j, k = ", i, j, k
  call add(i, j, k)
  write(*,*) "i, j, k = ", i, j, k
end program exchange