module vec_subprogs
  implicit none
contains
  subroutine print_lb(v, lb) ! 配列と配列の下限値を仮引数とする
    integer, intent(in) :: lb ! 配列の下限
    integer, intent(in) :: v(lb:) ! 下限を明示した形状引継ぎ入れう
    write(*,*) v(0), lbound(v, 1), ubound(v, 1) ! 要素v(0)と下限、上限を出力
  end subroutine print_lb
end module vec_subprogs

program main
  use vec_subprogs
  implicit none
  integer, parameter :: n = 3
  integer :: i, x(-n:n) = (/(i, i = -n, n)/)
  write(*,*) x(0), -n, n
  call print_lb(x, -n)
  call print_lb(x, lbound(x, 1))
end program main

! subroutine sub1(a, lb, ub) ! lb,ubは配列の上下限
!    integer, intent(in) :: lb, ub, a(lb:ub)
! or
! subroutine sub2(a, lb, n) ! lbは配列の下限、nは配列の寸法
!    integer, intent(in) :: lb, n, a(lb:lb + n- 1)


