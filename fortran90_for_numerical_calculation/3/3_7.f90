module subprogs
  implicit none
contains
  subroutine cone_vol(tate1, yoko1, takasa1, vol1, area1)
    real(8), intent(in) :: tate1, yoko1, takasa1 ! intent属性付きの仮引数の宣言
    real(8), intent(out) :: vol1, area1 ! intent属性付きの仮引数の宣言
    ! intent属性は仮引数に対してのみ使用できる
    vol1 = tate1 * yoko1 * takasa1
    area1 = tate1 * yoko1 * 2 + yoko1 * takasa1 * 2 + takasa1 * tate1 * 2
  end subroutine cone_vol
end module subprogs

program main
  use subprogs
  implicit none
  real(8) :: tate = 1.5d0, yoko = 3.0d0, takasa = 3.0d0, vol, area
  call cone_vol(tate, yoko, takasa, vol, area)
  write(*,*) vol, area
end program main


