! ==============================================
! 3-31.f90のモジュールを使用するonly句を用いた主プログラム
! ==============================================
module sample_mod
  implicit none
  private ! モジュール内のすべての変数などは外部から参照できなくなる
          ! 同じモジュール内のcontains文以下にあるサブルーチンからは参照可能
  integer, save :: ia = 1, ib = 2, ic = 3
  public ib, ic, sub ! 指定された変数などが外部から参照できるようになる
contains
  subroutine sub
    integer, save :: id = 4
    write(*,*) id
    end subroutine sub
end module sample_mod