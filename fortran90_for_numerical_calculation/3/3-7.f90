! =============================================
! 形状明示仮配列を使うモジュールサブルーチン
! =============================================
module mat_subprogs
  implicit none
contains
  subroutine print_mat(a, n) ! 配列aと配列の要素数nを仮引数としている
    integer, intent(in) :: n
    real(8), intent(in) :: a(n, n) ! 仮引数nを用いて宣言(形状明治配列)
    integer i
    do i = 1, n
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat
end module mat_subprogs

program random_mat
  use mat_subprogs
  implicit none
  real(8), allocatable :: a(:, :)
  integer n
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  if (n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 101"
  end if
  allocate (a(n, n))
  call random_number(a)
  call print_mat(a, n)
end program random_mat



