! ===========================================
! グローバル変数を用いて自動割付け配列の寸法を定める例
! ===========================================
module params ! グローバル変数モジュール
  implicit none
  integer :: n = 2 ! 配列の寸法
end module params

module sample
  implicit none
contains
  subroutine swapvec3(x, y)
    use params ! グローバル変数モジュールの使用宣言
    integer, intent(inout) :: x(n), y(n) ! 形状明示仮配列
    integer tmp(n) ! 自動割付け配列
    tmp(1:n) = x(1:n)
    x(1:n) = y(1:n)
    y(1:n) = tmp(1:n)
  end subroutine swapvec3
end module sample