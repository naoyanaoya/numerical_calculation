! ===========================================
! エラーメッセージを出力して停止するサブルーチン
! ===========================================
module sample
  implicit none
contains
  subroutine print_title(title)
    character(*), intent(in) :: title ! 仮引数となる文字列
    write(*,*) title, len(title)
  end subroutine print_title
  subroutine error_stop(emes)
    character(*), intent(in) :: emes
    write(*,*) emes
    stop
  end subroutine error_stop
end module sample

program moji
  use sample
  implicit none
  character(5) :: c = "hello"
  call print_title(c) ! cを引数としてサブルーチンを呼び出す
  call print_title("good bye") ! 文字定数を実引数としてサブルーチンを呼び出す
  call error_stop("positive is negative!")
end program moji