! ==========================================
! contains文より前に宣言された変数を副プログラムが共有する例
! =============================================
module mat_subprogs3
  implicit none
  real(8), allocatable, save :: a(:, :)
  integer, save :: m, n
contains
  subroutine allocate_rmat2
    write(*,"(a)",advance="no") "input m, n : "
    read(*,*) m, n
    allocate(a(m, n))
    call random_number(a) !配列要素に乱数を設定
  end subroutine allocate_rmat2

  subroutine print_mat3 ! 配列要素を出力するサブルーチン
    integer i
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat3
end module mat_subprogs3

program random_mod3
  use mat_subprogs3
  call allocate_rmat2
  call print_mat3
end program random_mod3