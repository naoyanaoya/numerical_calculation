! =======================================
! 正規化したベクトルを返すモジュール関数
! =======================================
module vec_subprogs
  implicit none
contains
  function normal_vec(v) result(nv) ! 仮配列はv、関数が返す配列はnv
    real(8), intent(in) :: v(:) ! 仮配列vは形状引継ぎ配列
    real(8) nv(size(v, 1)), v1 ! 配列nvの寸法はsizeを用いて定める
    v1 = sqrt(dot_product(v, v)) ! ベクトルの大きさを計算
    if (v1 == 0.0d0) then
      nv(:) = 0.0d0 ! v1 = 0のときはゼロベクトルを返す
    else
      nv(:) = v(:) / v1
    end if
  end function normal_vec
end module vec_subprogs

