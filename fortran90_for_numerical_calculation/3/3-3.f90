! ===============================================
! save属性を有する局所変数を使うプログラム例
! ==============================================
module subprogs
  implicit none
contains
  subroutine count
    integer :: ic1 = 0
    integer, save :: ic2 = 0 ! save属性をつけて、初期値を指定して宣言
    ! ic1 = 0
    ic1 = ic1 + 1
    ic2 = ic2 + 1
    write(*,*) ic1, ic2
  end subroutine count
end module subprogs

program main
  use subprogs
  implicit none
  integer i
  do i = 1, 3
    call count
  end do
end program main