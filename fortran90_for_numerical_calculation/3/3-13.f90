! ================================================
! 配列要素を実引数として、配列の一部を借り配列とする例
! ================================================

module sub_mod
  implicit none
contains
  subroutine print_column(x, m)
    integer, intent(in) :: m, x(m) ! xは形状明示仮配列(1次元配列)
    write(*,*) x(1:m)
  end subroutine print_column
end module sub_mod

program main
  use sub_mod
  implicit none
  integer a(3,3)
  a(1, 1:3) = (/11, 12, 13/)
  a(2, 1:3) = (/21, 22, 23/)
  a(3, 1:3) = (/31, 32, 33/)
  call print_column(a(1, 1), 3) ! 配列要素(スカラ)を質引数としている
  call print_column(a(1, 2), 3) ! 同上
  call print_column(a(1, 1), 9)
end program main
