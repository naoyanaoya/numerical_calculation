! =============================================
! 形状引継ぎ配列を使うモジュールサブルーチン
! =============================================
module mat_subprogs
  implicit none
contains
  subroutine print_rmatc(a, c)
    real(8), intent(in) :: a(:, :)
    character(*), intent(in) :: c
    integer i, m, n
    m = size(a, 1)
    n = size(a, 2)
    write(*,"(a)") c
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_rmatc

  subroutine print_imatc(ia, c)
    real(8), intent(in) :: ia(:, :)
    character(*), intent(in) :: c
    integer i, m, n
    m = size(ia, 1)
    n = size(ia, 2)
    write(*,"(a)") c
    do i = 1, m
      write(*,"(100e12.4)") ia(i, 1:n)
    end do
  end subroutine print_imatc
  
  subroutine print_mat2(a) ! 形状引継ぎ配列
    real(8), intent(in) :: a(:, :)
    integer i, m, n
    m = size(a, 1) ! 組み込み関数sizeによりaの最初の次元の寸法を得る
    n = size(a, 2) ! 組み込み関数sizeによりaの2番目の次元の寸法を得る
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat2
end module mat_subprogs

program random_mat
  use mat_subprogs
  implicit none
  real(8), allocatable :: a(:, :)
  integer(8), allocatable :: ia(:, :)
  ! integer :: n = 10, m = 11
  integer m , n
  write(*,"(a)",advance="no") "input m : "
  read(*,*) m
  if (m < 1 .or. 100 < m) then
    stop "m must be 0 < m < 101"
  end if
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  if (n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 101"
  end if
  allocate (a(m, n))
  allocate (ia(m, n))
  call random_number(a)
  ! call random_number(ia)
  call print_mat2(a)
  call print_rmatc(a, "matrix a")
end program random_mat

