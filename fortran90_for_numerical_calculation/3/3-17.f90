! ======================================
! 行列を表す2次元配列の行あるいは列ベクトルを実引数とする例
! =======================================
module subprogs
  implicit none
contains
  subroutine print_ivec(iv, m) ! 1次元配列を出力するサブルーチン
    integer ,intent(in) :: m, iv(m) ! 仮配列は形状明示仮配列
    write(*,*) iv(1:m)
  end subroutine print_ivec
end module subprogs

program main
  use subprogs
  implicit none
  integer i, j, ia(3, 3)
  do i = 1, 3
    ia(i, 1:3) = (/(10 * i + j, j = 1, 3)/)
  end do
  call print_ivec(ia(1:1, 1:3), 3) ! (A)最初の実引数は2次元配列
  call print_ivec(ia(1  , 1:3), 3) ! (B)最初の実引数は1次元配列
  call print_ivec(ia(1:3, 1  ), 3) ! (C)最初の実引数は1次元配列
  call print_ivec(ia(1  , 1  ), 3) ! (D)最初の実引数はスカラ
end program main