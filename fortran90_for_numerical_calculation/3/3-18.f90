function vec_cos(a, b) result(vcos)
  real(8), intent(in) :: a(:), b(:)
  real(8) ab, vcos
  if (size(a) /= size(b)) then
    stop "error: size(a) /= size(b)" ! 寸法をチェック
  end if
  ab = dot_product(a, a) * dot_-product(b, b)
  if(ab == 0.0d0) then
    vcos = 0.0d0
  else
    vcos = dot_product(a, b) / sqrt(ab)
  end if
end function vec_cos