! ===========================================
! 局所配列に自動割付け配列を利用するサブルーチン
! ===========================================
subroutine swapvec2(x, y, n) ! 配列の寸法nも仮引数とする
  integer, intent(in) :: n
  integer, intent(out) :: x(n), y(n) ! 形状明示仮配列
  integer tmp(n)
  tmp(1:n) = x(1:n)
  x(1:n) = y(1:n)
  y(1:n) = tmp(1:n)
  ! deallocate(tmp) ! 明示的に割付け解除はできない？
end subroutine swapvec2
