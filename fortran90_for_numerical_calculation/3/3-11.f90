! ============================================
! 大きさ引継ぎ配列を利用する例
! ============================================
module sub_mod
  implicit none
contains
  subroutine print1d(a, m)
    integer, intent(in) :: m, a(1:*) ! aは大きさ引継ぎ配列(1次元配列)、配列名(下限:*)、*は添字の上限を示す
    write(*,*) "sub  : ", a(1:m)
  end subroutine print1d
end module sub_mod

program main
  use sub_mod
  implicit none
  integer, parameter :: n = 2
  integer a(n, n, n), i, j, k
  do k = 1, n
    do j = 1, n
      do i = 1, n
        a(i, j, k) = 100 * i + 10 * j + k
      end do
    end do
  end do
  write(*,*) "main : ", a(1:n, 1:n, 1:n)
  call print1d(a, n ** 3) ! 実引数は3次元配列
end program main