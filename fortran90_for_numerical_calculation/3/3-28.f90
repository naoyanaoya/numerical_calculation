! ==========================================
! グラム-シュミットの直交化を行うモジュール関数(関数部分のみ)
! ==========================================
function gs(a, n) result(e)
  ! 生息行列aの列ベクトルを正規直交化して、eの各列に格納して返す
  integer, intent(in) :: n
  real(8), intent(in) :: a(n, n) ! 形状明示仮配列
  real(8) e(n, n), dotp
  integer k, j
  e(1:n, 1) = normal_vec(1(1:n), 1:1) ! e1を定める
  do k = 2, n
    e(1:n, k) = a(1:n, k)
    do j = 1, k - 1
      dotp = dot_product(a(1:n, k), e(1:n, j))
      e(1:n, k) = e(1:n, k) - dotp * e(1:n, j)
    end do
    e(1:n, k) = normal_vec2(e(1:n, k:k), n) ! ekを正規化する
  end do
end function gs