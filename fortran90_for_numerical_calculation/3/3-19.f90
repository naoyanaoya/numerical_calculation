! ===========================================
! 文字列を引数とするモジュールサブルーチンの例
! ===========================================
module sample
  implicit none
contains
  subroutine print_title(title)
    character(*), intent(in) :: title ! 仮引数となる文字列
    write(*,*) title, len(title)
  end subroutine print_title
end module sample

program moji
  use sample
  implicit none
  character(5) :: c = "hello"
  call print_title(c) ! cを引数としてサブルーチンを呼び出す
  call print_title("good bye") ! 文字定数を実引数としてサブルーチンを呼び出す
end program moji
