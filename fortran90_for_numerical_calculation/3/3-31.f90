! ==========================================
! 変数と副プログラムを含むモジュール
! ========================================
module sample_mod0
  implicit none
  integer, save :: ia = 1, ib = 2, ic = 3
contains
  subroutine sub
    integer, save :: id = 4
    write(*,*) id
  end subroutine sub
end module sample_mod0