! ==============================================
! 2次元配列に乱数を設定して出力するプログラム
! ==============================================

program random_mat
  implicit none
  real(8), allocatable :: a(:, :)
  integer n, i
  write(*,"(a)", advance="no") "input n : "
  read(*,*) n
  if(n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 101"
  end if
  allocate(a(n, n)) ! 割付け
  call random_number(a(:, :)) ! 全要素に[0,1)の乱数を設定
  do i = 1, n
    write(*, "(100e12.4)") a(i, 1:n)
  end do
end program random_mat