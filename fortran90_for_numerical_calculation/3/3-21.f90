! ==========================================
! 局所配列に割付け配列を利用するモジュールサブルーチン
! ==========================================
subroutine swapvec(x, y)
  integer, intent(inout) :: x(:), y(:) ! 形状引継ぎ配列
  integer, allocatable :: tmp(:) ! 局所配列に割付け配列を利用
  integer n
  n = size(x)
  if (n /= size(y)) then
    stop "size(x) /= size(y)"
  end if
  allocate(tmp(n)) ! 割付け
  tmp(1:n) = x(1:n)
  x(1:n) = y(1:n)
  y(1:n) = tmp(1:n)
  deallocate(tmp) ! 明示的に割付け解除
end subroutine swapvec

! 1次元配列xとyを借り配列として、それらは形状引継ぎ配列として宣言されている
! 演算内容としては、1次元配列xとyの寸法が等しい場合には要素を入れ替えるというものである。
! 局所配列といして用いられた割付け配列tmpはサブルーチンの演算が終了する前にdeallocate文を用いて割付け解除されている