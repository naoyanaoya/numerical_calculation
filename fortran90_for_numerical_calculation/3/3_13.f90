module vec_subprogs
  implicit none
contains
  subroutine allocate_rvec(v, n)
    integer, intent(inout) :: n
    real(8), allocatable, intent(inout) :: v(:) ! 未割付けの割付け配列が仮配列
    write(*,"(a)",advance="no") "input n : "
    read(*,*) n
    if(n < 1 .or. 100 < n) then
      stop "n must be 0 < n < 101"
    end if
    allocate(v(n))
    call random_number(v)
  end subroutine allocate_rvec

  subroutine print_vec2(v, n)
    real(8), intent(in) :: v(:)
    integer, intent(in) :: n
    write(*,"(100e12.4)") v
  end subroutine print_vec2
end module vec_subprogs

program random_vec
  use vec_subprogs ! モジュールの使用宣言
  implicit none
  real(8), allocatable :: v(:)
  integer n
  call allocate_rvec(v, n)
  call print_vec2(v, n)
end program random_vec
