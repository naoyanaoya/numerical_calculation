! =============================================
! 形状引継ぎ配列を使うモジュールサブルーチン
! =============================================
module mat_subprogs
  implicit none
contains
  subroutine print_mat2(a) ! 形状引継ぎ配列
    real(8), intent(in) :: a(:, :)
    integer i, m, n
    m = size(a, 1) ! 組み込み関数sizeによりaの最初の次元の寸法を得る
    n = size(a, 2) ! 組み込み関数sizeによりaの2番目の次元の寸法を得る
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat2
end module mat_subprogs

program random_mat
  use mat_subprogs
  implicit none
  real(8), allocatable :: a(:, :)
  ! integer :: n = 10, m = 11
  integer m , n
  write(*,"(a)",advance="no") "input m : "
  read(*,*) m
  if (m < 1 .or. 100 < m) then
    stop "m must be 0 < m < 101"
  end if
  write(*,"(a)",advance="no") "input n : "
  read(*,*) n
  if (n < 1 .or. 100 < n) then
    stop "n must be 0 < n < 101"
  end if
  allocate (a(m, n))
  call random_number(a)
  call print_mat2(a)
end program random_mat



