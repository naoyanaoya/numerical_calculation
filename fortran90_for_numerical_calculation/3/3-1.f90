! =========================================
! ファイル入力を行うテメの外部サブルーチンと主プログラム
! =========================================
program main ! 主プログラム
  implicit none
  integer :: f1=11, f2=12, i, j, k
  open(f1,file="3-1_data1.d")
  open(f2,file="3-1_data2.d") 
  call read_file(f1,i) ! 外部サブルーチンの呼び出し
  call read_file(f1,j) ! 同上
  call read_file(f2.k) ! 同上
  
end program 

subroutine read_file(fno,n) ! 外部サブルーチン
  implicit none
  integer fno, n
  read(fno,*) n
  write(*,*) n
  if(n < 0) then
    write(*,*) "error: negative value"
    stop
  end if
end subroutine read_file

