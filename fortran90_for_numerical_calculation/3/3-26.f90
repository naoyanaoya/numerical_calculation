! =====================================
! 文字列の並び順を逆にした文字列を返すモジュール関数
! =====================================
module sample
  implicit none
contains
  function revchar(c) result(rc)
    character(*), intent(in) :: c ! 仮引数となる文字型変数は長さを*で指定
    character(len(c)) rc ! 関数が返す文字列長さはlenで取得
    integer i
    do i = 1, len(c)
      rc(i: i) = c(len(c) - i + 1 : len(c) - i + 1)
    end do
  end function
end module sample

program chk
  use sample
  implicit none
  character(11) :: c = "I prefer Pi"
  write(*,"(a)") c
  write(*,"(a)") revchar(c)
end program chk