! ===========================================
! private属性およびpublic属性を含むモジュール
! =========================================
module sample_mod
  implicit none
  private ! モジュール内のすべての変数などは外部から参照できなくなる
          ! 同じモジュール内のcontains文以下にあるサブルーチンからは参照可能
  integer, save :: ia = 1, ib = 2, ic = 3
  public ib, ic, sub ! 指定された変数などが外部から参照できるようになる
contains
  subroutine sub
    integer, save :: id = 4
    write(*,*) id
    end subroutine sub
end module sample_mod

program chk_module
  use sample_mod, only : ib, sub ! only句を付けると参照が制限される
                                 ! icはモジュール側で参照可能としているが、only句により主プログラムからは参照できなくなる
  implicit none
  write(*,*) ib
  write(*,*) ic ! 参照が制限されて使えない
  call sub
end program chk_module