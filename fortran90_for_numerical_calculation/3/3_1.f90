! ===========================================
! 2つの整数型変数の値を交換するモジュールサブルーチン
! 引数と仮引数の整合を破壊した場合のエラー文を見る
! ===========================================
module subprog
  implicit none
contains
  subroutine swap(a, b, c)
    integer a, b, c
    integer tmp
    if(a == b) then
      write(*,*) "return"
      return
    end if
    tmp = a
    a = b
    b = tmp
  end subroutine swap
end module subprog

program exchange
  use subprog
  implicit none
  integer :: x = 77, y = 99, tmp = 0
  write(*,*) "x, y, tmp = ", x, y, tmp
  call swap(x, y, tmp)
  write(*,*) "x, y, tmp = ", x, y, tmp
end program exchange