! =====================================================
! 割付け配列を含むグローバル変数モジュールの利用例
! ======================================================
! ==================================================================================================
! 共有する変数を引数とする
! 共有する変数をモジュール内でsave属性を付けて宣言し、変数を使用するプログラム単位内で、このモジュールの使用宣言を行う
! ==================================================================================================
module globals ! グローバル変数モジュール
  real(8), allocatable, save :: a(:, :) ! save属性を付ける
  integer, save :: m, n ! save属性を付ける
end module globals

module mat_subprogs2 ! サブルーチンを含むモジュール
  use globals ! ここで使用宣言すると、contains文以下で共通にglobals内の変数を使用できる
  implicit none
contains
  subroutine allocate_rmat2 ! 配列割付けと乱数設定を行うサブルーチン
    write(*,"(a)",advance="no") "input m, n : "
    read(*,*) m, n
    allocate(a(m, n))
    call random_number(a) !配列要素に乱数を設定
  end subroutine allocate_rmat2

  subroutine print_mat3 ! 配列要素を出力するサブルーチン
    integer i
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat3
end module mat_subprogs2

program random_mod2
  use mat_subprogs2
  call allocate_rmat2 ! moduleで変数が共有されるので引数は不要
  call print_mat3
end program random_mod2