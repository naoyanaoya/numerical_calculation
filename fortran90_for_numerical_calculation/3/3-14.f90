module mat_subprogs
  implicit none
contains
  subroutine allocate_rmat(a)
    real(8), allocatable, intent(out) :: a(:, :) ! 未割付けの割付け配列が仮配列
    integer n
    write(*, "(a)", advance="no") "input n : "
    read(*,*) n
    if(n < 1 .or. 100 < n) then
      stop "n must be 0 < n < 101"
    end if
    allocate(a(n, n)) ! 割付け配列の割付け
    call random_number(a) ! 配列に乱数を設定
  end subroutine allocate_rmat

  subroutine print_mat2(a)
    real(8), intent(in) :: a(:, :)
    integer i, m, n
    m = size(a, 1) ! 組み込み関数sizeによりaの最初の次元の寸法を得る
    n = size(a, 2) ! 組み込み関数sizeによりaの2番目の次元の寸法を得る
    do i = 1, m
      write(*,"(100e12.4)") a(i, 1:n)
    end do
  end subroutine print_mat2
end module mat_subprogs

program random_mat
  use mat_subprogs ! モジュールの使用宣言
  implicit none
  real(8), allocatable :: a(:, :)
  call allocate_rmat(a) ! 未割付けの割付け配列を実引数とする
  call print_mat2(a) ! 要素の値を出力(形状引継ぎ配列を利用)
end program random_mat